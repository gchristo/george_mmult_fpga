#ifndef __CONOR_H__
#define __CONOR_H__

#define DEVICE_MEMORY_SIZE 2*(10^9)
#define CN_SUCCESS 0
#define CN_BUFFER_TOO_SMALL 1

extern char dev_name[];

void conor_debug(const char *);

int conorProofPrint(void);
void hello(void);

void __riffa_init (void) __attribute__((constructor(110)));
void __riffa_deinit (void) __attribute__((constructor(110)));

typedef int cn_device_id;

struct _cn_event{
	int event;
};

typedef struct _cn_event cn_event;

cn_device_id conorGetDeviceId(void);

//clGetDeviceInfo
//int->err
int conorGetDeviceMemorySize(cn_device_id ,long *);
int conorGetDeviceName(cn_device_id , int, char *);

#endif //__CONOR_H__
