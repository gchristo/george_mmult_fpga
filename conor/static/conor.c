#include<stdio.h>
#include<timer.h"
#include"conor.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define CHNL 0

fpga_t *fpga;
fpga_info_list info;
int id = info.id[0];
int num_chnls = info.num_chnls[0];
int chnl = CHNL;

void __riffa_init (void) __attribute__((constructor(110)))
{
	fpga = fpga_open(id);
}

void __riffa_deinit (void) __attribute__((constructor(110)))
{
	fpga_close(fpga);
}


int conor_data_recv(unsigned int *rbuf,size_t len){
	return fpga_recv(fpga, chnl, rbuf, len, 0, 1, 25000);
}

int conor_data_send(unsigned int sbuf,size_t len){
	return fpga_send(fpga, chnl, sbuf, len, 0, 1, 25000);
}

char dev_name[]="VC709";

void conor_debug(const char *message){
	printf("%sThis is a conor message:\t%s%s\n",KRED,message,KNRM);
}

int conorProofPrint(){
	printf("Conor properly linked\n");
	return 0;
}

void hello(){
	printf("Hello\n");
}

cn_device_id conorGetDeviceId(){
	return (cn_device_id)0;	
}

int conorGetDeviceMemorySize(cn_device_id id, long *size){

	*size=DEVICE_MEMORY_SIZE;
	
	return CN_SUCCESS;
}

int conorGetDeviceName(cn_device_id id, int size, char *name)
{
	if(size<5) return CN_BUFFER_TOO_SMALL;
	else{
		int count;
		for(count=0;count<5;count++)
			name[count]=dev_name[count];
	}
	return CN_SUCCESS;
}
