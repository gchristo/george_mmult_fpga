/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2010-2011, 2013-2014  Université de Bordeaux
 * Copyright (C) 2010, 2012  CNRS
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#ifndef __DRIVER_CONOR_H__
#define __DRIVER_CONOR_H__

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifdef STARPU_USE_CONOR

#if defined(STARPU_USE_CONOR) || defined(STARPU_SIMGRID)
struct _starpu_machine_config;
void
_starpu_conor_discover_devices(struct _starpu_machine_config *config);
#else
#define _starpu_conor_discover_devices(config) ((void) (config))
#endif

#ifdef STARPU_USE_CONOR
extern
char *_starpu_conor_program_dir;

extern
int _starpu_conor_init_context(int devid);

extern
int _starpu_conor_deinit_context(int devid);
#endif

#if defined(STARPU_USE_CONOR) || defined(STARPU_SIMGRID)
extern
unsigned _starpu_conor_get_device_count(void);
#endif


#if defined(STARPU_USE_CONOR) || defined(STARPU_SIMGRID)
extern
void _starpu_conor_init(void);

extern
void *_starpu_conor_worker(void *);
#endif

#ifdef STARPU_USE_CONOR
struct _starpu_worker;
extern
int _starpu_run_conor(struct _starpu_worker *);

extern
int _starpu_conor_driver_init(struct _starpu_worker *);

extern
int _starpu_conor_driver_run_once(struct _starpu_worker *);

extern
int _starpu_conor_driver_deinit(struct _starpu_worker *);
#endif // STARPU_USE_CONOR
#endif //  __DRIVER_CONOR_H__
