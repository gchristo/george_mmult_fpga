/* src/common/config.h.  Generated from config.h.in by configure.  */
/* src/common/config.h.in.  Generated from configure.ac by autoheader.  */

/* enable FUT traces */
/* #undef CONFIG_FUT */

/* Define to 1 if you have the <aio.h> header file. */
#define HAVE_AIO_H 1

/* Define to 1 when `struct attribute_spec' has the `affects_type_identity'
   field. */
/* #undef HAVE_ATTRIBUTE_SPEC_AFFECTS_TYPE_IDENTITY */

/* Define to 1 if you have the <Ayudame.h> header file. */
/* #undef HAVE_AYUDAME_H */

/* Define to 1 if you have the `clEnqueueMarkerWithWaitList' function. */
/* #undef HAVE_CLENQUEUEMARKERWITHWAITLIST */

/* Define to 1 if you have the `clGetExtensionFunctionAddressForPlatform'
   function. */
/* #undef HAVE_CLGETEXTENSIONFUNCTIONADDRESSFORPLATFORM */

/* Define to 1 if you have the `clock_gettime' function. */
#define HAVE_CLOCK_GETTIME 1

/* Define to 1 if you have the <CL/cl_ext.h> header file. */
/* #undef HAVE_CL_CL_EXT_H */

/* Define to 1 if you have the <cuda_gl_interop.h> header file. */
/* #undef HAVE_CUDA_GL_INTEROP_H */

/* Peer transfers are supported in CUDA */
/* #undef HAVE_CUDA_MEMCPY_PEER */

/* Define to 1 if you have the <c-common.h> header file. */
/* #undef HAVE_C_COMMON_H */

/* Define to 1 if you have the <c-family/c-common.h> header file. */
/* #undef HAVE_C_FAMILY_C_COMMON_H */

/* Define to 1 if you have the <c-family/c-pragma.h> header file. */
/* #undef HAVE_C_FAMILY_C_PRAGMA_H */

/* Define to 1 if you have the <c-pragma.h> header file. */
/* #undef HAVE_C_PRAGMA_H */

/* Define to 1 if you have the declaration of `build_array_ref', and to 0 if
   you don't. */
/* #undef HAVE_DECL_BUILD_ARRAY_REF */

/* Define to 1 if you have the declaration of `build_call_expr_loc_array', and
   to 0 if you don't. */
/* #undef HAVE_DECL_BUILD_CALL_EXPR_LOC_ARRAY */

/* Define to 1 if you have the declaration of `build_call_expr_loc_vec', and
   to 0 if you don't. */
/* #undef HAVE_DECL_BUILD_CALL_EXPR_LOC_VEC */

/* Define to 1 if you have the declaration of `build_zero_cst', and to 0 if
   you don't. */
/* #undef HAVE_DECL_BUILD_ZERO_CST */

/* Define to 1 if you have the declaration of `builtin_decl_explicit', and to
   0 if you don't. */
/* #undef HAVE_DECL_BUILTIN_DECL_EXPLICIT */

/* Define to 1 if you have the declaration of `enable_fut_flush', and to 0 if
   you don't. */
/* #undef HAVE_DECL_ENABLE_FUT_FLUSH */

/* Define to 1 if you have the declaration of `fut_set_filename', and to 0 if
   you don't. */
/* #undef HAVE_DECL_FUT_SET_FILENAME */

/* Define to 1 if you have the declaration of
   `hwloc_cuda_get_device_osdev_by_index', and to 0 if you don't. */
#define HAVE_DECL_HWLOC_CUDA_GET_DEVICE_OSDEV_BY_INDEX 0

/* Define to 1 if you have the declaration of `ptr_derefs_may_alias_p', and to
   0 if you don't. */
/* #undef HAVE_DECL_PTR_DEREFS_MAY_ALIAS_P */

/* Define to 1 if you have the declaration of `smpi_process_set_user_data',
   and to 0 if you don't. */
/* #undef HAVE_DECL_SMPI_PROCESS_SET_USER_DATA */

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the `enable_fut_flush' function. */
/* #undef HAVE_ENABLE_FUT_FLUSH */

/* Define to 1 if you have the `fut_set_filename' function. */
/* #undef HAVE_FUT_SET_FILENAME */

/* Define to 1 if you have the <glpk.h> header file. */
/* #undef HAVE_GLPK_H */

/* Define to 1 if you have the `hwloc_topology_dup' function. */
#define HAVE_HWLOC_TOPOLOGY_DUP 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the <leveldb/db.h> header file. */
/* #undef HAVE_LEVELDB_DB_H */

/* Define to 1 if you have the `atlas' library (-latlas). */
/* #undef HAVE_LIBATLAS */

/* Define to 1 if you have the `cblas' library (-lcblas). */
/* #undef HAVE_LIBCBLAS */

/* Define to 1 if you have the `gfortran' library (-lgfortran). */
/* #undef HAVE_LIBGFORTRAN */

/* Define to 1 if you have the `GL' library (-lGL). */
/* #undef HAVE_LIBGL */

/* Define to 1 if you have the `glpk' library (-lglpk). */
/* #undef HAVE_LIBGLPK */

/* Define to 1 if you have the `GLU' library (-lGLU). */
/* #undef HAVE_LIBGLU */

/* Define to 1 if you have the `glut' library (-lglut). */
/* #undef HAVE_LIBGLUT */

/* Define to 1 if you have the `goto' library (-lgoto). */
/* #undef HAVE_LIBGOTO */

/* Define to 1 if you have the `goto2' library (-lgoto2). */
/* #undef HAVE_LIBGOTO2 */

/* Define to 1 if you have the `ifcore' library (-lifcore). */
/* #undef HAVE_LIBIFCORE */

/* Define to 1 if you have the `leveldb' library (-lleveldb). */
/* #undef HAVE_LIBLEVELDB */

/* Define to 1 if you have the `pthread' library (-lpthread). */
#define HAVE_LIBPTHREAD 1

/* Define to 1 if you have the `RCCE_bigflags_nongory_nopwrmgmt' library
   (-lRCCE_bigflags_nongory_nopwrmgmt). */
/* #undef HAVE_LIBRCCE_BIGFLAGS_NONGORY_NOPWRMGMT */

/* Define to 1 if you have the `rt' library (-lrt). */
#define HAVE_LIBRT 1

/* Define to 1 if you have the `simgrid' library (-lsimgrid). */
/* #undef HAVE_LIBSIMGRID */

/* Define to 1 if you have the `ws2_32' library (-lws2_32). */
/* #undef HAVE_LIBWS2_32 */

/* Define to 1 if you have the <malloc.h> header file. */
#define HAVE_MALLOC_H 1

/* Define to 1 if you have the `memalign' function. */
#define HAVE_MEMALIGN 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the `mkostemp' function. */
#define HAVE_MKOSTEMP 1

/* Function MPI_Comm_f2c is available */
/* #undef HAVE_MPI_COMM_F2C */

/* Define to 1 if you have the `MSG_environment_get_routing_root' function. */
/* #undef HAVE_MSG_ENVIRONMENT_GET_ROUTING_ROOT */

/* Define to 1 if you have the `MSG_get_as_by_name' function. */
/* #undef HAVE_MSG_GET_AS_BY_NAME */

/* Define to 1 if you have the `MSG_host_get_speed' function. */
/* #undef HAVE_MSG_HOST_GET_SPEED */

/* Define to 1 if you have the `MSG_process_attach' function. */
/* #undef HAVE_MSG_PROCESS_ATTACH */

/* Define to 1 if you have the `MSG_process_join' function. */
/* #undef HAVE_MSG_PROCESS_JOIN */

/* Define to 1 if you have the `posix_memalign' function. */
#define HAVE_POSIX_MEMALIGN 1

/* Define to 1 if you have the `pread' function. */
#define HAVE_PREAD 1

/* Define to 1 if you have the `pthread_setaffinity_np' function. */
#define HAVE_PTHREAD_SETAFFINITY_NP 1

/* pthread_spin_lock is available */
#define HAVE_PTHREAD_SPIN_LOCK /**/

/* Define to 1 if you have the `pwrite' function. */
#define HAVE_PWRITE 1

/* Define to 1 if you have the <simgrid/msg.h> header file. */
/* #undef HAVE_SIMGRID_MSG_H */

/* Define to 1 if you have the `SIMIX_process_get_code' function. */
/* #undef HAVE_SIMIX_PROCESS_GET_CODE */

/* Define to 1 if you have the `smpi_process_set_user_data' function. */
/* #undef HAVE_SMPI_PROCESS_SET_USER_DATA */

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the `sysconf' function. */
#define HAVE_SYSCONF 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the <valgrind/helgrind.h> header file. */
/* #undef HAVE_VALGRIND_HELGRIND_H */

/* Define to 1 if you have the <valgrind/memcheck.h> header file. */
/* #undef HAVE_VALGRIND_MEMCHECK_H */

/* Define to 1 if you have the <valgrind/valgrind.h> header file. */
/* #undef HAVE_VALGRIND_VALGRIND_H */

/* Define to 1 if you have the `xbt_barrier_init' function. */
/* #undef HAVE_XBT_BARRIER_INIT */

/* Define to 1 if you have the `xbt_mutex_try_acquire' function. */
/* #undef HAVE_XBT_MUTEX_TRY_ACQUIRE */

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Name of package */
#define PACKAGE "starpu"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "starpu-devel@lists.gforge.inria.fr"

/* Define to the full name of this package. */
#define PACKAGE_NAME "StarPU"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "StarPU 1.2.0"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "starpu"

/* Define to the home page for this package. */
#define PACKAGE_URL "http://runtime.bordeaux.inria.fr/StarPU/"

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.2.0"

/* The size of `void *', as computed by sizeof. */
#define SIZEOF_VOID_P 8

/* use STARPU_ATLAS library */
/* #undef STARPU_ATLAS */

/* location of StarPU build directory */
#define STARPU_BUILD_DIR "/home/gchr/workspace/StarPU/objdir"

/* enable debugging statements */
/* #undef STARPU_DEBUG */

/* enable developer warnings */
/* #undef STARPU_DEVEL */

/* Define to 1 to disable asynchronous copy between CPU and GPU devices */
/* #undef STARPU_DISABLE_ASYNCHRONOUS_COPY */

/* Define to 1 to disable asynchronous copy between CPU and CUDA devices */
/* #undef STARPU_DISABLE_ASYNCHRONOUS_CUDA_COPY */

/* Define to 1 to disable asynchronous copy between CPU and MIC devices */
/* #undef STARPU_DISABLE_ASYNCHRONOUS_MIC_COPY */

/* Define to 1 to disable asynchronous copy between CPU and OpenCL devices */
/* #undef STARPU_DISABLE_ASYNCHRONOUS_OPENCL_COPY */

/* enable details about codelets in the paje trace */
/* #undef STARPU_ENABLE_PAJE_CODELET_DETAILS */

/* enable statistics */
/* #undef STARPU_ENABLE_STATS */

/* display verbose debug messages */
/* #undef STARPU_EXTRA_VERBOSE */

/* enable additional locking systems FxT traces */
/* #undef STARPU_FXT_LOCK_TRACES */

/* Path to the GNU debugger. */
#define STARPU_GDB_PATH "/usr/bin/gdb"

/* use STARPU_GOTO library */
/* #undef STARPU_GOTO */

/* Define to 1 if CUDA device properties include BusID */
/* #undef STARPU_HAVE_BUSID */

/* cufftDoubleComplex is available */
/* #undef STARPU_HAVE_CUFFTDOUBLECOMPLEX */

/* CURAND is available */
/* #undef STARPU_HAVE_CURAND */

/* Define this on darwin. */
/* #undef STARPU_HAVE_DARWIN */

/* Define to 1 if CUDA device properties include DomainID */
/* #undef STARPU_HAVE_DOMAINID */

/* Define to 1 if you have the <f77.h> header file. */
/* #undef STARPU_HAVE_F77_H */

/* Define this if a Fortran compiler is available */
/* #undef STARPU_HAVE_FC */

/* Define to 1 if you have the libfftw3 library. */
#define STARPU_HAVE_FFTW 1

/* Define to 1 if you have the libfftw3f library. */
#define STARPU_HAVE_FFTWF 1

/* Define to 1 if you have the libfftw3l library. */
#define STARPU_HAVE_FFTWL 1

/* Define to 1 if you have the <glpk.h> header file. */
/* #undef STARPU_HAVE_GLPK_H */

/* Define to 1 if you have the <valgrind/helgrind.h> header file. */
/* #undef STARPU_HAVE_HELGRIND_H */

/* Define to 1 if you have the hwloc library. */
#define STARPU_HAVE_HWLOC 1

/* Define this if icc is available */
/* #undef STARPU_HAVE_ICC */

/* Define to 1 if you have the <leveldb/db.h> header file. */
/* #undef STARPU_HAVE_LEVELDB */

/* libnuma is available */
#define STARPU_HAVE_LIBNUMA /**/

/* Define to 1 if you have the MAGMA library. */
/* #undef STARPU_HAVE_MAGMA */

/* Define to 1 if you have the <malloc.h> header file. */
#define STARPU_HAVE_MALLOC_H 1

/* Define to 1 if you have the `memalign' function. */
#define STARPU_HAVE_MEMALIGN 1

/* Define to 1 if you have the <valgrind/memcheck.h> header file. */
/* #undef STARPU_HAVE_MEMCHECK_H */

/* Define to 1 if the function nearbyintf is available. */
#define STARPU_HAVE_NEARBYINTF 1

/* Define to 1 if you have the `posix_memalign' function. */
#define STARPU_HAVE_POSIX_MEMALIGN 1

/* Define to 1 if you have libpoti */
/* #undef STARPU_HAVE_POTI */

/* pthread_barrier is available */
#define STARPU_HAVE_PTHREAD_BARRIER /**/

/* pthread_spin_lock is available */
#define STARPU_HAVE_PTHREAD_SPIN_LOCK /**/

/* Define to 1 if the function rintf is available. */
#define STARPU_HAVE_RINTF 1

/* Define to 1 if the function sched_yield is available. */
#define STARPU_HAVE_SCHED_YIELD 1

/* Define to 1 if the function setenv is available. */
#define STARPU_HAVE_SETENV 1

/* Define to 1 if you have msg.h in simgrid/. */
/* #undef STARPU_HAVE_SIMGRID_MSG_H */

/* Define to 1 if the function strerro_r is available. */
#define STARPU_HAVE_STRERROR_R 1

/* struct timespec is defined */
#define STARPU_HAVE_STRUCT_TIMESPEC /**/

/* Define to 1 if the target supports __sync_bool_compare_and_swap */
#define STARPU_HAVE_SYNC_BOOL_COMPARE_AND_SWAP 1

/* Define to 1 if the target supports __sync_fetch_and_add */
#define STARPU_HAVE_SYNC_FETCH_AND_ADD 1

/* Define to 1 if the target supports __sync_fetch_and_or */
#define STARPU_HAVE_SYNC_FETCH_AND_OR 1

/* Define to 1 if the target supports __sync_lock_test_and_set */
#define STARPU_HAVE_SYNC_LOCK_TEST_AND_SET 1

/* Define to 1 if the target supports __sync_synchronize */
#define STARPU_HAVE_SYNC_SYNCHRONIZE 1

/* Define to 1 if the target supports __sync_val_compare_and_swap */
#define STARPU_HAVE_SYNC_VAL_COMPARE_AND_SWAP 1

/* Define to 1 if you have the <unistd.h> header file. */
#define STARPU_HAVE_UNISTD_H 1

/* Define to 1 if the function unsetenv is available. */
#define STARPU_HAVE_UNSETENV 1

/* Define to 1 if you have the <valgrind/valgrind.h> header file. */
/* #undef STARPU_HAVE_VALGRIND_H */

/* Define this on windows. */
/* #undef STARPU_HAVE_WINDOWS */

/* enable X11 */
#define STARPU_HAVE_X11 1

/* calibration heuristic value */
#define STARPU_HISTORYMAXERROR 50

/* Define to the directory where StarPU's headers are installed. */
/* #undef STARPU_INCLUDE_DIR */

/* Define to 1 on Linux */
#define STARPU_LINUX_SYS 1

/* enable long check */
/* #undef STARPU_LONG_CHECK */

/* Major version number of StarPU. */
#define STARPU_MAJOR_VERSION 1

/* Maximum number of CPUs supported */
#define STARPU_MAXCPUS 64

/* maximum number of CUDA devices */
#define STARPU_MAXCUDADEVS 4

/* maximum number of implementations */
#define STARPU_MAXIMPLEMENTATIONS 4

/* maximum number of MIC cores */
#define STARPU_MAXMICCORES 120

/* maximum number of MIC devices */
#define STARPU_MAXMICDEVS 4

/* maximum number of message-passing kernels */
#define STARPU_MAXMPKERNELS 10

/* maximum number of memory nodes */
#define STARPU_MAXNODES 4

/* maximum number of OPENCL devices */
#define STARPU_MAXOPENCLDEVS 8

/* maximum number of SCC devices */
#define STARPU_MAXSCCDEVS 47

/* enable memory stats */
/* #undef STARPU_MEMORY_STATS */

/* MIC RMA transfer is enable */
#define STARPU_MIC_USE_RMA 1

/* Minor version number of StarPU. */
#define STARPU_MINOR_VERSION 2

/* use MKL library */
/* #undef STARPU_MKL */

/* enable performance model debug */
/* #undef STARPU_MODEL_DEBUG */

/* enable StarPU MPI activity polling method */
/* #undef STARPU_MPI_ACTIVITY */

/* Using native windows threads */
/* #undef STARPU_NATIVE_WINTHREADS */

/* enable new check */
/* #undef STARPU_NEW_CHECK */

/* how many buffers can be manipulated per task */
#define STARPU_NMAXBUFS 8

/* Maximum number of workers */
#define STARPU_NMAXWORKERS 80

/* Maximum number of worker combinations */
#define STARPU_NMAX_COMBINEDWORKERS 64

/* Maximum number of sched_ctxs supported */
#define STARPU_NMAX_SCHED_CTXS 10

/* drivers must progress */
#define STARPU_NON_BLOCKING_DRIVERS 1

/* disable assertions */
/* #undef STARPU_NO_ASSERT */

/* Define this to enable using an OpenCL simulator */
/* #undef STARPU_OPENCL_SIMULATOR */

/* enable OpenGL rendering of some examples */
/* #undef STARPU_OPENGL_RENDER */

/* Define this to enable OpenMP runtime support */
/* #undef STARPU_OPENMP */

/* enable performance debug */
/* #undef STARPU_PERF_DEBUG */

/* performance models location */
/* #undef STARPU_PERF_MODEL_DIR */

/* enable quick check */
/* #undef STARPU_QUICK_CHECK */

/* Release version number of StarPU. */
#define STARPU_RELEASE_VERSION 0

/* enable debug sc_hypervisor */
/* #undef STARPU_SC_HYPERVISOR_DEBUG */

/* Define this to enable simgrid execution */
/* #undef STARPU_SIMGRID */

/* Define to 1 if you have the `SIMIX_process_get_code' function. */
/* #undef STARPU_SIMGRID_HAVE_SIMIX_PROCESS_GET_CODE */

/* Define to 1 if you have the `xbt_barrier_init' function. */
/* #undef STARPU_SIMGRID_HAVE_XBT_BARRIER_INIT */

/* check spinlock use */
/* #undef STARPU_SPINLOCK_CHECK */

/* location of StarPU sources */
#define STARPU_SRC_DIR "/home/gchr/workspace/StarPU/objdir/.."

/* use user defined library */
#define STARPU_SYSTEM_BLAS 1

/* enable data allocation cache */
#define STARPU_USE_ALLOCATION_CACHE 1

/* CPU driver is activated */
#define STARPU_USE_CPU 1

/* CUDA support is activated */
/* #undef STARPU_USE_CUDA */

/* Define to 1 if drandr48 is available and should be used */
#define STARPU_USE_DRAND48 1

/* Define to 1 if erandr48_r is available */
#define STARPU_USE_ERAND48_R 1

/* enable FxT traces */
/* #undef STARPU_USE_FXT */

/* MIC workers support is enabled */
/* #undef STARPU_USE_MIC */

/* Message-passing SINKs support is enabled */
/* #undef STARPU_USE_MP */

/* whether the StarPU MPI library is available */
/* #undef STARPU_USE_MPI */

/* OpenCL support is activated */
/* #undef STARPU_USE_OPENCL */

/* SCC support is enabled */
/* #undef STARPU_USE_SCC */

/* enable sc_hypervisor lib */
/* #undef STARPU_USE_SC_HYPERVISOR */

/* StarPU-Top is activated */
#define STARPU_USE_TOP 1

/* Define to 1 to disable STARPU_SKIP_IF_VALGRIND when running tests. */
/* #undef STARPU_VALGRIND_FULL */

/* display verbose debug messages */
/* #undef STARPU_VERBOSE */

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "1.2.0"

/* Define to 1 if the X Window System is missing or not being used. */
/* #undef X_DISPLAY_MISSING */

/* Define to the equivalent of the C99 'restrict' keyword, or to
   nothing if this is not supported.  Do not define if restrict is
   supported directly.  */
#define restrict __restrict
/* Work around a bug in Sun C++: it does not support _Restrict or
   __restrict__, even though the corresponding Sun C compiler ends up with
   "#define restrict _Restrict" or "#define restrict __restrict__" in the
   previous line.  Perhaps some future version of Sun C++ will work with
   restrict; if so, hopefully it defines __RESTRICT like Sun C does.  */
#if defined __SUNPRO_CC && !defined __RESTRICT
# define _Restrict
# define __restrict__
#endif


#if defined(STARPU_DEVEL) && defined(BUILDING_STARPU)
#  ifndef STARPU_CHECKED_UNISTD_H
#    define STARPU_CHECKED_UNISTD_H
#    ifdef _UNISTD_H
#      define _UNISTD_H PLEASE_DONT_INCLUDE_IT
#      error Please do not unconditionally include unistd.h, it is not available on Windows, include config.h and test for HAVE_UNISTD_H
#    endif
#  endif
#endif

