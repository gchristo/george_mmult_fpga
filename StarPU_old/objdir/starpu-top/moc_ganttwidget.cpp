/****************************************************************************
** Meta object code from reading C++ file 'ganttwidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../starpu-top/ganttwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ganttwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GanttWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   13,   12,   12, 0x0a,
      38,   13,   12,   12, 0x0a,
      66,   61,   12,   12, 0x0a,
      86,   12,   12,   12, 0x0a,
      98,   12,   12,   12, 0x0a,
     113,   12,   12,   12, 0x0a,
     127,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_GanttWidget[] = {
    "GanttWidget\0\0value\0updateZoom(double)\0"
    "updatePrevLine(double)\0time\0"
    "updateTimeView(int)\0connected()\0"
    "disconnected()\0ganttLocked()\0"
    "ganttUnlocked()\0"
};

void GanttWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GanttWidget *_t = static_cast<GanttWidget *>(_o);
        switch (_id) {
        case 0: _t->updateZoom((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->updatePrevLine((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->updateTimeView((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->connected(); break;
        case 4: _t->disconnected(); break;
        case 5: _t->ganttLocked(); break;
        case 6: _t->ganttUnlocked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData GanttWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GanttWidget::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_GanttWidget,
      qt_meta_data_GanttWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GanttWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GanttWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GanttWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GanttWidget))
        return static_cast<void*>(const_cast< GanttWidget*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int GanttWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
