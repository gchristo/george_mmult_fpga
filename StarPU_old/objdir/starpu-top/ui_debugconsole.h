/********************************************************************************
** Form generated from reading UI file 'debugconsole.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEBUGCONSOLE_H
#define UI_DEBUGCONSOLE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DebugConsole
{
public:
    QGridLayout *gridLayout;
    QTextEdit *console;
    QPushButton *stepButton;

    void setupUi(QWidget *DebugConsole)
    {
        if (DebugConsole->objectName().isEmpty())
            DebugConsole->setObjectName(QString::fromUtf8("DebugConsole"));
        DebugConsole->resize(710, 50);
        DebugConsole->setMinimumSize(QSize(500, 50));
        gridLayout = new QGridLayout(DebugConsole);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        console = new QTextEdit(DebugConsole);
        console->setObjectName(QString::fromUtf8("console"));
        console->setReadOnly(true);

        gridLayout->addWidget(console, 0, 0, 1, 1);

        stepButton = new QPushButton(DebugConsole);
        stepButton->setObjectName(QString::fromUtf8("stepButton"));

        gridLayout->addWidget(stepButton, 0, 1, 1, 1);


        retranslateUi(DebugConsole);

        QMetaObject::connectSlotsByName(DebugConsole);
    } // setupUi

    void retranslateUi(QWidget *DebugConsole)
    {
        DebugConsole->setWindowTitle(QApplication::translate("DebugConsole", "Form", 0, QApplication::UnicodeUTF8));
        stepButton->setText(QApplication::translate("DebugConsole", "Step", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DebugConsole: public Ui_DebugConsole {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEBUGCONSOLE_H
