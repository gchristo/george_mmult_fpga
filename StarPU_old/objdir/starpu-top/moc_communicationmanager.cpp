/****************************************************************************
** Meta object code from reading C++ file 'communicationmanager.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../starpu-top/communicationmanager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'communicationmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CommunicationManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      51,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      16,       // signalCount

 // signals: signature, parameters, type, tag, flags
      38,   22,   21,   21, 0x05,
     131,   73,   21,   21, 0x05,
     250,  237,   21,   21, 0x05,
     281,  273,   21,   21, 0x05,
     319,  306,   21,   21, 0x05,
     359,  347,   21,   21, 0x05,
     411,  384,   21,   21, 0x05,
     448,  384,   21,   21, 0x05,
     484,  384,   21,   21, 0x05,
     552,  523,   21,   21, 0x05,
     590,  523,   21,   21, 0x05,
     627,  523,   21,   21, 0x05,
     721,  667,   21,   21, 0x05,
     807,  781,   21,   21, 0x05,
     865,  848,   21,   21, 0x05,
     900,   21,   21,   21, 0x05,

 // slots: signature, parameters, type, tag, flags
     917,   21,   21,   21, 0x08,
     937,   21,   21,   21, 0x08,
     969,  955,   21,   21, 0x08,
     995,  955,   21,   21, 0x08,
    1031,  955,   21,   21, 0x08,
    1061,  955,   21,   21, 0x08,
    1093,  955,   21,   21, 0x08,
    1122,  955,   21,   21, 0x08,
    1149,  955,   21,   21, 0x08,
    1175,  955,   21,   21, 0x08,
    1205,  955,   21,   21, 0x08,
    1236,  955,   21,   21, 0x08,
    1265,  955,   21,   21, 0x08,
    1297,  955,   21,   21, 0x08,
    1336,  955,   21,   21, 0x08,
    1370,  955,   21,   21, 0x08,
    1404,  955,   21,   21, 0x08,
    1443,   21, 1435,   21, 0x0a,
    1467, 1460, 1435,   21, 0x0a,
    1495, 1460, 1435,   21, 0x0a,
    1543, 1524, 1435,   21, 0x0a,
    1574, 1524, 1435,   21, 0x0a,
    1604, 1524, 1435,   21, 0x0a,
    1637,  273, 1435,   21, 0x0a,
    1668,   21, 1435,   21, 0x0a,
    1687,  955,   21,   21, 0x0a,
    1708,   21,   21,   21, 0x0a,
    1724, 1460,   21,   21, 0x0a,
    1751, 1460,   21,   21, 0x0a,
    1779, 1524,   21,   21, 0x0a,
    1809, 1524,   21,   21, 0x0a,
    1838, 1524,   21,   21, 0x0a,
    1870,  273,   21,   21, 0x0a,
    1900,   21,   21,   21, 0x0a,
    1918,   21,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CommunicationManager[] = {
    "CommunicationManager\0\0serverTimestamp\0"
    "sessionTimeSynchronized(qlonglong)\0"
    "serverID,dataDescriptions,paramDescriptions,serverDevices\0"
    "serverInitCompleted(QString,QList<DataDescription*>*,QList<ParamDescri"
    "ption*>*,QList<starpu_top_device>*)\0"
    "errorMessage\0protocolError(QString)\0"
    "enabled\0notifyDebugEnabled(bool)\0"
    "debugMessage\0notifyDebugMessage(QString)\0"
    "lockMessage\0notifyDebugLock(QString)\0"
    "dataId,dataValue,timestamp\0"
    "notifyDataUpdate(int,bool,qlonglong)\0"
    "notifyDataUpdate(int,int,qlonglong)\0"
    "notifyDataUpdate(int,double,qlonglong)\0"
    "paramId,paramValue,timestamp\0"
    "notifyParamUpdate(int,bool,qlonglong)\0"
    "notifyParamUpdate(int,int,qlonglong)\0"
    "notifyParamUpdate(int,double,qlonglong)\0"
    "taskId,deviceId,timestamp,timestampStart,timestampEnd\0"
    "notifyTaskPrevUpdate(int,int,qlonglong,qlonglong,qlonglong)\0"
    "taskId,deviceId,timestamp\0"
    "notifyTaskStartUpdate(int,int,qlonglong)\0"
    "taskId,timestamp\0notifyTaskEndUpdate(int,qlonglong)\0"
    "protoConnected()\0initializeSession()\0"
    "messageReceived()\0messageString\0"
    "parseInitMessage(QString)\0"
    "parseInitServerInfoMessage(QString)\0"
    "parseInitDataMessage(QString)\0"
    "parseInitParamsMessage(QString)\0"
    "parseInitDevMessage(QString)\0"
    "parseReadyMessage(QString)\0"
    "parseLoopMessage(QString)\0"
    "parseTaskPrevMessage(QString)\0"
    "parseTaskStartMessage(QString)\0"
    "parseTaskEndMessage(QString)\0"
    "parseDataUpdateMessage(QString)\0"
    "parseParamNotificationMessage(QString)\0"
    "parseDebugEnabledMessage(QString)\0"
    "parseDebugMessageMessage(QString)\0"
    "parseDebugLockMessage(QString)\0QString\0"
    "buildGoMessage()\0dataId\0"
    "buildDataEnableMessage(int)\0"
    "buildDataDisableMessage(int)\0"
    "paramId,paramValue\0buildParamSetMessage(int,bool)\0"
    "buildParamSetMessage(int,int)\0"
    "buildParamSetMessage(int,double)\0"
    "buildDebugEnabledMessage(bool)\0"
    "buildStepMessage()\0sendMessage(QString)\0"
    "sendGoMessage()\0sendDataEnableMessage(int)\0"
    "sendDataDisableMessage(int)\0"
    "sendParamSetMessage(int,bool)\0"
    "sendParamSetMessage(int,int)\0"
    "sendParamSetMessage(int,double)\0"
    "sendDebugEnabledMessage(bool)\0"
    "sendStepMessage()\0clearDescriptions()\0"
};

void CommunicationManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CommunicationManager *_t = static_cast<CommunicationManager *>(_o);
        switch (_id) {
        case 0: _t->sessionTimeSynchronized((*reinterpret_cast< qlonglong(*)>(_a[1]))); break;
        case 1: _t->serverInitCompleted((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QList<DataDescription*>*(*)>(_a[2])),(*reinterpret_cast< QList<ParamDescription*>*(*)>(_a[3])),(*reinterpret_cast< QList<starpu_top_device>*(*)>(_a[4]))); break;
        case 2: _t->protocolError((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->notifyDebugEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->notifyDebugMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->notifyDebugLock((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->notifyDataUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 7: _t->notifyDataUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 8: _t->notifyDataUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 9: _t->notifyParamUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 10: _t->notifyParamUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 11: _t->notifyParamUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 12: _t->notifyTaskPrevUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3])),(*reinterpret_cast< qlonglong(*)>(_a[4])),(*reinterpret_cast< qlonglong(*)>(_a[5]))); break;
        case 13: _t->notifyTaskStartUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 14: _t->notifyTaskEndUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< qlonglong(*)>(_a[2]))); break;
        case 15: _t->protoConnected(); break;
        case 16: _t->initializeSession(); break;
        case 17: _t->messageReceived(); break;
        case 18: _t->parseInitMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 19: _t->parseInitServerInfoMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 20: _t->parseInitDataMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 21: _t->parseInitParamsMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 22: _t->parseInitDevMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 23: _t->parseReadyMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 24: _t->parseLoopMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 25: _t->parseTaskPrevMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 26: _t->parseTaskStartMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 27: _t->parseTaskEndMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 28: _t->parseDataUpdateMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 29: _t->parseParamNotificationMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 30: _t->parseDebugEnabledMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 31: _t->parseDebugMessageMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 32: _t->parseDebugLockMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 33: { QString _r = _t->buildGoMessage();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 34: { QString _r = _t->buildDataEnableMessage((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 35: { QString _r = _t->buildDataDisableMessage((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 36: { QString _r = _t->buildParamSetMessage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 37: { QString _r = _t->buildParamSetMessage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 38: { QString _r = _t->buildParamSetMessage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 39: { QString _r = _t->buildDebugEnabledMessage((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 40: { QString _r = _t->buildStepMessage();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 41: _t->sendMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 42: _t->sendGoMessage(); break;
        case 43: _t->sendDataEnableMessage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 44: _t->sendDataDisableMessage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 45: _t->sendParamSetMessage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 46: _t->sendParamSetMessage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 47: _t->sendParamSetMessage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 48: _t->sendDebugEnabledMessage((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 49: _t->sendStepMessage(); break;
        case 50: _t->clearDescriptions(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CommunicationManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CommunicationManager::staticMetaObject = {
    { &QTcpSocket::staticMetaObject, qt_meta_stringdata_CommunicationManager,
      qt_meta_data_CommunicationManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CommunicationManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CommunicationManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CommunicationManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CommunicationManager))
        return static_cast<void*>(const_cast< CommunicationManager*>(this));
    return QTcpSocket::qt_metacast(_clname);
}

int CommunicationManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTcpSocket::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 51)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 51;
    }
    return _id;
}

// SIGNAL 0
void CommunicationManager::sessionTimeSynchronized(qlonglong _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CommunicationManager::serverInitCompleted(QString _t1, QList<DataDescription*> * _t2, QList<ParamDescription*> * _t3, QList<starpu_top_device> * _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void CommunicationManager::protocolError(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void CommunicationManager::notifyDebugEnabled(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void CommunicationManager::notifyDebugMessage(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void CommunicationManager::notifyDebugLock(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void CommunicationManager::notifyDataUpdate(int _t1, bool _t2, qlonglong _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void CommunicationManager::notifyDataUpdate(int _t1, int _t2, qlonglong _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void CommunicationManager::notifyDataUpdate(int _t1, double _t2, qlonglong _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void CommunicationManager::notifyParamUpdate(int _t1, bool _t2, qlonglong _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void CommunicationManager::notifyParamUpdate(int _t1, int _t2, qlonglong _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void CommunicationManager::notifyParamUpdate(int _t1, double _t2, qlonglong _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void CommunicationManager::notifyTaskPrevUpdate(int _t1, int _t2, qlonglong _t3, qlonglong _t4, qlonglong _t5)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void CommunicationManager::notifyTaskStartUpdate(int _t1, int _t2, qlonglong _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void CommunicationManager::notifyTaskEndUpdate(int _t1, qlonglong _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void CommunicationManager::protoConnected()
{
    QMetaObject::activate(this, &staticMetaObject, 15, 0);
}
QT_END_MOC_NAMESPACE
