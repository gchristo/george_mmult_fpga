/********************************************************************************
** Form generated from reading UI file 'aboutdialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUTDIALOG_H
#define UI_ABOUTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>

QT_BEGIN_NAMESPACE

class Ui_AboutDialog
{
public:
    QGridLayout *gridLayout;
    QGroupBox *developersGroupBox;
    QGridLayout *gridLayout_2;
    QListWidget *developersList;
    QLabel *titleLabel;
    QLabel *label;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QListWidget *listWidget;

    void setupUi(QDialog *AboutDialog)
    {
        if (AboutDialog->objectName().isEmpty())
            AboutDialog->setObjectName(QString::fromUtf8("AboutDialog"));
        AboutDialog->resize(328, 340);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(AboutDialog->sizePolicy().hasHeightForWidth());
        AboutDialog->setSizePolicy(sizePolicy);
        AboutDialog->setMinimumSize(QSize(0, 0));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/about.png"), QSize(), QIcon::Normal, QIcon::Off);
        AboutDialog->setWindowIcon(icon);
        AboutDialog->setModal(true);
        gridLayout = new QGridLayout(AboutDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        developersGroupBox = new QGroupBox(AboutDialog);
        developersGroupBox->setObjectName(QString::fromUtf8("developersGroupBox"));
        developersGroupBox->setMaximumSize(QSize(16777215, 110));
        gridLayout_2 = new QGridLayout(developersGroupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        developersList = new QListWidget(developersGroupBox);
        new QListWidgetItem(developersList);
        new QListWidgetItem(developersList);
        new QListWidgetItem(developersList);
        new QListWidgetItem(developersList);
        developersList->setObjectName(QString::fromUtf8("developersList"));
        developersList->setEnabled(true);
        developersList->setSelectionMode(QAbstractItemView::NoSelection);

        gridLayout_2->addWidget(developersList, 0, 0, 1, 1);


        gridLayout->addWidget(developersGroupBox, 3, 0, 1, 2);

        titleLabel = new QLabel(AboutDialog);
        titleLabel->setObjectName(QString::fromUtf8("titleLabel"));
        titleLabel->setMaximumSize(QSize(16777215, 50));
        titleLabel->setStyleSheet(QString::fromUtf8("font: 12pt \"MS Shell Dlg 2\";"));
        titleLabel->setScaledContents(false);
        titleLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(titleLabel, 0, 1, 1, 1);

        label = new QLabel(AboutDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMaximumSize(QSize(120, 110));
        label->setPixmap(QPixmap(QString::fromUtf8(":/images/starpu_top.png")));
        label->setScaledContents(true);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        groupBox = new QGroupBox(AboutDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(0, 0));
        groupBox->setMaximumSize(QSize(16777215, 90));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        listWidget = new QListWidget(groupBox);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setEnabled(true);
        listWidget->setMinimumSize(QSize(0, 0));
        listWidget->setMaximumSize(QSize(16777215, 16777215));
        listWidget->setSelectionMode(QAbstractItemView::NoSelection);

        gridLayout_3->addWidget(listWidget, 0, 0, 1, 1);


        gridLayout->addWidget(groupBox, 1, 0, 1, 2);


        retranslateUi(AboutDialog);

        QMetaObject::connectSlotsByName(AboutDialog);
    } // setupUi

    void retranslateUi(QDialog *AboutDialog)
    {
        AboutDialog->setWindowTitle(QApplication::translate("AboutDialog", "About StarPU-Top...", 0, QApplication::UnicodeUTF8));
        developersGroupBox->setTitle(QApplication::translate("AboutDialog", "Developers", 0, QApplication::UnicodeUTF8));

        const bool __sortingEnabled = developersList->isSortingEnabled();
        developersList->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = developersList->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("AboutDialog", "William BRAIK", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem1 = developersList->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("AboutDialog", "Yann COURTOIS", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem2 = developersList->item(2);
        ___qlistwidgetitem2->setText(QApplication::translate("AboutDialog", "Jean-Marie COUTEYEN", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem3 = developersList->item(3);
        ___qlistwidgetitem3->setText(QApplication::translate("AboutDialog", "Anthony ROY", 0, QApplication::UnicodeUTF8));
        developersList->setSortingEnabled(__sortingEnabled);

        titleLabel->setText(QApplication::translate("AboutDialog", "StarPU-Top, for StarPU", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        groupBox->setTitle(QApplication::translate("AboutDialog", "Software", 0, QApplication::UnicodeUTF8));

        const bool __sortingEnabled1 = listWidget->isSortingEnabled();
        listWidget->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem4 = listWidget->item(0);
        ___qlistwidgetitem4->setText(QApplication::translate("AboutDialog", "Year : 2011", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem5 = listWidget->item(1);
        ___qlistwidgetitem5->setText(QApplication::translate("AboutDialog", "Version : 0.1", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem6 = listWidget->item(2);
        ___qlistwidgetitem6->setText(QApplication::translate("AboutDialog", "Licence : LGPL", 0, QApplication::UnicodeUTF8));
        listWidget->setSortingEnabled(__sortingEnabled1);

    } // retranslateUi

};

namespace Ui {
    class AboutDialog: public Ui_AboutDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUTDIALOG_H
