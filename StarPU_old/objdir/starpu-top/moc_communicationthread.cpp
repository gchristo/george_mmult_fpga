/****************************************************************************
** Meta object code from reading C++ file 'communicationthread.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../starpu-top/communicationthread.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'communicationthread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CommunicationThread[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x05,
      71,   63,   20,   20, 0x05,
      89,   63,   20,   20, 0x05,
     104,   20,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
     125,  119,   20,   20, 0x08,
     171,   20,   20,   20, 0x08,
     193,   20,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CommunicationThread[] = {
    "CommunicationThread\0\0"
    "SocketError(QAbstractSocket::SocketError)\0"
    "message\0progress(QString)\0abort(QString)\0"
    "disconnected()\0error\0"
    "connectionError(QAbstractSocket::SocketError)\0"
    "connectionSucceeded()\0connectionDisconnected()\0"
};

void CommunicationThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CommunicationThread *_t = static_cast<CommunicationThread *>(_o);
        switch (_id) {
        case 0: _t->SocketError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 1: _t->progress((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->abort((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->disconnected(); break;
        case 4: _t->connectionError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 5: _t->connectionSucceeded(); break;
        case 6: _t->connectionDisconnected(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CommunicationThread::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CommunicationThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_CommunicationThread,
      qt_meta_data_CommunicationThread, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CommunicationThread::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CommunicationThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CommunicationThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CommunicationThread))
        return static_cast<void*>(const_cast< CommunicationThread*>(this));
    return QThread::qt_metacast(_clname);
}

int CommunicationThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void CommunicationThread::SocketError(QAbstractSocket::SocketError _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CommunicationThread::progress(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void CommunicationThread::abort(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void CommunicationThread::disconnected()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE
