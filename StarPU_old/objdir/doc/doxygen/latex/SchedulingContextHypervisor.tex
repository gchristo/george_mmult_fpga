\hypertarget{SchedulingContextHypervisor_WhatIsTheHypervisor}{}\section{What Is The Hypervisor}\label{SchedulingContextHypervisor_WhatIsTheHypervisor}
Star\+PU proposes a platform to construct Scheduling Contexts, to delete and modify them dynamically. A parallel kernel, can thus be isolated into a scheduling context and interferences between several parallel kernels are avoided. If the user knows exactly how many workers each scheduling context needs, he can assign them to the contexts at their creation time or modify them during the execution of the program.

The Scheduling Context Hypervisor Plugin is available for the users who do not dispose of a regular parallelism, who cannot know in advance the exact size of the context and need to resize the contexts according to the behavior of the parallel kernels.

The Hypervisor receives information from Star\+PU concerning the execution of the tasks, the efficiency of the resources, etc. and it decides accordingly when and how the contexts can be resized. Basic strategies of resizing scheduling contexts already exist but a platform for implementing additional custom ones is available.\hypertarget{SchedulingContextHypervisor_StartTheHypervisor}{}\section{Start the Hypervisor}\label{SchedulingContextHypervisor_StartTheHypervisor}
The Hypervisor must be initialized once at the beginning of the application. At this point a resizing policy should be indicated. This strategy depends on the information the application is able to provide to the hypervisor as well as on the accuracy needed for the resizing procedure. For example, the application may be able to provide an estimation of the workload of the contexts. In this situation the hypervisor may decide what resources the contexts need. However, if no information is provided the hypervisor evaluates the behavior of the resources and of the application and makes a guess about the future. The hypervisor resizes only the registered contexts.\hypertarget{SchedulingContextHypervisor_InterrogateTheRuntime}{}\section{Interrogate The Runtime}\label{SchedulingContextHypervisor_InterrogateTheRuntime}
The runtime provides the hypervisor with information concerning the behavior of the resources and the application. This is done by using the {\ttfamily performance\+\_\+counters} which represent callbacks indicating when the resources are idle or not efficient, when the application submits tasks or when it becomes to slow.\hypertarget{SchedulingContextHypervisor_TriggerTheHypervisor}{}\section{Trigger the Hypervisor}\label{SchedulingContextHypervisor_TriggerTheHypervisor}
The resizing is triggered either when the application requires it ({\ttfamily  sc\+\_\+hypervisor\+\_\+resize\+\_\+ctxs }) or when the initials distribution of resources alters the performance of the application (the application is to slow or the resource are idle for too long time). If the environment variable {\ttfamily S\+C\+\_\+\+H\+Y\+P\+E\+R\+V\+I\+S\+O\+R\+\_\+\+T\+R\+I\+G\+G\+E\+R\+\_\+\+R\+E\+S\+I\+ZE} is set to {\ttfamily speed} the monitored speed of the contexts is compared to a theoretical value computed with a linear program, and the resizing is triggered whenever the two values do not correspond. Otherwise, if the environment variable is set to {\ttfamily idle} the hypervisor triggers the resizing algorithm whenever the workers are idle for a period longer than the threshold indicated by the programmer. When this happens different resizing strategy are applied that target minimizing the total execution of the application, the instant speed or the idle time of the resources.\hypertarget{SchedulingContextHypervisor_ResizingStrategies}{}\section{Resizing Strategies}\label{SchedulingContextHypervisor_ResizingStrategies}
The plugin proposes several strategies for resizing the scheduling context.

The {\bfseries Application driven} strategy uses the user\textquotesingle{}s input concerning the moment when he wants to resize the contexts. Thus, the users tags the task that should trigger the resizing process. We can set directly the field \hyperlink{group__API__Codelet__And__Tasks_a48416cfa1692f8e8cb42d80ec9f1eb5e}{starpu\+\_\+task\+::hypervisor\+\_\+tag} or use the macro \+::\+S\+T\+A\+R\+P\+U\+\_\+\+H\+Y\+P\+E\+R\+V\+I\+S\+O\+R\+\_\+\+T\+AG in the function \hyperlink{group__API__Insert__Task_gad79a50a21fe717126659b2998209c1c6}{starpu\+\_\+task\+\_\+insert()}.


\begin{DoxyCode}
task.hypervisor\_tag = 2;
\end{DoxyCode}


or


\begin{DoxyCode}
\hyperlink{group__API__Insert__Task_gad79a50a21fe717126659b2998209c1c6}{starpu\_task\_insert}(&codelet,
                    ...,
                    STARPU\_HYPERVISOR\_TAG, 2,
                    0);
\end{DoxyCode}


Then the user has to indicate that when a task with the specified tag is executed the contexts should resize.


\begin{DoxyCode}
sc\_hypervisor\_resize(sched\_ctx, 2);
\end{DoxyCode}


The user can use the same tag to change the resizing configuration of the contexts if he considers it necessary.


\begin{DoxyCode}
\hyperlink{group__API__SC__Hypervisor__usage_ga0a4b27500994ca2c56047a7da18c6216}{sc\_hypervisor\_ctl}(sched\_ctx,
                    \hyperlink{group__API__SC__Hypervisor__usage_ga6085b1585a13c94d48f185687c6a117c}{SC\_HYPERVISOR\_MIN\_WORKERS}, 6,
                    \hyperlink{group__API__SC__Hypervisor__usage_gaafac498199c6441e2d45c9b4eac8dfe4}{SC\_HYPERVISOR\_MAX\_WORKERS}, 12,
                    \hyperlink{group__API__SC__Hypervisor__usage_gac3e457f9b5741f587d241d62cc843f9e}{SC\_HYPERVISOR\_TIME\_TO\_APPLY}, 2,
                    NULL);
\end{DoxyCode}


The {\bfseries Idleness} based strategy moves workers unused in a certain context to another one needing them. (see Users’ Input In The Resizing Process)


\begin{DoxyCode}
\textcolor{keywordtype}{int} workerids[3] = \{1, 3, 10\};
\textcolor{keywordtype}{int} workerids2[9] = \{0, 2, 4, 5, 6, 7, 8, 9, 11\};
\hyperlink{group__API__SC__Hypervisor__usage_ga0a4b27500994ca2c56047a7da18c6216}{sc\_hypervisor\_ctl}(sched\_ctx\_id,
            \hyperlink{group__API__SC__Hypervisor__usage_ga70c120158d02e550632de0ec5a207df4}{SC\_HYPERVISOR\_MAX\_IDLE}, workerids, 3, 10000.0,
            \hyperlink{group__API__SC__Hypervisor__usage_ga70c120158d02e550632de0ec5a207df4}{SC\_HYPERVISOR\_MAX\_IDLE}, workerids2, 9, 50000.0,
            NULL);
\end{DoxyCode}


The {\bfseries Gflops rate} based strategy resizes the scheduling contexts such that they all finish at the same time. The speed of each of them is computed and once one of them is significantly slower the resizing process is triggered. In order to do these computations the user has to input the total number of instructions needed to be executed by the parallel kernels and the number of instruction to be executed by each task.

The number of flops to be executed by a context are passed as parameter when they are registered to the hypervisor, ({\ttfamily sc\+\_\+hypervisor\+\_\+register\+\_\+ctx(sched\+\_\+ctx\+\_\+id, flops)}) and the one to be executed by each task are passed when the task is submitted. The corresponding field is \hyperlink{group__API__Codelet__And__Tasks_a840563895dbf036b6ffd783a8ea2504d}{starpu\+\_\+task\+::flops} and the corresponding macro in the function \hyperlink{group__API__Insert__Task_gad79a50a21fe717126659b2998209c1c6}{starpu\+\_\+task\+\_\+insert()} is \hyperlink{group__API__Insert__Task_ga1a0a565f2de522abc9c5f3457397b095}{S\+T\+A\+R\+P\+U\+\_\+\+F\+L\+O\+PS} ({\bfseries Caution}\+: but take care of passing a double, not an integer, otherwise parameter passing will be bogus). When the task is executed the resizing process is triggered.


\begin{DoxyCode}
task.flops = 100;
\end{DoxyCode}


or


\begin{DoxyCode}
\hyperlink{group__API__Insert__Task_gad79a50a21fe717126659b2998209c1c6}{starpu\_task\_insert}(&codelet,
                    ...,
                    \hyperlink{group__API__Insert__Task_ga1a0a565f2de522abc9c5f3457397b095}{STARPU\_FLOPS}, (\textcolor{keywordtype}{double}) 100,
                    0);
\end{DoxyCode}


The {\bfseries Feft} strategy uses a linear program to predict the best distribution of resources such that the application finishes in a minimum amount of time. As for the {\bfseries Gflops rate } strategy the programmers has to indicate the total number of flops to be executed when registering the context. This number of flops may be updated dynamically during the execution of the application whenever this information is not very accurate from the beginning. The function {\ttfamily sc\+\_\+hypervisor\+\_\+update\+\_\+diff\+\_\+total\+\_\+flop } is called in order add or remove a difference to the flops left to be executed. Tasks are provided also the number of flops corresponding to each one of them. During the execution of the application the hypervisor monitors the consumed flops and recomputes the time left and the number of resources to use. The speed of each type of resource is (re)evaluated and inserter in the linear program in order to better adapt to the needs of the application.

The {\bfseries Teft} strategy uses a linear program too, that considers all the types of tasks and the number of each of them and it tries to allocates resources such that the application finishes in a minimum amount of time. A previous calibration of Star\+PU would be useful in order to have good predictions of the execution time of each type of task.

The types of tasks may be determines directly by the hypervisor when they are submitted. However there are applications that do not expose all the graph of tasks from the beginning. In this case in order to let the hypervisor know about all the tasks the function {\ttfamily  sc\+\_\+hypervisor\+\_\+set\+\_\+type\+\_\+of\+\_\+task } will just inform the hypervisor about future tasks without submitting them right away.

The {\bfseries Ispeed } strategy divides the execution of the application in several frames. For each frame the hypervisor computes the speed of the contexts and tries making them run at the same speed. The strategy requires less contribution from the user as the hypervisor requires only the size of the frame in terms of flops.


\begin{DoxyCode}
\textcolor{keywordtype}{int} workerids[3] = \{1, 3, 10\};
\textcolor{keywordtype}{int} workerids2[9] = \{0, 2, 4, 5, 6, 7, 8, 9, 11\};
\hyperlink{group__API__SC__Hypervisor__usage_ga0a4b27500994ca2c56047a7da18c6216}{sc\_hypervisor\_ctl}(sched\_ctx\_id,
                  \hyperlink{group__API__SC__Hypervisor__usage_gac3b55f616c270eb90c0103d6f062d384}{SC\_HYPERVISOR\_ISPEED\_W\_SAMPLE}, workerids, 3, 2000000000.0,
                  \hyperlink{group__API__SC__Hypervisor__usage_gac3b55f616c270eb90c0103d6f062d384}{SC\_HYPERVISOR\_ISPEED\_W\_SAMPLE}, workerids2, 9, 200000000000.0
      ,
                  \hyperlink{group__API__SC__Hypervisor__usage_ga7ba4c0c3deca0dd4148ddd1f0b919792}{SC\_HYPERVISOR\_ISPEED\_CTX\_SAMPLE}, 60000000000.0,
            NULL);
\end{DoxyCode}


The {\bfseries Throughput } strategy focuses on maximizing the throughput of the resources and resizes the contexts such that the machine is running at its maximum efficiency (maximum instant speed of the workers).\hypertarget{SchedulingContextHypervisor_DefiningANewHypervisorPolicy}{}\section{Defining A New Hypervisor Policy}\label{SchedulingContextHypervisor_DefiningANewHypervisorPolicy}
While Scheduling Context Hypervisor Plugin comes with a variety of resizing policies (see \hyperlink{SchedulingContextHypervisor_ResizingStrategies}{Resizing Strategies}), it may sometimes be desirable to implement custom policies to address specific problems. The A\+PI described below allows users to write their own resizing policy.

Here an example of how to define a new policy


\begin{DoxyCode}
\textcolor{keyword}{struct }\hyperlink{group__API__SC__Hypervisor_structsc__hypervisor__policy}{sc\_hypervisor\_policy} dummy\_policy =
\{
       .\hyperlink{group__API__SC__Hypervisor_a38f0b680a437c321ed5f9a5f91261e73}{handle\_poped\_task} = dummy\_handle\_poped\_task,
       .handle\_pushed\_task = dummy\_handle\_pushed\_task,
       .handle\_idle\_cycle = dummy\_handle\_idle\_cycle,
       .handle\_idle\_end = dummy\_handle\_idle\_end,
       .handle\_post\_exec\_hook = dummy\_handle\_post\_exec\_hook,
       .custom = 1,
       .name = \textcolor{stringliteral}{"dummy"}
\};
\end{DoxyCode}
 