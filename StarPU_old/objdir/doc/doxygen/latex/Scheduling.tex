\hypertarget{Scheduling_TaskSchedulingPolicy}{}\section{Task Scheduling Policy}\label{Scheduling_TaskSchedulingPolicy}
The basics of the scheduling policy are that


\begin{DoxyItemize}
\item The scheduler gets to schedule tasks ({\ttfamily push} operation) when they become ready to be executed, i.\+e. they are not waiting for some tags, data dependencies or task dependencies. 
\item Workers pull tasks ({\ttfamily pop} operation) one by one from the scheduler. 
\end{DoxyItemize}

This means scheduling policies usually contain at least one queue of tasks to store them between the time when they become available, and the time when a worker gets to grab them.

By default, Star\+PU uses the simple greedy scheduler {\ttfamily eager}. This is because it provides correct load balance even if the application codelets do not have performance models. If your application codelets have performance models (\hyperlink{OnlinePerformanceTools_PerformanceModelExample}{Performance Model Example}), you should change the scheduler thanks to the environment variable \hyperlink{ExecutionConfigurationThroughEnvironmentVariables_STARPU_SCHED}{S\+T\+A\+R\+P\+U\+\_\+\+S\+C\+H\+ED}. For instance {\ttfamily export S\+T\+A\+R\+P\+U\+\_\+\+S\+C\+H\+ED=dmda} . Use {\ttfamily help} to get the list of available schedulers.

The {\bfseries eager} scheduler uses a central task queue, from which all workers draw tasks to work on concurrently. This however does not permit to prefetch data since the scheduling decision is taken late. If a task has a non-\/0 priority, it is put at the front of the queue.

The {\bfseries prio} scheduler also uses a central task queue, but sorts tasks by priority (between -\/5 and 5).

The {\bfseries random} scheduler uses a queue per worker, and distributes tasks randomly according to assumed worker overall performance.

The {\bfseries ws} (work stealing) scheduler uses a queue per worker, and schedules a task on the worker which released it by default. When a worker becomes idle, it steals a task from the most loaded worker.

The {\bfseries lws} (locality work stealing) scheduler uses a queue per worker, and schedules a task on the worker which released it by default. When a worker becomes idle, it steals a task from neighbour workers. It also takes into account priorities.

The {\bfseries dm} (deque model) scheduler uses task execution performance models into account to perform a H\+E\+F\+T-\/similar scheduling strategy\+: it schedules tasks where their termination time will be minimal. The difference with H\+E\+FT is that {\bfseries dm} schedules tasks as soon as they become available, and thus in the order they become available, without taking priorities into account.

The {\bfseries dmda} (deque model data aware) scheduler is similar to dm, but it also takes into account data transfer time.

The {\bfseries dmdar} (deque model data aware ready) scheduler is similar to dmda, but it also sorts tasks on per-\/worker queues by number of already-\/available data buffers on the target device.

The {\bfseries dmdas} (deque model data aware sorted) scheduler is similar to dmdar, except that it sorts tasks by priority order, which allows to become even closer to H\+E\+FT by respecting priorities after having made the scheduling decision (but it still schedules tasks in the order they become available).

The {\bfseries heft} (heterogeneous earliest finish time) scheduler is a deprecated alias for {\bfseries dmda}.

The {\bfseries pheft} (parallel H\+E\+FT) scheduler is similar to dmda, it also supports parallel tasks (still experimental). Should not be used when several contexts using it are being executed simultaneously.

The {\bfseries peager} (parallel eager) scheduler is similar to eager, it also supports parallel tasks (still experimental). Should not be used when several contexts using it are being executed simultaneously.

T\+O\+DO\+: describe modular schedulers\hypertarget{Scheduling_TaskDistributionVsDataTransfer}{}\section{Task Distribution Vs Data Transfer}\label{Scheduling_TaskDistributionVsDataTransfer}
Distributing tasks to balance the load induces data transfer penalty. Star\+PU thus needs to find a balance between both. The target function that the scheduler {\ttfamily dmda} of Star\+PU tries to minimize is {\ttfamily alpha $\ast$ T\+\_\+execution + beta $\ast$ T\+\_\+data\+\_\+transfer}, where {\ttfamily T\+\_\+execution} is the estimated execution time of the codelet (usually accurate), and {\ttfamily T\+\_\+data\+\_\+transfer} is the estimated data transfer time. The latter is estimated based on bus calibration before execution start, i.\+e. with an idle machine, thus without contention. You can force bus re-\/calibration by running the tool {\ttfamily starpu\+\_\+calibrate\+\_\+bus}. The beta parameter defaults to {\ttfamily 1}, but it can be worth trying to tweak it by using {\ttfamily export S\+T\+A\+R\+P\+U\+\_\+\+S\+C\+H\+E\+D\+\_\+\+B\+E\+TA=2} for instance, since during real application execution, contention makes transfer times bigger. This is of course imprecise, but in practice, a rough estimation already gives the good results that a precise estimation would give.\hypertarget{Scheduling_Energy-basedScheduling}{}\section{Energy-\/based Scheduling}\label{Scheduling_Energy-basedScheduling}
If the application can provide some energy consumption performance model (through the field \hyperlink{group__API__Codelet__And__Tasks_a83c8b0df97d1cbc3afe5ea603711fc47}{starpu\+\_\+codelet\+::energy\+\_\+model}), Star\+PU will take it into account when distributing tasks. The target function that the scheduler {\ttfamily dmda} minimizes becomes {\ttfamily alpha $\ast$ T\+\_\+execution + beta $\ast$ T\+\_\+data\+\_\+transfer + gamma $\ast$ Consumption} , where {\ttfamily Consumption} is the estimated task consumption in Joules. To tune this parameter, use {\ttfamily export S\+T\+A\+R\+P\+U\+\_\+\+S\+C\+H\+E\+D\+\_\+\+G\+A\+M\+MA=3000} for instance, to express that each Joule (i.\+e kW during 1000us) is worth 3000us execution time penalty. Setting {\ttfamily alpha} and {\ttfamily beta} to zero permits to only take into account energy consumption.

This is however not sufficient to correctly optimize energy\+: the scheduler would simply tend to run all computations on the most energy-\/conservative processing unit. To account for the consumption of the whole machine (including idle processing units), the idle power of the machine should be given by setting {\ttfamily export S\+T\+A\+R\+P\+U\+\_\+\+I\+D\+L\+E\+\_\+\+P\+O\+W\+ER=200} for 200W, for instance. This value can often be obtained from the machine power supplier.

The energy actually consumed by the total execution can be displayed by setting {\ttfamily export S\+T\+A\+R\+P\+U\+\_\+\+P\+R\+O\+F\+I\+L\+I\+NG=1 S\+T\+A\+R\+P\+U\+\_\+\+W\+O\+R\+K\+E\+R\+\_\+\+S\+T\+A\+TS=1} .

On-\/line task consumption measurement is currently only supported through the {\ttfamily C\+L\+\_\+\+P\+R\+O\+F\+I\+L\+I\+N\+G\+\_\+\+P\+O\+W\+E\+R\+\_\+\+C\+O\+N\+S\+U\+M\+ED} Open\+CL extension, implemented in the Movi\+Sim simulator. Applications can however provide explicit measurements by using the function \hyperlink{group__API__Performance__Model_gaecb9341bff471557abbb63a966449481}{starpu\+\_\+perfmodel\+\_\+update\+\_\+history()} (examplified in \hyperlink{OnlinePerformanceTools_PerformanceModelExample}{Performance Model Example} with the {\ttfamily energy\+\_\+model} performance model). Fine-\/grain measurement is often not feasible with the feedback provided by the hardware, so the user can for instance run a given task a thousand times, measure the global consumption for that series of tasks, divide it by a thousand, repeat for varying kinds of tasks and task sizes, and eventually feed Star\+PU with these manual measurements through \hyperlink{group__API__Performance__Model_gaecb9341bff471557abbb63a966449481}{starpu\+\_\+perfmodel\+\_\+update\+\_\+history()}. For instance, for C\+U\+DA devices, {\ttfamily nvidia-\/smi -\/q -\/d P\+O\+W\+ER} can be used to get the current consumption in Watt. Multiplying that value by the average duration of a single task gives the consumption of the task in Joules, which can be given to \hyperlink{group__API__Performance__Model_gaecb9341bff471557abbb63a966449481}{starpu\+\_\+perfmodel\+\_\+update\+\_\+history()}.\hypertarget{Scheduling_StaticScheduling}{}\section{Static Scheduling}\label{Scheduling_StaticScheduling}
In some cases, one may want to force some scheduling, for instance force a given set of tasks to G\+P\+U0, another set to G\+P\+U1, etc. while letting some other tasks be scheduled on any other device. This can indeed be useful to guide Star\+PU into some work distribution, while still letting some degree of dynamism. For instance, to force execution of a task on C\+U\+D\+A0\+:


\begin{DoxyCode}
task->execute\_on\_a\_specific\_worker = 1;
task->worker = \hyperlink{group__API__Workers__Properties_gab0ffe5622c5980444226f1ed8d6fee71}{starpu\_worker\_get\_by\_type}(
      \hyperlink{group__API__Workers__Properties_gga173d616aefe98c33a47a847fd2fca37dac95bbacadbe0599c06a48059d2821611}{STARPU\_CUDA\_WORKER}, 0);
\end{DoxyCode}


One can also specify the order in which tasks must be executed by setting the \hyperlink{group__API__Codelet__And__Tasks_a704b64ae35fc117b93a88ade0e1d6910}{starpu\+\_\+task\+::workerorder} field. If this field is set to a non-\/zero value, it provides the per-\/worker consecutive order in which tasks will be executed, starting from 1. For a given of such task, the worker will thus not execute it before all the tasks with smaller order value have been executed, notably in case those tasks are not available yet due to some dependencies. This eventually gives total control of task scheduling, and Star\+PU will only serve as a \char`\"{}self-\/timed\char`\"{} task runtime. Of course, the provided order has to be runnable, i.\+e. a task should should not depend on another task bound to the same worker with a bigger order.

Note however that using scheduling contexts while statically scheduling tasks on workers could be tricky. Be careful to schedule the tasks exactly on the workers of the corresponding contexts, otherwise the workers\textquotesingle{} corresponding scheduling structures may not be allocated or the execution of the application may deadlock. Moreover, the hypervisor should not be used when statically scheduling tasks.\hypertarget{Scheduling_DefiningANewSchedulingPolicy}{}\section{Defining A New Scheduling Policy}\label{Scheduling_DefiningANewSchedulingPolicy}
A full example showing how to define a new scheduling policy is available in the Star\+PU sources in the directory {\ttfamily examples/scheduler/}.

The scheduler has to provide methods\+:


\begin{DoxyCode}
\textcolor{keyword}{static} \textcolor{keyword}{struct }\hyperlink{group__API__Scheduling__Policy_structstarpu__sched__policy}{starpu\_sched\_policy} dummy\_sched\_policy = \{
    .\hyperlink{group__API__Scheduling__Policy_a58f189e5addbfef1c29cc2cc0d9c42c2}{init\_sched} = init\_dummy\_sched,
    .deinit\_sched = deinit\_dummy\_sched,
    .add\_workers = dummy\_sched\_add\_workers,
    .remove\_workers = dummy\_sched\_remove\_workers,
    .push\_task = push\_task\_dummy,
    .pop\_task = pop\_task\_dummy,
    .policy\_name = \textcolor{stringliteral}{"dummy"},
    .policy\_description = \textcolor{stringliteral}{"dummy scheduling strategy"}
\};
\end{DoxyCode}


The idea is that when a task becomes ready for execution, the \hyperlink{group__API__Scheduling__Policy_ae1e6249dab367bb624fff7ed78aaa44b}{starpu\+\_\+sched\+\_\+policy\+::push\+\_\+task} method is called. When a worker is idle, the \hyperlink{group__API__Scheduling__Policy_a1b2449ac50ca8db35234a36a3f77b0b5}{starpu\+\_\+sched\+\_\+policy\+::pop\+\_\+task} method is called to get a task. It is up to the scheduler to implement what is between. A simple eager scheduler is for instance to make \hyperlink{group__API__Scheduling__Policy_ae1e6249dab367bb624fff7ed78aaa44b}{starpu\+\_\+sched\+\_\+policy\+::push\+\_\+task} push the task to a global list, and make \hyperlink{group__API__Scheduling__Policy_a1b2449ac50ca8db35234a36a3f77b0b5}{starpu\+\_\+sched\+\_\+policy\+::pop\+\_\+task} pop from that list.

The \hyperlink{group__API__Scheduling__Policy_structstarpu__sched__policy}{starpu\+\_\+sched\+\_\+policy} section provides the exact rules that govern the methods of the policy.

Make sure to have a look at the \hyperlink{group__API__Scheduling__Policy}{Scheduling Policy} section, which provides a list of the available functions for writing advanced schedulers, such as starpu\+\_\+task\+\_\+expected\+\_\+length, starpu\+\_\+task\+\_\+expected\+\_\+data\+\_\+transfer\+\_\+time, starpu\+\_\+task\+\_\+expected\+\_\+energy, starpu\+\_\+prefetch\+\_\+task\+\_\+input\+\_\+node, etc. Other useful functions include starpu\+\_\+transfer\+\_\+bandwidth, starpu\+\_\+transfer\+\_\+latency, starpu\+\_\+transfer\+\_\+predict, ...

Usual functions can also be used on tasks, for instance one can do


\begin{DoxyCode}
size = 0;
write = 0;
\textcolor{keywordflow}{if} (task->cl)
    \textcolor{keywordflow}{for} (i = 0; i < \hyperlink{group__API__Codelet__And__Tasks_ga829d31913f4b0890192d4924bd264f88}{STARPU\_TASK\_GET\_NBUFFERS}(task); i++)
    \{
        \hyperlink{group__API__Data__Management_ga5f517ab725864d54b0459896a8f8ae07}{starpu\_data\_handle\_t} data = \hyperlink{group__API__Codelet__And__Tasks_ga260a1f7f8bef6b255dd0e0e45481385e}{STARPU\_TASK\_GET\_HANDLE}(task, 
      i)
        \textcolor{keywordtype}{size\_t} datasize = \hyperlink{group__API__Data__Interfaces_gac14cfc52c450c81dd4789a7aace79d3b}{starpu\_data\_get\_size}(data);
        size += datasize;
        \textcolor{keywordflow}{if} (\hyperlink{group__API__Codelet__And__Tasks_ga649920660819ed7d9922e896e63acd95}{STARPU\_TASK\_GET\_MODE}(task, i) & \hyperlink{group__API__Data__Management_gga1fb3a1ff8622747d653d1b5f41bc41dba052ab75035ca297c7955363c605231c8}{STARPU\_W})
            write += datasize;
    \}
\end{DoxyCode}


And various queues can be used in schedulers. A variety of examples of schedulers can be read in {\ttfamily src/sched\+\_\+policies}, for instance {\ttfamily random\+\_\+policy.\+c}, {\ttfamily eager\+\_\+central\+\_\+policy.\+c}, {\ttfamily work\+\_\+stealing\+\_\+policy.\+c}\hypertarget{Scheduling_GraphScheduling}{}\section{Graph-\/based scheduling}\label{Scheduling_GraphScheduling}
For performance reasons, most of the schedulers shipped with Star\+PU use simple list-\/scheduling heuristics, assuming that the application has already set priorities. That is why they do their scheduling between when tasks become available for execution and when a worker becomes idle, without looking at the task graph.

Other heuristics can however look at the task graph. Recording the task graph is expensive, so it is not available by default, the scheduling heuristic has to set \+\_\+starpu\+\_\+graph\+\_\+record to 1 from the initialization function, to make it available. Then the {\ttfamily \+\_\+starpu\+\_\+graph$\ast$} functions can be used.

{\ttfamily src/sched\+\_\+policies/graph\+\_\+test\+\_\+policy.\+c} is an example of simple greedy policy which automatically computes priorities by bottom-\/up rank.

The idea is that while the application submits tasks, they are only pushed to a bag of tasks. When the application is finished with submitting tasks, it calls starpu\+\_\+do\+\_\+schedule (or starpu\+\_\+task\+\_\+wait\+\_\+for\+\_\+all, which calls starpu\+\_\+do\+\_\+schedule), and the \hyperlink{group__API__Scheduling__Policy_a31a640d992698457c8ce098df4df780a}{starpu\+\_\+sched\+\_\+policy\+::do\+\_\+schedule} method of the scheduler is called. This method calls \+\_\+starpu\+\_\+graph\+\_\+compute\+\_\+depths to compute the bottom-\/up ranks, and then uses these rank to set priorities over tasks.

It then has two priority queues, one for C\+P\+Us, and one for G\+P\+Us, and uses a dumb heuristic based on the duration of the task over C\+P\+Us and G\+P\+Us to decide between the two queues. C\+PU workers can then pop from the C\+PU priority queue, and G\+PU workers from the G\+PU priority queue.\hypertarget{Scheduling_DebuggingScheduling}{}\section{Debugging scheduling}\label{Scheduling_DebuggingScheduling}
All the \hyperlink{OnlinePerformanceTools}{Online Performance Tools} and \hyperlink{OfflinePerformanceTools}{Offline Performance Tools} can be used to get information about how well the execution proceeded, and thus the overall quality of the execution.

Precise debugging can also be performed by using the \hyperlink{ExecutionConfigurationThroughEnvironmentVariables_STARPU_TASK_BREAK_ON_SCHED}{S\+T\+A\+R\+P\+U\+\_\+\+T\+A\+S\+K\+\_\+\+B\+R\+E\+A\+K\+\_\+\+O\+N\+\_\+\+S\+C\+H\+ED}, \hyperlink{ExecutionConfigurationThroughEnvironmentVariables_STARPU_TASK_BREAK_ON_PUSH}{S\+T\+A\+R\+P\+U\+\_\+\+T\+A\+S\+K\+\_\+\+B\+R\+E\+A\+K\+\_\+\+O\+N\+\_\+\+P\+U\+SH}, and \hyperlink{ExecutionConfigurationThroughEnvironmentVariables_STARPU_TASK_BREAK_ON_POP}{S\+T\+A\+R\+P\+U\+\_\+\+T\+A\+S\+K\+\_\+\+B\+R\+E\+A\+K\+\_\+\+O\+N\+\_\+\+P\+OP} environment variables. By setting the job\+\_\+id of a task in these environment variables, Star\+PU will raise S\+I\+G\+T\+R\+AP when the task is being scheduled, pushed, or popped by the scheduler. That means that when one notices that a task is being scheduled in a seemingly odd way, one can just reexecute the application in a debugger, with some of those variables set, and the execution will stop exactly at the scheduling points of that task, thus allowing to inspect the scheduler state etc. 