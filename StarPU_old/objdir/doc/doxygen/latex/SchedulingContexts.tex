T\+O\+DO\+: improve!\hypertarget{SchedulingContexts_GeneralIdeas}{}\section{General Ideas}\label{SchedulingContexts_GeneralIdeas}
Scheduling contexts represent abstracts sets of workers that allow the programmers to control the distribution of computational resources (i.\+e. C\+P\+Us and G\+P\+Us) to concurrent parallel kernels. The main goal is to minimize interferences between the execution of multiple parallel kernels, by partitioning the underlying pool of workers using contexts.\hypertarget{SchedulingContexts_CreatingAContext}{}\section{Creating A Context}\label{SchedulingContexts_CreatingAContext}
By default, the application submits tasks to an initial context, which disposes of all the computation resources available to Star\+PU (all the workers). If the application programmer plans to launch several parallel kernels simultaneously, by default these kernels will be executed within this initial context, using a single scheduler policy(see \hyperlink{Scheduling_TaskSchedulingPolicy}{Task Scheduling Policy}). Meanwhile, if the application programmer is aware of the demands of these kernels and of the specificity of the machine used to execute them, the workers can be divided between several contexts. These scheduling contexts will isolate the execution of each kernel and they will permit the use of a scheduling policy proper to each one of them.

Scheduling Contexts may be created in two ways\+: either the programmers indicates the set of workers corresponding to each context (providing he knows the identifiers of the workers running within Star\+PU), or the programmer does not provide any worker list and leaves the Hypervisor assign workers to each context according to their needs (\hyperlink{SchedulingContextHypervisor}{Scheduling Context Hypervisor})

Both cases require a call to the function {\ttfamily starpu\+\_\+sched\+\_\+ctx\+\_\+create}, which requires as input the worker list (the exact list or a N\+U\+LL pointer) and the scheduling policy. The latter one can be a character list corresponding to the name of a Star\+PU predefined policy or the pointer to a custom policy. The function returns an identifier of the context created which you will use to indicate the context you want to submit the tasks to.


\begin{DoxyCode}
\textcolor{comment}{/* the list of resources the context will manage */}
\textcolor{keywordtype}{int} workerids[3] = \{1, 3, 10\};

\textcolor{comment}{/* indicate the scheduling policy to be used within the context, the list of}
\textcolor{comment}{   workers assigned to it, the number of workers, the name of the context */}
\textcolor{keywordtype}{int} id\_ctx = \hyperlink{group__API__Scheduling__Contexts_ga517b7cf4bfac60f2faf484584f19d9d3}{starpu\_sched\_ctx\_create}(\textcolor{stringliteral}{"dmda"}, workerids, 3, \textcolor{stringliteral}{"my\_ctx"});

\textcolor{comment}{/* let StarPU know that the following tasks will be submitted to this context */}
starpu\_sched\_ctx\_set\_task\_context(\textcolor{keywordtype}{id});

\textcolor{comment}{/* submit the task to StarPU */}
\hyperlink{group__API__Codelet__And__Tasks_gaa32228bf7f452f7d664986668ea46590}{starpu\_task\_submit}(task);
\end{DoxyCode}


Note\+: Parallel greedy and parallel heft scheduling policies do not support the existence of several disjoint contexts on the machine. Combined workers are constructed depending on the entire topology of the machine, not only the one belonging to a context.\hypertarget{SchedulingContexts_ModifyingAContext}{}\section{Modifying A Context}\label{SchedulingContexts_ModifyingAContext}
A scheduling context can be modified dynamically. The applications may change its requirements during the execution and the programmer can add additional workers to a context or remove if no longer needed. In the following example we have two scheduling contexts {\ttfamily sched\+\_\+ctx1} and {\ttfamily sched\+\_\+ctx2}. After executing a part of the tasks some of the workers of {\ttfamily sched\+\_\+ctx1} will be moved to context {\ttfamily sched\+\_\+ctx2}.


\begin{DoxyCode}
\textcolor{comment}{/* the list of ressources that context 1 will give away */}
\textcolor{keywordtype}{int} workerids[3] = \{1, 3, 10\};

\textcolor{comment}{/* add the workers to context 1 */}
\hyperlink{group__API__Scheduling__Contexts_ga4938f463f59aaea3f0b766c5da770fb9}{starpu\_sched\_ctx\_add\_workers}(workerids, 3, sched\_ctx2);

\textcolor{comment}{/* remove the workers from context 2 */}
\hyperlink{group__API__Scheduling__Contexts_ga5fced8ae787486d906b057c20dfe5926}{starpu\_sched\_ctx\_remove\_workers}(workerids, 3, sched\_ctx1);
\end{DoxyCode}
\hypertarget{SchedulingContexts_SubmittingTasksToAContext}{}\section{Submitting Tasks To A Context}\label{SchedulingContexts_SubmittingTasksToAContext}
The application may submit tasks to several contexts either simultaneously or sequnetially. If several threads of submission are used the function {\ttfamily starpu\+\_\+sched\+\_\+ctx\+\_\+set\+\_\+context} may be called just before {\ttfamily starpu\+\_\+task\+\_\+submit}. Thus Star\+PU considers that the current thread will submit tasks to the coresponding context.

When the application may not assign a thread of submission to each context, the id of the context must be indicated by using the function {\ttfamily starpu\+\_\+task\+\_\+submit\+\_\+to\+\_\+ctx} or the field {\ttfamily S\+T\+A\+R\+P\+U\+\_\+\+S\+C\+H\+E\+D\+\_\+\+C\+TX} for \hyperlink{group__API__Insert__Task_gad79a50a21fe717126659b2998209c1c6}{starpu\+\_\+task\+\_\+insert()}.\hypertarget{SchedulingContexts_DeletingAContext}{}\section{Deleting A Context}\label{SchedulingContexts_DeletingAContext}
When a context is no longer needed it must be deleted. The application can indicate which context should keep the resources of a deleted one. All the tasks of the context should be executed before doing this. Thus, the programmer may use either a barrier and then delete the context directly, or just indicate that other tasks will not be submitted later on to the context (such that when the last task is executed its workers will be moved to the inheritor) and delete the context at the end of the execution (when a barrier will be used eventually).


\begin{DoxyCode}
\textcolor{comment}{/* when the context 2 is deleted context 1 inherits its resources */}
\hyperlink{group__API__Scheduling__Contexts_gad6b7f96254526bb145e0df5ed0354a13}{starpu\_sched\_ctx\_set\_inheritor}(sched\_ctx2, sched\_ctx1);

\textcolor{comment}{/* submit tasks to context 2 */}
\textcolor{keywordflow}{for} (i = 0; i < ntasks; i++)
    \hyperlink{group__API__Codelet__And__Tasks_ga3c6c4dd317df02d7021e23ab6b14e3e2}{starpu\_task\_submit\_to\_ctx}(task[i],sched\_ctx2);

\textcolor{comment}{/* indicate that context 2 finished submitting and that */}
\textcolor{comment}{/* as soon as the last task of context 2 finished executing */}
\textcolor{comment}{/* its workers can be moved to the inheritor context */}
\hyperlink{group__API__Scheduling__Contexts_ga11e94f2968626d9b46ccf021057ddd90}{starpu\_sched\_ctx\_finished\_submit}(sched\_ctx1);

\textcolor{comment}{/* wait for the tasks of both contexts to finish */}
\hyperlink{group__API__Codelet__And__Tasks_gad0baa8dbfd13e5a7bc3651bcd76022aa}{starpu\_task\_wait\_for\_all}();

\textcolor{comment}{/* delete context 2 */}
\hyperlink{group__API__Scheduling__Contexts_ga101c3a13d4adc8d932c46324ae71b93a}{starpu\_sched\_ctx\_delete}(sched\_ctx2);

\textcolor{comment}{/* delete context 1 */}
\hyperlink{group__API__Scheduling__Contexts_ga101c3a13d4adc8d932c46324ae71b93a}{starpu\_sched\_ctx\_delete}(sched\_ctx1);
\end{DoxyCode}
\hypertarget{SchedulingContexts_EmptyingAContext}{}\section{Emptying A Context}\label{SchedulingContexts_EmptyingAContext}
A context may have no resources at the begining or at a certain moment of the execution. Task can still be submitted to these contexts and they will be executed as soon as the contexts will have resources. A list of tasks pending to be executed is kept and when workers are added to the contexts these tasks start being submitted. However, if resources are never allocated to the context the program will not terminate. If these tasks have low priority the programmer can forbid the application to submit them by calling the function {\ttfamily \hyperlink{group__API__Scheduling__Contexts_gafe295aec3c152fe1fc0dd74e17f82961}{starpu\+\_\+sched\+\_\+ctx\+\_\+stop\+\_\+task\+\_\+submission()}}.\hypertarget{SchedulingContexts_ContextsSharingWorkers}{}\section{Contexts Sharing Workers}\label{SchedulingContexts_ContextsSharingWorkers}
Contexts may share workers when a single context cannot execute efficiently enough alone on these workers or when the application decides to express a hierarchy of contexts. The workers apply an alogrithm of ``\+Round-\/\+Robin\textquotesingle{}\textquotesingle{} to chose the context on which they will ``pop\textquotesingle{}\textquotesingle{} next. By using the function {\ttfamily starpu\+\_\+sched\+\_\+ctx\+\_\+set\+\_\+turn\+\_\+to\+\_\+other\+\_\+ctx}, the programmer can impose the {\ttfamily workerid} to ``pop\textquotesingle{}\textquotesingle{} in the context {\ttfamily sched\+\_\+ctx\+\_\+id} next. 