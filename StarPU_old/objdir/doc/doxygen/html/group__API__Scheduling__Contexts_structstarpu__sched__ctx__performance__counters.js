var group__API__Scheduling__Contexts_structstarpu__sched__ctx__performance__counters =
[
    [ "notify_idle_cycle", "group__API__Scheduling__Contexts.html#a14324cd3fcaa8c82ebe115c51bc52a7a", null ],
    [ "notify_poped_task", "group__API__Scheduling__Contexts.html#a49506fe068d8397c0eaaff04a8196c29", null ],
    [ "notify_pushed_task", "group__API__Scheduling__Contexts.html#a3be16a22d2122868d2949596f6c7dd38", null ],
    [ "notify_post_exec_task", "group__API__Scheduling__Contexts.html#a507f97eb95585e2f853ea8d9dc169a6a", null ],
    [ "notify_submitted_job", "group__API__Scheduling__Contexts.html#a95eede95625a1f46b8ca41ca5c7871fd", null ],
    [ "notify_empty_ctx", "group__API__Scheduling__Contexts.html#ac8f3b877dcfa732f8c74cb79be0e087e", null ],
    [ "notify_delete_context", "group__API__Scheduling__Contexts.html#ab9790a9fdc26eca8c1ff12bd0b19b4a5", null ]
];