var group__API__OpenMP__Runtime__Support_structstarpu__omp__parallel__region__attr =
[
    [ "cl", "group__API__OpenMP__Runtime__Support.html#a983c8209cef872e2798c2fee32012195", null ],
    [ "handles", "group__API__OpenMP__Runtime__Support.html#afa36936cd4bef49a178eb807bb4848ea", null ],
    [ "cl_arg", "group__API__OpenMP__Runtime__Support.html#ae3947bd966486ee4eef7611f0e502e90", null ],
    [ "cl_arg_size", "group__API__OpenMP__Runtime__Support.html#ab804599e2d1ce232830954dbdf3ed886", null ],
    [ "cl_arg_free", "group__API__OpenMP__Runtime__Support.html#aea66f9b4181afb2e4b4c799a09d8a4f0", null ],
    [ "if_clause", "group__API__OpenMP__Runtime__Support.html#aecf5779017beb31e13424202f9dc013b", null ],
    [ "num_threads", "group__API__OpenMP__Runtime__Support.html#a294032947604cf227abad87b30a08c27", null ]
];