var group__API__Modularized__Scheduler_structstarpu__sched__component__specs =
[
    [ "hwloc_machine_composed_sched_component", "group__API__Modularized__Scheduler.html#abec52a111491d978567b509117607aa3", null ],
    [ "hwloc_component_composed_sched_component", "group__API__Modularized__Scheduler.html#ad859296dbded64c8593d613286893a93", null ],
    [ "hwloc_socket_composed_sched_component", "group__API__Modularized__Scheduler.html#a783d5970fe203d01442fb32bc4cafb7c", null ],
    [ "hwloc_cache_composed_sched_component", "group__API__Modularized__Scheduler.html#a59dedc6c07c0bcfd78639da0e4073d73", null ],
    [ "worker_composed_sched_component", "group__API__Modularized__Scheduler.html#af659dfd36be42caa6e0b688d6a9c8888", null ],
    [ "mix_heterogeneous_workers", "group__API__Modularized__Scheduler.html#a5d4260fca364243f76305e6ea7a1a69f", null ]
];