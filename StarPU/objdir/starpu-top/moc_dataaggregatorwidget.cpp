/****************************************************************************
** Meta object code from reading C++ file 'dataaggregatorwidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../starpu-top/dataaggregatorwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dataaggregatorwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DataAggregatorWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      29,   22,   21,   21, 0x0a,
      42,   22,   21,   21, 0x0a,
      58,   21,   21,   21, 0x08,
      94,   81,   21,   21, 0x08,
     110,   81,   21,   21, 0x08,
     143,  129,   21,   21, 0x08,
     175,   21,   21,   21, 0x08,
     187,   21,   21,   21, 0x08,
     201,   22,   21,   21, 0x08,
     218,   22,   21,   21, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DataAggregatorWidget[] = {
    "DataAggregatorWidget\0\0dataId\0addData(int)\0"
    "removeData(int)\0createInternalWidget()\0"
    "value,dataId\0update(int,int)\0"
    "update(double,int)\0curve,checked\0"
    "curveChecked(QwtPlotItem*,bool)\0"
    "dataAdded()\0dataRemoved()\0disableData(int)\0"
    "enableData(int)\0"
};

void DataAggregatorWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DataAggregatorWidget *_t = static_cast<DataAggregatorWidget *>(_o);
        switch (_id) {
        case 0: _t->addData((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->removeData((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->createInternalWidget(); break;
        case 3: _t->update((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->update((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->curveChecked((*reinterpret_cast< QwtPlotItem*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 6: _t->dataAdded(); break;
        case 7: _t->dataRemoved(); break;
        case 8: _t->disableData((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->enableData((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DataAggregatorWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DataAggregatorWidget::staticMetaObject = {
    { &AbstractWidgetWindow::staticMetaObject, qt_meta_stringdata_DataAggregatorWidget,
      qt_meta_data_DataAggregatorWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DataAggregatorWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DataAggregatorWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DataAggregatorWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DataAggregatorWidget))
        return static_cast<void*>(const_cast< DataAggregatorWidget*>(this));
    return AbstractWidgetWindow::qt_metacast(_clname);
}

int DataAggregatorWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractWidgetWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
