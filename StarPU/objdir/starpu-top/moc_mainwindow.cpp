/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../starpu-top/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      71,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      42,   11,   11,   11, 0x05,
      58,   11,   11,   11, 0x05,
      74,   11,   11,   11, 0x05,
      98,   91,   11,   11, 0x05,
     115,   91,   11,   11, 0x05,
     152,  133,   11,   11, 0x05,
     180,  133,   11,   11, 0x05,
     207,  133,   11,   11, 0x05,
     245,  237,   11,   11, 0x05,
     264,   11,   11,   11, 0x05,
     279,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
     293,   11,   11,   11, 0x08,
     322,   11,   11,   11, 0x08,
     357,   11,   11,   11, 0x08,
     395,  237,   11,   11, 0x08,
     426,   11,   11,   11, 0x08,
     452,   11,   11,   11, 0x08,
     485,   11,   11,   11, 0x08,
     512,   11,   11,   11, 0x08,
     567,  557,   11,   11, 0x08,
     592,  586,   11,   11, 0x08,
     615,  607,   11,   11, 0x08,
     632,  237,   11,   11, 0x08,
     659,  647,   11,   11, 0x08,
     687,   11,   11,   11, 0x08,
     709,   11,   11,   11, 0x08,
     756,  738,   11,   11, 0x08,
     801,   11,   11,   11, 0x08,
     830,   11,   11,   11, 0x08,
     866,   11,   11,   11, 0x08,
     905,   11,   11,   11, 0x08,
     925,   11,   11,   11, 0x08,
     949,   11,   11,   11, 0x08,
     974,   11,   11,   11, 0x08,
     989,   11,   11,   11, 0x08,
    1008,   11,   11,   11, 0x08,
    1034,   11,   11,   11, 0x08,
    1048,   11,   11,   11, 0x08,
    1072, 1063,   11,   11, 0x08,
    1106, 1098,   11,   11, 0x08,
    1135,   11,   11,   11, 0x08,
    1147,   11,   11,   11, 0x08,
    1175, 1159,   11,   11, 0x08,
    1241, 1221,   11,   11, 0x08,
    1339, 1295,   11,   11, 0x08,
    1444, 1428,   11,   11, 0x0a,
    1536, 1478,   11,   11, 0x0a,
    1633,   11,   11,   11, 0x0a,
    1655, 1098,   11,   11, 0x0a,
    1682,   11,   11,   11, 0x0a,
    1703, 1697,   11,   11, 0x0a,
    1737, 1697,   11,   11, 0x0a,
    1782, 1765,   11,   11, 0x0a,
    1841, 1823,   11,   11, 0x0a,
    1896, 1098,   11,   11, 0x0a,
    1941, 1928,   11,   11, 0x0a,
    1983, 1970,   11,   11, 0x0a,
    2025, 2008,   11,   11, 0x0a,
    2076, 2047,   11,   11, 0x0a,
    2113, 2047,   11,   11, 0x0a,
    2149, 2047,   11,   11, 0x0a,
    2224, 2188,   11,   11, 0x0a,
    2268, 2188,   11,   11, 0x0a,
    2311, 2188,   11,   11, 0x0a,
    2411, 2357,   11,   11, 0x0a,
    2491, 2465,   11,   11, 0x0a,
    2543, 2526,   11,   11, 0x0a,
    2598, 2572,   11,   11, 0x0a,
    2633, 2572,   11,   11, 0x0a,
    2667, 2572,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0preferencesDialogCreated(int)\0"
    "sessionOpened()\0sessionClosed()\0"
    "clientLaunched()\0dataId\0dataEnabled(int)\0"
    "dataDisabled(int)\0paramId,paramValue\0"
    "paramValueUpdated(int,bool)\0"
    "paramValueUpdated(int,int)\0"
    "paramValueUpdated(int,double)\0enabled\0"
    "debugEnabled(bool)\0debugStepped()\0"
    "debugLocked()\0on_actionConnect_triggered()\0"
    "on_actionLaunch_StarPU_triggered()\0"
    "on_actionSaveSessionSetup_triggered()\0"
    "on_actionDebug_triggered(bool)\0"
    "on_actionQuit_triggered()\0"
    "on_actionPreferences_triggered()\0"
    "on_actionAbout_triggered()\0"
    "on_actionAddDataAggregatorWidget_triggered()\0"
    "connected\0setConnected(bool)\0ready\0"
    "setReady(bool)\0running\0setRunning(bool)\0"
    "setDebug(bool)\0displayMode\0"
    "setDisplayMode(DisplayMode)\0"
    "initDataWidgetLists()\0"
    "initInteractiveWidgetLists()\0"
    "interactiveWidget\0"
    "displayInteractiveWidget(InteractiveWidget*)\0"
    "removeDestroyedDataWidgets()\0"
    "removeDestroyedInteractiveWidgets()\0"
    "removeDestroyedDataAggregatorWidgets()\0"
    "clearDescriptions()\0clearDataDescriptions()\0"
    "clearParamDescriptions()\0clearWidgets()\0"
    "clearDataWidgets()\0clearInteractiveWidgets()\0"
    "openSession()\0closeSession()\0iconFile\0"
    "setStatusBarIcon(QString)\0message\0"
    "setStatusBarMessage(QString)\0debugLock()\0"
    "debugStep()\0mainWindowSetup\0"
    "sessionMainWindowSetupLoaded(MainWindowSetup)\0"
    "parametersDockSetup\0"
    "sessionParametersDockSetupLoaded(ParametersDockSetup)\0"
    "dataWidgetsSetup,dataAggregatorWidgetsSetup\0"
    "sessionWidgetWindowsSetupLoaded(QList<DataWidgetSetup>,QList<DataAggre"
    "gatorWidgetSetup>)\0"
    "serverTimestamp\0synchronizeSessionTime(qlonglong)\0"
    "serverID,dataDescriptions,paramDescriptions,serverDevices\0"
    "initClient(QString,QList<DataDescription*>*,QList<ParamDescription*>*,"
    "QList<starpu_top_device>*)\0"
    "connectionSucceeded()\0connectionAborted(QString)\0"
    "disconnected()\0index\0"
    "updateDataWidgetsDisplayMode(int)\0"
    "updateDisplayModeIndex(int)\0"
    "dataId,newWidget\0"
    "updateDataWidgetType(int,DataWidgetType)\0"
    "paramId,newWidget\0"
    "updateInteractiveWidgetType(int,InteractiveWidgetType)\0"
    "updateStatusBarMessage(QString)\0"
    "errorMessage\0protocolErrorCaught(QString)\0"
    "debugMessage\0setDebugMessage(QString)\0"
    "debugLockMessage\0setDebugLock(QString)\0"
    "dataWidgetId,value,timestamp\0"
    "updateDataWidget(int,bool,qlonglong)\0"
    "updateDataWidget(int,int,qlonglong)\0"
    "updateDataWidget(int,double,qlonglong)\0"
    "interactiveWidgetId,value,timestamp\0"
    "updateInteractiveWidget(int,bool,qlonglong)\0"
    "updateInteractiveWidget(int,int,qlonglong)\0"
    "updateInteractiveWidget(int,double,qlonglong)\0"
    "taskId,deviceId,timestamp,timestampStart,timestampEnd\0"
    "updateTaskPrev(int,int,qlonglong,qlonglong,qlonglong)\0"
    "taskId,deviceId,timestamp\0"
    "updateTaskStart(int,int,qlonglong)\0"
    "taskId,timestamp\0updateTaskEnd(int,qlonglong)\0"
    "interactiveWidgetId,value\0"
    "interactiveWidgetUpdated(int,bool)\0"
    "interactiveWidgetUpdated(int,int)\0"
    "interactiveWidgetUpdated(int,double)\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->preferencesDialogCreated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->sessionOpened(); break;
        case 2: _t->sessionClosed(); break;
        case 3: _t->clientLaunched(); break;
        case 4: _t->dataEnabled((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->dataDisabled((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->paramValueUpdated((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 7: _t->paramValueUpdated((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: _t->paramValueUpdated((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 9: _t->debugEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->debugStepped(); break;
        case 11: _t->debugLocked(); break;
        case 12: _t->on_actionConnect_triggered(); break;
        case 13: _t->on_actionLaunch_StarPU_triggered(); break;
        case 14: _t->on_actionSaveSessionSetup_triggered(); break;
        case 15: _t->on_actionDebug_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->on_actionQuit_triggered(); break;
        case 17: _t->on_actionPreferences_triggered(); break;
        case 18: _t->on_actionAbout_triggered(); break;
        case 19: _t->on_actionAddDataAggregatorWidget_triggered(); break;
        case 20: _t->setConnected((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->setReady((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: _t->setRunning((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->setDebug((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->setDisplayMode((*reinterpret_cast< DisplayMode(*)>(_a[1]))); break;
        case 25: _t->initDataWidgetLists(); break;
        case 26: _t->initInteractiveWidgetLists(); break;
        case 27: _t->displayInteractiveWidget((*reinterpret_cast< InteractiveWidget*(*)>(_a[1]))); break;
        case 28: _t->removeDestroyedDataWidgets(); break;
        case 29: _t->removeDestroyedInteractiveWidgets(); break;
        case 30: _t->removeDestroyedDataAggregatorWidgets(); break;
        case 31: _t->clearDescriptions(); break;
        case 32: _t->clearDataDescriptions(); break;
        case 33: _t->clearParamDescriptions(); break;
        case 34: _t->clearWidgets(); break;
        case 35: _t->clearDataWidgets(); break;
        case 36: _t->clearInteractiveWidgets(); break;
        case 37: _t->openSession(); break;
        case 38: _t->closeSession(); break;
        case 39: _t->setStatusBarIcon((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 40: _t->setStatusBarMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 41: _t->debugLock(); break;
        case 42: _t->debugStep(); break;
        case 43: _t->sessionMainWindowSetupLoaded((*reinterpret_cast< MainWindowSetup(*)>(_a[1]))); break;
        case 44: _t->sessionParametersDockSetupLoaded((*reinterpret_cast< ParametersDockSetup(*)>(_a[1]))); break;
        case 45: _t->sessionWidgetWindowsSetupLoaded((*reinterpret_cast< QList<DataWidgetSetup>(*)>(_a[1])),(*reinterpret_cast< QList<DataAggregatorWidgetSetup>(*)>(_a[2]))); break;
        case 46: _t->synchronizeSessionTime((*reinterpret_cast< qlonglong(*)>(_a[1]))); break;
        case 47: _t->initClient((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QList<DataDescription*>*(*)>(_a[2])),(*reinterpret_cast< QList<ParamDescription*>*(*)>(_a[3])),(*reinterpret_cast< QList<starpu_top_device>*(*)>(_a[4]))); break;
        case 48: _t->connectionSucceeded(); break;
        case 49: _t->connectionAborted((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 50: _t->disconnected(); break;
        case 51: _t->updateDataWidgetsDisplayMode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 52: _t->updateDisplayModeIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 53: _t->updateDataWidgetType((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< DataWidgetType(*)>(_a[2]))); break;
        case 54: _t->updateInteractiveWidgetType((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< InteractiveWidgetType(*)>(_a[2]))); break;
        case 55: _t->updateStatusBarMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 56: _t->protocolErrorCaught((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 57: _t->setDebugMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 58: _t->setDebugLock((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 59: _t->updateDataWidget((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 60: _t->updateDataWidget((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 61: _t->updateDataWidget((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 62: _t->updateInteractiveWidget((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 63: _t->updateInteractiveWidget((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 64: _t->updateInteractiveWidget((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 65: _t->updateTaskPrev((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3])),(*reinterpret_cast< qlonglong(*)>(_a[4])),(*reinterpret_cast< qlonglong(*)>(_a[5]))); break;
        case 66: _t->updateTaskStart((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< qlonglong(*)>(_a[3]))); break;
        case 67: _t->updateTaskEnd((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< qlonglong(*)>(_a[2]))); break;
        case 68: _t->interactiveWidgetUpdated((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 69: _t->interactiveWidgetUpdated((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 70: _t->interactiveWidgetUpdated((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 71)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 71;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::preferencesDialogCreated(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::sessionOpened()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void MainWindow::sessionClosed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void MainWindow::clientLaunched()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void MainWindow::dataEnabled(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MainWindow::dataDisabled(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void MainWindow::paramValueUpdated(int _t1, bool _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void MainWindow::paramValueUpdated(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void MainWindow::paramValueUpdated(int _t1, double _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void MainWindow::debugEnabled(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void MainWindow::debugStepped()
{
    QMetaObject::activate(this, &staticMetaObject, 10, 0);
}

// SIGNAL 11
void MainWindow::debugLocked()
{
    QMetaObject::activate(this, &staticMetaObject, 11, 0);
}
QT_END_MOC_NAMESPACE
