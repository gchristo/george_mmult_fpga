/****************************************************************************
** Meta object code from reading C++ file 'preferencesdialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../starpu-top/preferencesdialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'preferencesdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PreferencesDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x08,
      46,   18,   18,   18, 0x08,
      79,   73,   18,   18, 0x08,
     107,   73,   18,   18, 0x08,
     133,   73,   18,   18, 0x08,
     175,  166,   18,   18, 0x08,
     201,   18,   18,   18, 0x08,
     226,   18,   18,   18, 0x08,
     303,  258,   18,   18, 0x08,
     408,  392,   18,   18, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_PreferencesDialog[] = {
    "PreferencesDialog\0\0displayWidgetPreferences()\0"
    "displaySessionSetupsList()\0index\0"
    "updateDisplayModeIndex(int)\0"
    "updateDataWidgetType(int)\0"
    "updateInteractiveWidgetType(int)\0"
    "fileName\0loadSessionSetup(QString)\0"
    "loadDefaultDataWidgets()\0"
    "loadDefaultInteractiveWidgets()\0"
    "dataDescriptionsSetup,paramDescriptionsSetup\0"
    "sessionDescriptionsSetupLoaded(QList<DataDescriptionSetup>,QList<Param"
    "DescriptionSetup>)\0"
    "mainWindowSetup\0"
    "sessionMainWindowSetupLoaded(MainWindowSetup)\0"
};

void PreferencesDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PreferencesDialog *_t = static_cast<PreferencesDialog *>(_o);
        switch (_id) {
        case 0: _t->displayWidgetPreferences(); break;
        case 1: _t->displaySessionSetupsList(); break;
        case 2: _t->updateDisplayModeIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->updateDataWidgetType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->updateInteractiveWidgetType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->loadSessionSetup((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->loadDefaultDataWidgets(); break;
        case 7: _t->loadDefaultInteractiveWidgets(); break;
        case 8: _t->sessionDescriptionsSetupLoaded((*reinterpret_cast< QList<DataDescriptionSetup>(*)>(_a[1])),(*reinterpret_cast< QList<ParamDescriptionSetup>(*)>(_a[2]))); break;
        case 9: _t->sessionMainWindowSetupLoaded((*reinterpret_cast< MainWindowSetup(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PreferencesDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PreferencesDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_PreferencesDialog,
      qt_meta_data_PreferencesDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PreferencesDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PreferencesDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PreferencesDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PreferencesDialog))
        return static_cast<void*>(const_cast< PreferencesDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int PreferencesDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
