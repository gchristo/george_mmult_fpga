/****************************************************************************
** Meta object code from reading C++ file 'sessionsetupmanager.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../starpu-top/sessionsetupmanager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sessionsetupmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SessionSetupManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      66,   21,   20,   20, 0x05,
     164,  148,   20,   20, 0x05,
     223,  203,   20,   20, 0x05,
     314,  270,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
     418,  401,  396,   20, 0x0a,
     444,  401,  396,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SessionSetupManager[] = {
    "SessionSetupManager\0\0"
    "dataDescriptionsSetup,paramDescriptionsSetup\0"
    "descriptionsSetupLoaded(QList<DataDescriptionSetup>,QList<ParamDescrip"
    "tionSetup>)\0"
    "mainWindowSetup\0mainWindowSetupLoaded(MainWindowSetup)\0"
    "parametersDockSetup\0"
    "parametersDockSetupLoaded(ParametersDockSetup)\0"
    "dataWidgetsSetup,dataAggregatorWidgetsSetup\0"
    "widgetWindowsSetupLoaded(QList<DataWidgetSetup>,QList<DataAggregatorWi"
    "dgetSetup>)\0"
    "bool\0sessionSetupName\0saveSessionSetup(QString)\0"
    "loadSessionSetup(QString)\0"
};

void SessionSetupManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SessionSetupManager *_t = static_cast<SessionSetupManager *>(_o);
        switch (_id) {
        case 0: _t->descriptionsSetupLoaded((*reinterpret_cast< QList<DataDescriptionSetup>(*)>(_a[1])),(*reinterpret_cast< QList<ParamDescriptionSetup>(*)>(_a[2]))); break;
        case 1: _t->mainWindowSetupLoaded((*reinterpret_cast< MainWindowSetup(*)>(_a[1]))); break;
        case 2: _t->parametersDockSetupLoaded((*reinterpret_cast< ParametersDockSetup(*)>(_a[1]))); break;
        case 3: _t->widgetWindowsSetupLoaded((*reinterpret_cast< QList<DataWidgetSetup>(*)>(_a[1])),(*reinterpret_cast< QList<DataAggregatorWidgetSetup>(*)>(_a[2]))); break;
        case 4: { bool _r = _t->saveSessionSetup((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: { bool _r = _t->loadSessionSetup((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SessionSetupManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SessionSetupManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SessionSetupManager,
      qt_meta_data_SessionSetupManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SessionSetupManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SessionSetupManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SessionSetupManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SessionSetupManager))
        return static_cast<void*>(const_cast< SessionSetupManager*>(this));
    return QObject::qt_metacast(_clname);
}

int SessionSetupManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void SessionSetupManager::descriptionsSetupLoaded(QList<DataDescriptionSetup> _t1, QList<ParamDescriptionSetup> _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SessionSetupManager::mainWindowSetupLoaded(MainWindowSetup _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void SessionSetupManager::parametersDockSetupLoaded(ParametersDockSetup _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void SessionSetupManager::widgetWindowsSetupLoaded(QList<DataWidgetSetup> _t1, QList<DataAggregatorWidgetSetup> _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
