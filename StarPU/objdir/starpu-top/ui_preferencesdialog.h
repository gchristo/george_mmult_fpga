/********************************************************************************
** Form generated from reading UI file 'preferencesdialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PREFERENCESDIALOG_H
#define UI_PREFERENCESDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QScrollArea>
#include <QtGui/QSpinBox>
#include <QtGui/QTabWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PreferencesDialog
{
public:
    QGridLayout *gridLayout_6;
    QTabWidget *preferencesTab;
    QWidget *connectionTab;
    QGridLayout *gridLayout_2;
    QGroupBox *connectionServerGroupBox;
    QGridLayout *gridLayout_4;
    QLabel *portLabel;
    QLabel *ipLabel;
    QLineEdit *ipEdit;
    QSpinBox *portEdit;
    QGroupBox *connectionSSHGroupBox;
    QGridLayout *gridLayout_11;
    QLabel *commandLabel;
    QLineEdit *commandEdit;
    QWidget *displayTab;
    QGridLayout *gridLayout_5;
    QGroupBox *displayWidgetsGroupBox;
    QGridLayout *gridLayout_9;
    QScrollArea *dataWidgetsScroll;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_7;
    QGroupBox *dataWidgetsGroupBox;
    QGridLayout *gridLayout;
    QScrollArea *interactiveWidgetsScroll;
    QWidget *scrollAreaWidgetContents_2;
    QGridLayout *gridLayout_8;
    QGroupBox *interactiveWidgetsGroupBox;
    QGridLayout *gridLayout_3;
    QComboBox *loadSessionSetupComboBox;
    QLabel *loadSessionSetupLabel;
    QGroupBox *displayGeneralGroupBox;
    QGridLayout *gridLayout_10;
    QLabel *displayModeLabel;
    QComboBox *displayModeComboBox;
    QLabel *antialiasingLabel;
    QCheckBox *antialiasingCheckBox;

    void setupUi(QDialog *PreferencesDialog)
    {
        if (PreferencesDialog->objectName().isEmpty())
            PreferencesDialog->setObjectName(QString::fromUtf8("PreferencesDialog"));
        PreferencesDialog->resize(387, 490);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PreferencesDialog->sizePolicy().hasHeightForWidth());
        PreferencesDialog->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/preferences.png"), QSize(), QIcon::Normal, QIcon::Off);
        PreferencesDialog->setWindowIcon(icon);
        PreferencesDialog->setModal(true);
        gridLayout_6 = new QGridLayout(PreferencesDialog);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        preferencesTab = new QTabWidget(PreferencesDialog);
        preferencesTab->setObjectName(QString::fromUtf8("preferencesTab"));
        preferencesTab->setMinimumSize(QSize(250, 0));
        connectionTab = new QWidget();
        connectionTab->setObjectName(QString::fromUtf8("connectionTab"));
        gridLayout_2 = new QGridLayout(connectionTab);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        connectionServerGroupBox = new QGroupBox(connectionTab);
        connectionServerGroupBox->setObjectName(QString::fromUtf8("connectionServerGroupBox"));
        gridLayout_4 = new QGridLayout(connectionServerGroupBox);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        portLabel = new QLabel(connectionServerGroupBox);
        portLabel->setObjectName(QString::fromUtf8("portLabel"));

        gridLayout_4->addWidget(portLabel, 2, 1, 1, 1);

        ipLabel = new QLabel(connectionServerGroupBox);
        ipLabel->setObjectName(QString::fromUtf8("ipLabel"));

        gridLayout_4->addWidget(ipLabel, 1, 1, 1, 1);

        ipEdit = new QLineEdit(connectionServerGroupBox);
        ipEdit->setObjectName(QString::fromUtf8("ipEdit"));

        gridLayout_4->addWidget(ipEdit, 1, 2, 1, 1);

        portEdit = new QSpinBox(connectionServerGroupBox);
        portEdit->setObjectName(QString::fromUtf8("portEdit"));
        portEdit->setAccelerated(true);
        portEdit->setMinimum(1024);
        portEdit->setMaximum(65535);
        portEdit->setValue(2011);

        gridLayout_4->addWidget(portEdit, 2, 2, 1, 1);


        gridLayout_2->addWidget(connectionServerGroupBox, 0, 0, 1, 1);

        connectionSSHGroupBox = new QGroupBox(connectionTab);
        connectionSSHGroupBox->setObjectName(QString::fromUtf8("connectionSSHGroupBox"));
        connectionSSHGroupBox->setCheckable(true);
        connectionSSHGroupBox->setChecked(false);
        gridLayout_11 = new QGridLayout(connectionSSHGroupBox);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        commandLabel = new QLabel(connectionSSHGroupBox);
        commandLabel->setObjectName(QString::fromUtf8("commandLabel"));

        gridLayout_11->addWidget(commandLabel, 0, 0, 1, 1);

        commandEdit = new QLineEdit(connectionSSHGroupBox);
        commandEdit->setObjectName(QString::fromUtf8("commandEdit"));

        gridLayout_11->addWidget(commandEdit, 0, 1, 1, 1);


        gridLayout_2->addWidget(connectionSSHGroupBox, 1, 0, 1, 1);

        preferencesTab->addTab(connectionTab, QString());
        displayTab = new QWidget();
        displayTab->setObjectName(QString::fromUtf8("displayTab"));
        gridLayout_5 = new QGridLayout(displayTab);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        displayWidgetsGroupBox = new QGroupBox(displayTab);
        displayWidgetsGroupBox->setObjectName(QString::fromUtf8("displayWidgetsGroupBox"));
        gridLayout_9 = new QGridLayout(displayWidgetsGroupBox);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        dataWidgetsScroll = new QScrollArea(displayWidgetsGroupBox);
        dataWidgetsScroll->setObjectName(QString::fromUtf8("dataWidgetsScroll"));
        dataWidgetsScroll->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 100, 51));
        gridLayout_7 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        dataWidgetsGroupBox = new QGroupBox(scrollAreaWidgetContents);
        dataWidgetsGroupBox->setObjectName(QString::fromUtf8("dataWidgetsGroupBox"));
        dataWidgetsGroupBox->setEnabled(true);
        sizePolicy.setHeightForWidth(dataWidgetsGroupBox->sizePolicy().hasHeightForWidth());
        dataWidgetsGroupBox->setSizePolicy(sizePolicy);
        dataWidgetsGroupBox->setMaximumSize(QSize(16777215, 16777215));
        dataWidgetsGroupBox->setFlat(false);
        dataWidgetsGroupBox->setCheckable(false);
        gridLayout = new QGridLayout(dataWidgetsGroupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));

        gridLayout_7->addWidget(dataWidgetsGroupBox, 1, 0, 1, 1);

        dataWidgetsScroll->setWidget(scrollAreaWidgetContents);

        gridLayout_9->addWidget(dataWidgetsScroll, 4, 0, 1, 2);

        interactiveWidgetsScroll = new QScrollArea(displayWidgetsGroupBox);
        interactiveWidgetsScroll->setObjectName(QString::fromUtf8("interactiveWidgetsScroll"));
        interactiveWidgetsScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 130, 51));
        gridLayout_8 = new QGridLayout(scrollAreaWidgetContents_2);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        interactiveWidgetsGroupBox = new QGroupBox(scrollAreaWidgetContents_2);
        interactiveWidgetsGroupBox->setObjectName(QString::fromUtf8("interactiveWidgetsGroupBox"));
        gridLayout_3 = new QGridLayout(interactiveWidgetsGroupBox);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));

        gridLayout_8->addWidget(interactiveWidgetsGroupBox, 0, 0, 1, 1);

        interactiveWidgetsScroll->setWidget(scrollAreaWidgetContents_2);

        gridLayout_9->addWidget(interactiveWidgetsScroll, 5, 0, 1, 2);

        loadSessionSetupComboBox = new QComboBox(displayWidgetsGroupBox);
        loadSessionSetupComboBox->setObjectName(QString::fromUtf8("loadSessionSetupComboBox"));

        gridLayout_9->addWidget(loadSessionSetupComboBox, 1, 1, 1, 1);

        loadSessionSetupLabel = new QLabel(displayWidgetsGroupBox);
        loadSessionSetupLabel->setObjectName(QString::fromUtf8("loadSessionSetupLabel"));

        gridLayout_9->addWidget(loadSessionSetupLabel, 1, 0, 1, 1);


        gridLayout_5->addWidget(displayWidgetsGroupBox, 2, 0, 1, 2);

        displayGeneralGroupBox = new QGroupBox(displayTab);
        displayGeneralGroupBox->setObjectName(QString::fromUtf8("displayGeneralGroupBox"));
        gridLayout_10 = new QGridLayout(displayGeneralGroupBox);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        displayModeLabel = new QLabel(displayGeneralGroupBox);
        displayModeLabel->setObjectName(QString::fromUtf8("displayModeLabel"));

        gridLayout_10->addWidget(displayModeLabel, 0, 0, 1, 1);

        displayModeComboBox = new QComboBox(displayGeneralGroupBox);
        displayModeComboBox->setObjectName(QString::fromUtf8("displayModeComboBox"));

        gridLayout_10->addWidget(displayModeComboBox, 0, 1, 1, 1);

        antialiasingLabel = new QLabel(displayGeneralGroupBox);
        antialiasingLabel->setObjectName(QString::fromUtf8("antialiasingLabel"));

        gridLayout_10->addWidget(antialiasingLabel, 1, 0, 1, 1);

        antialiasingCheckBox = new QCheckBox(displayGeneralGroupBox);
        antialiasingCheckBox->setObjectName(QString::fromUtf8("antialiasingCheckBox"));
        antialiasingCheckBox->setChecked(true);

        gridLayout_10->addWidget(antialiasingCheckBox, 1, 1, 1, 1);


        gridLayout_5->addWidget(displayGeneralGroupBox, 0, 0, 1, 2);

        preferencesTab->addTab(displayTab, QString());

        gridLayout_6->addWidget(preferencesTab, 0, 0, 1, 1);


        retranslateUi(PreferencesDialog);

        preferencesTab->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(PreferencesDialog);
    } // setupUi

    void retranslateUi(QDialog *PreferencesDialog)
    {
        PreferencesDialog->setWindowTitle(QApplication::translate("PreferencesDialog", "Preferences", 0, QApplication::UnicodeUTF8));
        connectionServerGroupBox->setTitle(QApplication::translate("PreferencesDialog", "Server settings", 0, QApplication::UnicodeUTF8));
        portLabel->setText(QApplication::translate("PreferencesDialog", "Port", 0, QApplication::UnicodeUTF8));
        ipLabel->setText(QApplication::translate("PreferencesDialog", "IP Address", 0, QApplication::UnicodeUTF8));
        ipEdit->setText(QApplication::translate("PreferencesDialog", "127.0.0.1", 0, QApplication::UnicodeUTF8));
        connectionSSHGroupBox->setTitle(QApplication::translate("PreferencesDialog", "SSH", 0, QApplication::UnicodeUTF8));
        commandLabel->setText(QApplication::translate("PreferencesDialog", "Command Line", 0, QApplication::UnicodeUTF8));
        commandEdit->setText(QApplication::translate("PreferencesDialog", "ssh -L", 0, QApplication::UnicodeUTF8));
        preferencesTab->setTabText(preferencesTab->indexOf(connectionTab), QApplication::translate("PreferencesDialog", "Connection", 0, QApplication::UnicodeUTF8));
        displayWidgetsGroupBox->setTitle(QApplication::translate("PreferencesDialog", "Widgets", 0, QApplication::UnicodeUTF8));
        dataWidgetsGroupBox->setTitle(QApplication::translate("PreferencesDialog", "Data widgets", 0, QApplication::UnicodeUTF8));
        interactiveWidgetsGroupBox->setTitle(QApplication::translate("PreferencesDialog", "Interactive widgets", 0, QApplication::UnicodeUTF8));
        loadSessionSetupLabel->setText(QApplication::translate("PreferencesDialog", "Load a session setup", 0, QApplication::UnicodeUTF8));
        displayGeneralGroupBox->setTitle(QApplication::translate("PreferencesDialog", "General", 0, QApplication::UnicodeUTF8));
        displayModeLabel->setText(QApplication::translate("PreferencesDialog", "Display mode", 0, QApplication::UnicodeUTF8));
        antialiasingLabel->setText(QApplication::translate("PreferencesDialog", "Enable antialiasing", 0, QApplication::UnicodeUTF8));
        antialiasingCheckBox->setText(QString());
        preferencesTab->setTabText(preferencesTab->indexOf(displayTab), QApplication::translate("PreferencesDialog", "Display", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class PreferencesDialog: public Ui_PreferencesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PREFERENCESDIALOG_H
