/****************************************************************************
** Meta object code from reading C++ file 'datawidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../starpu-top/datawidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'datawidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DataWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      25,   12,   11,   11, 0x05,
      48,   12,   11,   11, 0x05,
      70,   12,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
     101,   95,   11,   11, 0x0a,
     116,   95,   11,   11, 0x0a,
     130,   95,   11,   11, 0x0a,
     147,   11,   11,   11, 0x0a,
     172,   11,   11,   11, 0x08,
     195,   11,   11,   11, 0x08,
     225,  215,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DataWidget[] = {
    "DataWidget\0\0value,dataId\0"
    "valueChanged(bool,int)\0valueChanged(int,int)\0"
    "valueChanged(double,int)\0value\0"
    "setValue(bool)\0setValue(int)\0"
    "setValue(double)\0recreateInternalWidget()\0"
    "createInternalWidget()\0widgetTypeChanged()\0"
    "newWidget\0updateAction(DataWidgetType)\0"
};

void DataWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DataWidget *_t = static_cast<DataWidget *>(_o);
        switch (_id) {
        case 0: _t->valueChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->valueChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->valueChanged((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->setValue((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setValue((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->setValue((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->recreateInternalWidget(); break;
        case 7: _t->createInternalWidget(); break;
        case 8: _t->widgetTypeChanged(); break;
        case 9: _t->updateAction((*reinterpret_cast< DataWidgetType(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DataWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DataWidget::staticMetaObject = {
    { &AbstractWidgetWindow::staticMetaObject, qt_meta_stringdata_DataWidget,
      qt_meta_data_DataWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DataWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DataWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DataWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DataWidget))
        return static_cast<void*>(const_cast< DataWidget*>(this));
    return AbstractWidgetWindow::qt_metacast(_clname);
}

int DataWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractWidgetWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void DataWidget::valueChanged(bool _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DataWidget::valueChanged(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void DataWidget::valueChanged(double _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
