/****************************************************************************
** Meta object code from reading C++ file 'interactivewidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../starpu-top/interactivewidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'interactivewidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_InteractiveWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   19,   18,   18, 0x05,
      56,   19,   18,   18, 0x05,
      83,   19,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
     119,  113,   18,   18, 0x0a,
     134,  113,   18,   18, 0x0a,
     148,  113,   18,   18, 0x0a,
     165,   18,   18,   18, 0x0a,
     190,   18,   18,   18, 0x08,
     211,   18,   18,   18, 0x08,
     231,   18,   18,   18, 0x08,
     250,   18,   18,   18, 0x08,
     269,  113,   18,   18, 0x08,
     294,  113,   18,   18, 0x08,
     318,  113,   18,   18, 0x08,
     345,   18,   18,   18, 0x08,
     375,  365,   18,   18, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_InteractiveWidget[] = {
    "InteractiveWidget\0\0id,value\0"
    "paramValueChanged(int,bool)\0"
    "paramValueChanged(int,int)\0"
    "paramValueChanged(int,double)\0value\0"
    "setValue(bool)\0setValue(int)\0"
    "setValue(double)\0recreateInternalWidget()\0"
    "sliderValueChanged()\0wheelValueChanged()\0"
    "knobValueChanged()\0dialValueChanged()\0"
    "notifyValueChanged(bool)\0"
    "notifyValueChanged(int)\0"
    "notifyValueChanged(double)\0"
    "widgetTypeChanged()\0newWidget\0"
    "updateAction(InteractiveWidgetType)\0"
};

void InteractiveWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        InteractiveWidget *_t = static_cast<InteractiveWidget *>(_o);
        switch (_id) {
        case 0: _t->paramValueChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 1: _t->paramValueChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->paramValueChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 3: _t->setValue((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setValue((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->setValue((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->recreateInternalWidget(); break;
        case 7: _t->sliderValueChanged(); break;
        case 8: _t->wheelValueChanged(); break;
        case 9: _t->knobValueChanged(); break;
        case 10: _t->dialValueChanged(); break;
        case 11: _t->notifyValueChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->notifyValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->notifyValueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->widgetTypeChanged(); break;
        case 15: _t->updateAction((*reinterpret_cast< InteractiveWidgetType(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData InteractiveWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject InteractiveWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_InteractiveWidget,
      qt_meta_data_InteractiveWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &InteractiveWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *InteractiveWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *InteractiveWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_InteractiveWidget))
        return static_cast<void*>(const_cast< InteractiveWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int InteractiveWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void InteractiveWidget::paramValueChanged(int _t1, bool _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void InteractiveWidget::paramValueChanged(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void InteractiveWidget::paramValueChanged(int _t1, double _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
