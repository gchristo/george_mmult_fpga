  \hypertarget{index_Motivation}{}\section{Motivation}\label{index_Motivation}
The use of specialized hardware such as accelerators or coprocessors offers an interesting approach to overcome the physical limits encountered by processor architects. As a result, many machines are now equipped with one or several accelerators (e.\+g. a G\+PU), in addition to the usual processor(s). While a lot of efforts have been devoted to offload computation onto such accelerators, very little attention as been paid to portability concerns on the one hand, and to the possibility of having heterogeneous accelerators and processors to interact on the other hand.

Star\+PU is a runtime system that offers support for heterogeneous multicore architectures, it not only offers a unified view of the computational resources (i.\+e. C\+P\+Us and accelerators at the same time), but it also takes care of efficiently mapping and executing tasks onto an heterogeneous machine while transparently handling low-\/level issues such as data transfers in a portable fashion.\hypertarget{index_StarPUInANutshell}{}\section{Star\+P\+U in a Nutshell}\label{index_StarPUInANutshell}
Star\+PU is a software tool aiming to allow programmers to exploit the computing power of the available C\+P\+Us and G\+P\+Us, while relieving them from the need to specially adapt their programs to the target machine and processing units.

At the core of Star\+PU is its run-\/time support library, which is responsible for scheduling application-\/provided tasks on heterogeneous C\+P\+U/\+G\+PU machines. In addition, Star\+PU comes with programming language support, in the form of extensions to languages of the C family (\hyperlink{cExtensions}{C Extensions}), as well as an Open\+CL front-\/end (\hyperlink{SOCLOpenclExtensions}{S\+O\+CL Open\+CL Extensions}).

Star\+PU\textquotesingle{}s run-\/time and programming language extensions support a task-\/based programming model. Applications submit computational tasks, with C\+PU and/or G\+PU implementations, and Star\+PU schedules these tasks and associated data transfers on available C\+P\+Us and G\+P\+Us. The data that a task manipulates are automatically transferred among accelerators and the main memory, so that programmers are freed from the scheduling issues and technical details associated with these transfers.

Star\+PU takes particular care of scheduling tasks efficiently, using well-\/known algorithms from the literature (\hyperlink{Scheduling_TaskSchedulingPolicy}{Task Scheduling Policy}). In addition, it allows scheduling experts, such as compiler or computational library developers, to implement custom scheduling policies in a portable fashion (\hyperlink{Scheduling_DefiningANewSchedulingPolicy}{Defining A New Scheduling Policy}).

The remainder of this section describes the main concepts used in Star\+PU.\hypertarget{index_CodeletAndTasks}{}\subsection{Codelet and Tasks}\label{index_CodeletAndTasks}
One of the Star\+PU primary data structures is the {\bfseries codelet}. A codelet describes a computational kernel that can possibly be implemented on multiple architectures such as a C\+PU, a C\+U\+DA device or an Open\+CL device.

Another important data structure is the {\bfseries task}. Executing a Star\+PU task consists in applying a codelet on a data set, on one of the architectures on which the codelet is implemented. A task thus describes the codelet that it uses, but also which data are accessed, and how they are accessed during the computation (read and/or write). Star\+PU tasks are asynchronous\+: submitting a task to Star\+PU is a non-\/blocking operation. The task structure can also specify a {\bfseries callback} function that is called once Star\+PU has properly executed the task. It also contains optional fields that the application may use to give hints to the scheduler (such as priority levels).

By default, task dependencies are inferred from data dependency (sequential coherency) by Star\+PU. The application can however disable sequential coherency for some data, and dependencies can be specifically expressed. A task may be identified by a unique 64-\/bit number chosen by the application which we refer as a {\bfseries tag}. Task dependencies can be enforced either by the means of callback functions, by submitting other tasks, or by expressing dependencies between tags (which can thus correspond to tasks that have not yet been submitted).\hypertarget{index_StarPUDataManagementLibrary}{}\subsection{Star\+P\+U Data Management Library}\label{index_StarPUDataManagementLibrary}
Because Star\+PU schedules tasks at runtime, data transfers have to be done automatically and ``just-\/in-\/time\textquotesingle{}\textquotesingle{} between processing units, relieving application programmers from explicit data transfers. Moreover, to avoid unnecessary transfers, Star\+PU keeps data where it was last needed, even if was modified there, and it allows multiple copies of the same data to reside at the same time on several processing units as long as it is not modified.\hypertarget{index_ApplicationTaskification}{}\section{Application Taskification}\label{index_ApplicationTaskification}
T\+O\+DO\hypertarget{index_Glossary}{}\section{Glossary}\label{index_Glossary}
A {\bfseries codelet} records pointers to various implementations of the same theoretical function.

A {\bfseries memory node} can be either the main R\+AM, G\+P\+U-\/embedded memory or a disk memory.

A {\bfseries bus} is a link between memory nodes.

A {\bfseries data handle} keeps track of replicates of the same data ({\bfseries registered} by the application) over various memory nodes. The data management library manages to keep them coherent.

The {\bfseries home} memory node of a data handle is the memory node from which the data was registered (usually the main memory node).

A {\bfseries task} represents a scheduled execution of a codelet on some data handles.

A {\bfseries tag} is a rendez-\/vous point. Tasks typically have their own tag, and can depend on other tags. The value is chosen by the application.

A {\bfseries worker} execute tasks. There is typically one per C\+PU computation core and one per accelerator (for which a whole C\+PU core is dedicated).

A {\bfseries driver} drives a given kind of workers. There are currently C\+PU, C\+U\+DA, and Open\+CL drivers. They usually start several workers to actually drive them.

A {\bfseries performance model} is a (dynamic or static) model of the performance of a given codelet. Codelets can have execution time performance model as well as energy consumption performance models.

A data {\bfseries interface} describes the layout of the data\+: for a vector, a pointer for the start, the number of elements and the size of elements ; for a matrix, a pointer for the start, the number of elements per row, the offset between rows, and the size of each element ; etc. To access their data, codelet functions are given interfaces for the local memory node replicates of the data handles of the scheduled task.

{\bfseries Partitioning} data means dividing the data of a given data handle (called {\bfseries father}) into a series of {\bfseries children} data handles which designate various portions of the former.

A {\bfseries filter} is the function which computes children data handles from a father data handle, and thus describes how the partitioning should be done (horizontal, vertical, etc.)

{\bfseries Acquiring} a data handle can be done from the main application, to safely access the data of a data handle from its home node, without having to unregister it.\hypertarget{index_ResearchPapers}{}\section{Research Papers}\label{index_ResearchPapers}
Research papers about Star\+PU can be found at \href{http://runtime.bordeaux.inria.fr/Publis/Keyword/STARPU.html}{\tt http\+://runtime.\+bordeaux.\+inria.\+fr/\+Publis/\+Keyword/\+S\+T\+A\+R\+P\+U.\+html}.

A good overview is available in the research report at \href{http://hal.archives-ouvertes.fr/inria-00467677}{\tt http\+://hal.\+archives-\/ouvertes.\+fr/inria-\/00467677}.\hypertarget{index_StarPUApplications}{}\section{Star\+P\+U Applications}\label{index_StarPUApplications}
You can first have a look at the chapters \hyperlink{BasicExamples}{Basic Examples} and \hyperlink{AdvancedExamples}{Advanced Examples}. A tutorial is also installed in the directory {\ttfamily share/doc/starpu/tutorial/}.

Many examples are also available in the Star\+PU sources in the directory {\ttfamily examples/}. Simple examples include\+:


\begin{DoxyDescription}
\item[{\ttfamily incrementer/}  ]Trivial incrementation test.  
\item[{\ttfamily basic\+\_\+examples/}  ]Simple documented Hello world and vector/scalar product (as shown in \hyperlink{BasicExamples}{Basic Examples}), matrix product examples (as shown in \hyperlink{OnlinePerformanceTools_PerformanceModelExample}{Performance Model Example}), an example using the blocked matrix data interface, an example using the variable data interface, and an example using different formats on C\+P\+Us and G\+P\+Us.  
\item[{\ttfamily matvecmult/} ]Open\+CL example from N\+Vidia, adapted to Star\+PU.  
\item[{\ttfamily axpy/} ]A\+X\+PY C\+U\+B\+L\+AS operation adapted to Star\+PU.  
\item[{\ttfamily fortran/}  ]Example of Fortran bindings.  
\end{DoxyDescription}

More advanced examples include\+:


\begin{DoxyDescription}
\item[{\ttfamily filters/} ]Examples using filters, as shown in \hyperlink{DataManagement_PartitioningData}{Partitioning Data}.  
\item[{\ttfamily lu/} ]LU matrix factorization, see for instance {\ttfamily xlu\+\_\+implicit.\+c}  
\item[{\ttfamily cholesky/} ]Cholesky matrix factorization, see for instance {\ttfamily cholesky\+\_\+implicit.\+c}.  
\end{DoxyDescription}\hypertarget{index_FurtherReading}{}\section{Further Reading}\label{index_FurtherReading}
The documentation chapters include


\begin{DoxyItemize}
\item Part 1\+: Star\+PU Basics 
\begin{DoxyItemize}
\item \hyperlink{BuildingAndInstallingStarPU}{Building and Installing Star\+PU} 
\item \hyperlink{BasicExamples}{Basic Examples} 
\end{DoxyItemize}
\item Part 2\+: Star\+PU Quick Programming Guide 
\begin{DoxyItemize}
\item \hyperlink{AdvancedExamples}{Advanced Examples} 
\item \hyperlink{CheckListWhenPerformanceAreNotThere}{Check List When Performance Are Not There} 
\end{DoxyItemize}
\item Part 3\+: Star\+PU Inside 
\begin{DoxyItemize}
\item \hyperlink{TasksInStarPU}{Tasks In Star\+PU} 
\item \hyperlink{DataManagement_DataManagement}{Data Management} 
\item \hyperlink{Scheduling}{Scheduling} 
\item \hyperlink{SchedulingContexts}{Scheduling Contexts} 
\item \hyperlink{SchedulingContextHypervisor}{Scheduling Context Hypervisor} 
\item \hyperlink{DebuggingTools}{Debugging Tools} 
\item \hyperlink{OnlinePerformanceTools}{Online Performance Tools} 
\item \hyperlink{OfflinePerformanceTools}{Offline Performance Tools} 
\item \hyperlink{FrequentlyAskedQuestions}{Frequently Asked Questions} 
\end{DoxyItemize}
\item Part 4\+: Star\+PU Extensions 
\begin{DoxyItemize}
\item \hyperlink{OutOfCore}{Out Of Core} 
\item \hyperlink{MPISupport}{M\+PI Support} 
\item \hyperlink{FFTSupport}{F\+FT Support} 
\item \hyperlink{MICSCCSupport}{M\+IC Xeon Phi / S\+CC Support} 
\item \hyperlink{cExtensions}{C Extensions} 
\item \hyperlink{SOCLOpenclExtensions}{S\+O\+CL Open\+CL Extensions} 
\item \hyperlink{SimGridSupport}{Sim\+Grid Support} 
\end{DoxyItemize}
\item Part 5\+: Star\+PU Reference A\+PI 
\begin{DoxyItemize}
\item \hyperlink{ExecutionConfigurationThroughEnvironmentVariables}{Execution Configuration Through Environment Variables} 
\item \hyperlink{CompilationConfiguration}{Compilation Configuration} 
\item Module\+Documentation 
\item File\+Documentation 
\item \hyperlink{deprecated}{Deprecated List} 
\end{DoxyItemize}
\item Part\+: Appendix 
\begin{DoxyItemize}
\item \hyperlink{FullSourceCodeVectorScal}{Full source code for the ’\+Scaling a Vector’ example} 
\item \hyperlink{GNUFreeDocumentationLicense}{The G\+NU Free Documentation License} 
\end{DoxyItemize}
\end{DoxyItemize}

Make sure to have had a look at those too! 