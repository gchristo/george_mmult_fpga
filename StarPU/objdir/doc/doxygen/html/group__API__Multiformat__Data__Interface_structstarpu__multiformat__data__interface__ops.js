var group__API__Multiformat__Data__Interface_structstarpu__multiformat__data__interface__ops =
[
    [ "cpu_elemsize", "group__API__Multiformat__Data__Interface.html#a67a1f3202dfc2512ed7ed14bed4f3623", null ],
    [ "opencl_elemsize", "group__API__Multiformat__Data__Interface.html#a7c0e09ea6559bef4579a1fb1e57f67d2", null ],
    [ "cpu_to_opencl_cl", "group__API__Multiformat__Data__Interface.html#af77c2819850101b0ae361c7d2ce5c6fa", null ],
    [ "opencl_to_cpu_cl", "group__API__Multiformat__Data__Interface.html#a7e499a277c5d18b2c0d614af5e80212e", null ],
    [ "cuda_elemsize", "group__API__Multiformat__Data__Interface.html#a00927819b3899892ec152968efe0b158", null ],
    [ "cpu_to_cuda_cl", "group__API__Multiformat__Data__Interface.html#a5214fba07c50c3c31682f8442ace9030", null ],
    [ "cuda_to_cpu_cl", "group__API__Multiformat__Data__Interface.html#ad40fb377f8bc7ae07a8bb710e9becfda", null ],
    [ "mic_elemsize", "group__API__Multiformat__Data__Interface.html#a3788fdc38fb2941ca16761d33e96c022", null ],
    [ "cpu_to_mic_cl", "group__API__Multiformat__Data__Interface.html#add5d3461daef2bd585cefacf7cabe3e7", null ],
    [ "mic_to_cpu_cl", "group__API__Multiformat__Data__Interface.html#abd294b29880ba6f4a363a3ff48a2d332", null ]
];