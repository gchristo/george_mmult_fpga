var group__API__Performance__Model_structstarpu__perfmodel__regression__model =
[
    [ "sumlny", "group__API__Performance__Model.html#a77f3000cf6f0dff4ac86b6664cc386ea", null ],
    [ "sumlnx", "group__API__Performance__Model.html#acce4202682709ec0e1f0f22ecc55d1d0", null ],
    [ "sumlnx2", "group__API__Performance__Model.html#a4f23ac95d204cc38c51c3e0e5552ef62", null ],
    [ "minx", "group__API__Performance__Model.html#a6e3fa8bbf9c3b3e09d248d1d9abd62ca", null ],
    [ "maxx", "group__API__Performance__Model.html#ad691b4ef9fc2367e13aba7d578832763", null ],
    [ "sumlnxlny", "group__API__Performance__Model.html#a8ddcc5e8aeaf6dd2459e883fab483409", null ],
    [ "alpha", "group__API__Performance__Model.html#afb6b1e89bf7820131bddf970bb0df58b", null ],
    [ "beta", "group__API__Performance__Model.html#aa69cc173b9ebdbf129eb1b30ca64ddd9", null ],
    [ "valid", "group__API__Performance__Model.html#a1e6c12b85fc84336010f580874ec494a", null ],
    [ "a", "group__API__Performance__Model.html#ab718a53c300f99766bdc42ca1909b49b", null ],
    [ "b", "group__API__Performance__Model.html#af5497d7f073aa115e25d6d1a32c2e668", null ],
    [ "c", "group__API__Performance__Model.html#ac651825cdcd22fb9e6389d73c1b4f275", null ],
    [ "nl_valid", "group__API__Performance__Model.html#a8c4568a5722baebd67148642ee3377e1", null ],
    [ "nsample", "group__API__Performance__Model.html#ad141b7ae874d40dabeea131474ba29c0", null ]
];