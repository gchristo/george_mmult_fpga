var group__API__Performance__Model_structstarpu__perfmodel__history__entry =
[
    [ "mean", "group__API__Performance__Model.html#a8ae18db359c517f5a60c4aff98f12235", null ],
    [ "deviation", "group__API__Performance__Model.html#a2a0fd05b4b1256f854f8c8e0e3d708c0", null ],
    [ "sum", "group__API__Performance__Model.html#ab1a7822099d984503f1c67d45ab6f4b8", null ],
    [ "sum2", "group__API__Performance__Model.html#a0d9823a671de1b10de6c2bb4ced92163", null ],
    [ "nsample", "group__API__Performance__Model.html#a9e662c85f8bb1e134d4885c77ba8a32f", null ],
    [ "nerror", "group__API__Performance__Model.html#ac8204083a8e9e6c70a7530dc1797563b", null ],
    [ "footprint", "group__API__Performance__Model.html#adb5e09576697a74975467483d3c080a2", null ],
    [ "size", "group__API__Performance__Model.html#a247765c3a84e99b1a0906e26f63151d3", null ],
    [ "flops", "group__API__Performance__Model.html#af39728c1095f645bf195430903532a36", null ]
];