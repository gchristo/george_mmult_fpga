var group__API__Performance__Model_structstarpu__perfmodel__per__arch =
[
    [ "cost_function", "group__API__Performance__Model.html#a13f34a1532294c101254beb106f124f3", null ],
    [ "size_base", "group__API__Performance__Model.html#aa82fae955253d333733d34fe4a44e298", null ],
    [ "history", "group__API__Performance__Model.html#a27f17b0a2f067fc3f2cbd79ddaab7513", null ],
    [ "list", "group__API__Performance__Model.html#a96e7105b1a578d11be0b206a1708a133", null ],
    [ "regression", "group__API__Performance__Model.html#a1b50329c04b9326c72130f83c91c1c28", null ],
    [ "debug_path", "group__API__Performance__Model.html#ab927550449b0de134469c3050b2d570d", null ]
];