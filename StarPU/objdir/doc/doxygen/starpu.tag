<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>sc_hypervisor.h</name>
    <path>/home/gchr/workspace/StarPU/sc_hypervisor/include/</path>
    <filename>sc__hypervisor_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <includes id="starpu__sched__ctx__hypervisor_8h" name="starpu_sched_ctx_hypervisor.h" local="no" imported="no">starpu_sched_ctx_hypervisor.h</includes>
    <includes id="sc__hypervisor__config_8h" name="sc_hypervisor_config.h" local="no" imported="no">sc_hypervisor_config.h</includes>
    <includes id="sc__hypervisor__monitoring_8h" name="sc_hypervisor_monitoring.h" local="no" imported="no">sc_hypervisor_monitoring.h</includes>
    <class kind="struct">sc_hypervisor_policy</class>
    <member kind="function">
      <type>void *</type>
      <name>sc_hypervisor_init</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga15d008933c3c89906f613b3a24fff3cc</anchor>
      <arglist>(struct sc_hypervisor_policy *policy)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_shutdown</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga07b07d1fe5d41055f451fc92c60361e1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_register_ctx</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga4c83e2984867267ea8c7da9fbaf23858</anchor>
      <arglist>(unsigned sched_ctx, double total_flops)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_unregister_ctx</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga0a20609c85fa479c3fd5892399099416</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_post_resize_request</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga5f2b33a41a697759ee2a222e623f723c</anchor>
      <arglist>(unsigned sched_ctx, int task_tag)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_resize_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga67d1dbb2ab01c8b712a9cd270fba4155</anchor>
      <arglist>(unsigned *sched_ctxs, int nsched_ctxs, int *workers, int nworkers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_stop_resize</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gaa1e2878a06847489a4de2f33f62fa1af</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_start_resize</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gae3ad958a1d841587df13c814ce966477</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>sc_hypervisor_get_policy</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga57103c5a62477b730b8d6d9a8f3514c7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_add_workers_to_sched_ctx</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga886533650f0dbbef618c7fdfa75f9f0e</anchor>
      <arglist>(int *workers_to_add, unsigned nworkers_to_add, unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_remove_workers_from_sched_ctx</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga37713b9ab6d282f2b705240e630f1a4c</anchor>
      <arglist>(int *workers_to_remove, unsigned nworkers_to_remove, unsigned sched_ctx, unsigned now)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_move_workers</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gaccfcc47b137572f79916bff5d12414d0</anchor>
      <arglist>(unsigned sender_sched_ctx, unsigned receiver_sched_ctx, int *workers_to_move, unsigned nworkers_to_move, unsigned now)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_size_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gadd6bc55408b1f618edb44da49300bb5c</anchor>
      <arglist>(unsigned *sched_ctxs, int nsched_ctxs, int *workers, int nworkers)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>sc_hypervisor_get_size_req</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga1366fc9bb28170e9012dcad441696e52</anchor>
      <arglist>(unsigned **sched_ctxs, int *nsched_ctxs, int **workers, int *nworkers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_save_size_req</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga960ae31e317c65bfa9cc6a4e539a0c84</anchor>
      <arglist>(unsigned *sched_ctxs, int nsched_ctxs, int *workers, int nworkers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_free_size_req</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga5f34faa17f98f25bd874950ba1a15578</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>sc_hypervisor_can_resize</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>gadf5585c656c7db3ee8f7b630b86e25e3</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_set_type_of_task</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga6398be50d801f0fc5ad73506e392d476</anchor>
      <arglist>(struct starpu_codelet *cl, unsigned sched_ctx, uint32_t footprint, size_t data_size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_update_diff_total_flops</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga4dfbeda855bb8963c5332fdd796c0dcf</anchor>
      <arglist>(unsigned sched_ctx, double diff_total_flops)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_update_diff_elapsed_flops</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gac89f5d5eba3f79151ef6b2a0e895f144</anchor>
      <arglist>(unsigned sched_ctx, double diff_task_flops)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>sc_hypervisor_config.h</name>
    <path>/home/gchr/workspace/StarPU/sc_hypervisor/include/</path>
    <filename>sc__hypervisor__config_8h</filename>
    <includes id="sc__hypervisor_8h" name="sc_hypervisor.h" local="no" imported="no">sc_hypervisor.h</includes>
    <class kind="struct">sc_hypervisor_policy_config</class>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_MAX_IDLE</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga70c120158d02e550632de0ec5a207df4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_PRIORITY</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gad9267ab01a8e5db5b357d8749f9e8e1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_MIN_WORKERS</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga6085b1585a13c94d48f185687c6a117c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_MAX_WORKERS</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gaafac498199c6441e2d45c9b4eac8dfe4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_GRANULARITY</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gaf1f5a13894874d87363c32351f573c86</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_FIXED_WORKERS</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga94eea335704bbfa52bc025ad4d49eb14</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_MIN_TASKS</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga04164c5a91df51882e8bdd84fd1b1e3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_NEW_WORKERS_MAX_IDLE</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga4ab22667e3bd2e963f7451552f716540</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_TIME_TO_APPLY</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gac3e457f9b5741f587d241d62cc843f9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_NULL</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gabf2475a79863da74418a12b2c5c37871</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_ISPEED_W_SAMPLE</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gac3b55f616c270eb90c0103d6f062d384</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_ISPEED_CTX_SAMPLE</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga7ba4c0c3deca0dd4148ddd1f0b919792</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_set_config</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga838703a5d26ea31f7415155ecbf1e490</anchor>
      <arglist>(unsigned sched_ctx, void *config)</arglist>
    </member>
    <member kind="function">
      <type>struct sc_hypervisor_policy_config *</type>
      <name>sc_hypervisor_get_config</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>gaf3e884b4995e3dd608120e7fe0e48fae</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_ctl</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga0a4b27500994ca2c56047a7da18c6216</anchor>
      <arglist>(unsigned sched_ctx,...)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>sc_hypervisor_lp.h</name>
    <path>/home/gchr/workspace/StarPU/sc_hypervisor/include/</path>
    <filename>sc__hypervisor__lp_8h</filename>
    <includes id="sc__hypervisor_8h" name="sc_hypervisor.h" local="no" imported="no">sc_hypervisor.h</includes>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
  </compound>
  <compound kind="file">
    <name>sc_hypervisor_monitoring.h</name>
    <path>/home/gchr/workspace/StarPU/sc_hypervisor/include/</path>
    <filename>sc__hypervisor__monitoring_8h</filename>
    <includes id="sc__hypervisor_8h" name="sc_hypervisor.h" local="no" imported="no">sc_hypervisor.h</includes>
    <class kind="struct">sc_hypervisor_resize_ack</class>
    <class kind="struct">sc_hypervisor_wrapper</class>
    <member kind="function">
      <type>struct sc_hypervisor_wrapper *</type>
      <name>sc_hypervisor_get_wrapper</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>gab02cf89fff4e322e47bc96fb3b60b1ac</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>unsigned *</type>
      <name>sc_hypervisor_get_sched_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga5450aa9edc6b19accef60539bbbd2e08</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>sc_hypervisor_get_nsched_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga7136151247abc7aab59ec8e0fe6e5224</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>sc_hypervisor_get_elapsed_flops_per_sched_ctx</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga32444b31927fbc49dc721ecbb66eae28</anchor>
      <arglist>(struct sc_hypervisor_wrapper *sc_w)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>sc_hypervisor_policy.h</name>
    <path>/home/gchr/workspace/StarPU/sc_hypervisor/include/</path>
    <filename>sc__hypervisor__policy_8h</filename>
    <includes id="sc__hypervisor_8h" name="sc_hypervisor.h" local="no" imported="no">sc_hypervisor.h</includes>
    <class kind="struct">types_of_workers</class>
    <class kind="struct">sc_hypervisor_policy_task_pool</class>
  </compound>
  <compound kind="file">
    <name>starpu.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <includes id="starpu__opencl_8h" name="starpu_opencl.h" local="no" imported="no">starpu_opencl.h</includes>
    <includes id="starpu__thread_8h" name="starpu_thread.h" local="no" imported="no">starpu_thread.h</includes>
    <includes id="starpu__thread__util_8h" name="starpu_thread_util.h" local="no" imported="no">starpu_thread_util.h</includes>
    <includes id="starpu__util_8h" name="starpu_util.h" local="no" imported="no">starpu_util.h</includes>
    <includes id="starpu__data_8h" name="starpu_data.h" local="no" imported="no">starpu_data.h</includes>
    <includes id="starpu__disk_8h" name="starpu_disk.h" local="no" imported="no">starpu_disk.h</includes>
    <includes id="starpu__data__interfaces_8h" name="starpu_data_interfaces.h" local="no" imported="no">starpu_data_interfaces.h</includes>
    <includes id="starpu__data__filters_8h" name="starpu_data_filters.h" local="no" imported="no">starpu_data_filters.h</includes>
    <includes id="starpu__stdlib_8h" name="starpu_stdlib.h" local="no" imported="no">starpu_stdlib.h</includes>
    <includes id="starpu__perfmodel_8h" name="starpu_perfmodel.h" local="no" imported="no">starpu_perfmodel.h</includes>
    <includes id="starpu__worker_8h" name="starpu_worker.h" local="no" imported="no">starpu_worker.h</includes>
    <includes id="starpu__task_8h" name="starpu_task.h" local="no" imported="no">starpu_task.h</includes>
    <includes id="starpu__task__list_8h" name="starpu_task_list.h" local="no" imported="no">starpu_task_list.h</includes>
    <includes id="starpu__task__util_8h" name="starpu_task_util.h" local="no" imported="no">starpu_task_util.h</includes>
    <includes id="starpu__sched__ctx_8h" name="starpu_sched_ctx.h" local="no" imported="no">starpu_sched_ctx.h</includes>
    <includes id="starpu__expert_8h" name="starpu_expert.h" local="no" imported="no">starpu_expert.h</includes>
    <includes id="starpu__rand_8h" name="starpu_rand.h" local="no" imported="no">starpu_rand.h</includes>
    <includes id="starpu__cuda_8h" name="starpu_cuda.h" local="no" imported="no">starpu_cuda.h</includes>
    <includes id="starpu__cublas_8h" name="starpu_cublas.h" local="no" imported="no">starpu_cublas.h</includes>
    <includes id="starpu__bound_8h" name="starpu_bound.h" local="no" imported="no">starpu_bound.h</includes>
    <includes id="starpu__hash_8h" name="starpu_hash.h" local="no" imported="no">starpu_hash.h</includes>
    <includes id="starpu__profiling_8h" name="starpu_profiling.h" local="no" imported="no">starpu_profiling.h</includes>
    <includes id="starpu__top_8h" name="starpu_top.h" local="no" imported="no">starpu_top.h</includes>
    <includes id="starpu__fxt_8h" name="starpu_fxt.h" local="no" imported="no">starpu_fxt.h</includes>
    <includes id="starpu__driver_8h" name="starpu_driver.h" local="no" imported="no">starpu_driver.h</includes>
    <includes id="starpu__tree_8h" name="starpu_tree.h" local="no" imported="no">starpu_tree.h</includes>
    <includes id="starpu__openmp_8h" name="starpu_openmp.h" local="no" imported="no">starpu_openmp.h</includes>
    <includes id="starpu__bitmap_8h" name="starpu_bitmap.h" local="no" imported="no">starpu_bitmap.h</includes>
    <includes id="starpu__deprecated__api_8h" name="starpu_deprecated_api.h" local="yes" imported="no">starpu_deprecated_api.h</includes>
    <class kind="struct">starpu_conf</class>
    <member kind="function">
      <type>int</type>
      <name>starpu_conf_init</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga7f7f154a1bf9600b8ccd10436412660e</anchor>
      <arglist>(struct starpu_conf *conf)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_init</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga9ce171bcbbee2edd169ba2649e6e75e3</anchor>
      <arglist>(struct starpu_conf *conf) STARPU_WARN_UNUSED_RESULT</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_initialize</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>gab2948c26e01bbc6b29f11bfc7268e7d9</anchor>
      <arglist>(struct starpu_conf *user_conf, int *argc, char ***argv)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_pause</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga887fbd250ba7843400b4438d617213d6</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_resume</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>gad1aff0c793b50e50f995232c110bde66</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_shutdown</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga48edf5e30e71fbb71923e3867ad16c0a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_topology_print</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga0702f607811a2af4f7d2bc271eb434e9</anchor>
      <arglist>(FILE *f)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_asynchronous_copy_disabled</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga2cf027abaf6581c7baf98f9caeb9e4e5</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_asynchronous_cuda_copy_disabled</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga994d261c0ac05522b02ed3ab72dce2bd</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_asynchronous_opencl_copy_disabled</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga8a817e603dd7ce77a7712f7c981da081</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_asynchronous_mic_copy_disabled</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>gacfd8a8d350b5d7f874bbeef57975634d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_get_version</name>
      <anchorfile>group__API__Versioning.html</anchorfile>
      <anchor>gaa9151820d2ef6a8231b510b4b3c19ca4</anchor>
      <arglist>(int *major, int *minor, int *release)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_bitmap.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__bitmap_8h</filename>
    <member kind="function">
      <type>struct starpu_bitmap *</type>
      <name>starpu_bitmap_create</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gad46a1d8559731ed2b5619f90240b6c1b</anchor>
      <arglist>(void) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_destroy</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga870bdc74a6ff9db855de24622ced5f5f</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_set</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gaf7debe919f07b41a8aa4d35262838e26</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_unset</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gab9dcc731b430f29d7cd65451df6d14c0</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_unset_all</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gafa9cd8be3ba6463b967e1c5bcb37587e</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_get</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga997748478e809c6dd577d204deaa5d57</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_unset_and</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gae528ad901cbbdda91d0aa25d4a97ff59</anchor>
      <arglist>(struct starpu_bitmap *a, struct starpu_bitmap *b, struct starpu_bitmap *c)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_or</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga9f3c69615da232aa98ca0d82b05d2fa2</anchor>
      <arglist>(struct starpu_bitmap *a, struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_and_get</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gac7dbca81bf45d4d07f774057ef6790bc</anchor>
      <arglist>(struct starpu_bitmap *b1, struct starpu_bitmap *b2, int e)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_cardinal</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga03a6d6e8dc484f0be14ebb733bc587d6</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_first</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga1a0689f2f21ccded428555a580567b61</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_last</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga6e979d89f277827e58aa0e6fe84c67cf</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_next</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gaa2063e19f69f9fd87dd3107cf49db94e</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_has_next</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga7765890a66b9ecb0cedfd30ad0180333</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_bound.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__bound_8h</filename>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_start</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga284f3571becb60b2354cc1ce121e4778</anchor>
      <arglist>(int deps, int prio)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_stop</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga5f1859599a28105aea4c0f33fd871218</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_print_dot</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga7fc9141929ef926d346431307afb0ff1</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_compute</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga99f073d0ad7604366ef3a8f805b5f060</anchor>
      <arglist>(double *res, double *integer_res, int integer)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_print_lp</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga066ce2e396d5b676af7a5209b0079610</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_print_mps</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>gad0cf05d0bb9b21964fb911cfebf252df</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_print</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga834aa7094c173d42985d4d70f1694f57</anchor>
      <arglist>(FILE *output, int integer)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_config.h</name>
    <path>/home/gchr/workspace/StarPU/objdir/doc/doxygen/</path>
    <filename>starpu__config_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAJOR_VERSION</name>
      <anchorfile>group__API__Versioning.html</anchorfile>
      <anchor>ga61698352a3ca7e28b4bcdd8bcad8d8cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MINOR_VERSION</name>
      <anchorfile>group__API__Versioning.html</anchorfile>
      <anchor>ga847f129c7b859f46c8bd69c9c8cbcde5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_RELEASE_VERSION</name>
      <anchorfile>group__API__Versioning.html</anchorfile>
      <anchor>ga43895a109dc6f27de709208f460b321d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_CUDA</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga384db70769609ec68fae5a0105821299</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_OPENCL</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga160dc78c0f6a90b5aa2200b49c5b4d7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_MIC</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga08715106f0b8262604d469ed0aa962aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_SCC</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>gaed05094ef8e7bd93c99315705cc4b87a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL_DATADIR</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gababf3ee0552f34c89fdc8ebdec116dbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_NMAXBUFS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad9efd8b217907a2fe26d92bd91438cdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAXCUDADEVS</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>gae2a0ce53013a0c3d7bafaf0ec000cdd5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAXOPENCLDEVS</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga1b591248c13dc33e2e9b00ace593405e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAXMICDEVS</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga06ae5976f7c5d39961019127497b097b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAXSCCDEVS</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>ga651a759ee07740d52e96d15ca3c68ae8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_NMAXWORKERS</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gad7c443d1341e4976d63fb5d77e74bf09</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENMP</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga0a86f6a9d780b564dae9b9a71cfe5e19</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_MPI</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabefad62cd0f0ea270a95e97c0fae17d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_MPI</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabefad62cd0f0ea270a95e97c0fae17d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENMP</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga0a86f6a9d780b564dae9b9a71cfe5e19</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_cublas.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__cublas_8h</filename>
    <member kind="function">
      <type>void</type>
      <name>starpu_cublas_init</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga9f70358bd39f2228d7b0558702306d96</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_cublas_shutdown</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga3cd8378588620422bc6c3246016ffcd8</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_cuda.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__cuda_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CUBLAS_REPORT_ERROR</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga305c371567096cedd87290bf68123ced</anchor>
      <arglist>(status)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CUDA_REPORT_ERROR</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>gae30cd6ebde1dc4001ae8a17cabd5dbc4</anchor>
      <arglist>(status)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_cublas_report_error</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga1f4ce467c5135681226d3764911d838c</anchor>
      <arglist>(const char *func, const char *file, int line, int status)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_cuda_report_error</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga08ef1dd9623d7adb87cede9e60d3e1dc</anchor>
      <arglist>(const char *func, const char *file, int line, cudaError_t status)</arglist>
    </member>
    <member kind="function">
      <type>cudaStream_t</type>
      <name>starpu_cuda_get_local_stream</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>gad7d80d054bd2b9570e1d7e24442e19c0</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>const struct cudaDeviceProp *</type>
      <name>starpu_cuda_get_device_properties</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga71b71760c8b71cb6c3f0dc7495c84036</anchor>
      <arglist>(unsigned workerid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_cuda_copy_async_sync</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga20c5b69812878a57e22f4f3c5cc8f56f</anchor>
      <arglist>(void *src_ptr, unsigned src_node, void *dst_ptr, unsigned dst_node, size_t ssize, cudaStream_t stream, enum cudaMemcpyKind kind)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_cuda_set_device</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga3b170653608d3381b660afba63be39da</anchor>
      <arglist>(unsigned devid)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_data.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__data_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <class kind="struct">starpu_data_descr</class>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_malloc_pinned_if_possible</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gad1e07e6fc879ed3d05d2a6c5c2a04b56</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_free_pinned_if_possible</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga4e9089680daf0b2d7f26e4657ffe9f48</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAIN_RAM</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga64855af2ea04f74a1a261724b3b79046</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct _starpu_data_state *</type>
      <name>starpu_data_handle_t</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga5f517ab725864d54b0459896a8f8ae07</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct starpu_arbiter *</type>
      <name>starpu_arbiter_t</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gab442391c29c432bdb20e5d70f008f48f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_data_access_mode</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1fb3a1ff8622747d653d1b5f41bc41db</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_NONE</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaacd20c7596d4c1ffc6dbeaa632a6a6a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_R</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaa4c2445a344c592fb7b1adfcf1d2e1f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_W</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba052ab75035ca297c7955363c605231c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_RW</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba20da2e02cd303015b5967dbf72ef3e1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCRATCH</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba2a5d5e11b2ec3aa14f5c9bf94accf6f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_REDUX</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba6011b5abab92fd996b29d622c4a488f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_COMMUTE</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba64859f7fc1e4f2484dd7f0500f12c30a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SSEND</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaf96ec45077e70239a826867ed897bd0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_LOCALITY</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaed3d2281d14b6e53ecb04c1c29a3d688</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_node_kind</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga6f3fbb6e918d0135ccb68473a8ee5ca1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_UNUSED</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a92a0688575c41519f031fefd35f51c9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CPU_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a9578288c35b06ee94ab4ef6a41d16674</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CUDA_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a619d788452761975aa62ea517c86104c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_OPENCL_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a6e0c8b16179c2829393a373831c3b802</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MIC_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a61616139496d3d57b4cd181459709a11</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCC_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a7169c196e1585c24e9f2ab73d7d42e3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCC_SHM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1ad14cce897c4dc582b773d35ab93cec7c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_display_memory_stats</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga6b93e19cf6ddd63d7eaee6749a7bdcf1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_worker_get_memory_node</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga7de6654141ce89ea83c3aba60486396e</anchor>
      <arglist>(unsigned workerid)</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_node_kind</type>
      <name>starpu_node_get_kind</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga9b2541fc1a20c4c486c499af01a3283c</anchor>
      <arglist>(unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_sequential_consistency_flag</name>
      <anchorfile>group__API__Implicit__Data__Dependencies.html</anchorfile>
      <anchor>ga273df19e4cad1c05ec5df697bcec4444</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned flag)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_data_get_sequential_consistency_flag</name>
      <anchorfile>group__API__Implicit__Data__Dependencies.html</anchorfile>
      <anchor>gac88c5f320d2c900923b7934d9a733104</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_data_get_default_sequential_consistency_flag</name>
      <anchorfile>group__API__Implicit__Data__Dependencies.html</anchorfile>
      <anchor>gaef7203edaf1725977dffac23296ede4e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_default_sequential_consistency_flag</name>
      <anchorfile>group__API__Implicit__Data__Dependencies.html</anchorfile>
      <anchor>ga26f17239b14354e61eef0710ffadc434</anchor>
      <arglist>(unsigned flag)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unregister</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga586146498466b60d6b81145dfaeb8948</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unregister_no_coherency</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gaa1f95d17759711f7703bf02820e2e49b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unregister_submit</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga4fa34753bff1d29c20f0a0e361020b4e</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_invalidate</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga4e82fe020ec010bcacb6aee16021607c</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_invalidate_submit</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga06b01fdf769f8f2eb222ecde42afbc81</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_advise_as_important</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gaedc8031c1b437d35232cd1700d83d472</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned is_important)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_request_allocation</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gac561fd637be17b74f0456106dc25f5d1</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_fetch_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga9ce32e79b2f07fb474b6ae10006abb1f</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, unsigned async)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_prefetch_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga57687b811ced00dbfc35af73164a72aa</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, unsigned async)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_idle_prefetch_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gad5a24f94d0aa2bbfab8957c3dd13949a</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, unsigned async)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_wont_use</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gafd4b4f7f9f0a26f65a1e149525a09bfd</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_wt_mask</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gae53e7b21c7426c9845a1046cfe5becce</anchor>
      <arglist>(starpu_data_handle_t handle, uint32_t wt_mask)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_query_status</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga9a6122bae224d714383ae775434cdce3</anchor>
      <arglist>(starpu_data_handle_t handle, int memory_node, int *is_allocated, int *is_valid, int *is_requested)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_reduction_methods</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1f5ba1c1cfefc1f81a4095cf3c213e54</anchor>
      <arglist>(starpu_data_handle_t handle, struct starpu_codelet *redux_cl, struct starpu_codelet *init_cl)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_data_interface_ops *</type>
      <name>starpu_data_get_interface_ops</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gac0d64eee72db978a53bf3e5081766d1b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unregister</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga586146498466b60d6b81145dfaeb8948</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unregister_no_coherency</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gaa1f95d17759711f7703bf02820e2e49b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unregister_submit</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga4fa34753bff1d29c20f0a0e361020b4e</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_invalidate</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga4e82fe020ec010bcacb6aee16021607c</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_invalidate_submit</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga06b01fdf769f8f2eb222ecde42afbc81</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_advise_as_important</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gaedc8031c1b437d35232cd1700d83d472</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned is_important)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_request_allocation</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gac561fd637be17b74f0456106dc25f5d1</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_fetch_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga9ce32e79b2f07fb474b6ae10006abb1f</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, unsigned async)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_prefetch_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga57687b811ced00dbfc35af73164a72aa</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, unsigned async)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_idle_prefetch_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gad5a24f94d0aa2bbfab8957c3dd13949a</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, unsigned async)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_wont_use</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gafd4b4f7f9f0a26f65a1e149525a09bfd</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_wt_mask</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gae53e7b21c7426c9845a1046cfe5becce</anchor>
      <arglist>(starpu_data_handle_t handle, uint32_t wt_mask)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_query_status</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga9a6122bae224d714383ae775434cdce3</anchor>
      <arglist>(starpu_data_handle_t handle, int memory_node, int *is_allocated, int *is_valid, int *is_requested)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_reduction_methods</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1f5ba1c1cfefc1f81a4095cf3c213e54</anchor>
      <arglist>(starpu_data_handle_t handle, struct starpu_codelet *redux_cl, struct starpu_codelet *init_cl)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_data_interface_ops *</type>
      <name>starpu_data_get_interface_ops</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gac0d64eee72db978a53bf3e5081766d1b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_DATA_ACQUIRE_CB</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga2b9b64ac9a650d8c8942b4227e6fce75</anchor>
      <arglist>(handle, mode, code)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gae6eb535cf9bf46a7ef9ad2d845c675a2</anchor>
      <arglist>(starpu_data_handle_t handle, enum starpu_data_access_mode mode)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga3b2ca6406b5839062346200123551638</anchor>
      <arglist>(starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_cb</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga358aba7459b7f99a6dbaa189ce57b925</anchor>
      <arglist>(starpu_data_handle_t handle, enum starpu_data_access_mode mode, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_on_node_cb</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga8db878885ebbee4729d8aed6a0479262</anchor>
      <arglist>(starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_cb_sequential_consistency</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga70ee8a92cbb3935ec3ca0d40f443f860</anchor>
      <arglist>(starpu_data_handle_t handle, enum starpu_data_access_mode mode, void(*callback)(void *), void *arg, int sequential_consistency)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_on_node_cb_sequential_consistency</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gafd48044a523ed5a4e11598596096dc52</anchor>
      <arglist>(starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode, void(*callback)(void *), void *arg, int sequential_consistency)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_release</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gadc145017bafa2948109c624715de77a2</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_release_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga5b95764e9545fc367cb995950e82f449</anchor>
      <arglist>(starpu_data_handle_t handle, int node)</arglist>
    </member>
    <member kind="function">
      <type>starpu_arbiter_t</type>
      <name>starpu_arbiter_create</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga8771ea54051655e68ed7a95b4b831fc7</anchor>
      <arglist>(void) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_assign_arbiter</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga9cbc51da2f2b7025c534d6862866bf81</anchor>
      <arglist>(starpu_data_handle_t handle, starpu_arbiter_t arbiter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_arbiter_destroy</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga337cf01e2a0ccc1c3cc2ea27d5bdf131</anchor>
      <arglist>(starpu_arbiter_t arbiter)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_data_filters.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__data__filters_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <class kind="struct">starpu_data_filter</class>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga1363109ba0e36c1b6c7f1a40c9608791</anchor>
      <arglist>(starpu_data_handle_t initial_handle, struct starpu_data_filter *f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unpartition</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gae80794b9cad7855a3ee54a4361f656ed</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned gathering_node)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_get_nb_children</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga6a3f729055f14384e7397d2815a2c9a5</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_get_child</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga29e07c2c0604da63e7746a8018d8a62f</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned i)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_get_sub_data</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac24101bbe28b1d7d4a0874d349ba8979</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned depth,...)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_vget_sub_data</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7904efb86ab3f9d6d682a3a3be3646fe</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned depth, va_list pa)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_map_filters</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga551d6fa7fead5b9f7c8a85b1f9885e91</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned nfilters,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_vmap_filters</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga15a28291a5045ef7ed3c93afc94ed248</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned nfilters, va_list pa)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_plan</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gaa4407a8734e1fbdbb63b83351769476c</anchor>
      <arglist>(starpu_data_handle_t initial_handle, struct starpu_data_filter *f, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga994cbae9c619b070f8d219f6bfffff06</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_readonly_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7fcf158f5196d62610c5017993442c53</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_readwrite_upgrade_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga8a26c673507a7de484071e0926cb5638</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unpartition_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga46d2b144a7de2e17d17b1383ef5f522d</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children, int gathering_node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unpartition_readonly_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac1e66049a48764a29d867a02bcc9d0ce</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children, int gathering_node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_clean</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac4f0b5ce7cdee16c1311595e9ca53e98</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bcsr_filter_canonical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga0e1bee4821237529d554605d333e9109</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_csr_filter_vertical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga554a2fb14fdee9353364c39f36ee3a6f</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga8c86b2af9e0806e631c1cbb5d506506b</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga88fbca61843b76314e39a2c0f8b93d6c</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_vertical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga2925be576ac7d597ecead381ff32a894</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_vertical_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7132923bd901e0e4254cc0b20d49997a</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga212189d3b83dfa4e225609b5f2bf8461</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gaab49915dc0462c1b145bfb0a9ce4cf52</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_list</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gab9fa487bfff5ccdd59210bdde65a11db</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_divide_in_2</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gab639622ea4929c36df704a0bebfd3fac</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga1a265ffca51fae58701832a4daa53bd9</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7cc8832e25f2f4049ba5a0053b122dd9</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_vertical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga73a2c9af1200c68f0403e70e36c020d0</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_vertical_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gafa4818b571e98acd8696a1251b0d4e74</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_depth_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gaae4f93ab3326ded72c3a80d337e6f4a1</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_depth_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac4b9ec529f67e5c300e7eed3e185fbaf</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga1363109ba0e36c1b6c7f1a40c9608791</anchor>
      <arglist>(starpu_data_handle_t initial_handle, struct starpu_data_filter *f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unpartition</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gae80794b9cad7855a3ee54a4361f656ed</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned gathering_node)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_get_nb_children</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga6a3f729055f14384e7397d2815a2c9a5</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_get_child</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga29e07c2c0604da63e7746a8018d8a62f</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned i)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_get_sub_data</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac24101bbe28b1d7d4a0874d349ba8979</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned depth,...)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_vget_sub_data</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7904efb86ab3f9d6d682a3a3be3646fe</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned depth, va_list pa)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_map_filters</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga551d6fa7fead5b9f7c8a85b1f9885e91</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned nfilters,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_vmap_filters</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga15a28291a5045ef7ed3c93afc94ed248</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned nfilters, va_list pa)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_plan</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gaa4407a8734e1fbdbb63b83351769476c</anchor>
      <arglist>(starpu_data_handle_t initial_handle, struct starpu_data_filter *f, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga994cbae9c619b070f8d219f6bfffff06</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_readonly_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7fcf158f5196d62610c5017993442c53</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_readwrite_upgrade_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga8a26c673507a7de484071e0926cb5638</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unpartition_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga46d2b144a7de2e17d17b1383ef5f522d</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children, int gathering_node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unpartition_readonly_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac1e66049a48764a29d867a02bcc9d0ce</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children, int gathering_node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_clean</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac4f0b5ce7cdee16c1311595e9ca53e98</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga212189d3b83dfa4e225609b5f2bf8461</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gaab49915dc0462c1b145bfb0a9ce4cf52</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_list</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gab9fa487bfff5ccdd59210bdde65a11db</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_divide_in_2</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gab639622ea4929c36df704a0bebfd3fac</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga8c86b2af9e0806e631c1cbb5d506506b</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga88fbca61843b76314e39a2c0f8b93d6c</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_vertical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga2925be576ac7d597ecead381ff32a894</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_vertical_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7132923bd901e0e4254cc0b20d49997a</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga1a265ffca51fae58701832a4daa53bd9</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7cc8832e25f2f4049ba5a0053b122dd9</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_vertical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga73a2c9af1200c68f0403e70e36c020d0</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_vertical_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gafa4818b571e98acd8696a1251b0d4e74</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_depth_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gaae4f93ab3326ded72c3a80d337e6f4a1</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_depth_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac4b9ec529f67e5c300e7eed3e185fbaf</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bcsr_filter_canonical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga0e1bee4821237529d554605d333e9109</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_csr_filter_vertical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga554a2fb14fdee9353364c39f36ee3a6f</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_data_interfaces.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__data__interfaces_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <class kind="struct">starpu_data_copy_methods</class>
    <class kind="struct">starpu_data_interface_ops</class>
    <class kind="struct">starpu_matrix_interface</class>
    <class kind="struct">starpu_coo_interface</class>
    <class kind="struct">starpu_block_interface</class>
    <class kind="struct">starpu_vector_interface</class>
    <class kind="struct">starpu_variable_interface</class>
    <class kind="struct">starpu_csr_interface</class>
    <class kind="struct">starpu_bcsr_interface</class>
    <class kind="struct">starpu_multiformat_data_interface_ops</class>
    <class kind="struct">starpu_multiformat_interface</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_CPU_PTR</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>gaf0937aae4bedeb03e9ebbec36cc1dad6</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_CUDA_PTR</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>gaeb6356075b3f57e8c7aeccc85f076d87</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_OPENCL_PTR</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>ga0b6869745c464525b25a0757d112c318</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_MIC_PTR</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>ga9a85e9fa7f275ae4805a7208e4ef2702</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_NX</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>ga9719eab3a5e9fbaad2837b4a985a3a14</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_COLUMNS</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga56d6ae5fe9283bfb77e13f07c2513184</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_COLUMNS_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gade238eb388cbb34bf9a93643bfa4df83</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_ROWS</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac739182a43ec4dd00618d783b1f2a16c</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_ROWS_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab1c57c537a980a2de5e595b2f6204772</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_VALUES</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga001c8954d20044b466c661cf7c9f9fe0</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_VALUES_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadf8eac3f96b6e63911f23165141411df</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga1e92dc5d143be583708d87498d2dc9cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_NX</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac77fef789ec34682ba00b38dcc501170</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_NY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa15cff8f1148e572348095ac5a2ab421</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_NVALUES</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga550d6222ff484201dbd3d39bb7baeb7d</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gae95b657d947e611060590d506d8e15dc</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_data_interface_id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_UNKNOWN_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca26edb6f25f81276cc829b070fd240472</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MATRIX_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357caf3641ac475b4b69aa6ef5758b950348a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_BLOCK_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca61676e55a5c706ecb014c772a39e292d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_VECTOR_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357cabd89ff25621eab18a8a7306842c13217</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CSR_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca9566de0c4f51f4e48f6ca681d2701eed</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_BCSR_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357caf0717d5dd8045aa04c0d4ea65dc632be</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_VARIABLE_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357cae02c3d444d843270b692718f22173ab3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_VOID_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca3867ecf55ad294013fd36c502af39d12</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MULTIFORMAT_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357cace921145fc7dd66a423b1c52991c6335</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_COO_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357cad84c1b3c230ee8b5136302fa63dfc9d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MAX_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca0eed341fd3aa3fdc35d2f38b3eae09b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_multiformat_data_register</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>gae36d714a8bf065925923a35fd6fd4ae5</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, void *ptr, uint32_t nobjects, struct starpu_multiformat_data_interface_ops *format_ops)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_interface_copy</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7af8e4f90b557fefaa09f40a930efd74</anchor>
      <arglist>(uintptr_t src, size_t src_offset, unsigned src_node, uintptr_t dst, size_t dst_offset, unsigned dst_node, size_t size, void *async_data)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_malloc_on_node_flags</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab2bf7713cad5570775bdf4efec79502d</anchor>
      <arglist>(unsigned dst_node, size_t size, int flags)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_malloc_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab91cbc596a65e6a4322b657c79934269</anchor>
      <arglist>(unsigned dst_node, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_free_on_node_flags</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga02005aa2a3c838802d95d0426a937d8d</anchor>
      <arglist>(unsigned dst_node, uintptr_t addr, size_t size, int flags)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_free_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7e3ef9efbc7a65adad27f9ac27493493</anchor>
      <arglist>(unsigned dst_node, uintptr_t addr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_malloc_on_node_set_default_flags</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga2af8405c3f0fff62ea7269fc279cab55</anchor>
      <arglist>(unsigned node, int flags)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_interface_get_next_id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf5ea640f2c977e3ae95a6be9b3be3bee</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_register</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga74d62dfa2a733db4bac71caaae751d9d</anchor>
      <arglist>(starpu_data_handle_t *handleptr, unsigned home_node, void *data_interface, struct starpu_data_interface_ops *ops)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_ptr_register</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1d0a0dc4903585e099b2e4b16a22946a</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_register_same</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1be29ee257e3d9c3321df759b8105cb1</anchor>
      <arglist>(starpu_data_handle_t *handledst, starpu_data_handle_t handlesrc)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_lookup</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga75907283eef0a058a040ff06de87b4b2</anchor>
      <arglist>(const void *ptr)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_data_handle_to_pointer</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga011705e0a88798562f7afb2753a6b1ce</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_data_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gabcce7df711e56cafddfd82f8f5ee794b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_data_interface_id</type>
      <name>starpu_data_get_interface_id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad6ca41b04e0265671d4bf899dd433c1e</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_pack</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad7e204d690945cbe5145bc8950078291</anchor>
      <arglist>(starpu_data_handle_t handle, void **ptr, starpu_ssize_t *count)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_unpack</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga97c7b78c97576eb44741bdd7e2183fa7</anchor>
      <arglist>(starpu_data_handle_t handle, void *ptr, size_t count)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_data_get_size</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac14cfc52c450c81dd4789a7aace79d3b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_data_get_interface_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab6e12b04b231773f2eff496f57d29ee8</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned memory_node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7af287b9089acc7cf4b3b7ed19d82abb</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, uint32_t ld, uint32_t nx, uint32_t ny, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga22a8126d41b43e71c3c79e210401b12e</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset, uint32_t ld)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_coo_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gace26ae5c977d1fd0fbea538b582bab43</anchor>
      <arglist>(starpu_data_handle_t *handleptr, unsigned home_node, uint32_t nx, uint32_t ny, uint32_t n_values, uint32_t *columns, uint32_t *rows, uintptr_t values, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab67a02c78b113f6b3e031735418fd838</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, uint32_t ldy, uint32_t ldz, uint32_t nx, uint32_t ny, uint32_t nz, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8aa19dcac39f053949c1bb1b4f5fad1d</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset, uint32_t ldy, uint32_t ldz)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga98ea4083ac39db45b6be75354e66bca1</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, uint32_t nx, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gacb816db830d6991909a35a8c0e93a843</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_variable_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadcd1aee7fb7226edebe64dfcc46c1d69</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_variable_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf972d52af628f82488eeea311eadb4f6</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_void_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab7f9484602aeef560cc2e5904e3b2cff</anchor>
      <arglist>(starpu_data_handle_t *handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_csr_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga81426fc1860aabaefed1a33946df6f51</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uint32_t nnz, uint32_t nrow, uintptr_t nzval, uint32_t *colind, uint32_t *rowptr, uint32_t firstentry, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bcsr_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga336c1bc150930da84a2abc7d146839c3</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uint32_t nnz, uint32_t nrow, uintptr_t nzval, uint32_t *colind, uint32_t *rowptr, uint32_t firstentry, uint32_t r, uint32_t c, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_data_get_interface_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab6e12b04b231773f2eff496f57d29ee8</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned memory_node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7af287b9089acc7cf4b3b7ed19d82abb</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, uint32_t ld, uint32_t nx, uint32_t ny, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga22a8126d41b43e71c3c79e210401b12e</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset, uint32_t ld)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_coo_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gace26ae5c977d1fd0fbea538b582bab43</anchor>
      <arglist>(starpu_data_handle_t *handleptr, unsigned home_node, uint32_t nx, uint32_t ny, uint32_t n_values, uint32_t *columns, uint32_t *rows, uintptr_t values, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab67a02c78b113f6b3e031735418fd838</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, uint32_t ldy, uint32_t ldz, uint32_t nx, uint32_t ny, uint32_t nz, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8aa19dcac39f053949c1bb1b4f5fad1d</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset, uint32_t ldy, uint32_t ldz)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga98ea4083ac39db45b6be75354e66bca1</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, uint32_t nx, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gacb816db830d6991909a35a8c0e93a843</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_variable_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadcd1aee7fb7226edebe64dfcc46c1d69</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_variable_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf972d52af628f82488eeea311eadb4f6</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_void_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab7f9484602aeef560cc2e5904e3b2cff</anchor>
      <arglist>(starpu_data_handle_t *handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_csr_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga81426fc1860aabaefed1a33946df6f51</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uint32_t nnz, uint32_t nrow, uintptr_t nzval, uint32_t *colind, uint32_t *rowptr, uint32_t firstentry, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bcsr_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga336c1bc150930da84a2abc7d146839c3</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uint32_t nnz, uint32_t nrow, uintptr_t nzval, uint32_t *colind, uint32_t *rowptr, uint32_t firstentry, uint32_t r, uint32_t c, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_data_handle_to_pointer</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga011705e0a88798562f7afb2753a6b1ce</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_data_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gabcce7df711e56cafddfd82f8f5ee794b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_data_interface_id</type>
      <name>starpu_data_get_interface_id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad6ca41b04e0265671d4bf899dd433c1e</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_pack</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad7e204d690945cbe5145bc8950078291</anchor>
      <arglist>(starpu_data_handle_t handle, void **ptr, starpu_ssize_t *count)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_unpack</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga97c7b78c97576eb44741bdd7e2183fa7</anchor>
      <arglist>(starpu_data_handle_t handle, void *ptr, size_t count)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_data_get_size</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac14cfc52c450c81dd4789a7aace79d3b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_GET_PTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad99e209fafc3ad5e0c4da0de02e5bda2</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga748def68db84b3cb9da305b0e3f7c935</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7d44b8a0594932a8cece13e1cc9932bb</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_GET_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf011a057a557a66271ce06e8109c11c4</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_variable_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga988036ae1f9385832c2988064a4f27af</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_variable_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga540f980b1c4bee16c78d84097a8b07f9</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_PTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa23416a6d049b276fe57b19e069b68b3</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadccf196d01dd9c43e931827a86f886b2</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8fa1c7e18fa7c621ec68fc5aa32ef181</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_NX</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7cc322b2f03830ec7afbb58b7416a283</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga3a3cf303a3e3be144aab7abdf587c900</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_SLICE_BASE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa35025b4b6126c3f22232cea8915cdad</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_vector_get_nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga08bed7d1e9b7cb2b2b76932eb9c33353</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_vector_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gacd95c31b537322b1d4000a1248b9c425</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_vector_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gacee0a9e166f87b48da1f56e1ae5d988d</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_PTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gacd9e0b5bd06d1b7d88be4e0143be8a68</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac046f5fb0472cada47cd44650194b1c8</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac23356ed31839f1d1c52efccb7a99dd7</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_NX</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gae3d95a1379358cbd85c663c8462da98c</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_NY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab915146b6d59fbd8e6d04480511f7945</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_LD</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga6f28e1a774923108005760426cf8da5b</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaef6039a7976d3ee6c6916df28a08da1a</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_matrix_get_nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga461a1d65e0f4559dacbc1ba77eafbb2b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_matrix_get_ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga720fcf673578ec23e583b5f674626847</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_matrix_get_local_ld</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga5c4dfa96a4ffa7be08e22a9dc20484e4</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_matrix_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga9f6457e4a7e558101dd6ad64076a6a99</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_matrix_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaebeefdd9980c18575924ed9860315bcf</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_PTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaaeff5c191edc5f3df8ba1a319acd0af9</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga335bad7c45aa5df70227ce11f03edc0f</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gafd9d133c4d658b777d9800eba79765b3</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_NX</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf0fa0d2dc99ab880ccfe12061770dad2</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_NY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga87725438ebaccd40d8b1b90e17b46247</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_NZ</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab316b2febb2b5fb92e83bbaab780f046</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_LDY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac2fe5221c0ccc0d082e5bc65830e497b</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_LDZ</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf3bd64a64625f45859f596db3bb9b40a</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga3bb8d6c4169e578b219849148eece329</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8599a8f318fcadbafcc55f9c568678f1</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga81b34820b1a410d218ad08528c0317f6</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_nz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac7b7e5f3d4d9a1468df3b9b92743a9fd</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_local_ldy</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf1843d7f93510e6bf53da173eb9dd75e</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_local_ldz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga66508648e621019be00d86832d66f278</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_block_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga66df9aff4ea14be546df745f17365568</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_block_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga47cea6fc5b88aaa359066e8192fafe95</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_NNZ</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaea999f5e67e778ac28c0a8bc1633ff28</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_NZVAL</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad4850366b77ead9f60c6f7e1d1064b22</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_NZVAL_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga2cab8f7f6a3f484e16241c41239664fa</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_COLIND</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga588a7cbb105708b588e5fe008fbb9e9c</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_COLIND_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga59cbe4cadbe314b73807dc20363db5b9</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_ROWPTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga451ea6b212d0442b2bda2937c08968f5</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_ROWPTR_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga28e9d620c4dc360cdbb0437e81d66543</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad3c08c06e55e86993fa5cb77dce95095</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_nnz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8a4c1ddb71e09437717e298c3e2c39f9</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_nrow</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab59c3b606776bdc54f6ccf3f1db4bf38</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_firstentry</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8c04948b40dc3902e9b3de7a84383381</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_bcsr_get_local_nzval</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gae48e867d81c554b199b127cf9bd4f5b2</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t *</type>
      <name>starpu_bcsr_get_local_colind</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga54dca826f319369d94c4cd6be0eb4828</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t *</type>
      <name>starpu_bcsr_get_local_rowptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga97859bcb7bc60911362087eefc9249ed</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_r</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadcf7063b1cd8d0feed7054e6e4d9d217</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_c</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga6d0c09ee7cd90ac23454ef2040a4bc25</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_bcsr_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga40f5b44c5d9bfd39f2f305aa32a25787</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_NNZ</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab9110ecfa66b148e770a375566c1c19d</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_NROW</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga86b64dc68625997cf3c0bb9eae3ee68a</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_NZVAL</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab7b07d0d2a253e02e1b2d2260cc3fa94</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_NZVAL_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga9dc1bbf7a41070a6fcf82e728cae1542</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_COLIND</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga567d2e3780dabc30269af5e54fe20660</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_COLIND_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaeed0ab452c0906a650097368bd1c6ba3</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_ROWPTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga69dd5090534b513a374089627254f73c</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_ROWPTR_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga21ae3f41a59615f892f196b8cab16ee4</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa46eb27abc7439c03776d68062f029d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_FIRSTENTRY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga83ba12ab43444d61f4966dc76aa160d6</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga82eea89e2f23a9fdc1f34c03db68a97b</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_csr_get_nnz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga66106513707ed71b8849b1c1bbb3df75</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_csr_get_nrow</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga72c9db50152ed5bf9f18ad3a355c5376</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_csr_get_firstentry</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga40f2d8762bc1060e939eecae75f9be08</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_csr_get_local_nzval</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga4f137b139435d4cddcd15704fe46cbbd</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t *</type>
      <name>starpu_csr_get_local_colind</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad5ffed03f141096fca4e1e2583c84e22</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t *</type>
      <name>starpu_csr_get_local_rowptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga78a31ef2148b8439a2c0e897d9179b84</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_csr_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab328b8e0b234147ebe36ceb8e7a55b0a</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_COLUMNS</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga56d6ae5fe9283bfb77e13f07c2513184</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_COLUMNS_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gade238eb388cbb34bf9a93643bfa4df83</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_ROWS</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac739182a43ec4dd00618d783b1f2a16c</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_ROWS_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab1c57c537a980a2de5e595b2f6204772</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_VALUES</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga001c8954d20044b466c661cf7c9f9fe0</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_VALUES_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadf8eac3f96b6e63911f23165141411df</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga1e92dc5d143be583708d87498d2dc9cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_NX</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac77fef789ec34682ba00b38dcc501170</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_NY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa15cff8f1148e572348095ac5a2ab421</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_NVALUES</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga550d6222ff484201dbd3d39bb7baeb7d</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gae95b657d947e611060590d506d8e15dc</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_interface_copy</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7af8e4f90b557fefaa09f40a930efd74</anchor>
      <arglist>(uintptr_t src, size_t src_offset, unsigned src_node, uintptr_t dst, size_t dst_offset, unsigned dst_node, size_t size, void *async_data)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_malloc_on_node_flags</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab2bf7713cad5570775bdf4efec79502d</anchor>
      <arglist>(unsigned dst_node, size_t size, int flags)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_malloc_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab91cbc596a65e6a4322b657c79934269</anchor>
      <arglist>(unsigned dst_node, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_free_on_node_flags</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga02005aa2a3c838802d95d0426a937d8d</anchor>
      <arglist>(unsigned dst_node, uintptr_t addr, size_t size, int flags)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_free_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7e3ef9efbc7a65adad27f9ac27493493</anchor>
      <arglist>(unsigned dst_node, uintptr_t addr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_malloc_on_node_set_default_flags</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga2af8405c3f0fff62ea7269fc279cab55</anchor>
      <arglist>(unsigned node, int flags)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_interface_get_next_id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf5ea640f2c977e3ae95a6be9b3be3bee</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_register</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga74d62dfa2a733db4bac71caaae751d9d</anchor>
      <arglist>(starpu_data_handle_t *handleptr, unsigned home_node, void *data_interface, struct starpu_data_interface_ops *ops)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_ptr_register</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1d0a0dc4903585e099b2e4b16a22946a</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_register_same</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1be29ee257e3d9c3321df759b8105cb1</anchor>
      <arglist>(starpu_data_handle_t *handledst, starpu_data_handle_t handlesrc)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_lookup</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga75907283eef0a058a040ff06de87b4b2</anchor>
      <arglist>(const void *ptr)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_deprecated_api.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__deprecated__api_8h</filename>
  </compound>
  <compound kind="file">
    <name>starpu_disk.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__disk_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <class kind="struct">starpu_disk_ops</class>
    <member kind="function">
      <type>void</type>
      <name>starpu_disk_close</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga8cd566efd70ad57cda18a6b9a7be4ac7</anchor>
      <arglist>(unsigned node, void *obj, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_disk_open</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>gaddcdc1beb6f41ed8d38778fb51cc0e6d</anchor>
      <arglist>(unsigned node, void *pos, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_disk_register</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>gac9ed48246cb2b326ca6a199b6183aa75</anchor>
      <arglist>(struct starpu_disk_ops *func, void *parameter, starpu_ssize_t size)</arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_disk_ops</type>
      <name>starpu_disk_stdio_ops</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga5bb135a02ac97b9efa9ab0280a604868</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_disk_ops</type>
      <name>starpu_disk_unistd_ops</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga647a9db1632791e9410643658ad50ffd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_disk_ops</type>
      <name>starpu_disk_unistd_o_direct_ops</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga902a7c5a3920a874f6e4db36c86c1e4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_disk_ops</type>
      <name>starpu_disk_leveldb_ops</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga4a1bd09de991de1491afa05f5e528e94</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_driver.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__driver_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <includes id="starpu__opencl_8h" name="starpu_opencl.h" local="no" imported="no">starpu_opencl.h</includes>
    <class kind="struct">starpu_driver</class>
    <class kind="union">starpu_driver.id</class>
    <member kind="function">
      <type>int</type>
      <name>starpu_driver_run</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>ga68b320765b17e5e4a256f156c9932e69</anchor>
      <arglist>(struct starpu_driver *d)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_drivers_request_termination</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>ga497d48a24107dfebf6ef3ad40d4a6df8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_driver_init</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>gab274599ec82fe38cb41267b8a1384995</anchor>
      <arglist>(struct starpu_driver *d)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_driver_run_once</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>gad6d01b57f2f84df90f554ea205dcf90d</anchor>
      <arglist>(struct starpu_driver *d)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_driver_deinit</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>gac8f8848b64980676190d77d265fda841</anchor>
      <arglist>(struct starpu_driver *d)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_expert.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__expert_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <member kind="function">
      <type>void</type>
      <name>starpu_wake_all_blocked_workers</name>
      <anchorfile>group__API__Expert__Mode.html</anchorfile>
      <anchor>gaf5f4a32a78630fb051a846cbdcd77d8b</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_progression_hook_register</name>
      <anchorfile>group__API__Expert__Mode.html</anchorfile>
      <anchor>ga4e8d1e607b2383f80b232c27ec6a9386</anchor>
      <arglist>(unsigned(*func)(void *arg), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_progression_hook_deregister</name>
      <anchorfile>group__API__Expert__Mode.html</anchorfile>
      <anchor>ga5d52c35e46e387eed77dd9b7b39507a7</anchor>
      <arglist>(int hook_id)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_fxt.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__fxt_8h</filename>
    <includes id="starpu__perfmodel_8h" name="starpu_perfmodel.h" local="no" imported="no">starpu_perfmodel.h</includes>
    <class kind="struct">starpu_fxt_codelet_event</class>
    <class kind="struct">starpu_fxt_options</class>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_options_init</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>gaf4a2675d7c5f2dc473879ef0a0a2a114</anchor>
      <arglist>(struct starpu_fxt_options *options)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_generate_trace</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga9249a38d77ab128f665b4359ac0c4781</anchor>
      <arglist>(struct starpu_fxt_options *options)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_autostart_profiling</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga6a7af6697ae7a67161d3655c59215c09</anchor>
      <arglist>(int autostart)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_start_profiling</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga5793bf5c8fc0bedf76d18a5fa38593df</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_stop_profiling</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga52a3e1a95689315f0b789a3325ec2a90</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_write_data_trace</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>gaf3cbbddf8c840dae5a649800c0c08b5c</anchor>
      <arglist>(char *filename_in)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_trace_user_event</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga2e55ad671e94b6161755717b1c8b44c1</anchor>
      <arglist>(unsigned long code)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_hash.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__hash_8h</filename>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_hash_crc32c_be_n</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga86956bce305447697cb9346bee692b24</anchor>
      <arglist>(const void *input, size_t n, uint32_t inputcrc)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_hash_crc32c_be</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa29d5f4bd11fce82cd9a01b0e860bf75</anchor>
      <arglist>(uint32_t input, uint32_t inputcrc)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_hash_crc32c_string</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaec6ca64cc2c52e319b9b73cc4f753658</anchor>
      <arglist>(const char *str, uint32_t inputcrc)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_hash_crc32c_be_n</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga86956bce305447697cb9346bee692b24</anchor>
      <arglist>(const void *input, size_t n, uint32_t inputcrc)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_hash_crc32c_be</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa29d5f4bd11fce82cd9a01b0e860bf75</anchor>
      <arglist>(uint32_t input, uint32_t inputcrc)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_hash_crc32c_string</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaec6ca64cc2c52e319b9b73cc4f753658</anchor>
      <arglist>(const char *str, uint32_t inputcrc)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_mic.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__mic_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <member kind="typedef">
      <type>void *</type>
      <name>starpu_mic_func_symbol_t</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga23012ab87c695eae83f9ad32607f9043</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mic_register_kernel</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga83b91d88db5e1e326149f836b7b48ea1</anchor>
      <arglist>(starpu_mic_func_symbol_t *symbol, const char *func_name)</arglist>
    </member>
    <member kind="function">
      <type>starpu_mic_kernel_t</type>
      <name>starpu_mic_get_kernel</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga17637a17347c4ecdec828cd996331710</anchor>
      <arglist>(starpu_mic_func_symbol_t symbol)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_mpi.h</name>
    <path>/home/gchr/workspace/StarPU/mpi/include/</path>
    <filename>starpu__mpi_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gafc29de4080f202511f2b558673304b8c</anchor>
      <arglist>(starpu_data_handle_t data_handle, starpu_mpi_req *req, int dest, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabb6c92b51c1e76408e0816385fde75c5</anchor>
      <arglist>(starpu_data_handle_t data_handle, starpu_mpi_req *req, int source, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_send</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaca01fd4db21aebb91da808a3d03f36dc</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_recv</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaa92438804968616ed4fee7246ec254aa</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, MPI_Status *status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae9506ceb82e72ab1096ac8443dbf13e4</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabdf54ffd31c0b0908d7751e1b2257897</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_issend</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga8374cb42eb85e86e58ccfa8e0c5e8aab</anchor>
      <arglist>(starpu_data_handle_t data_handle, starpu_mpi_req *req, int dest, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_issend_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae247da864753534ad06c6eef58db4f7e</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_wait</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaad03f0a288bad79078045130b9a83188</anchor>
      <arglist>(starpu_mpi_req *req, MPI_Status *status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_test</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae8e8389eb3e916131946028e9ebee670</anchor>
      <arglist>(starpu_mpi_req *req, int *flag, MPI_Status *status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_barrier</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga77aa675b91da2a069caeebdb11a3edc9</anchor>
      <arglist>(MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_detached_sequential_consistency</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga68cd36f12bdc9ee79034ee71b48ab8ef</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg, int sequential_consistency)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae6957a62f85340484cbcebbf65b573c4</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga6cbbe5e484f38cb5e4e601330f2c5b0d</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend_array_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga546dbbe7e9e40817b23bd37fb0eccfe1</anchor>
      <arglist>(unsigned array_size, starpu_data_handle_t *data_handle, int *dest, int *mpi_tag, MPI_Comm *comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_array_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga11f71926b324d325d8a6f6b72b58cdf1</anchor>
      <arglist>(unsigned array_size, starpu_data_handle_t *data_handle, int *source, int *mpi_tag, MPI_Comm *comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_get_communication_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga1d0194c08bd343a68c833f449fb62fa8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_set_communication_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaa734bb0c57e4a48a7fe6e41f2e563e68</anchor>
      <arglist>(int tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_wait_for_all</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabb4f38872a0b5fd8122b681acc0cf932</anchor>
      <arglist>(MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_datatype_register</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaf9345fd74fd060cc6712457e711b29c1</anchor>
      <arglist>(starpu_data_handle_t handle, starpu_mpi_datatype_allocate_func_t allocate_datatype_func, starpu_mpi_datatype_free_func_t free_datatype_func)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_datatype_unregister</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga02b8209239a54d25ec39d93f9ccb9805</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_init_comm</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabdb1e15d1fc805115d660848a711f101</anchor>
      <arglist>(int *argc, char ***argv, int initialize_mpi, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_init</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga008d0df24380455d6b9301ae750ccd4c</anchor>
      <arglist>(int *argc, char ***argv, int initialize_mpi)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_initialize</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga0ec1ce958654468b20224c9ae772f98f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_initialize_extended</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga234e28905c976ed1b03fcdf75812a06d</anchor>
      <arglist>(int *rank, int *world_size)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_shutdown</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga87b22900dc132a958a0f7bce68d32153</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_comm_amounts_retrieve</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga3a6dcddd7eaf6cd3265fd837d67800a2</anchor>
      <arglist>(size_t *comm_amounts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_redux_data</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga565996704c6707a55410488f8d569357</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_scatter_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga39ae075749aef08653ade368b9a95ee0</anchor>
      <arglist>(starpu_data_handle_t *data_handles, int count, int root, MPI_Comm comm, void(*scallback)(void *), void *sarg, void(*rcallback)(void *), void *rarg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_gather_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaf83405a3154137f00ccf554c5e5684b5</anchor>
      <arglist>(starpu_data_handle_t *data_handles, int count, int root, MPI_Comm comm, void(*scallback)(void *), void *sarg, void(*rcallback)(void *), void *rarg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_cache_flush</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gacb05635d95f946f099a1772c8bd59c8b</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_cache_flush_all_data</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gadf1f289d1cf35bd31df930bc347fb9ce</anchor>
      <arglist>(MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_cache_is_enabled</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gac0558236c9fb4cfc333f413c0ca191a9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_cache_set</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga82dceaf1ad42b9a0ab4ed22f9c03b1a8</anchor>
      <arglist>(int enabled)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_register_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gab69b1f823043d16b7d0d799d1ad88a04</anchor>
      <arglist>(starpu_mpi_select_node_policy_func_t policy_func)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_unregister_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gad6453ef7697756dd3b1746399eec4867</anchor>
      <arglist>(int policy)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_get_current_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga9eb17d58fc8f0c33fc4e5985d440079e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_set_current_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga35f7d1ea8e747cbf1637301b14ef0db8</anchor>
      <arglist>(int policy)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_init_comm</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabdb1e15d1fc805115d660848a711f101</anchor>
      <arglist>(int *argc, char ***argv, int initialize_mpi, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_init</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga008d0df24380455d6b9301ae750ccd4c</anchor>
      <arglist>(int *argc, char ***argv, int initialize_mpi)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_initialize</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga0ec1ce958654468b20224c9ae772f98f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_initialize_extended</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga234e28905c976ed1b03fcdf75812a06d</anchor>
      <arglist>(int *rank, int *world_size)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_shutdown</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga87b22900dc132a958a0f7bce68d32153</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_comm_amounts_retrieve</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga3a6dcddd7eaf6cd3265fd837d67800a2</anchor>
      <arglist>(size_t *comm_amounts)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gafc29de4080f202511f2b558673304b8c</anchor>
      <arglist>(starpu_data_handle_t data_handle, starpu_mpi_req *req, int dest, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabb6c92b51c1e76408e0816385fde75c5</anchor>
      <arglist>(starpu_data_handle_t data_handle, starpu_mpi_req *req, int source, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_send</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaca01fd4db21aebb91da808a3d03f36dc</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_recv</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaa92438804968616ed4fee7246ec254aa</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, MPI_Status *status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae9506ceb82e72ab1096ac8443dbf13e4</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabdf54ffd31c0b0908d7751e1b2257897</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_issend</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga8374cb42eb85e86e58ccfa8e0c5e8aab</anchor>
      <arglist>(starpu_data_handle_t data_handle, starpu_mpi_req *req, int dest, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_issend_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae247da864753534ad06c6eef58db4f7e</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_wait</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaad03f0a288bad79078045130b9a83188</anchor>
      <arglist>(starpu_mpi_req *req, MPI_Status *status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_test</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae8e8389eb3e916131946028e9ebee670</anchor>
      <arglist>(starpu_mpi_req *req, int *flag, MPI_Status *status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_barrier</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga77aa675b91da2a069caeebdb11a3edc9</anchor>
      <arglist>(MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_detached_sequential_consistency</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga68cd36f12bdc9ee79034ee71b48ab8ef</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg, int sequential_consistency)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae6957a62f85340484cbcebbf65b573c4</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga6cbbe5e484f38cb5e4e601330f2c5b0d</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend_array_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga546dbbe7e9e40817b23bd37fb0eccfe1</anchor>
      <arglist>(unsigned array_size, starpu_data_handle_t *data_handle, int *dest, int *mpi_tag, MPI_Comm *comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_array_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga11f71926b324d325d8a6f6b72b58cdf1</anchor>
      <arglist>(unsigned array_size, starpu_data_handle_t *data_handle, int *source, int *mpi_tag, MPI_Comm *comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_get_communication_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga1d0194c08bd343a68c833f449fb62fa8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_set_communication_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaa734bb0c57e4a48a7fe6e41f2e563e68</anchor>
      <arglist>(int tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_wait_for_all</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabb4f38872a0b5fd8122b681acc0cf932</anchor>
      <arglist>(MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_datatype_register</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaf9345fd74fd060cc6712457e711b29c1</anchor>
      <arglist>(starpu_data_handle_t handle, starpu_mpi_datatype_allocate_func_t allocate_datatype_func, starpu_mpi_datatype_free_func_t free_datatype_func)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_datatype_unregister</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga02b8209239a54d25ec39d93f9ccb9805</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_cache_flush</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gacb05635d95f946f099a1772c8bd59c8b</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_cache_flush_all_data</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gadf1f289d1cf35bd31df930bc347fb9ce</anchor>
      <arglist>(MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_cache_is_enabled</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gac0558236c9fb4cfc333f413c0ca191a9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_cache_set</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga82dceaf1ad42b9a0ab4ed22f9c03b1a8</anchor>
      <arglist>(int enabled)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_mpi_data_register</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gac45e4d77c3d077955bcdc6e1bf0e8cd8</anchor>
      <arglist>(data_handle, tag, rank)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_mpi_data_set_rank</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga79ca5db23d267ee7c199864993e434a9</anchor>
      <arglist>(handle, rank)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_set_rank</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gadce715a49b477ef0204be1a199abfddb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_set_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga42225d9bf5ed973056f29f9b86907c8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_get_rank</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga5281ad23b39e64ca254347e89cbb54e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_get_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaaf806ce3131edd757470c7401bd41bd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_mpi_task_build</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga03f635684df3c110aea79ebd59f73165</anchor>
      <arglist>(MPI_Comm comm, struct starpu_codelet *codelet,...)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_task_post_build</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga9c609f3cc579af484808b3e1b8b787d9</anchor>
      <arglist>(MPI_Comm comm, struct starpu_codelet *codelet,...)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_task_insert</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaa823d6398e61516bba887b90ad048914</anchor>
      <arglist>(MPI_Comm comm, struct starpu_codelet *codelet,...)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_insert_task</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga04229edbc905be8eefce8fb58bfccf8f</anchor>
      <arglist>(MPI_Comm comm, struct starpu_codelet *codelet,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_get_data_on_node</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga0ca9fc89dc1315bf32f38cd14caf24a5</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle, int node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_get_data_on_node_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga6c581f7133773a98b583e0c51970ecb7</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle, int node, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_data_register_comm</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga83339cfcecff5b7e64f85e3e391aa783</anchor>
      <arglist>(starpu_data_handle_t data_handle, int tag, int rank, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_data_set_rank_comm</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga7d4b05715446d730022ea8a1fbac4f60</anchor>
      <arglist>(starpu_data_handle_t handle, int rank, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_data_set_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga5eb9762df37404b659a49daec17e9c82</anchor>
      <arglist>(starpu_data_handle_t handle, int tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_data_get_rank</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga0a464b972068bac652622d6a1ed0a996</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_data_get_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gacf1e74720ea6884fd8d1ded3cb4eaae9</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_register_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gab69b1f823043d16b7d0d799d1ad88a04</anchor>
      <arglist>(starpu_mpi_select_node_policy_func_t policy_func)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_unregister_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gad6453ef7697756dd3b1746399eec4867</anchor>
      <arglist>(int policy)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_get_current_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga9eb17d58fc8f0c33fc4e5985d440079e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_set_current_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga35f7d1ea8e747cbf1637301b14ef0db8</anchor>
      <arglist>(int policy)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_redux_data</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga565996704c6707a55410488f8d569357</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_scatter_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga39ae075749aef08653ade368b9a95ee0</anchor>
      <arglist>(starpu_data_handle_t *data_handles, int count, int root, MPI_Comm comm, void(*scallback)(void *), void *sarg, void(*rcallback)(void *), void *rarg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_gather_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaf83405a3154137f00ccf554c5e5684b5</anchor>
      <arglist>(starpu_data_handle_t *data_handles, int count, int root, MPI_Comm comm, void(*scallback)(void *), void *sarg, void(*rcallback)(void *), void *rarg)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_opencl.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__opencl_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <class kind="struct">starpu_opencl_program</class>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_context</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga7ad0ab374a65417ae3d3a9ceed8f24c4</anchor>
      <arglist>(int devid, cl_context *context)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_device</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga84a524eaac758722f083ee129a19a567</anchor>
      <arglist>(int devid, cl_device_id *device)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_queue</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga8318641373e06a2a635cb3ab377c5994</anchor>
      <arglist>(int devid, cl_command_queue *queue)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_current_context</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gad18d6a0dfbfadcb0fe0b2be6294aa87d</anchor>
      <arglist>(cl_context *context)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_current_queue</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gab43c1f30361aaa72ca98a9a9fdec792b</anchor>
      <arglist>(cl_command_queue *queue)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_set_kernel_args</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga80f968f210417eb9fbf2bfe82e1953a9</anchor>
      <arglist>(cl_int *err, cl_kernel *kernel,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_load_program_source</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga7dd3784262c0be223394bc8c2fe81935</anchor>
      <arglist>(const char *source_file_name, char *located_file_name, char *located_dir_name, char *opencl_program_source)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_load_program_source_malloc</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gadc4689d1ad238ba0b296e205e3bb6317</anchor>
      <arglist>(const char *source_file_name, char **located_file_name, char **located_dir_name, char **opencl_program_source)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_compile_opencl_from_file</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga03f34ac98495afc5d0f268e366f80598</anchor>
      <arglist>(const char *source_file_name, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_compile_opencl_from_string</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gac543428352d04b3ff8f735cfc71c3b99</anchor>
      <arglist>(const char *opencl_program_source, const char *file_name, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_binary_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga893f462bfed53eff3a54f341488db7ad</anchor>
      <arglist>(const char *kernel_id, struct starpu_opencl_program *opencl_programs)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_opencl_from_file</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga3441dd1c6e61717dd81a3a6eb0bf75a8</anchor>
      <arglist>(const char *source_file_name, struct starpu_opencl_program *opencl_programs, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_opencl_from_string</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga9768e086b947961c9db5199e676f05db</anchor>
      <arglist>(const char *opencl_program_source, struct starpu_opencl_program *opencl_programs, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_unload_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gaa38e7cb3231ed30303e50f46c8f6e39c</anchor>
      <arglist>(struct starpu_opencl_program *opencl_programs)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_kernel</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga1d06b6c00b15f4fcd8d4c0c998f955ac</anchor>
      <arglist>(cl_kernel *kernel, cl_command_queue *queue, struct starpu_opencl_program *opencl_programs, const char *kernel_name, int devid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_release_kernel</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga68adf424491ec715e891000fa4a6030d</anchor>
      <arglist>(cl_kernel kernel)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_collect_stats</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gaeaabc8e5d90531a21a8307c06c659984</anchor>
      <arglist>(cl_event event)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_context</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga7ad0ab374a65417ae3d3a9ceed8f24c4</anchor>
      <arglist>(int devid, cl_context *context)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_device</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga84a524eaac758722f083ee129a19a567</anchor>
      <arglist>(int devid, cl_device_id *device)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_queue</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga8318641373e06a2a635cb3ab377c5994</anchor>
      <arglist>(int devid, cl_command_queue *queue)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_current_context</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gad18d6a0dfbfadcb0fe0b2be6294aa87d</anchor>
      <arglist>(cl_context *context)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_current_queue</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gab43c1f30361aaa72ca98a9a9fdec792b</anchor>
      <arglist>(cl_command_queue *queue)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_set_kernel_args</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga80f968f210417eb9fbf2bfe82e1953a9</anchor>
      <arglist>(cl_int *err, cl_kernel *kernel,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_load_program_source</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga7dd3784262c0be223394bc8c2fe81935</anchor>
      <arglist>(const char *source_file_name, char *located_file_name, char *located_dir_name, char *opencl_program_source)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_load_program_source_malloc</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gadc4689d1ad238ba0b296e205e3bb6317</anchor>
      <arglist>(const char *source_file_name, char **located_file_name, char **located_dir_name, char **opencl_program_source)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_compile_opencl_from_file</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga03f34ac98495afc5d0f268e366f80598</anchor>
      <arglist>(const char *source_file_name, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_compile_opencl_from_string</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gac543428352d04b3ff8f735cfc71c3b99</anchor>
      <arglist>(const char *opencl_program_source, const char *file_name, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_binary_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga893f462bfed53eff3a54f341488db7ad</anchor>
      <arglist>(const char *kernel_id, struct starpu_opencl_program *opencl_programs)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_opencl_from_file</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga3441dd1c6e61717dd81a3a6eb0bf75a8</anchor>
      <arglist>(const char *source_file_name, struct starpu_opencl_program *opencl_programs, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_opencl_from_string</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga9768e086b947961c9db5199e676f05db</anchor>
      <arglist>(const char *opencl_program_source, struct starpu_opencl_program *opencl_programs, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_unload_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gaa38e7cb3231ed30303e50f46c8f6e39c</anchor>
      <arglist>(struct starpu_opencl_program *opencl_programs)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_kernel</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga1d06b6c00b15f4fcd8d4c0c998f955ac</anchor>
      <arglist>(cl_kernel *kernel, cl_command_queue *queue, struct starpu_opencl_program *opencl_programs, const char *kernel_name, int devid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_release_kernel</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga68adf424491ec715e891000fa4a6030d</anchor>
      <arglist>(cl_kernel kernel)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_collect_stats</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gaeaabc8e5d90531a21a8307c06c659984</anchor>
      <arglist>(cl_event event)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL_DISPLAY_ERROR</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gae023af317c4040f926d2c50a8f96b5d2</anchor>
      <arglist>(status)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL_REPORT_ERROR</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga6680ed21ce09f073fa0256169f4e4868</anchor>
      <arglist>(status)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL_REPORT_ERROR_WITH_MSG</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga23fa324d87f923f39f05bfdc9d3fe7e9</anchor>
      <arglist>(msg, status)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>starpu_opencl_error_string</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga7f8e1507dec24eaa7427923bbacb873f</anchor>
      <arglist>(cl_int status)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_display_error</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga9e0073e4c839eec6096f6eeca21d7e36</anchor>
      <arglist>(const char *func, const char *file, int line, const char *msg, cl_int status)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static __starpu_inline void</type>
      <name>starpu_opencl_report_error</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga15f6e4c82407cd3c16e461fc2f129a40</anchor>
      <arglist>(const char *func, const char *file, int line, const char *msg, cl_int status)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_allocate_memory</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga2708694c061fd89272b0355bcaac11df</anchor>
      <arglist>(int devid, cl_mem *addr, size_t size, cl_mem_flags flags)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_copy_ram_to_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga9a1331f26cc54a1f5406770f68b95d29</anchor>
      <arglist>(void *ptr, unsigned src_node, cl_mem buffer, unsigned dst_node, size_t size, size_t offset, cl_event *event, int *ret)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_copy_opencl_to_ram</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga2bd30ada22e761a6b889f0661b54592f</anchor>
      <arglist>(cl_mem buffer, unsigned src_node, void *ptr, unsigned dst_node, size_t size, size_t offset, cl_event *event, int *ret)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_copy_opencl_to_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga002e85a0f6bcd087df54287de5ae5e07</anchor>
      <arglist>(cl_mem src, unsigned src_node, size_t src_offset, cl_mem dst, unsigned dst_node, size_t dst_offset, size_t size, cl_event *event, int *ret)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_copy_async_sync</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga95184b2188d67976e00bcb4637ac1933</anchor>
      <arglist>(uintptr_t src, size_t src_offset, unsigned src_node, uintptr_t dst, size_t dst_offset, unsigned dst_node, size_t size, cl_event *event)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_openmp.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__openmp_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <class kind="struct">starpu_omp_lock_t</class>
    <class kind="struct">starpu_omp_nest_lock_t</class>
    <class kind="struct">starpu_omp_parallel_region_attr</class>
    <class kind="struct">starpu_omp_task_region_attr</class>
    <member kind="enumeration">
      <type></type>
      <name>starpu_omp_sched_value</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga33af06875785da29f3988d3a985e99f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_undefined</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a2b1faa3fe1b297e1d8b2c87bb804a46f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_static</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8ab6df9d00a7ef9f3acce6508d5b24079e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_dynamic</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a74f498a75f2ba89ef620a761bc2f1696</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_guided</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a08bd52bc5cc5af8ec937618f1402bb4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_auto</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8aa2a597e81534c3a170429c535b970466</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_runtime</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a9f23fc3844c5fbe86ba2b09ce4c73f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_omp_proc_bind_value</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gacc6078a78820c367f07d37da11c04520</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_undefined</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520ac9fd1a7f2323bf9cd55ef6ca44b09dea</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_false</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a86961832c7cc5d9f7821143e1c185257</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_true</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a645c937c29cc24aabe9fb1af0be00cfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_master</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520aee55c45804c821880591cf98989daa72</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_close</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a92928e124254f30c345ebf8c616e2846</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_spread</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a225aa415ff421aba9f93f09b8201725e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_init</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa5c38dfec313088ca5e527a97f40969d</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_shutdown</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa4b70af0a6cf757e05e300c5eeea0a46</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_parallel_region</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga41d9b4f1e57c02d34c4bd6e53469c3fc</anchor>
      <arglist>(const struct starpu_omp_parallel_region_attr *attr) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_master</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga3be0d3b2145764db53eb59001ac74e2b</anchor>
      <arglist>(void(*f)(void *arg), void *arg) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_master_inline</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gac9613ed1365bf0db54b6225a1d2c6124</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_barrier</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gac9279b86d3c79fc918856bf1f9cde7d6</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_critical</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga9f66990dcc824f3078afbba559817c8d</anchor>
      <arglist>(void(*f)(void *arg), void *arg, const char *name) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_critical_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaaf0d71971d1ab1b0d4f9919830693d98</anchor>
      <arglist>(const char *name) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_critical_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2f1f0d70df3de43bf9069eb4c2e1ca9f</anchor>
      <arglist>(const char *name) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_single</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga9db57e7c1c67a02837b279145fb1883d</anchor>
      <arglist>(void(*f)(void *arg), void *arg, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_single_inline</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa3127cc9d95397b4f066cef20f9ef99d</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_single_copyprivate</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga5790ead92feacb71e3882de730170b05</anchor>
      <arglist>(void(*f)(void *arg, void *data, unsigned long long data_size), void *arg, void *data, unsigned long long data_size) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_omp_single_copyprivate_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gabe2275b4c4295825181e559affb6dcf2</anchor>
      <arglist>(void *data) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_single_copyprivate_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga7082f9fb91da015537bf7e65611c9db6</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_for</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga343f59fea8ebbd23c73fcac25571ca20</anchor>
      <arglist>(void(*f)(unsigned long long _first_i, unsigned long long _nb_i, void *arg), void *arg, unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_first</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga896cbd300cf111eb09251995b0576533</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_first_i, unsigned long long *_nb_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_next</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga6551d8b2707c8808c78eb97955928bc8</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_first_i, unsigned long long *_nb_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_for_alt</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2c2739d7b7a1702540b878684b8e4e9b</anchor>
      <arglist>(void(*f)(unsigned long long _begin_i, unsigned long long _end_i, void *arg), void *arg, unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_first_alt</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga698809a6ff7858a8f26e96635b5efd7e</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_begin_i, unsigned long long *_end_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_next_alt</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga0a2d26b924681a81e92562edf7fa6742</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_begin_i, unsigned long long *_end_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_ordered_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga82338737b7712d171bbbf6125bac33d3</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_ordered_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1080ae6a620b2c40c71311df32ab44d4</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_ordered</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gad8abcefdd1731fe8269d833e8103a945</anchor>
      <arglist>(void(*f)(void *arg), void *arg) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_sections</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga544807d42cb7522f8441f1a499f203d3</anchor>
      <arglist>(unsigned long long nb_sections, void(**section_f)(void *arg), void **section_arg, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_sections_combined</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga650f12381df2c525c058e6c137930e50</anchor>
      <arglist>(unsigned long long nb_sections, void(*section_f)(unsigned long long section_num, void *arg), void *section_arg, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_task_region</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gadaf4576512b9ecc58c71cb36a87105b7</anchor>
      <arglist>(const struct starpu_omp_task_region_attr *attr) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskwait</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga82292171a04e7de120b878ea52e32bd6</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskgroup</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga6561f2bb3fd370901216986ae8692cc7</anchor>
      <arglist>(void(*f)(void *arg), void *arg) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskgroup_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa63576df9729394a3d53137d7021bab5</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskgroup_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga541bb9f73081830e3173cf71d57bee94</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_num_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2c980c583e9faf124b4446fa90b5ece9</anchor>
      <arglist>(int threads) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga3f9e6a6eff37b7bde50539551d2f40f7</anchor>
      <arglist>() __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_thread_num</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gacc19dd986f7ad6aac6ac24f5f86aa912</anchor>
      <arglist>() __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_max_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga417fc9433682c4817f35623bb2c9dafd</anchor>
      <arglist>() __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_procs</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gacc9c3792ea07d0bf694d8b0c8b8c5fb7</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_in_parallel</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gae134345a048428661b344b671155cdd9</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_dynamic</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga3cc840689933be3d9e7952eb76e0f44e</anchor>
      <arglist>(int dynamic_threads) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_dynamic</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa15212ca3137d7aba827794b26fee9e0</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_nested</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga7d6a35f049f4c449c02680f480239754</anchor>
      <arglist>(int nested) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_nested</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1118d277474ac2dc1a22bf6ef4c59fa9</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_cancellation</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gabbd8c55707eb2173324c7dcfe04832f3</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_schedule</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga906913c3eb454506682a130c6bcd6337</anchor>
      <arglist>(enum starpu_omp_sched_value kind, int modifier) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_get_schedule</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8de196a9b2248ee34f9183a081da8be5</anchor>
      <arglist>(enum starpu_omp_sched_value *kind, int *modifier) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_thread_limit</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga05ecbf96c69f19525730f46d49d92937</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_max_active_levels</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga846b449fccf725b83acf1c3b14f3c740</anchor>
      <arglist>(int max_levels) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_max_active_levels</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gafe303ff7d26bcd668959ee967eedda97</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_level</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga51dbabbff069511b043833847fb45f30</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_ancestor_thread_num</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1b951f0922d794e6deb2579c81bcf41b</anchor>
      <arglist>(int level) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_team_size</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1a6d1009b6ff1f68617ade03554d03f3</anchor>
      <arglist>(int level) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_active_level</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2cb37eec19b8a24242819e666ecea8fc</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_in_final</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaac482422d2b9bc26c1877f49eaa5fca2</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_omp_proc_bind_value</type>
      <name>starpu_omp_get_proc_bind</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga478c43dbbdd549ebe481610fd42e1b92</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_default_device</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga409d48dc7e7158cb7cbdaca31a4b0a27</anchor>
      <arglist>(int device_num) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_default_device</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga5a6c050bf14b81d49716946d68e1d7f9</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_devices</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga63f94bc02b7c56c48bd3cbd0a38796ee</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_teams</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1b1d7573040775370ffd49b7accad0f2</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_team_num</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaff3980856fe3f8ebded80d72ef13ab78</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_is_initial_device</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaede763d0f3b47343eb1a5e64ceec0062</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_init_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8969523514417ac253a9007ffb1eabe0</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_destroy_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga58fb716554b6a79a3207ba114c49abdb</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gadca05b4b67afb0c82cdd39c4773b8fce</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_unset_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaf820ec6afd10011a711cfe1140f2789c</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_test_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gabba6e6a047b80568ba0c98d725290650</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_init_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga444e5466c7f457ace4e9aa78cf27e6ef</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_destroy_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga0fb5be52e8f7630a4f24c2e844a22519</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8c913c32ba007ce53cd3c3cfcecf2342</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_unset_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaf704c62ed129ed488c4bfd3567ef244d</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_test_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gab2727bdd191a5d1c495dc01ccf9872f0</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_atomic_fallback_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga86414f0d5062cf47d64b0599bcdd1ccf</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_atomic_fallback_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gad8189d962807a8504a5fad8cc2bf58ba</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_omp_get_wtime</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaedec99a0472438a5da0c014a47087878</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_omp_get_wtick</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga706d36f77512013d4e98e8f42da02ff3</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_vector_annotate</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8bc9734bc0f9fb5485b4708b4f3ea219</anchor>
      <arglist>(starpu_data_handle_t handle, uint32_t slice_base) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_init</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa5c38dfec313088ca5e527a97f40969d</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_shutdown</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa4b70af0a6cf757e05e300c5eeea0a46</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_parallel_region</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga41d9b4f1e57c02d34c4bd6e53469c3fc</anchor>
      <arglist>(const struct starpu_omp_parallel_region_attr *attr) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_master</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga3be0d3b2145764db53eb59001ac74e2b</anchor>
      <arglist>(void(*f)(void *arg), void *arg) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_master_inline</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gac9613ed1365bf0db54b6225a1d2c6124</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_barrier</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gac9279b86d3c79fc918856bf1f9cde7d6</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_critical</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga9f66990dcc824f3078afbba559817c8d</anchor>
      <arglist>(void(*f)(void *arg), void *arg, const char *name) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_critical_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaaf0d71971d1ab1b0d4f9919830693d98</anchor>
      <arglist>(const char *name) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_critical_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2f1f0d70df3de43bf9069eb4c2e1ca9f</anchor>
      <arglist>(const char *name) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_single</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga9db57e7c1c67a02837b279145fb1883d</anchor>
      <arglist>(void(*f)(void *arg), void *arg, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_single_inline</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa3127cc9d95397b4f066cef20f9ef99d</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_single_copyprivate</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga5790ead92feacb71e3882de730170b05</anchor>
      <arglist>(void(*f)(void *arg, void *data, unsigned long long data_size), void *arg, void *data, unsigned long long data_size) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_omp_single_copyprivate_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gabe2275b4c4295825181e559affb6dcf2</anchor>
      <arglist>(void *data) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_single_copyprivate_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga7082f9fb91da015537bf7e65611c9db6</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_for</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga343f59fea8ebbd23c73fcac25571ca20</anchor>
      <arglist>(void(*f)(unsigned long long _first_i, unsigned long long _nb_i, void *arg), void *arg, unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_first</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga896cbd300cf111eb09251995b0576533</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_first_i, unsigned long long *_nb_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_next</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga6551d8b2707c8808c78eb97955928bc8</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_first_i, unsigned long long *_nb_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_for_alt</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2c2739d7b7a1702540b878684b8e4e9b</anchor>
      <arglist>(void(*f)(unsigned long long _begin_i, unsigned long long _end_i, void *arg), void *arg, unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_first_alt</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga698809a6ff7858a8f26e96635b5efd7e</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_begin_i, unsigned long long *_end_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_next_alt</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga0a2d26b924681a81e92562edf7fa6742</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_begin_i, unsigned long long *_end_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_ordered_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga82338737b7712d171bbbf6125bac33d3</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_ordered_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1080ae6a620b2c40c71311df32ab44d4</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_ordered</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gad8abcefdd1731fe8269d833e8103a945</anchor>
      <arglist>(void(*f)(void *arg), void *arg) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_sections</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga544807d42cb7522f8441f1a499f203d3</anchor>
      <arglist>(unsigned long long nb_sections, void(**section_f)(void *arg), void **section_arg, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_sections_combined</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga650f12381df2c525c058e6c137930e50</anchor>
      <arglist>(unsigned long long nb_sections, void(*section_f)(unsigned long long section_num, void *arg), void *section_arg, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_task_region</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gadaf4576512b9ecc58c71cb36a87105b7</anchor>
      <arglist>(const struct starpu_omp_task_region_attr *attr) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskwait</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga82292171a04e7de120b878ea52e32bd6</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskgroup</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga6561f2bb3fd370901216986ae8692cc7</anchor>
      <arglist>(void(*f)(void *arg), void *arg) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskgroup_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa63576df9729394a3d53137d7021bab5</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskgroup_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga541bb9f73081830e3173cf71d57bee94</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_num_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2c980c583e9faf124b4446fa90b5ece9</anchor>
      <arglist>(int threads) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga3f9e6a6eff37b7bde50539551d2f40f7</anchor>
      <arglist>() __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_thread_num</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gacc19dd986f7ad6aac6ac24f5f86aa912</anchor>
      <arglist>() __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_max_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga417fc9433682c4817f35623bb2c9dafd</anchor>
      <arglist>() __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_procs</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gacc9c3792ea07d0bf694d8b0c8b8c5fb7</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_in_parallel</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gae134345a048428661b344b671155cdd9</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_dynamic</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga3cc840689933be3d9e7952eb76e0f44e</anchor>
      <arglist>(int dynamic_threads) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_dynamic</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa15212ca3137d7aba827794b26fee9e0</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_nested</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga7d6a35f049f4c449c02680f480239754</anchor>
      <arglist>(int nested) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_nested</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1118d277474ac2dc1a22bf6ef4c59fa9</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_cancellation</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gabbd8c55707eb2173324c7dcfe04832f3</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_schedule</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga906913c3eb454506682a130c6bcd6337</anchor>
      <arglist>(enum starpu_omp_sched_value kind, int modifier) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_get_schedule</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8de196a9b2248ee34f9183a081da8be5</anchor>
      <arglist>(enum starpu_omp_sched_value *kind, int *modifier) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_thread_limit</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga05ecbf96c69f19525730f46d49d92937</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_max_active_levels</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga846b449fccf725b83acf1c3b14f3c740</anchor>
      <arglist>(int max_levels) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_max_active_levels</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gafe303ff7d26bcd668959ee967eedda97</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_level</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga51dbabbff069511b043833847fb45f30</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_ancestor_thread_num</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1b951f0922d794e6deb2579c81bcf41b</anchor>
      <arglist>(int level) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_team_size</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1a6d1009b6ff1f68617ade03554d03f3</anchor>
      <arglist>(int level) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_active_level</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2cb37eec19b8a24242819e666ecea8fc</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_in_final</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaac482422d2b9bc26c1877f49eaa5fca2</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_omp_proc_bind_value</type>
      <name>starpu_omp_get_proc_bind</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga478c43dbbdd549ebe481610fd42e1b92</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_default_device</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga409d48dc7e7158cb7cbdaca31a4b0a27</anchor>
      <arglist>(int device_num) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_default_device</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga5a6c050bf14b81d49716946d68e1d7f9</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_devices</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga63f94bc02b7c56c48bd3cbd0a38796ee</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_teams</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1b1d7573040775370ffd49b7accad0f2</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_team_num</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaff3980856fe3f8ebded80d72ef13ab78</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_is_initial_device</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaede763d0f3b47343eb1a5e64ceec0062</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_init_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8969523514417ac253a9007ffb1eabe0</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_destroy_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga58fb716554b6a79a3207ba114c49abdb</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gadca05b4b67afb0c82cdd39c4773b8fce</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_unset_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaf820ec6afd10011a711cfe1140f2789c</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_test_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gabba6e6a047b80568ba0c98d725290650</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_init_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga444e5466c7f457ace4e9aa78cf27e6ef</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_destroy_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga0fb5be52e8f7630a4f24c2e844a22519</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8c913c32ba007ce53cd3c3cfcecf2342</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_unset_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaf704c62ed129ed488c4bfd3567ef244d</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_test_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gab2727bdd191a5d1c495dc01ccf9872f0</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_atomic_fallback_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga86414f0d5062cf47d64b0599bcdd1ccf</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_atomic_fallback_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gad8189d962807a8504a5fad8cc2bf58ba</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_omp_get_wtime</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaedec99a0472438a5da0c014a47087878</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_omp_get_wtick</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga706d36f77512013d4e98e8f42da02ff3</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_vector_annotate</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8bc9734bc0f9fb5485b4708b4f3ea219</anchor>
      <arglist>(starpu_data_handle_t handle, uint32_t slice_base) __STARPU_OMP_NOTHROW</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_perfmodel.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__perfmodel_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <includes id="starpu__util_8h" name="starpu_util.h" local="no" imported="no">starpu_util.h</includes>
    <includes id="starpu__worker_8h" name="starpu_worker.h" local="no" imported="no">starpu_worker.h</includes>
    <class kind="struct">starpu_perfmodel_device</class>
    <class kind="struct">starpu_perfmodel_arch</class>
    <class kind="struct">starpu_perfmodel_history_entry</class>
    <class kind="struct">starpu_perfmodel_history_list</class>
    <class kind="struct">starpu_perfmodel_regression_model</class>
    <class kind="struct">starpu_perfmodel_per_arch</class>
    <class kind="struct">starpu_perfmodel</class>
    <member kind="enumeration">
      <type></type>
      <name>starpu_perfmodel_type</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gae161a7cae376f3fc831a2b764e8144e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_PER_ARCH</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6ae4b97f418bd3496f23889a6950d7be82</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_COMMON</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6aeedb2a69541e9b7380f766711f8eab26</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_HISTORY_BASED</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6a61a1a08e950abce6779d8eaee6cfe395</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_REGRESSION_BASED</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6ab46450dd0cc93e3aaed1900afdcad26f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_NL_REGRESSION_BASED</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6a4b1935cc6389974fe69120e978aa3a29</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_init</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga299bb26d02dfb4802c633d06b8b5ff8b</anchor>
      <arglist>(struct starpu_perfmodel *model)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_load_file</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gaf02aaa9dd29cab640329f496aaefcc65</anchor>
      <arglist>(const char *filename, struct starpu_perfmodel *model)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_load_symbol</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga5ab7213f5c229c16e783004853fcba4c</anchor>
      <arglist>(const char *symbol, struct starpu_perfmodel *model)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_unload_model</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga96746e1665eeffec32a82a465325f7dc</anchor>
      <arglist>(struct starpu_perfmodel *model)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_free_sampling_directories</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga4da8a63a264195f54ccaa9f49d188897</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_perfmodel_arch *</type>
      <name>starpu_worker_get_perf_archtype</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gadb2ada1d388826505ff79352a469cac3</anchor>
      <arglist>(int workerid, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_debugfilepath</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga2ec4c9ec1ac320aae99e31871bfed90d</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_perfmodel_arch *arch, char *path, size_t maxlen, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>starpu_perfmodel_get_archtype_name</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga8b39921ed28de141fb99a79415b8c489</anchor>
      <arglist>(enum starpu_worker_archtype archtype)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_get_arch_name</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga73e84b64af1ce96eec9b1c83e7468c60</anchor>
      <arglist>(struct starpu_perfmodel_arch *arch, char *archname, size_t maxlen, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_perfmodel_history_based_expected_perf</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga03fc571c3f6132339a75004899f577af</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_perfmodel_arch *arch, uint32_t footprint)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_list</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga7562bd09e4894eeb15391f2a0f21cf00</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_print</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga19aebb6aee49b5760d4546d0044c87ea</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_perfmodel_arch *arch, unsigned nimpl, char *parameter, uint32_t *footprint, FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_print_all</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gab70e8b18742e868e5f6505814938bdd1</anchor>
      <arglist>(struct starpu_perfmodel *model, char *arch, char *parameter, uint32_t *footprint, FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_update_history</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gaecb9341bff471557abbb63a966449481</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned cpuid, unsigned nimpl, double measured)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_directory</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gae99274bc3bc2d6692c0e22113971b9ef</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bus_print_bandwidth</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gac4075eddef3625eccbbc9f1670c69244</anchor>
      <arglist>(FILE *f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bus_print_affinity</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga712a3e3498f113efb81c42c6b7a48335</anchor>
      <arglist>(FILE *f)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_transfer_bandwidth</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga767f88b087d4b0bf4086ade9bb673826</anchor>
      <arglist>(unsigned src_node, unsigned dst_node)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_transfer_latency</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga77915973bf2f03bd70fb84ee4c9c51a0</anchor>
      <arglist>(unsigned src_node, unsigned dst_node)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_transfer_predict</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga9a4af8d1b5a3486a5a21c72dd4a6c4c2</anchor>
      <arglist>(unsigned src_node, unsigned dst_node, size_t size)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_profiling.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__profiling_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <includes id="starpu__util_8h" name="starpu_util.h" local="no" imported="no">starpu_util.h</includes>
    <class kind="struct">starpu_profiling_task_info</class>
    <class kind="struct">starpu_profiling_worker_info</class>
    <class kind="struct">starpu_profiling_bus_info</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PROFILING_DISABLE</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gacd7d4b9e0d31ea949f7c14fed739a502</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PROFILING_ENABLE</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga24254aaa82ee94c3e6b8b2e21185db32</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_profiling_init</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga6ab0a1e4a8a55e0c54e2151fd0a82a36</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_profiling_set_id</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gad126fc77826a0472b6bd872e7ceb40d4</anchor>
      <arglist>(int new_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_profiling_status_set</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gabeb22bbe8062a45507cfc6273aae51ae</anchor>
      <arglist>(int status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_profiling_status_get</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gaf014e2e050ebd38bc4cffec0081f96bf</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_profiling_worker_get_info</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga8a423df93ec48a7391b2f439357a5544</anchor>
      <arglist>(int workerid, struct starpu_profiling_worker_info *worker_info)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_count</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga8dc4665fdbbbf8ce8f1b905aa8411642</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_id</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga7c70803a54aa1e5b463ebb19110c4aa2</anchor>
      <arglist>(int src, int dst)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_src</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga858f465a2dcbcadb6169a7926e38a4d1</anchor>
      <arglist>(int busid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_dst</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga8ded3aa2a94ec7a5cd46e2e70a918c65</anchor>
      <arglist>(int busid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_profiling_info</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga76ad19fc8b99bcd42d505c76aafae6b6</anchor>
      <arglist>(int busid, struct starpu_profiling_bus_info *bus_info)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_timing_timespec_delay_us</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gacf0b5499290848f07c97a32c6c2412db</anchor>
      <arglist>(struct timespec *start, struct timespec *end)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_timing_timespec_to_us</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga87639260ff5c89f466e83fcc093e77fe</anchor>
      <arglist>(struct timespec *ts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_profiling_bus_helper_display_summary</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga6511f50290edcabbdf605a1b74746645</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_profiling_worker_helper_display_summary</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga4e4f9fb593d29219a1109062ba9d72ef</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_rand.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__rand_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
  </compound>
  <compound kind="file">
    <name>starpu_scc.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__scc_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <member kind="typedef">
      <type>void *</type>
      <name>starpu_scc_func_symbol_t</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>ga70c024e24bd926107296a279fde13538</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_scc_register_kernel</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>ga32bb32785d9fd5f0ab47ed53c7db643f</anchor>
      <arglist>(starpu_scc_func_symbol_t *symbol, const char *func_name)</arglist>
    </member>
    <member kind="function">
      <type>starpu_scc_kernel_t</type>
      <name>starpu_scc_get_kernel</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>gaf0d2b4ba0e50de56c994f9d351efdca3</anchor>
      <arglist>(starpu_scc_func_symbol_t symbol)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_sched_ctx.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__sched__ctx_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_get_min_priority</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga6d8a3d42b5f2fee81785b3cf650fa6d2</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_get_max_priority</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga6c2233ec86c827981117306f63da7679</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_set_min_priority</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gace525e755092440f63acbe5cc0b4d066</anchor>
      <arglist>(int min_prio)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_set_max_priority</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gab8e4a2cd9f4fbad7b20b1086494d842d</anchor>
      <arglist>(int max_prio)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_worker_collection *</type>
      <name>starpu_sched_ctx_create_worker_collection</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae3c8990276aaca0a3b44e3177ab3e0fb</anchor>
      <arglist>(unsigned sched_ctx_id, enum starpu_worker_collection_type type) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_delete_worker_collection</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga5de2e1e6295cf1fa5723d1164d0a76e6</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_worker_collection *</type>
      <name>starpu_sched_ctx_get_worker_collection</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga6e559a6889a89ec0b9a3b995a41f5f8f</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_policy_data</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga20077926e6cd8264bcbb831a394c381a</anchor>
      <arglist>(unsigned sched_ctx_id, void *policy_data)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_sched_ctx_get_policy_data</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaacd0aff652dc75ac37aa51c9c826e043</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_sched_ctx_exec_parallel_code</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaf9a95bcf18710622750367c0ba6acbb4</anchor>
      <arglist>(void *(*func)(void *), void *param, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_get_nready_tasks</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga5162ab4e7c0775a4357f951122784cb3</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_sched_ctx_get_nready_flops</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga9cd4e3a034cb9ff6eb8281e449d395c9</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_call_pushed_task_cb</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga62867177a48ccd40ac944602630cd8f0</anchor>
      <arglist>(int workerid, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX_POLICY_NAME</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga66ed9279fc4eb746a0596c2f0be7daa2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX_POLICY_STRUCT</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga9cdf99b6998b774eb53a8cc845f4e597</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX_POLICY_MIN_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga80f88b1017cfde58c18381a52ba0a122</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX_POLICY_MAX_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga80c723b1a18a2104a28320430d8e8489</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_create</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga517b7cf4bfac60f2faf484584f19d9d3</anchor>
      <arglist>(int *workerids_ctx, int nworkers_ctx, const char *sched_ctx_name,...)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_create_inside_interval</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga1fbdfe8bacedde944e4c39e4bb168651</anchor>
      <arglist>(const char *policy_name, const char *sched_ctx_name, int min_ncpus, int max_ncpus, int min_ngpus, int max_ngpus, unsigned allow_overlap)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_register_close_callback</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaadbcaf9118792338aafe480da6fc6d94</anchor>
      <arglist>(unsigned sched_ctx_id, void(*close_callback)(unsigned sched_ctx_id, void *args), void *args)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_add_workers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga4938f463f59aaea3f0b766c5da770fb9</anchor>
      <arglist>(int *workerids_ctx, int nworkers_ctx, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_remove_workers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga5fced8ae787486d906b057c20dfe5926</anchor>
      <arglist>(int *workerids_ctx, int nworkers_ctx, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_display_workers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga02858b688df9fed9e593040de0d28357</anchor>
      <arglist>(unsigned sched_ctx_id, FILE *f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_delete</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga101c3a13d4adc8d932c46324ae71b93a</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_inheritor</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gad6b7f96254526bb145e0df5ed0354a13</anchor>
      <arglist>(unsigned sched_ctx_id, unsigned inheritor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_context</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga4e1aaf03ba327f47e5027c6fb7e5ed00</anchor>
      <arglist>(unsigned *sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_get_context</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae6b235a9555a74e5cb4c89ca5117b419</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_stop_task_submission</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gafe295aec3c152fe1fc0dd74e17f82961</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_finished_submit</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga11e94f2968626d9b46ccf021057ddd90</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_get_workers_list</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga182b6196b075139f611cfd34b42b86e8</anchor>
      <arglist>(unsigned sched_ctx_id, int **workerids)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_get_nworkers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga18af575377ee066528081b5aec754ea2</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_get_nshared_workers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae00f5710446d14161beefdad2e48833d</anchor>
      <arglist>(unsigned sched_ctx_id, unsigned sched_ctx_id2)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_contains_worker</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga0fe5c1b14fc254c8b2a737402870a149</anchor>
      <arglist>(int workerid, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_worker_get_id</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaa06e10ab1edfe31d7da2f3359630e6f4</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_overlapping_ctxs_on_worker</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga66d2d4d08df845a40b7d19f12bf791c0</anchor>
      <arglist>(int workerid)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MIN_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga7988c6119e176957987baac65f4fc62c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAX_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaf213c18d752d78286cff68a379ee7611</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_DEFAULT_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gabd6c53517107a2b467bdbc8af21cee2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_get_min_priority</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga6ae38faa632f496d81f0cf3b0cec5c77</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_get_max_priority</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga1cfc7b78954e33f2738547cbf05e820e</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_set_min_priority</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga6b8a02442fc4b18ae7f1c67a1a4add35</anchor>
      <arglist>(unsigned sched_ctx_id, int min_prio)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_set_max_priority</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga1945cfb563891ad94861e5a5f6b01495</anchor>
      <arglist>(unsigned sched_ctx_id, int max_prio)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_min_priority_is_set</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gadaa6d7d42fb01d94aece1c82efb020a3</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_max_priority_is_set</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga56c083b8731592e1c51f5e81669a8eb5</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_worker_collection *</type>
      <name>starpu_sched_ctx_create_worker_collection</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae3c8990276aaca0a3b44e3177ab3e0fb</anchor>
      <arglist>(unsigned sched_ctx_id, enum starpu_worker_collection_type type) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_delete_worker_collection</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga5de2e1e6295cf1fa5723d1164d0a76e6</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_worker_collection *</type>
      <name>starpu_sched_ctx_get_worker_collection</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga6e559a6889a89ec0b9a3b995a41f5f8f</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_policy_data</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga20077926e6cd8264bcbb831a394c381a</anchor>
      <arglist>(unsigned sched_ctx_id, void *policy_data)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_sched_ctx_get_policy_data</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaacd0aff652dc75ac37aa51c9c826e043</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_sched_ctx_exec_parallel_code</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaf9a95bcf18710622750367c0ba6acbb4</anchor>
      <arglist>(void *(*func)(void *), void *param, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_get_nready_tasks</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga5162ab4e7c0775a4357f951122784cb3</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_sched_ctx_get_nready_flops</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga9cd4e3a034cb9ff6eb8281e449d395c9</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_call_pushed_task_cb</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga62867177a48ccd40ac944602630cd8f0</anchor>
      <arglist>(int workerid, unsigned sched_ctx_id)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_sched_ctx_hypervisor.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__sched__ctx__hypervisor_8h</filename>
    <class kind="struct">starpu_sched_ctx_performance_counters</class>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_perf_counters</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaee24cd7da1cde089e038b93f52d1bf1d</anchor>
      <arglist>(unsigned sched_ctx_id, void *perf_counters)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_notify_hypervisor_exists</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae7de365db4b774cdea340f7548e93c9d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_check_if_hypervisor_exists</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga95283c927a7787e2859cb4a42c93bf19</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_perf_counters</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaee24cd7da1cde089e038b93f52d1bf1d</anchor>
      <arglist>(unsigned sched_ctx_id, void *perf_counters)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_notify_hypervisor_exists</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae7de365db4b774cdea340f7548e93c9d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_check_if_hypervisor_exists</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga95283c927a7787e2859cb4a42c93bf19</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_scheduler.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__scheduler_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <class kind="struct">starpu_sched_policy</class>
    <member kind="function">
      <type>struct starpu_sched_policy **</type>
      <name>starpu_sched_get_predefined_policies</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gaca9fb71cec7aa99dfd7a7a6dcafefb88</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_worker_get_sched_condition</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gadf8e42d41b76117293db5651b0912c78</anchor>
      <arglist>(int workerid, starpu_pthread_mutex_t **sched_mutex, starpu_pthread_cond_t **sched_cond)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_can_execute_task</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga2c45db0c01e86497082c907b9cb0f4cc</anchor>
      <arglist>(unsigned workerid, struct starpu_task *task, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_can_execute_task_impl</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga40338a935c8428167c73e1d5b681b4e5</anchor>
      <arglist>(unsigned workerid, struct starpu_task *task, unsigned *impl_mask)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_can_execute_task_first_impl</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga706830b50d163715c28d2fb19abd13bd</anchor>
      <arglist>(unsigned workerid, struct starpu_task *task, unsigned *nimpl)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_push_local_task</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gae0b8a9bb53822f3d92cb2d19c2520e5e</anchor>
      <arglist>(int workerid, struct starpu_task *task, int back)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_push_task_end</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga24154b37920144de18bca59090429d6d</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_assign_workerid</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga176c8b45655615fbd09f0b70ba8b78b5</anchor>
      <arglist>(int nworkers, int workerid_array[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_get_description</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga720a70f5297ffe3b5ea5cf6eff549c8c</anchor>
      <arglist>(int workerid, int *worker_size, int **combined_workerid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_can_execute_task</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>gae3fe82c940e36715145baa9fe003e453</anchor>
      <arglist>(unsigned workerid, struct starpu_task *task, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_get_prefetch_flag</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga94edfd4a02666d8c1b8c506423586426</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_prefetch_task_input_on_node</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga3bf8df94c9ea3f1370b53fff54e72888</anchor>
      <arglist>(struct starpu_task *task, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_idle_prefetch_task_input_on_node</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga20525c5d674b32a9f64fbc9e23c5f55f</anchor>
      <arglist>(struct starpu_task *task, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_task_footprint</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga21385d19bc75a7581e774bd455c70790</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_task_data_footprint</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gaa859b0ee59cfa2bde4d241f854c3bbea</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_expected_length</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga2dc83adec9e479e967a1d1c2ae40f916</anchor>
      <arglist>(struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_worker_get_relative_speedup</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga87e2aacf5892951d5333a3421e90f278</anchor>
      <arglist>(struct starpu_perfmodel_arch *perf_arch)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_expected_data_transfer_time</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga4faf5df3280a5331bdddd3f878a3024d</anchor>
      <arglist>(unsigned memory_node, struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_data_expected_transfer_time</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga1e159d56ffa2d7705bdfd1ceb93e0d64</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned memory_node, enum starpu_data_access_mode mode)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_expected_energy</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga3e9efb4ca2cbc1c74757e0a9cb0f26c6</anchor>
      <arglist>(struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_expected_conversion_time</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga53b042c111b450ebaa99e26388394ff4</anchor>
      <arglist>(struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_bundle_expected_length</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>ga278de79c7cbeaa68e5239b415cc0d29c</anchor>
      <arglist>(starpu_task_bundle_t bundle, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_bundle_expected_data_transfer_time</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>gaf589986f5e69b7452ba90857d1931594</anchor>
      <arglist>(starpu_task_bundle_t bundle, unsigned memory_node)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_bundle_expected_energy</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>ga4eac9575958c784ce48d2787b908ab3b</anchor>
      <arglist>(starpu_task_bundle_t bundle, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_worker_shares_tasks_lists</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga315482308105aa57ecc59e88f71823aa</anchor>
      <arglist>(int workerid, int sched_ctx_id)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_sink.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__sink_8h</filename>
  </compound>
  <compound kind="file">
    <name>starpu_stdlib.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__stdlib_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MALLOC_PINNED</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga2abe959e39acfb75c7c0652706dfe84c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MALLOC_COUNT</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gaace5eebbb6662fb1be7c79a65464e1cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MALLOC_NORECLAIM</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gaaa8051f7b2b765dc707895572547c181</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MEMORY_WAIT</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga8601b3e35b74e9cbbcefd7a9b9c6d0b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MEMORY_OVERFLOW</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gaffa4b2a7af68027551855bd6c9520914</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MALLOC_SIMULATION_FOLDED</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga67c2ba763b260bea998b16881c788ae6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_malloc_set_align</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga52ec305b6afdd76130a4f6877e23849c</anchor>
      <arglist>(size_t align)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_malloc</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga4afc214c8964f0fc8d0f25d5a85ea6c8</anchor>
      <arglist>(void **A, size_t dim) STARPU_ATTRIBUTE_ALLOC_SIZE(2)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_free</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga889e8ea2a1becb4eff634e264973b261</anchor>
      <arglist>(void *A)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_malloc_flags</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gae3bd40d6e18d317fbb808962b9762c1a</anchor>
      <arglist>(void **A, size_t dim, int flags) STARPU_ATTRIBUTE_ALLOC_SIZE(2)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_free_flags</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gad60ddd8514e8ed664ecf1f21ad84e6f5</anchor>
      <arglist>(void *A, size_t dim, int flags)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_memory_pin</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga5a6ea6d03d7b0f4a97a8046b30ecd0bb</anchor>
      <arglist>(void *addr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_memory_unpin</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga3506e4b5ab7a605e25968883ded27d64</anchor>
      <arglist>(void *addr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>starpu_ssize_t</type>
      <name>starpu_memory_get_total</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gaccfcb477defdecd13c2d2471e9810c6b</anchor>
      <arglist>(unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>starpu_ssize_t</type>
      <name>starpu_memory_get_available</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga897b57b3349c1d495eeb144d64ff12c3</anchor>
      <arglist>(unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_memory_wait_available</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga71ec84f082fd96f53612e684eda0e178</anchor>
      <arglist>(unsigned node, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_memory_allocate</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga84894d9a6726bca707043f484bd1a8f5</anchor>
      <arglist>(unsigned node, size_t size, int flags)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_memory_deallocate</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga9e545c4701e6db29ab38bd0f7e4a76b5</anchor>
      <arglist>(unsigned node, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sleep</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga4c38ddeaf0bba0226f8a5c2c83cf7ab7</anchor>
      <arglist>(float nb_sec)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_task.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__task_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <includes id="starpu__data_8h" name="starpu_data.h" local="no" imported="no">starpu_data.h</includes>
    <includes id="starpu__util_8h" name="starpu_util.h" local="no" imported="no">starpu_util.h</includes>
    <includes id="starpu__task__bundle_8h" name="starpu_task_bundle.h" local="no" imported="no">starpu_task_bundle.h</includes>
    <class kind="struct">starpu_codelet</class>
    <class kind="struct">starpu_task</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_NOWHERE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga7d5977bc7532b4b357ea8b1017b5aaca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CPU</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaf577e4415a639beffbc48b65454b88ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CUDA</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga43c37484ac60c15cd6f45ab25c277213</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaec9e5cdf8ac48607ce4495ded31001d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MIC</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga6f980630004573f662253ff25317382e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCC</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad17a66a704d34499ea478841f3d3c691</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CODELET_SIMGRID_EXECUTE</name>
      <anchorfile>starpu__task_8h.html</anchorfile>
      <anchor>a84a0cbbc97e89b4104b4ce3047608b94</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CUDA_ASYNC</name>
      <anchorfile>starpu__task_8h.html</anchorfile>
      <anchor>ac91ae22b428595d3956a1c2225e2621e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL_ASYNC</name>
      <anchorfile>starpu__task_8h.html</anchorfile>
      <anchor>a3c2546614b19e43aad2bed0d935b38b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_INVALID</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga5cd260cfe96251890a0749743692de09</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIPLE_CPU_IMPLEMENTATIONS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga29760ecd052f42a017005498d707d649</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIPLE_CUDA_IMPLEMENTATIONS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad41f0586b5bd0f116a9106457117e7f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIPLE_OPENCL_IMPLEMENTATIONS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gabf8eee9a9ff1a110f06e6d9c95fd1e8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_NBUFFERS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga8bf45c6964c57e5a63f7ae225e1ec141</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_INITIALIZER</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gafe2c37f8164584418e7781fc15fb054c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_GET_NBUFFERS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga829d31913f4b0890192d4924bd264f88</anchor>
      <arglist>(task)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_GET_HANDLE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga260a1f7f8bef6b255dd0e0e45481385e</anchor>
      <arglist>(task, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_SET_HANDLE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga82b2ad7adb9e1c70d4c901a89be88d5d</anchor>
      <arglist>(task, handle, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CODELET_GET_MODE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga5dea5d6f5018e13a6b2c5d7a09f08054</anchor>
      <arglist>(codelet, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CODELET_SET_MODE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gae7be080fd53d7a402620cabd4070b6ca</anchor>
      <arglist>(codelet, mode, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_GET_MODE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga649920660819ed7d9922e896e63acd95</anchor>
      <arglist>(task, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_SET_MODE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga22df1754b8d59c3fa06a366554fa2c3a</anchor>
      <arglist>(task, mode, i)</arglist>
    </member>
    <member kind="typedef">
      <type>uint64_t</type>
      <name>starpu_tag_t</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>ga6e19fdd4a84d04323ee05e9b7ec5990e</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_cpu_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga8a4fb4a4c3d4b96159949bda6b235c1f</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_cuda_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga54f6cb6c50dab31bd1d0aaa7f7f3c10b</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_opencl_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga5580d9f3ee1cf070e0bdca49534fecde</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_mic_kernel_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga28acec4e7ec3e6dd5183fb7d4070cab1</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_scc_kernel_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga2d5c6d8edf1a289187c0f0b09b224a14</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>starpu_mic_kernel_t(*</type>
      <name>starpu_mic_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga59d5a960d0137ecc4c1c4d16087402df</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="typedef">
      <type>starpu_scc_kernel_t(*</type>
      <name>starpu_scc_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga06d73291da4a6e975b51d35ecf9428dc</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_codelet_type</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gac127ad106bf376da993d3f4caa979b2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SEQ</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ggac127ad106bf376da993d3f4caa979b2fa6622e698785a33f14549958f44577fcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SPMD</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ggac127ad106bf376da993d3f4caa979b2fad58c1d1b9c143aed79f1ddf6dc104746</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_FORKJOIN</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ggac127ad106bf376da993d3f4caa979b2faaaad35e6da5417989b55d70fa7e65447</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_task_status</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga0088c4ab87a08b3e7a375c1a79e91202</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a677b001ff383c2e11850a03595949a6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_READY</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a77b817e6cd032658581f7b9cf12dd3e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_RUNNING</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202afcf62642e96b8982135f58a477e9ea9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_FINISHED</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a01380324ad453dd9154c38582df6359e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED_ON_TAG</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202afa01b5f07d5e628759f9a26c7077f9d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED_ON_TASK</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a8f0b251db9e88be0bd7af98816c22a78</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED_ON_DATA</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a5ff984632a93a5e0c5d8579c78b1edb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_declare_deps</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>ga863d71b775ec517493f81e731a64d134</anchor>
      <arglist>(starpu_tag_t id, unsigned ndeps,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_declare_deps_array</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gab5c86743355724ee2f9596de1ae221df</anchor>
      <arglist>(starpu_tag_t id, unsigned ndeps, starpu_tag_t *array)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_declare_deps_array</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gada6692ca393aa820bc105252ca19830f</anchor>
      <arglist>(struct starpu_task *task, unsigned ndeps, struct starpu_task *task_array[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_get_task_succs</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>ga53dc7bff09b441ee6a200cae99ca2cb8</anchor>
      <arglist>(struct starpu_task *task, unsigned ndeps, struct starpu_task *task_array[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_get_task_scheduled_succs</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gae33176debf369b29f12e2e390f2d67e1</anchor>
      <arglist>(struct starpu_task *task, unsigned ndeps, struct starpu_task *task_array[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_tag_wait</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gaf5090c3c45414d693445e79ab7842992</anchor>
      <arglist>(starpu_tag_t id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_tag_wait_array</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gaf5cf68c72a863a6ab0ef8737c92b57ba</anchor>
      <arglist>(unsigned ntags, starpu_tag_t *id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_notify_from_apps</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gabdb8d20fca507a93450fe775233dd20c</anchor>
      <arglist>(starpu_tag_t id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_restart</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gaef0b7cee7536d4a5de1b10888ab13add</anchor>
      <arglist>(starpu_tag_t id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_remove</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gadbed48d64331463d4f9a9090933e5b9f</anchor>
      <arglist>(starpu_tag_t id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_init</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gadf8b76ef62ba0e68684e07ebf1db1731</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_clean</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaecb3efa04cb10b049b10c6166dd9c30c</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_task_create</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga48c439a84fdee4310c3481ca7e56156a</anchor>
      <arglist>(void) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_destroy</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga8fdfb4c2276013b699f5398a1c528bba</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_submit</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaa32228bf7f452f7d664986668ea46590</anchor>
      <arglist>(struct starpu_task *task) STARPU_WARN_UNUSED_RESULT</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_submit_to_ctx</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga3c6c4dd317df02d7021e23ab6b14e3e2</anchor>
      <arglist>(struct starpu_task *task, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad915868fe3e292657cefc6e1c404c7f4</anchor>
      <arglist>(struct starpu_task *task) STARPU_WARN_UNUSED_RESULT</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_all</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad0baa8dbfd13e5a7bc3651bcd76022aa</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_n_submitted</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga724cda8b0ffeaa7532a5e573b86bc90b</anchor>
      <arglist>(unsigned n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_all_in_ctx</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaf963bbf24935bdf137d2eefa7fd06b80</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_n_submitted_in_ctx</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gabba5bab6e4eb84f21f01ba53008a0cd3</anchor>
      <arglist>(unsigned sched_ctx_id, unsigned n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_no_ready</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga50ff3cb4b73a5e44e51035166127d40a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_nready</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga18bec8430309a613520d6042d701790d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_nsubmitted</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gae2740e2b2b948a5c48f5424651dfc721</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_init</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga46b34c28e0b9b2e1726b9d033639dda7</anchor>
      <arglist>(struct starpu_codelet *cl)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_display_stats</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga0e7db78b32d57db548555c1425808dd0</anchor>
      <arglist>(struct starpu_codelet *cl)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_task_get_current</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gab0a96cce01be9f4eccad9c783e8e27bb</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_parallel_task_barrier_init</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga3d362cb0de1da4e2659f602eff325479</anchor>
      <arglist>(struct starpu_task *task, int workerid)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_parallel_task_barrier_init_n</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>gaf99bc08bf6f586b658febdd1dc6fef80</anchor>
      <arglist>(struct starpu_task *task, int worker_size)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_task_dup</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gafae324cd8f5a0851d66bffc95bb28d49</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_set_implementation</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaabb4805df503ece40a5e092d8e99ae52</anchor>
      <arglist>(struct starpu_task *task, unsigned impl)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_task_get_implementation</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaac492d7132cc930b8483b571d0dd4333</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_task_bundle.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__task__bundle_8h</filename>
    <member kind="typedef">
      <type>struct _starpu_task_bundle *</type>
      <name>starpu_task_bundle_t</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>ga1be1e845a54cec05cbc371d1f0c9a2e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_bundle_create</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>gadd37abde6806819967151a64146b19d6</anchor>
      <arglist>(starpu_task_bundle_t *bundle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_bundle_insert</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>gae8be4224d241ac2d30d9d573359e3280</anchor>
      <arglist>(starpu_task_bundle_t bundle, struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_bundle_remove</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>gad904bbf27569e421454a2c8b25458eb3</anchor>
      <arglist>(starpu_task_bundle_t bundle, struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_bundle_close</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>ga7fcf9fedcf2a3f79784425431980a480</anchor>
      <arglist>(starpu_task_bundle_t bundle)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_task_list.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__task__list_8h</filename>
    <includes id="starpu__task_8h" name="starpu_task.h" local="no" imported="no">starpu_task.h</includes>
    <includes id="starpu__util_8h" name="starpu_util.h" local="no" imported="no">starpu_util.h</includes>
    <class kind="struct">starpu_task_list</class>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE void</type>
      <name>starpu_task_list_init</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>ga5618612b0ce2f914ab9032474ddf072f</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE void</type>
      <name>starpu_task_list_push_front</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gabf5bb8daf00f9902ea501009720af19d</anchor>
      <arglist>(struct starpu_task_list *list, struct starpu_task *task)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE void</type>
      <name>starpu_task_list_push_back</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gae29a8aae4e82286c80656a7afdc7a106</anchor>
      <arglist>(struct starpu_task_list *list, struct starpu_task *task)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_front</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gad38d569521bb710afc2299a9150f083f</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_back</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>ga871dd2f2bb4429d4edc9777abf3490b3</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE int</type>
      <name>starpu_task_list_empty</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>ga477ef573c883af23f946ec291801458a</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE void</type>
      <name>starpu_task_list_erase</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gaedf8ff832aea57abbb8ab94304a85c13</anchor>
      <arglist>(struct starpu_task_list *list, struct starpu_task *task)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_pop_front</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gad0e3e0684ea77c786b7c5b83894b5e5d</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_pop_back</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gad88622cd18759da0a9b30ca75c11e22b</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_begin</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gac3ea85752262f5281b745b2792af9266</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_next</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>ga4630f5b59e347b36b27f025b9c8801a1</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE int</type>
      <name>starpu_task_list_ismember</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gad9543a989bd53e77ef1731f4431b4da0</anchor>
      <arglist>(struct starpu_task_list *list, struct starpu_task *look)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_task_util.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__task__util_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VALUE</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gaff4c08029dceb2d7f158d49def9aec00</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CALLBACK</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gae053c49170a0f318357ba3aa8e08ea89</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CALLBACK_WITH_ARG</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga12a05bcd675b1b88ec73ea9d8c8ccd74</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CALLBACK_ARG</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gaa2c1a7638d3cf2ddfe6cc51aa2fc4a46</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PRIORITY</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gab9d32b41f7fbae16e9ed2331f6dde8ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_DATA_ARRAY</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga9eb57b0eb0ee7ce31661eed55d3a5f4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_DATA_MODE_ARRAY</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga30d3c482ec1e2b9b6868988abcb02596</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TAG</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga66222fb40e0942f5b93aed24f0917e3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_FLOPS</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga1a0a565f2de522abc9c5f3457397b095</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga45ae06dad37b9a8518198b1c3b06f276</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_EXECUTE_ON_WORKER</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga71d2b5f941b3c05d67a7d002e834a753</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TAG_ONLY</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga2a2f1dbde5737ec9cc60773ea5dd124d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_WORKER_ORDER</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gaff5e270481f2931229a4b388f386ddf0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_NAME</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga663bee5a319a5c763af85f0b0071f92f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CL_ARGS</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga8be38949dd0489761bd2768f8e584c70</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_EXECUTE_ON_NODE</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga750b87a6ea137c903f02c53029d1131d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_EXECUTE_ON_DATA</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gadc9112873f348b1f6daa0da4b93b061f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_create_sync_task</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga8b83ae8b1c2015f961e60627d4913549</anchor>
      <arglist>(starpu_tag_t sync_tag, unsigned ndeps, starpu_tag_t *deps, void(*callback)(void *), void *callback_arg)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_task_build</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga8f7f5033f2077ebae698741750884ff2</anchor>
      <arglist>(struct starpu_codelet *cl,...)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_insert</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gad79a50a21fe717126659b2998209c1c6</anchor>
      <arglist>(struct starpu_codelet *cl,...)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_insert_task</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga4907c59974b5b30964a979083853d24e</anchor>
      <arglist>(struct starpu_codelet *cl,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_unpack_args</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gaa2e8c2206e1ea56aec65b09efb82f13a</anchor>
      <arglist>(void *cl_arg,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_unpack_args_and_copyleft</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gab3448f8c28eef89affc2169cad340a21</anchor>
      <arglist>(void *cl_arg, void *buffer, size_t buffer_size,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_pack_args</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga083f02c3ed2a1ab58058ba17e36e773b</anchor>
      <arglist>(void **arg_buffer, size_t *arg_buffer_size,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_EXECUTE_ON_NODE</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga750b87a6ea137c903f02c53029d1131d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_EXECUTE_ON_DATA</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gadc9112873f348b1f6daa0da4b93b061f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_thread.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__thread_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <includes id="starpu__util_8h" name="starpu_util.h" local="no" imported="no">starpu_util.h</includes>
    <class kind="struct">starpu_pthread_barrier_t</class>
    <class kind="struct">starpu_pthread_spinlock_t</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_INITIALIZER</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gac93d8e7fcb44d354421a77b1c0bc9569</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_INITIALIZER</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gac83bf1cce7a509b76cae9e191fb39bb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_barrier_init</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab3f630ba782864f80fb7d8fceb7db399</anchor>
      <arglist>(starpu_pthread_barrier_t *barrier, const starpu_pthread_barrierattr_t *attr, unsigned count)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_barrier_destroy</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga0573dd7985127d363fd5dbcb8d67796b</anchor>
      <arglist>(starpu_pthread_barrier_t *barrier)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_barrier_wait</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab522d9c9ae9747db8123344f49421725</anchor>
      <arglist>(starpu_pthread_barrier_t *barrier)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_init</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga3e6f70cbaed94500cd4b8229db361a7a</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock, int pshared)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_destroy</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga5ec606cedc543ee35f3c7dc8a8e2505b</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_lock</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga9ef49f133fa6f1374769aab8a4ff9474</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_trylock</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gac5bf366536c4f722dec41da48618b937</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_unlock</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaa8d07e258b61aa10fc8ce2111f4a37a9</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_thread_util.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__thread__util_8h</filename>
    <includes id="starpu__util_8h" name="starpu_util.h" local="no" imported="no">starpu_util.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_CREATE_ON</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaea6e537f9a02061690e845082158b3b4</anchor>
      <arglist>(name, thread, attr, routine, arg, where)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_CREATE</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga43ef5044dfe6169c422d3ded42aa69e4</anchor>
      <arglist>(thread, attr, routine, arg)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_INIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab1d60ac7acd857f056de5d9cdd3b8b93</anchor>
      <arglist>(mutex, attr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_DESTROY</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab99612af964c7e92d99e3fcc9513c909</anchor>
      <arglist>(mutex)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_LOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gacabc76c4b40c66603b5d417d7039439a</anchor>
      <arglist>(mutex)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_UNLOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga5e53025adc42966a97d616304bd654bd</anchor>
      <arglist>(mutex)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_KEY_CREATE</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab1f13e8c7f4e9c7a93e35ffa5f3c8925</anchor>
      <arglist>(key, destr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_KEY_DELETE</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gac1f65845c0c1c76e65c63fc5174ff5cb</anchor>
      <arglist>(key)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_SETSPECIFIC</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga0f4c9033a1cbf7b7ef88c5124a65149f</anchor>
      <arglist>(key, ptr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_GETSPECIFIC</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gae024574a5ff66361d2b00f10924dbf3a</anchor>
      <arglist>(key)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_INIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab24c9ad484185c1b0fc2518a27824041</anchor>
      <arglist>(rwlock, attr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_RDLOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaede3f824ce3ecf6079987a68da9eb621</anchor>
      <arglist>(rwlock)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_WRLOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga2331ffac0d863e2d34463003fa961550</anchor>
      <arglist>(rwlock)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_UNLOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga7cf8cbee2a33b4714cf37724f657bd4a</anchor>
      <arglist>(rwlock)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_DESTROY</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga348ba00ab90144b68f094631fb3657de</anchor>
      <arglist>(rwlock)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_INIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaba0e781d7d8a22c313200edaa9ec59f9</anchor>
      <arglist>(cond, attr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_DESTROY</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga0f576d7a841d598e1f8c78390a539ab6</anchor>
      <arglist>(cond)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_SIGNAL</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga42b7eaa98fe66b90eb67f3fdb51fd14d</anchor>
      <arglist>(cond)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_BROADCAST</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga81126f750a75beef8a9679dd261e1bd2</anchor>
      <arglist>(cond)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_WAIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gad37f49e0fb051b7a3a66208a4bc67397</anchor>
      <arglist>(cond, mutex)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_BARRIER_INIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga707b1afc766917d4ddbbe19e5569359b</anchor>
      <arglist>(barrier, attr, count)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_BARRIER_DESTROY</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga2bd1ebeca9a2d4eefc3d0a399ce3ae3c</anchor>
      <arglist>(barrier)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_BARRIER_WAIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaa0871b23fee2d5cfa1e4930108b53708</anchor>
      <arglist>(barrier)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_top.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__top_8h</filename>
    <includes id="starpu_8h" name="starpu.h" local="no" imported="no">starpu.h</includes>
    <class kind="struct">starpu_top_data</class>
    <class kind="struct">starpu_top_param</class>
    <member kind="enumeration">
      <type></type>
      <name>starpu_top_data_type</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga7b7dfbb9c273503e6e7f5790b39e2d27</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_DATA_BOOLEAN</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga7b7dfbb9c273503e6e7f5790b39e2d27a8ae4c093533aae3d4ece7910d16fa7cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_DATA_INTEGER</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga7b7dfbb9c273503e6e7f5790b39e2d27a70a25dacfac1c08f2e73263f9b10a21e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_DATA_FLOAT</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga7b7dfbb9c273503e6e7f5790b39e2d27adfb99eeccad78dbb404dc39f058e22be</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_top_param_type</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga1979aeea98f354bd500f4902ef2299d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_BOOLEAN</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9a6e452fdfa703a7292d7526844309dd34</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_INTEGER</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9ac1737a4533dd745432df144d19f7874f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_FLOAT</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9a2d732b79d2e553a6d248a348b07e1422</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_ENUM</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9aaea8e0c88de44de91feaf091f2f8f431</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_top_message_type</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga568af4507d96390316b93c92fee45f52</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_GO</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a2980d96d782513d6a2ba7e320e4b67f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_SET</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a3c1b1dcb8ad446753dbde70039aac0e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_CONTINUE</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a9dad9fc65f92a2ece0935fd17ca0b1a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_ENABLE</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52ad2658f645e029c0013185fd47bb99cbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_DISABLE</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52ae0d6282a934eccc5bfee15a5657ce69a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_DEBUG</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a952a3745b30ea9defd6b4e397fe7bcc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_UNKNOW</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a49307bcbda7b5b9e34e0fd76fea2a372</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_data *</type>
      <name>starpu_top_add_data_boolean</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga016dd52f4fb957306fbc0d8dfdf50c71</anchor>
      <arglist>(const char *data_name, int active)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_data *</type>
      <name>starpu_top_add_data_integer</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gad2d523db513a2e74f8cc417926fe4886</anchor>
      <arglist>(const char *data_name, int minimum_value, int maximum_value, int active)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_data *</type>
      <name>starpu_top_add_data_float</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gae79ecd90078b87ebf97cbb127effde89</anchor>
      <arglist>(const char *data_name, double minimum_value, double maximum_value, int active)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_boolean</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gadae398d33233f7d7f8860562885c564d</anchor>
      <arglist>(const char *param_name, int *parameter_field, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_integer</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga97bf62e4f04016e273f364ab99b3d146</anchor>
      <arglist>(const char *param_name, int *parameter_field, int minimum_value, int maximum_value, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_float</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga91db3cdf3b86e37066e625e504b33fb0</anchor>
      <arglist>(const char *param_name, double *parameter_field, double minimum_value, double maximum_value, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_enum</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga3406e743f8db81f60c019b1be2bbb46f</anchor>
      <arglist>(const char *param_name, int *parameter_field, char **values, int nb_values, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_init_and_wait</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga84638ef94fd71d3192c194053e91b4d8</anchor>
      <arglist>(const char *server_name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_parameter</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga4afdee5376361117e11e8a9ee4a47bad</anchor>
      <arglist>(const struct starpu_top_param *param)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_data_boolean</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga87b6dcc03287a2a19788c984443160c2</anchor>
      <arglist>(const struct starpu_top_data *data, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_data_integer</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga68fb3d6362e00a8d32668389e98dca07</anchor>
      <arglist>(const struct starpu_top_data *data, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_data_float</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga373e3fecfd9fccc0ac3e3857f2e5a0e8</anchor>
      <arglist>(const struct starpu_top_data *data, double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_task_prevision</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gac361229a1e495768bf5f1653940528de</anchor>
      <arglist>(struct starpu_task *task, int devid, unsigned long long start, unsigned long long end)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_debug_log</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga07e4329145ef683b44df8f36e5331158</anchor>
      <arglist>(const char *message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_debug_lock</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga414faad453d183d1c026eb04fc59b07f</anchor>
      <arglist>(const char *message)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_data *</type>
      <name>starpu_top_add_data_boolean</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga016dd52f4fb957306fbc0d8dfdf50c71</anchor>
      <arglist>(const char *data_name, int active)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_data *</type>
      <name>starpu_top_add_data_integer</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gad2d523db513a2e74f8cc417926fe4886</anchor>
      <arglist>(const char *data_name, int minimum_value, int maximum_value, int active)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_data *</type>
      <name>starpu_top_add_data_float</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gae79ecd90078b87ebf97cbb127effde89</anchor>
      <arglist>(const char *data_name, double minimum_value, double maximum_value, int active)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_boolean</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gadae398d33233f7d7f8860562885c564d</anchor>
      <arglist>(const char *param_name, int *parameter_field, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_integer</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga97bf62e4f04016e273f364ab99b3d146</anchor>
      <arglist>(const char *param_name, int *parameter_field, int minimum_value, int maximum_value, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_float</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga91db3cdf3b86e37066e625e504b33fb0</anchor>
      <arglist>(const char *param_name, double *parameter_field, double minimum_value, double maximum_value, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_enum</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga3406e743f8db81f60c019b1be2bbb46f</anchor>
      <arglist>(const char *param_name, int *parameter_field, char **values, int nb_values, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_init_and_wait</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga84638ef94fd71d3192c194053e91b4d8</anchor>
      <arglist>(const char *server_name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_parameter</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga4afdee5376361117e11e8a9ee4a47bad</anchor>
      <arglist>(const struct starpu_top_param *param)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_data_boolean</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga87b6dcc03287a2a19788c984443160c2</anchor>
      <arglist>(const struct starpu_top_data *data, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_data_integer</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga68fb3d6362e00a8d32668389e98dca07</anchor>
      <arglist>(const struct starpu_top_data *data, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_data_float</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga373e3fecfd9fccc0ac3e3857f2e5a0e8</anchor>
      <arglist>(const struct starpu_top_data *data, double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_task_prevision</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gac361229a1e495768bf5f1653940528de</anchor>
      <arglist>(struct starpu_task *task, int devid, unsigned long long start, unsigned long long end)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_debug_log</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga07e4329145ef683b44df8f36e5331158</anchor>
      <arglist>(const char *message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_debug_lock</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga414faad453d183d1c026eb04fc59b07f</anchor>
      <arglist>(const char *message)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_tree.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__tree_8h</filename>
    <class kind="struct">starpu_tree</class>
    <member kind="function">
      <type>void</type>
      <name>starpu_tree_insert</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>ga318b363110b391920b4cb223fe8424bd</anchor>
      <arglist>(struct starpu_tree *tree, int id, int level, int is_pu, int arity, struct starpu_tree *father)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_tree *</type>
      <name>starpu_tree_get</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>ga57a6a8f70ee46ca8dfe503b52bd31e74</anchor>
      <arglist>(struct starpu_tree *tree, int id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tree_free</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>ga6cc51deaa659ac90881ed1126432c4fb</anchor>
      <arglist>(struct starpu_tree *tree)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_util.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__util_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <includes id="starpu__task_8h" name="starpu_task.h" local="no" imported="no">starpu_task.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_GNUC_PREREQ</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga4fc771a29d7dc0e826da42777edbcf15</anchor>
      <arglist>(maj, min)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_UNLIKELY</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga23742c6fca8b5386e1f3e3f7bbf3a692</anchor>
      <arglist>(expr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_LIKELY</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga4456c2db2cc8df7c98d62ecc21b2bd1d</anchor>
      <arglist>(expr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_UNUSED</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga9e7c20a6cdce868976a0046206b104e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_INTERNAL</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga903e600059ba1735d5d6bd688dc0cc7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_MALLOC</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga214c60ee1b6cbc27ab08ba6f339ef8e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_WARN_UNUSED_RESULT</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga71e28fc2a0a5267bc7edc0909d9d5117</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_PURE</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga057b12b615ce9e8a1fe7b5e10d9a5c2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_ALIGNED</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gae8c513ecd1a5bd87a7bdd71260c49ffc</anchor>
      <arglist>(size)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_WARN_UNUSED_RESULT</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga718857e58c604e0fab6a45a217961277</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_POISON_PTR</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga4c4f93f8ed79333f5801058af4267d84</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MIN</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gad614bcaae1a110a4ddc0ddc3636dac5d</anchor>
      <arglist>(a, b)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAX</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gaa156afa885bb447bc0ced3ea66bcf7e6</anchor>
      <arglist>(a, b)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ASSERT</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga05ac0dda104331f57d85823a4f9318ce</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ASSERT_MSG</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gabb6941113e19ee4045a014943cd62aae</anchor>
      <arglist>(x, msg,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ABORT</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga3a4d1fb56668323d4f44c18f6a6dfb3f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ABORT_MSG</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga4268950ebf63c0f1dabcfd0cd8808067</anchor>
      <arglist>(msg,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CHECK_RETURN_VALUE</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gaa3503ea5efd00806a0f9234f033e6ea1</anchor>
      <arglist>(err, message,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CHECK_RETURN_VALUE_IS</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga1b41d799a04a1d6483e982ffb2f74432</anchor>
      <arglist>(err, value, message,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_RMB</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gaa5363993d4f519517316ccf9dc47a7bb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_WMB</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga07b2628bd6680fe49d92c7f816d7ce53</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static __starpu_inline int</type>
      <name>starpu_get_env_number</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gac9eef7c5611291831a56af2134d9945c</anchor>
      <arglist>(const char *str)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_execute_on_each_worker</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>ga7a97b0699b97b30b4d408c660be46102</anchor>
      <arglist>(void(*func)(void *), void *arg, uint32_t where)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_execute_on_each_worker_ex</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>ga77f0f3796a059b3651f7ecb4535dad82</anchor>
      <arglist>(void(*func)(void *), void *arg, uint32_t where, const char *name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_execute_on_specific_workers</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>ga090e97b588f83718c19f0ed3d07d1d70</anchor>
      <arglist>(void(*func)(void *), void *arg, unsigned num_workers, unsigned *workers, const char *name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_cpy</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>gaaff4ecf5818fd6f8d005f6bf57b0eb4b</anchor>
      <arglist>(starpu_data_handle_t dst_handle, starpu_data_handle_t src_handle, int asynchronous, void(*callback_func)(void *), void *callback_arg)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_timing_now</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>ga34953348991f74a1cbd694fffb27f8b7</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpu_worker.h</name>
    <path>/home/gchr/workspace/StarPU/include/</path>
    <filename>starpu__worker_8h</filename>
    <includes id="starpu__config_8h" name="starpu_config.h" local="no" imported="no">starpu_config.h</includes>
    <includes id="starpu__thread_8h" name="starpu_thread.h" local="no" imported="no">starpu_thread.h</includes>
    <class kind="struct">starpu_sched_ctx_iterator</class>
    <class kind="struct">starpu_worker_collection</class>
    <member kind="define">
      <type>#define</type>
      <name>starpu_worker_get_id_check</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga177bffe36c7df3f91aaebe2dd03b8cda</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_worker_archtype</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga173d616aefe98c33a47a847fd2fca37d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CPU_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37da5b5aeeaa21f925e63f5fed338baf0588</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CUDA_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37dac95bbacadbe0599c06a48059d2821611</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_OPENCL_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37da113d54380f4e41e4da1f94b1a779634f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MIC_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37da58985be48577b6cdb2baf5605e26698d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCC_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37dacb52719c80c5191875563436fca71096</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_ANY_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37dac4e189acc1d1b0971c8d950034048501</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_worker_collection_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga80b06886ee8a4c0e99b09ab638113af3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_WORKER_LIST</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga80b06886ee8a4c0e99b09ab638113af3a7fb30f298c82f9375f49a42664d473bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga6312aa4b12ce7e06fe391bcafda6796a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_combined_worker_get_count</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga49a6732f413550652450baa64594bc5e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_cpu_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga8c4e370ff327c38a3ec79c9249ab233a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_cuda_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gacb9d262c066c047ae55f691adc876db3</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_opencl_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga0dd6ff2d77c6a520a6cc4c051eb40bfa</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_mic_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga9b6da05473fbc3dbebc4da805d10d404</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_scc_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga9b0b790e402b113cc56af695167f96c9</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_mic_device_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga8be255f32d6f0a112b5f84bf572304cf</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_id</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gac6d06f4e22b63bf50bc8e836cf16f81f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_get_id</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>gae53499613fb72e9aa1abad6437a926f9</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_get_size</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga573d2513a10cf1d14fee0467eda0884d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_get_rank</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga4eb5aaf2f4e0c9a7f07cbc80f3ae9664</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_worker_archtype</type>
      <name>starpu_worker_get_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga4998e4e1dfa0d121059c60f790496a03</anchor>
      <arglist>(int id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_count_by_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga60b4a796a3dba40e5159158750805bf2</anchor>
      <arglist>(enum starpu_worker_archtype type)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_ids_by_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gaddb73ee5de667ba2e3353eb977d22cde</anchor>
      <arglist>(enum starpu_worker_archtype type, int *workerids, int maxsize)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_by_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gab0ffe5622c5980444226f1ed8d6fee71</anchor>
      <arglist>(enum starpu_worker_archtype type, int num)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_by_devid</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gae6166f338640d033b740f5c8008b851b</anchor>
      <arglist>(enum starpu_worker_archtype type, int devid)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_worker_get_name</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gae35a841f996b8758f3d0ce2ac2d066a5</anchor>
      <arglist>(int id, char *dst, size_t maxlen)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_devid</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga0c9b9b7f5195cb8cbc5ee1223ec0f264</anchor>
      <arglist>(int id)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>starpu_worker_get_type_as_string</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gab62d05f84a4ca631cc148e115d7dd995</anchor>
      <arglist>(enum starpu_worker_archtype type)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>starpufft.h</name>
    <path>/home/gchr/workspace/StarPU/starpufft/include/</path>
    <filename>starpufft_8h</filename>
    <member kind="function">
      <type>starpufft_plan</type>
      <name>starpufft_plan_dft_1d</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga642104c645445688177f1a44afeeefb0</anchor>
      <arglist>(int n, int sign, unsigned flags)</arglist>
    </member>
    <member kind="function">
      <type>starpufft_plan</type>
      <name>starpufft_plan_dft_2d</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>gaf263d43c43927007819edf069cc8c93b</anchor>
      <arglist>(int n, int m, int sign, unsigned flags)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpufft_malloc</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>gabf7989748571ab9155a865a19002d1fa</anchor>
      <arglist>(size_t n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpufft_free</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga6809e3bc78ad0d7f9df41e99ddada069</anchor>
      <arglist>(void *p)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpufft_execute</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>gae73ea67d1d7df63938661c7a1105800a</anchor>
      <arglist>(starpufft_plan p, void *in, void *out)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpufft_start</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga8e37e0bd1edc887cf456dcf7d95b86c4</anchor>
      <arglist>(starpufft_plan p, void *in, void *out)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpufft_execute_handle</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga22ad2f1c869305d11132322039d38876</anchor>
      <arglist>(starpufft_plan p, starpu_data_handle_t in, starpu_data_handle_t out)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpufft_start_handle</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>gabff378384df24d2c2ea296397b600569</anchor>
      <arglist>(starpufft_plan p, starpu_data_handle_t in, starpu_data_handle_t out)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpufft_cleanup</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga4670f7ac8c6980f78b80b48bbe561299</anchor>
      <arglist>(starpufft_plan p)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpufft_destroy_plan</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga2b1a62489b5b48491067bc8dc39f4b16</anchor>
      <arglist>(starpufft_plan p)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>sc_hypervisor_policy</name>
    <filename>group__API__SC__Hypervisor.html</filename>
    <anchor>structsc__hypervisor__policy</anchor>
    <member kind="variable">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a411c84854af7ba5a563050c22ed65d24</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>custom</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a975a6241c01771b6ebedc56da6e3a933</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>size_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a81431b76980a77c424c38772968102e5</anchor>
      <arglist>)(unsigned *sched_ctxs, int nsched_ctxs, int *workers, int nworkers)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>resize_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a323600b52a95f2be31f1f72f59c613b3</anchor>
      <arglist>)(unsigned *sched_ctxs, int nsched_ctxs, int *workers, int nworkers)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>handle_idle_cycle</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>aa227429c6cf34c69c32b143b933a07b7</anchor>
      <arglist>)(unsigned sched_ctx, int worker)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>handle_pushed_task</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a9a10eb6dd3eb12543e505411585bafb7</anchor>
      <arglist>)(unsigned sched_ctx, int worker)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>handle_poped_task</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a38f0b680a437c321ed5f9a5f91261e73</anchor>
      <arglist>)(unsigned sched_ctx, int worker, struct starpu_task *task, uint32_t footprint)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>handle_idle_end</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a9ef96b3ab6d2247ecb336900e5051cd1</anchor>
      <arglist>)(unsigned sched_ctx, int worker)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>handle_post_exec_hook</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a8360ac46ace04f60b44fd83ed6db16ee</anchor>
      <arglist>)(unsigned sched_ctx, int task_tag)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>handle_submitted_job</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a31a2a9dc2fba831c86f3369974c3b874</anchor>
      <arglist>)(struct starpu_codelet *cl, unsigned sched_ctx, uint32_t footprint, size_t data_size)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>end_ctx</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>aff6339dc221097287b29cf0fdb78e8f8</anchor>
      <arglist>)(unsigned sched_ctx)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>sc_hypervisor_policy_config</name>
    <filename>group__API__SC__Hypervisor.html</filename>
    <anchor>structsc__hypervisor__policy__config</anchor>
    <member kind="variable">
      <type>int</type>
      <name>min_nworkers</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a309596658ce45de36fc2871a6df4d282</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>max_nworkers</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a9f31e224b7b8f06edfe2d31cafd40139</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>granularity</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>afe75ee5e11144af39796d8adefa1c1dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>priority</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>aa0aa3bf7583a12c3346a3e06236a7d05</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>max_idle</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a6a36830f17ce8f284c3543aafdc49b79</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>min_working</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a4b4cf5e2b01c071cb5451bca3446c941</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>fixed_workers</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>aee28c4abb71f645cb48edf4677366742</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>new_workers_max_idle</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a154dac023379e94c0a04efdd822a3e50</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ispeed_w_sample</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>acacb7cb4b04509ac0f912f1cf65661d4</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ispeed_ctx_sample</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a111f937e2d3126fe9fef849ac6796147</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>time_sample</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a7b31e50d194de4a09929756f320ea1b3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>sc_hypervisor_policy_task_pool</name>
    <filename>group__API__SC__Hypervisor.html</filename>
    <anchor>structsc__hypervisor__policy__task__pool</anchor>
    <member kind="variable">
      <type>struct starpu_codelet *</type>
      <name>cl</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a2b5d9d991c03d651ec0b5353d6772696</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>footprint</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a2b49b6e39b8b0882bc5fc0f2aee71f66</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>sched_ctx_id</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a8c2e8240be080f6aa6f0ad02c9918a95</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned long</type>
      <name>n</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a742686cd824c01df912c4443f57f0d57</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>data_size</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>afdf2824e028c9ead969948ccb0efe6c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct sc_hypervisor_policy_task_pool *</type>
      <name>next</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a1063b7c16ddaeab110882a8dbdb7e9da</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>sc_hypervisor_resize_ack</name>
    <filename>group__API__SC__Hypervisor.html</filename>
    <anchor>structsc__hypervisor__resize__ack</anchor>
    <member kind="variable">
      <type>int</type>
      <name>receiver_sched_ctx</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ac24cdb1fe5c0efc413267a980a8976bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>moved_workers</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>acd68e423efe1271125432108dd44e13a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nmoved_workers</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a4e24e8772a56d038634be9302eb9e93c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>acked_workers</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a5ebd37bd5c225c6b354750be598cd90f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>sc_hypervisor_wrapper</name>
    <filename>group__API__SC__Hypervisor.html</filename>
    <anchor>structsc__hypervisor__wrapper</anchor>
    <member kind="variable">
      <type>unsigned</type>
      <name>sched_ctx</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ab56be8402f4a8e45938a7f688dc0e304</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct sc_hypervisor_policy_config *</type>
      <name>config</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a9a2aa297cdeafdd703b84bd380b41a9b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>current_idle_time</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>aab7f7cf4560defaa76962057fbdc79ef</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>idle_time</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a8ddc223936bf073c6de110cf4cb9fcb3</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>idle_start_time</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a15b7572ea77ba649646ea7a2e618c708</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>worker_to_be_removed</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a4de5ef755a7476cfac78dcc2290ec8e8</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>pushed_tasks</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a93caf46bb69880d5da0548f056d77148</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>poped_tasks</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a72579a0dddfd47b7908e1b362abb2818</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>total_flops</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>adf798abf84171bf99afaffad01b25c21</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>total_elapsed_flops</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ac49459facb1a0ea95921dcb070e837a0</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>elapsed_flops</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a88b5959a57bf1498f4316cc6172a0b96</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elapsed_data</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a5256359f2725c36c087725384a89a9f9</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>elapsed_tasks</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>af976911e407e1e13fe4db83213116d55</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>ref_speed</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a9e05a2c227901a17b2cd15107b9d4640</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>submitted_flops</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a874e7f62575f017b1956e7ec4035514a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>remaining_flops</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>aa89295f0dbed47350995451736d4d1a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>start_time</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>adc95fd3044241fb7a37334c55c90e597</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>real_start_time</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a38be8cb0907ba69c715cbbfa0a078e65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct sc_hypervisor_resize_ack</type>
      <name>resize_ack</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a701a3bd29f67cdcc71a2507bac866553</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_pthread_mutex_t</type>
      <name>mutex</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ab0df538a6d83e44b6e9390fab1c0a7a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>total_flops_available</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>a2d42696426b58a5b899aaf0d8a42cf7c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_bcsr_interface</name>
    <filename>group__API__Data__Interfaces.html</filename>
    <anchor>structstarpu__bcsr__interface</anchor>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aeaaaed6dbef4bfc013c133654c1825fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nnz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a16fe3ad95b82e3d8d698274c89fca02a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nrow</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a116539ced9567e49b1c3d37cbb9daf95</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>nzval</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af0499fdbb48df14c8630d2b45520cdad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>colind</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a26dc564759b82b2f2a5362fe05c9529c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>rowptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>abdf514c239dab02cae28add81eef1fe1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>firstentry</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aea8014509eb5d21ba81d400c877d6d8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>r</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aaae06f78b08695614601fae3d676dd62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>c</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ad25ad86f33897a36c7197bcf80242df4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a143fa2154e1a6f621f46301bd174b09d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aeaaaed6dbef4bfc013c133654c1825fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nnz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a16fe3ad95b82e3d8d698274c89fca02a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nrow</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a116539ced9567e49b1c3d37cbb9daf95</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>nzval</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af0499fdbb48df14c8630d2b45520cdad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>colind</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a26dc564759b82b2f2a5362fe05c9529c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>rowptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>abdf514c239dab02cae28add81eef1fe1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>firstentry</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aea8014509eb5d21ba81d400c877d6d8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>r</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aaae06f78b08695614601fae3d676dd62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>c</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ad25ad86f33897a36c7197bcf80242df4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a143fa2154e1a6f621f46301bd174b09d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_block_interface</name>
    <filename>group__API__Data__Interfaces.html</filename>
    <anchor>structstarpu__block__interface</anchor>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae2a9b938ace8afeb6de759dadb7181a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a82f9e85c27ab3fc9371b6b092c8e3743</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>dev_handle</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a87c8121d720c200b8bf5710d5b34ed87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>offset</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a081cbedadaaa3b94bec18dd2e71e2d58</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af4cf7dcbcb99e697561312b2b7c44ffa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a98ab405852ad8f5b23b4c305633a8761</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4f9492b1f9efc6139cd434b546e4cb28</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ldy</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4e92a6ef9a315f84065f078d5643cbcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ldz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aaaae7566c1b274a8e208828792043a1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae2bd3e92fba4412cabef91740fa6d30f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae2a9b938ace8afeb6de759dadb7181a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a82f9e85c27ab3fc9371b6b092c8e3743</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>dev_handle</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a87c8121d720c200b8bf5710d5b34ed87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>offset</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a081cbedadaaa3b94bec18dd2e71e2d58</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af4cf7dcbcb99e697561312b2b7c44ffa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a98ab405852ad8f5b23b4c305633a8761</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4f9492b1f9efc6139cd434b546e4cb28</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ldy</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4e92a6ef9a315f84065f078d5643cbcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ldz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aaaae7566c1b274a8e208828792043a1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae2bd3e92fba4412cabef91740fa6d30f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_codelet</name>
    <filename>group__API__Codelet__And__Tasks.html</filename>
    <anchor>structstarpu__codelet</anchor>
    <member kind="variable">
      <type>uint32_t</type>
      <name>where</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a504b8e20f53f469358eadb1ed800f72c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>can_execute</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a7259d5e2f09e1003d7feda51f9c5bdc7</anchor>
      <arglist>)(unsigned workerid, struct starpu_task *task, unsigned nimpl)</arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_codelet_type</type>
      <name>type</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>af46aad8aa73b1a3fa855606d2d1a7cdd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>max_parallelism</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a06016fcaee131c683fb0953e27f160bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_cpu_func_t</type>
      <name>cpu_func</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ad5f00ed5a0a59b72c0d90d9718376893</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_cuda_func_t</type>
      <name>cuda_func</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a3d895b523ca1bf1b72ac8d337514d331</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_opencl_func_t</type>
      <name>opencl_func</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a20c2ee87052f53f2641736e57551011b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_cpu_func_t</type>
      <name>cpu_funcs</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a593418a8961318e4085177abeeaa43ad</anchor>
      <arglist>[STARPU_MAXIMPLEMENTATIONS]</arglist>
    </member>
    <member kind="variable">
      <type>starpu_cuda_func_t</type>
      <name>cuda_funcs</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>aa6a8436117176270c5372d4dfb006a1f</anchor>
      <arglist>[STARPU_MAXIMPLEMENTATIONS]</arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>cuda_flags</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>aaa458493da38622be2713700f3c96a5d</anchor>
      <arglist>[STARPU_MAXIMPLEMENTATIONS]</arglist>
    </member>
    <member kind="variable">
      <type>starpu_opencl_func_t</type>
      <name>opencl_funcs</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a757a9831b9e2b5e0d503e902d9f94529</anchor>
      <arglist>[STARPU_MAXIMPLEMENTATIONS]</arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>opencl_flags</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a2de77d2c65169749809c078367905a93</anchor>
      <arglist>[STARPU_MAXIMPLEMENTATIONS]</arglist>
    </member>
    <member kind="variable">
      <type>starpu_mic_func_t</type>
      <name>mic_funcs</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a45f9032d2e398aca9121d133e93c626d</anchor>
      <arglist>[STARPU_MAXIMPLEMENTATIONS]</arglist>
    </member>
    <member kind="variable">
      <type>starpu_scc_func_t</type>
      <name>scc_funcs</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a61ae676f51abb49231aecc69ca28413a</anchor>
      <arglist>[STARPU_MAXIMPLEMENTATIONS]</arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>cpu_funcs_name</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a323b97f3a8b106d6f6d4cbc79b52de92</anchor>
      <arglist>[STARPU_MAXIMPLEMENTATIONS]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nbuffers</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a1bb02f890f2e10c348499dbd92b56496</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_access_mode</type>
      <name>modes</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a680eeffb0ba785eeb203a8ad2e2870da</anchor>
      <arglist>[STARPU_NMAXBUFS]</arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_access_mode *</type>
      <name>dyn_modes</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a01077ed4d7fb3195c88d84dc4133bfa6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>specific_nodes</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a018dfc2c8999ad7646d37ac6c60a5c9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nodes</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ab801e0bab4e8a5feafa30d29335e7c9e</anchor>
      <arglist>[STARPU_NMAXBUFS]</arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>dyn_nodes</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>aaafdd996659109eef4524a25530882e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_perfmodel *</type>
      <name>model</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>af8a5572376b191b6e1884a46b74fb026</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_perfmodel *</type>
      <name>energy_model</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a83c8b0df97d1cbc3afe5ea603711fc47</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned long</type>
      <name>per_worker_stats</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a24e7ec85b5c3c7800676db5c2061a71a</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>af24d61809e453e55cfc5de0d4bef05eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>flags</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a9ddae82a84d18b96367e9a3831b1a805</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_conf</name>
    <filename>group__API__Initialization__and__Termination.html</filename>
    <anchor>structstarpu__conf</anchor>
    <member kind="variable">
      <type>int</type>
      <name>magic</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a057facba9480299fca80def536f2314a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>sched_policy_name</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>adfc5911b1042a9d0edd8ef1ed16f03d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_sched_policy *</type>
      <name>sched_policy</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>adf3d42945dba3e6c41a7c9258b192acd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ncpus</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a68e93f60a9f8e4c82ab2af6a9cd3888a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>ncuda</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>af4d7aba8f400e06ffd8847c7f12e21e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nopencl</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>aaf12d9f477d96213136483378f9b589b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nmic</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a4fef1f76f17a358df65a3c07cbce82d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nscc</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>aeeecb48dec535b9209ef91cb6e7e9cdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>use_explicit_workers_bindid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>affd5b335a988c417ecb679d5ecf7c489</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>workers_bindid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a2275c89ee0aded13cc8836a0c5ef0188</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>use_explicit_workers_cuda_gpuid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a27718585874f9012e9955ba308e2cd8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>workers_cuda_gpuid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>af482c3dac3ed6d28ee51e72c9ad636e1</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>use_explicit_workers_opencl_gpuid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a30cab554913e27a02512badaf204a266</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>workers_opencl_gpuid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a62170298d1991dc5a9deb44c641cd405</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>use_explicit_workers_mic_deviceid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a052f3cac0ebec9932c66d8f08cd9b6d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>workers_mic_deviceid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a4c988699b09aebac518afd6d30d1e0e8</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>use_explicit_workers_scc_deviceid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a06fa152063011cd6a2f488ffa1e9edf4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>workers_scc_deviceid</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ae3680571934c7ce5137544f00c2fe5ef</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>bus_calibrate</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a7296fd188dcf3c5cc42f2be1cfab8fe2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>calibrate</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ab05d39eda9e1eef89024575fcaccef23</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>single_combined_worker</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a1fc825906ff041878bc3a05ce9681e9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char *</type>
      <name>mic_sink_program_path</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a2e76922dbcff13aab8a12c5f2630ca14</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>disable_asynchronous_copy</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a29b3ff0d1afa9e24a7edbb0765cea487</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>disable_asynchronous_cuda_copy</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a290f151da116c90c48a1885dbba9c072</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>disable_asynchronous_opencl_copy</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>af4da5685fda97febf35c7f02faee66e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>disable_asynchronous_mic_copy</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a24e88b01a2dac637c91f21026bad95dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned *</type>
      <name>cuda_opengl_interoperability</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a172b32e69b5b0d2f1a0566f00b975c30</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>n_cuda_opengl_interoperability</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a3efb09a64357452502843f583b94e5dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_driver *</type>
      <name>not_launched_drivers</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a0977758f4062eccedf0ebd8dab868b5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>n_not_launched_drivers</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ae34c59d8118504e53af4ba88b4254678</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>trace_buffer_size</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a198b97daa32f2d7d7e710be6cbae63c2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_coo_interface</name>
    <filename>group__API__Data__Interfaces.html</filename>
    <anchor>structstarpu__coo__interface</anchor>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a5bf21cb9a0c92c55915427fb2b4cc220</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>columns</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a36f0dcffe049259c8dd7bc13569350f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>rows</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a8cc40e3be0846c55cac5bdffb463e3a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>values</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a3e71b630f78f6f51a0d3942514f7ff9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af9bc59bf8aa5132dcfeaf2c48713b5e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae739aa4a6e1eee69f1fcf144443c98d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>n_values</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a84c37663a9635c0d5699228d37b31324</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>acae72d4dcc23fd3901b3f126ab0c61fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a5bf21cb9a0c92c55915427fb2b4cc220</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>columns</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a36f0dcffe049259c8dd7bc13569350f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>rows</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a8cc40e3be0846c55cac5bdffb463e3a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>values</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a3e71b630f78f6f51a0d3942514f7ff9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af9bc59bf8aa5132dcfeaf2c48713b5e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae739aa4a6e1eee69f1fcf144443c98d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>n_values</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a84c37663a9635c0d5699228d37b31324</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>acae72d4dcc23fd3901b3f126ab0c61fd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_csr_interface</name>
    <filename>group__API__Data__Interfaces.html</filename>
    <anchor>structstarpu__csr__interface</anchor>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a8fca87a97f1f7d086377f4c40aec3f29</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nnz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aad762e1231c7623b5831de7839b91a53</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nrow</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a94fe9c7865c4efd4ffacfeabedce1da7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>nzval</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a74579a6b3cb9e5cedfe7f5e2ae9254b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>colind</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae4c40c6bba28b5c52e143a6c988db6b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>rowptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a222afba88cad959c9108bd15ef18d2ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>firstentry</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4a0da8c56586c9bb0de5001649513b63</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a45554b96395b1b711db98ceb1d4535b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a8fca87a97f1f7d086377f4c40aec3f29</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nnz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aad762e1231c7623b5831de7839b91a53</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nrow</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a94fe9c7865c4efd4ffacfeabedce1da7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>nzval</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a74579a6b3cb9e5cedfe7f5e2ae9254b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>colind</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae4c40c6bba28b5c52e143a6c988db6b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t *</type>
      <name>rowptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a222afba88cad959c9108bd15ef18d2ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>firstentry</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4a0da8c56586c9bb0de5001649513b63</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a45554b96395b1b711db98ceb1d4535b8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_data_copy_methods</name>
    <filename>group__API__Data__Interfaces.html</filename>
    <anchor>structstarpu__data__copy__methods</anchor>
    <member kind="variable">
      <type>int(*</type>
      <name>can_copy</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a0b7f49db0750de6d12b12cde2273908b</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, unsigned handling_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>ram_to_ram</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ac15d27ccf8cdb229d5f232db91301adf</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>ram_to_cuda</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a9dd59b67662ec9b3da55db98e3853cda</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>ram_to_opencl</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a9733f2db0e65e2b057cd0a5840ea7950</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>ram_to_mic</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af2e7b1664ea162473cfdbcf2ab5ec5bf</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>cuda_to_ram</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aeb8e6efbdfbf678de94a7e074e2dda6a</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>cuda_to_cuda</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ada081be63659dbac8f49050cfee4a8bd</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>cuda_to_opencl</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ab4f7966f783722b9b335b7f4649db435</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>opencl_to_ram</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ab662f64e9032d2e3e8c3066dccd1684e</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>opencl_to_cuda</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a85916be52c73ecc7b2116a397e50d05f</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>opencl_to_opencl</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a39acf95bf062cdae5b1e57e851db88de</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>mic_to_ram</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a927710b6c2d099dbe36145ba87df86d1</anchor>
      <arglist>)(void *src_interface, unsigned srd_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>scc_src_to_sink</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a84146443d094c81cdc7cac52b28f5114</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>scc_sink_to_src</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a8b9f37c7fe837334f66ce327a87b89f1</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>scc_sink_to_sink</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aa1025d14aed49b801917a6fa2e60c390</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>ram_to_cuda_async</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a95f800e916adcbfc1aaa36ce9bf1b490</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, starpu_cudaStream_t stream)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>cuda_to_ram_async</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aded4f7a86082a5bf2dbcb8947fa58345</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, starpu_cudaStream_t stream)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>cuda_to_cuda_async</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a00ebf589b7263e0043303c3ecfeb9fa4</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, starpu_cudaStream_t stream)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>ram_to_opencl_async</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a845b9ecc783192f7d2efb3c1485f3fb1</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, cl_event *event)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>opencl_to_ram_async</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a125f9eb59cf59baf4271316d2e445095</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, cl_event *event)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>opencl_to_opencl_async</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ab2bb0678d85ab51345078e581648f17c</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, cl_event *event)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>ram_to_mic_async</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a7d9d144962d2fa3e92d8426e43bdb350</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>mic_to_ram_async</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae5d37ff9f9780d19eae172b0256181ab</anchor>
      <arglist>)(void *src_interface, unsigned srd_node, void *dst_interface, unsigned dst_node)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>any_to_any</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a85548570581df8ee7f1fe24ebf3c7b9b</anchor>
      <arglist>)(void *src_interface, unsigned src_node, void *dst_interface, unsigned dst_node, void *async_data)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_data_descr</name>
    <filename>group__API__Codelet__And__Tasks.html</filename>
    <anchor>structstarpu__data__descr</anchor>
    <member kind="variable">
      <type>starpu_data_handle_t</type>
      <name>handle</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>acb02fc7b0a1ca4f6146cfe5eaaff8f87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_access_mode</type>
      <name>mode</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ab5db40a50f4f086fad3207d8e50de261</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_data_filter</name>
    <filename>group__API__Data__Partition.html</filename>
    <anchor>structstarpu__data__filter</anchor>
    <member kind="variable">
      <type>void(*</type>
      <name>filter_func</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>a1fe0cc960cc4a6a2db7c2e8295b121ca</anchor>
      <arglist>)(void *father_interface, void *child_interface, struct starpu_data_filter *, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>nchildren</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>aec46586c98b5631201151b7582724e12</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned(*</type>
      <name>get_nchildren</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>aba1a8e13ad0f9c970a30232d583b07b4</anchor>
      <arglist>)(struct starpu_data_filter *, starpu_data_handle_t initial_handle)</arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_data_interface_ops *(*</type>
      <name>get_child_ops</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>a930eea80978a13822793f04e93e8a445</anchor>
      <arglist>)(struct starpu_data_filter *, unsigned id)</arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>filter_arg</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>a6cb631e64e61f862647040852fdabe49</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>filter_arg_ptr</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>aeadc5f4c1657e545f2139332321c6450</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_data_interface_ops</name>
    <filename>group__API__Data__Interfaces.html</filename>
    <anchor>structstarpu__data__interface__ops</anchor>
    <member kind="variable">
      <type>void(*</type>
      <name>register_data_handle</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a1827ba20583230840ea591976c103268</anchor>
      <arglist>)(starpu_data_handle_t handle, unsigned home_node, void *data_interface)</arglist>
    </member>
    <member kind="variable">
      <type>starpu_ssize_t(*</type>
      <name>allocate_data_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a2d73b3dc3003c90c9bf537873a6d3d33</anchor>
      <arglist>)(void *data_interface, unsigned node)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>free_data_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ac9d3c54929998e30a5c4946f6ff44d24</anchor>
      <arglist>)(void *data_interface, unsigned node)</arglist>
    </member>
    <member kind="variable">
      <type>const struct starpu_data_copy_methods *</type>
      <name>copy_methods</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a47105eaece0b026f31800291e49cec92</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>handle_to_pointer</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af8f3490f77b1d16073f0e746eda8f561</anchor>
      <arglist>)(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="variable">
      <type>size_t(*</type>
      <name>get_size</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae88d481b7694f7a29af4ee2b629ed069</anchor>
      <arglist>)(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>footprint</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a7ae946a50e588351543c7a4bd1e41b21</anchor>
      <arglist>)(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>compare</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae3809000fed6dc9e95bdfac5a123fe9f</anchor>
      <arglist>)(void *data_interface_a, void *data_interface_b)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>display</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a8f2c112102540473de4333ea30bfb8ba</anchor>
      <arglist>)(starpu_data_handle_t handle, FILE *f)</arglist>
    </member>
    <member kind="variable">
      <type>starpu_ssize_t(*</type>
      <name>describe</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af1969d217f6af0797bc709b016b61bc4</anchor>
      <arglist>)(void *data_interface, char *buf, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>interfaceid</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a755b7d337f0ac2bdf8752bba1039e1b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>interface_size</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a8c4ce623e92d8fd3d87e0ab841ae6038</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>is_multiformat</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ae56f2716fc0a28af725cd6e4148a6844</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>dontcache</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a2c4333ac3a911e749197069fadca1568</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_multiformat_data_interface_ops *(*</type>
      <name>get_mf_ops</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ad444006047b93da8ed6028102a6cdd41</anchor>
      <arglist>)(void *data_interface)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>pack_data</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a3fd7bff8deaf3056c25424d2d958e60b</anchor>
      <arglist>)(starpu_data_handle_t handle, unsigned node, void **ptr, starpu_ssize_t *count)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>unpack_data</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aba40ff0a66fefffc49f2440e43a9d98a</anchor>
      <arglist>)(starpu_data_handle_t handle, unsigned node, void *ptr, size_t count)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_disk_ops</name>
    <filename>group__API__Out__Of__Core.html</filename>
    <anchor>structstarpu__disk__ops</anchor>
    <member kind="variable">
      <type>void *(*</type>
      <name>plug</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a5c9a2e621b2245d2b9a684b94dbc1dea</anchor>
      <arglist>)(void *parameter, starpu_ssize_t size)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>unplug</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>aad1b835b2c5dceb66d254ebfbb9c8641</anchor>
      <arglist>)(void *base)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>bandwidth</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a288de4b6f4c98dd092ea61400d8707b3</anchor>
      <arglist>)(unsigned node)</arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>alloc</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a961bb551927dba496faa49eecb1bc038</anchor>
      <arglist>)(void *base, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>free</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a48f728320480e5d25d8994f35d22ee44</anchor>
      <arglist>)(void *base, void *obj, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>open</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ae36a0753adcf848d1ebc921d861d8596</anchor>
      <arglist>)(void *base, void *pos, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>close</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ab6cd099afd9d1e13db2a6fd0f7492444</anchor>
      <arglist>)(void *base, void *obj, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>read</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ab0553e3a7ebc9e2b758bd899a1daa1ee</anchor>
      <arglist>)(void *base, void *obj, void *buf, off_t offset, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>write</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>af13aed2b7fdca833d2f0048bd5fa7a2c</anchor>
      <arglist>)(void *base, void *obj, const void *buf, off_t offset, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>full_read</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a7987ae5399f0ad84f4ecadbd8245d8aa</anchor>
      <arglist>)(void *base, void *obj, void **ptr, size_t *size)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>full_write</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a1ffd343dbc9ede17d031afe95a9330ba</anchor>
      <arglist>)(void *base, void *obj, void *ptr, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>async_write</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>aeb20d34b157e2ba7e440cf956c2fdcea</anchor>
      <arglist>)(void *base, void *obj, void *buf, off_t offset, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>async_read</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a20a4ebefcef246101a918d98f5d7b434</anchor>
      <arglist>)(void *base, void *obj, void *buf, off_t offset, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>async_full_read</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a7908803880126fdd01315d8426cd0649</anchor>
      <arglist>)(void *base, void *obj, void **ptr, size_t *size)</arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>async_full_write</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a2d0388b7a7f7f7b0e221eb2af8975ece</anchor>
      <arglist>)(void *base, void *obj, void *ptr, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>copy</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a8de6e65b6acc90d8fff377f90dd50563</anchor>
      <arglist>)(void *base_src, void *obj_src, off_t offset_src, void *base_dst, void *obj_dst, off_t offset_dst, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>wait_request</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a9df8ee8098d6304314f95a448cbf8822</anchor>
      <arglist>)(void *async_channel)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>test_request</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ac6f3ab7262523ecde01e75fa80fff32f</anchor>
      <arglist>)(void *async_channel)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>free_request</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>a2b9bc28f2e7af2aa4d0aa666e1ef0bee</anchor>
      <arglist>)(void *async_channel)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_driver</name>
    <filename>group__API__Initialization__and__Termination.html</filename>
    <anchor>structstarpu__driver</anchor>
    <member kind="variable">
      <type>enum starpu_worker_archtype</type>
      <name>type</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>a50af396b52e8c54e4e3b23803105fc0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>union starpu_driver::@0</type>
      <name>id</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>aa29a395ef3d7f4c9f060fc2a5db73c60</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="union">
    <name>starpu_driver.id</name>
    <filename>group__API__Initialization__and__Termination.html</filename>
    <anchor>unionstarpu__driver_8id</anchor>
  </compound>
  <compound kind="struct">
    <name>starpu_fxt_codelet_event</name>
    <filename>group__API__FxT__Support.html</filename>
    <anchor>structstarpu__fxt__codelet__event</anchor>
    <member kind="variable">
      <type>char</type>
      <name>symbol</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>a51da03a4801f1824a16cddcbcc515054</anchor>
      <arglist>[256]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_fxt_options</name>
    <filename>group__API__FxT__Support.html</filename>
    <anchor>structstarpu__fxt__options</anchor>
    <member kind="variable">
      <type>char *</type>
      <name>file_prefix</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>a14983a34fc1560aa035636df29abf532</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint64_t</type>
      <name>file_offset</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>a2ce33ee285499c1ca00f149293a1978d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>file_rank</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ad989ef1acebea5296285ba1543b9b8d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char</type>
      <name>worker_names</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>a3999b83fc470285e27bcdd04c9fa7f86</anchor>
      <arglist>[STARPU_NMAXWORKERS][256]</arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_perfmodel_arch</type>
      <name>worker_archtypes</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>a41dd735e884cd2e03ecd52e9d030ec6f</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nworkers</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>a2a1bec69241ff9acf6f991492de18190</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_fxt_codelet_event **</type>
      <name>dumped_codelets</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ae817dcebd02f4ad96baad94450dd3a84</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>long</type>
      <name>dumped_codelets_count</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>a66ac04619aec557f95ddf5a5df848341</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_matrix_interface</name>
    <filename>group__API__Data__Interfaces.html</filename>
    <anchor>structstarpu__matrix__interface</anchor>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aaff7aa638a24957eb76c5c40f96cddbe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4b6ebe9c32b5ea09236351b673beb24b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>dev_handle</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ac4101c57f4ebdb84a770d7a6450a6f15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>offset</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aad976625b7cb9b3044bc698b442fb4e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af00bd371238ac5b99036775525ac4543</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ad608771aa4de1f8e55ec6f00092d1705</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ld</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a5b0873b99f5b554ba3378c41cb31a63c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4768b7c5a77140d416c879c797d4c0e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aaff7aa638a24957eb76c5c40f96cddbe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4b6ebe9c32b5ea09236351b673beb24b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>dev_handle</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ac4101c57f4ebdb84a770d7a6450a6f15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>offset</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aad976625b7cb9b3044bc698b442fb4e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>af00bd371238ac5b99036775525ac4543</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ad608771aa4de1f8e55ec6f00092d1705</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>ld</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a5b0873b99f5b554ba3378c41cb31a63c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4768b7c5a77140d416c879c797d4c0e2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_multiformat_data_interface_ops</name>
    <filename>group__API__Multiformat__Data__Interface.html</filename>
    <anchor>structstarpu__multiformat__data__interface__ops</anchor>
    <member kind="variable">
      <type>size_t</type>
      <name>cpu_elemsize</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>a67a1f3202dfc2512ed7ed14bed4f3623</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>opencl_elemsize</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>a7c0e09ea6559bef4579a1fb1e57f67d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_codelet *</type>
      <name>cpu_to_opencl_cl</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>af77c2819850101b0ae361c7d2ce5c6fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_codelet *</type>
      <name>opencl_to_cpu_cl</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>a7e499a277c5d18b2c0d614af5e80212e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>cuda_elemsize</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>a00927819b3899892ec152968efe0b158</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_codelet *</type>
      <name>cpu_to_cuda_cl</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>a5214fba07c50c3c31682f8442ace9030</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_codelet *</type>
      <name>cuda_to_cpu_cl</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>ad40fb377f8bc7ae07a8bb710e9becfda</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>mic_elemsize</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>a3788fdc38fb2941ca16761d33e96c022</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_codelet *</type>
      <name>cpu_to_mic_cl</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>add5d3461daef2bd585cefacf7cabe3e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_codelet *</type>
      <name>mic_to_cpu_cl</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>abd294b29880ba6f4a363a3ff48a2d332</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_multiformat_interface</name>
    <filename>group__API__Multiformat__Data__Interface.html</filename>
    <anchor>structstarpu__multiformat__interface</anchor>
  </compound>
  <compound kind="struct">
    <name>starpu_omp_lock_t</name>
    <filename>group__API__OpenMP__Runtime__Support.html</filename>
    <anchor>structstarpu__omp__lock__t</anchor>
    <member kind="variable">
      <type>void *</type>
      <name>internal</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>a95ee21c4ac86e49158c8ea32084f7231</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_omp_nest_lock_t</name>
    <filename>group__API__OpenMP__Runtime__Support.html</filename>
    <anchor>structstarpu__omp__nest__lock__t</anchor>
    <member kind="variable">
      <type>void *</type>
      <name>internal</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>a7ab2535d19a318b9981c45468ea25c0f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_omp_parallel_region_attr</name>
    <filename>group__API__OpenMP__Runtime__Support.html</filename>
    <anchor>structstarpu__omp__parallel__region__attr</anchor>
    <member kind="variable">
      <type>struct starpu_codelet</type>
      <name>cl</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>a983c8209cef872e2798c2fee32012195</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_data_handle_t *</type>
      <name>handles</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>afa36936cd4bef49a178eb807bb4848ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>cl_arg</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ae3947bd966486ee4eef7611f0e502e90</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>cl_arg_size</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ab804599e2d1ce232830954dbdf3ed886</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>cl_arg_free</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>aea66f9b4181afb2e4b4c799a09d8a4f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>if_clause</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>aecf5779017beb31e13424202f9dc013b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>num_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>a294032947604cf227abad87b30a08c27</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_omp_task_region_attr</name>
    <filename>group__API__OpenMP__Runtime__Support.html</filename>
    <anchor>structstarpu__omp__task__region__attr</anchor>
    <member kind="variable">
      <type>struct starpu_codelet</type>
      <name>cl</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>a4f4e595f12101bbace47d4748d6d3e3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_data_handle_t *</type>
      <name>handles</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>afe263b12cfb25f6a02b273c371f40965</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>cl_arg</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>aa39a9bb2f3af5b0e3dc1d5d52da7d0f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>cl_arg_size</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>a594489c9f845922d685046c25f5540e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>cl_arg_free</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ad64a8be227ee0a2c4ea8fa2eca3d20c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>if_clause</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>afa30d3c752ab9665112df925a39cb4ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>final_clause</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ae97da9038c9a034dc8734ae4c34ebc21</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>untied_clause</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>abb648c16a9ec65f2834cf05994b33df2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>mergeable_clause</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>a6e3c542eac204dd67f343427c8a0a17c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_opencl_program</name>
    <filename>group__API__OpenCL__Extensions.html</filename>
    <anchor>structstarpu__opencl__program</anchor>
    <member kind="variable">
      <type>cl_program</type>
      <name>programs</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>a685357d9709ece920bf269a917ed8297</anchor>
      <arglist>[STARPU_MAXOPENCLDEVS]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_perfmodel</name>
    <filename>group__API__Performance__Model.html</filename>
    <anchor>structstarpu__perfmodel</anchor>
    <member kind="variable">
      <type>enum starpu_perfmodel_type</type>
      <name>type</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>afe2d561aaba7bf1ad1cf03974ee8c53c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double(*</type>
      <name>cost_function</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a7eef396db161173be7cc698c0ef4da85</anchor>
      <arglist>)(struct starpu_task *, unsigned nimpl)</arglist>
    </member>
    <member kind="variable">
      <type>double(*</type>
      <name>arch_cost_function</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>addb917cb65aee1ebb24a6af57ef31b4e</anchor>
      <arglist>)(struct starpu_task *, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="variable">
      <type>size_t(*</type>
      <name>size_base</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a3cc9f010dd62bf59c1e43e361ea8302f</anchor>
      <arglist>)(struct starpu_task *, unsigned nimpl)</arglist>
    </member>
    <member kind="variable">
      <type>uint32_t(*</type>
      <name>footprint</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>abc0f160693746f0b2b7c0af52a186783</anchor>
      <arglist>)(struct starpu_task *)</arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>symbol</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>aa1c188b698608555c94952f9c09e3825</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>is_loaded</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ae37737aa0b7abc5f5813bf61814d996f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>is_init</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>add502ebee1bcb74c0453fac74b7b0f9e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_perfmodel_arch</name>
    <filename>starpu__perfmodel_8h.html</filename>
    <anchor>structstarpu__perfmodel__arch</anchor>
  </compound>
  <compound kind="struct">
    <name>starpu_perfmodel_device</name>
    <filename>starpu__perfmodel_8h.html</filename>
    <anchor>structstarpu__perfmodel__device</anchor>
  </compound>
  <compound kind="struct">
    <name>starpu_perfmodel_history_entry</name>
    <filename>group__API__Performance__Model.html</filename>
    <anchor>structstarpu__perfmodel__history__entry</anchor>
    <member kind="variable">
      <type>double</type>
      <name>mean</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a8ae18db359c517f5a60c4aff98f12235</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>deviation</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a2a0fd05b4b1256f854f8c8e0e3d708c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>sum</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ab1a7822099d984503f1c67d45ab6f4b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>sum2</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a0d9823a671de1b10de6c2bb4ced92163</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>nsample</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a9e662c85f8bb1e134d4885c77ba8a32f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>footprint</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>adb5e09576697a74975467483d3c080a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>size</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a247765c3a84e99b1a0906e26f63151d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>flops</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>af39728c1095f645bf195430903532a36</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_perfmodel_history_list</name>
    <filename>group__API__Performance__Model.html</filename>
    <anchor>structstarpu__perfmodel__history__list</anchor>
    <member kind="variable">
      <type>struct starpu_perfmodel_history_list *</type>
      <name>next</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>aeed4207613396d7e3bf51b559d1c238c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_perfmodel_history_entry *</type>
      <name>entry</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ad193f9da07009593714f515a16be0eeb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_perfmodel_per_arch</name>
    <filename>group__API__Performance__Model.html</filename>
    <anchor>structstarpu__perfmodel__per__arch</anchor>
    <member kind="variable">
      <type>starpu_perfmodel_per_arch_cost_function</type>
      <name>cost_function</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a13f34a1532294c101254beb106f124f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_perfmodel_per_arch_size_base</type>
      <name>size_base</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>aa82fae955253d333733d34fe4a44e298</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_perfmodel_history_table *</type>
      <name>history</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a27f17b0a2f067fc3f2cbd79ddaab7513</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_perfmodel_history_list *</type>
      <name>list</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a96e7105b1a578d11be0b206a1708a133</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_perfmodel_regression_model</type>
      <name>regression</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a1b50329c04b9326c72130f83c91c1c28</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_perfmodel_regression_model</name>
    <filename>group__API__Performance__Model.html</filename>
    <anchor>structstarpu__perfmodel__regression__model</anchor>
    <member kind="variable">
      <type>double</type>
      <name>sumlny</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a77f3000cf6f0dff4ac86b6664cc386ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>sumlnx</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>acce4202682709ec0e1f0f22ecc55d1d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>sumlnx2</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a4f23ac95d204cc38c51c3e0e5552ef62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned long</type>
      <name>minx</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a6e3fa8bbf9c3b3e09d248d1d9abd62ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned long</type>
      <name>maxx</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ad691b4ef9fc2367e13aba7d578832763</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>sumlnxlny</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a8ddcc5e8aeaf6dd2459e883fab483409</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>alpha</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>afb6b1e89bf7820131bddf970bb0df58b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>beta</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>aa69cc173b9ebdbf129eb1b30ca64ddd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>valid</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a1e6c12b85fc84336010f580874ec494a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>a</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ab718a53c300f99766bdc42ca1909b49b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>b</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>af5497d7f073aa115e25d6d1a32c2e668</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>c</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ac651825cdcd22fb9e6389d73c1b4f275</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>nl_valid</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>a8c4568a5722baebd67148642ee3377e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>nsample</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ad141b7ae874d40dabeea131474ba29c0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_profiling_bus_info</name>
    <filename>group__API__Profiling.html</filename>
    <anchor>structstarpu__profiling__bus__info</anchor>
    <member kind="variable">
      <type>struct timespec</type>
      <name>start_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a8032d6b4c3391555b255718d7da4279c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>total_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a46492a6afad2c9069b84110d6d9e86be</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int long long</type>
      <name>transferred_bytes</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a029068c588c17476bf07a4a4a2bd690b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>transfer_count</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a34ad3d96e949d8030473300e13aa1c0c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_profiling_task_info</name>
    <filename>group__API__Profiling.html</filename>
    <anchor>structstarpu__profiling__task__info</anchor>
    <member kind="variable">
      <type>struct timespec</type>
      <name>submit_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a3a1a7c8e4ce2552a0f96a8dd4f9ad37f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>push_start_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>aa28c145b4543cceed05a1bba0478577e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>push_end_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a650860076a4d4c07929e97ec64386724</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>pop_start_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a55f39da3b4c223d4cfc5bf7456692179</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>pop_end_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a9e1ae88eb46e9b9867cb41d0584ace7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>acquire_data_start_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a8f1fe52f21a7fc58860cf2f163ec868a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>acquire_data_end_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a0a43cc6fa5233d8d7e7ab0821607068c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>start_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ab8de457be74df606d29c3efa1169998a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>end_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ade189cd6175d789db1eb6bfad19b3e35</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>release_data_start_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>aae7bd9f557dffe9883629df980eee606</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>release_data_end_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a8f4291afbbd7fcf885a3d30a72f2dd78</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>callback_start_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a4ad1ae7ed15ea7001b5dc8e1d9579235</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>callback_end_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a3296b9aa5339bf99f00f5377517c2390</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>workerid</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a19a06e7ef34a9a4587e77949ded36644</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint64_t</type>
      <name>used_cycles</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>aec88f33a8b4777727d80296c16d85f24</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint64_t</type>
      <name>stall_cycles</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a78e32404bdff71292562d2dc5892061c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>energy_consumed</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a6e85ac77428f572c5380e283b943459c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_profiling_worker_info</name>
    <filename>group__API__Profiling.html</filename>
    <anchor>structstarpu__profiling__worker__info</anchor>
    <member kind="variable">
      <type>struct timespec</type>
      <name>start_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>aed6a8be9b919f03e093912bfc44c5559</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>total_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>aae49335c3c189b58103cb649fc169096</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>executing_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a050199a48e0073f892cd6dd9049a4edf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct timespec</type>
      <name>sleeping_time</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>acc882d8569e28d51fd5e820962fed773</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>executed_tasks</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a73a1303e033dffe94ff162e8e21122d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint64_t</type>
      <name>used_cycles</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a5f76b7a4cc42a28735e0871439040319</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint64_t</type>
      <name>stall_cycles</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a24037f0f53113b23f32c7af7a39f982d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>energy_consumed</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>a52240aa53000a12a7b913f9f5822e032</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_pthread_barrier_t</name>
    <filename>starpu__thread_8h.html</filename>
    <anchor>structstarpu__pthread__barrier__t</anchor>
  </compound>
  <compound kind="struct">
    <name>starpu_pthread_spinlock_t</name>
    <filename>structstarpu__pthread__spinlock__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_component</name>
    <filename>group__API__Modularized__Scheduler.html</filename>
    <anchor>structstarpu__sched__component</anchor>
    <member kind="variable">
      <type>struct starpu_sched_tree *</type>
      <name>tree</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>aeff012ebe6a003c49c1d89b602129bea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_bitmap *</type>
      <name>workers</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a46b4575caf31a2457725a41932a054a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_bitmap *</type>
      <name>workers_in_ctx</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a971c0818769e5b2f7b57258391d940db</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>data</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a46fb568ec57c55b9ebbcc0ae97517b70</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nchildren</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a0c314e02fd923cd38dab4f1330f1367a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_sched_component **</type>
      <name>children</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a50f58b478e1299c55f27d6b31b3ba353</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nparents</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>abc1ee760eaa745ac58589a062d360eab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_sched_component **</type>
      <name>parents</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a6b9e7e18acce28c825c41afcea0fae23</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>add_child</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a7d4a90d8feb4b998353f66c7dbb79407</anchor>
      <arglist>)(struct starpu_sched_component *component, struct starpu_sched_component *child)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>remove_child</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a3c23297722c21dfafd05770292e56951</anchor>
      <arglist>)(struct starpu_sched_component *component, struct starpu_sched_component *child)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>add_parent</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ae469819240b44a6042ae0066745dc928</anchor>
      <arglist>)(struct starpu_sched_component *component, struct starpu_sched_component *parent)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>remove_parent</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>aefb1c62cc3c72089a321da3be4ed0795</anchor>
      <arglist>)(struct starpu_sched_component *component, struct starpu_sched_component *parent)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>push_task</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a8de7d0d16c872794140e3befe7603267</anchor>
      <arglist>)(struct starpu_sched_component *, struct starpu_task *)</arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_task *(*</type>
      <name>pull_task</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ad55b64f2d5f2600410d985f05863eca4</anchor>
      <arglist>)(struct starpu_sched_component *)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>can_push</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a8ec954f0a3a703befd1baa79b1eba644</anchor>
      <arglist>)(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>can_pull</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a7cad596a6fec4e4869375bac61aa66e7</anchor>
      <arglist>)(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="variable">
      <type>double(*</type>
      <name>estimated_load</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>aec1b7fdefed30731c295e1b38d2d11ad</anchor>
      <arglist>)(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="variable">
      <type>double(*</type>
      <name>estimated_end</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a407a9af14c54c81812a65f20b00af962</anchor>
      <arglist>)(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>deinit_data</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a77aa479ad0dfc8ab1eefb036d94969b3</anchor>
      <arglist>)(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>notify_change_workers</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a50970e654ab95a4022b507b847c012d4</anchor>
      <arglist>)(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>properties</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a9d6dd088345bfc11045a3954e4c02fb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>hwloc_obj_t</type>
      <name>obj</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ac2709f00c42977b8df96d30015fc9cb1</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_component_composed_recipe</name>
    <filename>group__API__Modularized__Scheduler.html</filename>
    <anchor>structstarpu__sched__component__composed__recipe</anchor>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_component_fifo_data</name>
    <filename>group__API__Modularized__Scheduler.html</filename>
    <anchor>structstarpu__sched__component__fifo__data</anchor>
    <member kind="variable">
      <type>unsigned</type>
      <name>ntasks_threshold</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ad59e7b45478c7b5cc2b2220ca36d575d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>exp_len_threshold</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a0c2968f2cd91ab056140d392e507a84d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>ntasks_threshold</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ad59e7b45478c7b5cc2b2220ca36d575d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>exp_len_threshold</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a0c2968f2cd91ab056140d392e507a84d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_component_mct_data</name>
    <filename>group__API__Modularized__Scheduler.html</filename>
    <anchor>structstarpu__sched__component__mct__data</anchor>
    <member kind="variable">
      <type>double</type>
      <name>alpha</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a460be4e5f430a5486cc9b96ed0529e96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>beta</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a34c42373292795f52d2584647f169ab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>_gamma</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ab828520069d7eeb7444b024f3afcd124</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>idle_power</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>aa8044e4496b591ffb05a2529c21223a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>alpha</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a460be4e5f430a5486cc9b96ed0529e96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>beta</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a34c42373292795f52d2584647f169ab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>_gamma</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ab828520069d7eeb7444b024f3afcd124</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>idle_power</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>aa8044e4496b591ffb05a2529c21223a8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_component_perfmodel_select_data</name>
    <filename>group__API__Modularized__Scheduler.html</filename>
    <anchor>structstarpu__sched__component__perfmodel__select__data</anchor>
    <member kind="variable">
      <type>struct starpu_sched_component *</type>
      <name>calibrator_component</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ae54560a56adf9ceac105010e78183a25</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_sched_component *</type>
      <name>no_perfmodel_component</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ae059d3bec90ede78b9f364f260cb2f4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_sched_component *</type>
      <name>perfmodel_component</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a3018b6f30df9abd050d59769aa0ab97a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_sched_component *</type>
      <name>calibrator_component</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ae54560a56adf9ceac105010e78183a25</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_sched_component *</type>
      <name>no_perfmodel_component</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ae059d3bec90ede78b9f364f260cb2f4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_sched_component *</type>
      <name>perfmodel_component</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a3018b6f30df9abd050d59769aa0ab97a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_component_prio_data</name>
    <filename>group__API__Modularized__Scheduler.html</filename>
    <anchor>structstarpu__sched__component__prio__data</anchor>
    <member kind="variable">
      <type>unsigned</type>
      <name>ntasks_threshold</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a7fe9a9a5f284c1688e02ead74e3ae57e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>exp_len_threshold</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a1fd09bcc399aaacfaab10016f5f6df01</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>ntasks_threshold</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a7fe9a9a5f284c1688e02ead74e3ae57e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>exp_len_threshold</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a1fd09bcc399aaacfaab10016f5f6df01</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_component_specs</name>
    <filename>group__API__Modularized__Scheduler.html</filename>
    <anchor>structstarpu__sched__component__specs</anchor>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_ctx_iterator</name>
    <filename>group__API__Workers__Properties.html</filename>
    <anchor>structstarpu__sched__ctx__iterator</anchor>
    <member kind="variable">
      <type>int</type>
      <name>cursor</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>a3460c754c642b2244d4c2ceb98101ece</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_ctx_performance_counters</name>
    <filename>group__API__Scheduling__Contexts.html</filename>
    <anchor>structstarpu__sched__ctx__performance__counters</anchor>
    <member kind="variable">
      <type>void(*</type>
      <name>notify_idle_cycle</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>a14324cd3fcaa8c82ebe115c51bc52a7a</anchor>
      <arglist>)(unsigned sched_ctx_id, int worker, double idle_time)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>notify_poped_task</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>a49506fe068d8397c0eaaff04a8196c29</anchor>
      <arglist>)(unsigned sched_ctx_id, int worker)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>notify_pushed_task</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>a3be16a22d2122868d2949596f6c7dd38</anchor>
      <arglist>)(unsigned sched_ctx_id, int worker)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>notify_post_exec_task</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>a507f97eb95585e2f853ea8d9dc169a6a</anchor>
      <arglist>)(struct starpu_task *task, size_t data_size, uint32_t footprint, int hypervisor_tag, double flops)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>notify_submitted_job</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>a95eede95625a1f46b8ca41ca5c7871fd</anchor>
      <arglist>)(struct starpu_task *task, uint32_t footprint, size_t data_size)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>notify_delete_context</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ab9790a9fdc26eca8c1ff12bd0b19b4a5</anchor>
      <arglist>)(unsigned sched_ctx)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_policy</name>
    <filename>group__API__Scheduling__Policy.html</filename>
    <anchor>structstarpu__sched__policy</anchor>
    <member kind="variable">
      <type>void(*</type>
      <name>init_sched</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>a58f189e5addbfef1c29cc2cc0d9c42c2</anchor>
      <arglist>)(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>deinit_sched</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ac9c1098f2c5d568f0d11e31e15a797ad</anchor>
      <arglist>)(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>push_task</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ae1e6249dab367bb624fff7ed78aaa44b</anchor>
      <arglist>)(struct starpu_task *)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>push_task_notify</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>a286509b7e7746b6a38aacf99bd75dba1</anchor>
      <arglist>)(struct starpu_task *, int workerid, int perf_workerid, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_task *(*</type>
      <name>pop_task</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>a1b2449ac50ca8db35234a36a3f77b0b5</anchor>
      <arglist>)(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_task *(*</type>
      <name>pop_every_task</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>aeaf111fe27e97f13d9fd72a547ef5479</anchor>
      <arglist>)(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>submit_hook</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>aa635379ad01834a6189fc1fe68fa845b</anchor>
      <arglist>)(struct starpu_task *task)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>pre_exec_hook</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ae72a2f3680ad01dc38471140c94dadb8</anchor>
      <arglist>)(struct starpu_task *)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>post_exec_hook</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>aba44c1b62ba18004d79f28c53682d2ea</anchor>
      <arglist>)(struct starpu_task *)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>do_schedule</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>a31a640d992698457c8ce098df4df780a</anchor>
      <arglist>)(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>add_workers</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>a842e6c7a3060d9bc3e0a665c43ff48a9</anchor>
      <arglist>)(unsigned sched_ctx_id, int *workerids, unsigned nworkers)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>remove_workers</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>a3d09b198c79e9db70171836867c8f6ce</anchor>
      <arglist>)(unsigned sched_ctx_id, int *workerids, unsigned nworkers)</arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>policy_name</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>a2beaee653d6a33fab4be1b05382efd56</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>policy_description</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>a707d1428ca78b1c0363d640d3db647ff</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_sched_tree</name>
    <filename>group__API__Modularized__Scheduler.html</filename>
    <anchor>structstarpu__sched__tree</anchor>
    <member kind="variable">
      <type>struct starpu_sched_component *</type>
      <name>root</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>adcfa35af2007049101230176b74eed79</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_bitmap *</type>
      <name>workers</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a39346323faf0473e52712f1da6fa17ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>sched_ctx_id</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a79f98f04b4efe58b32fe1ab4f244fe46</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_sched_component *</type>
      <name>worker_components</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a99cd4b522f2b1153d37dbc8804c87080</anchor>
      <arglist>[STARPU_NMAXWORKERS]</arglist>
    </member>
    <member kind="variable">
      <type>starpu_pthread_mutex_t</type>
      <name>lock</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>a66b33bee46a8d853dd25ee16645cf673</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_task</name>
    <filename>group__API__Codelet__And__Tasks.html</filename>
    <anchor>structstarpu__task</anchor>
    <member kind="variable">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>abae08b8725060ad6341d77cbc6fe7527</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_codelet *</type>
      <name>cl</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ac4012012eb243e8d6f6574334dc99460</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nbuffers</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>aa9986f125389781fb7ba29e888f2dc05</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_data_handle_t</type>
      <name>handles</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>af3ce0252f1ac2238325033386a726df3</anchor>
      <arglist>[STARPU_NMAXBUFS]</arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>interfaces</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a08159d40a2d691042239d59abdac738f</anchor>
      <arglist>[STARPU_NMAXBUFS]</arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_access_mode</type>
      <name>modes</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a44a0198b94a434286072f5beae8869d6</anchor>
      <arglist>[STARPU_NMAXBUFS]</arglist>
    </member>
    <member kind="variable">
      <type>starpu_data_handle_t *</type>
      <name>dyn_handles</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a971793c38524b5932783e64c117125fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void **</type>
      <name>dyn_interfaces</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a768d94c0e4f3683c5188700a52f0bbea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_access_mode *</type>
      <name>dyn_modes</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ae6973a1933b672d81008e09cbe4942cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>cl_arg</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a08dfa6e007d0ada924ac388679ec2b91</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>cl_arg_size</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>adac9af88fed23241fc65259712352126</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>callback_func</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>aece9779d31a532e836b2cf5e654d7fb6</anchor>
      <arglist>)(void *)</arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>callback_arg</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>aaa114d89c298bbd66c6d8b1c06130297</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>prologue_callback_func</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a94f417b6a5ffc7b523d2c8c5216fc761</anchor>
      <arglist>)(void *)</arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>prologue_callback_arg</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a88b3546e5009ea2a1912db3974f609c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_tag_t</type>
      <name>tag_id</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a845187be6bfe1c241663913a98eefa65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>cl_arg_free</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a3da11609ab9c508bb753cae1b2d8f24f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>callback_arg_free</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a397c52cbdde57ee5d09b7d929989ca8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>prologue_callback_arg_free</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a60a68835ae9e767fa8c3fb483cebf786</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>use_tag</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a31788bf459e8db1e90aa6673b1c72fa1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>sequential_consistency</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ae58d5501929b01629dc8b9e3da0cf9ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>synchronous</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a18439d7a6d4ad65b75c75ec02d60075e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>execute_on_a_specific_worker</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a52e061fbdb64f2e48569951ca4b253be</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>detach</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a9adac8be1a174cb4ad9d8e68c2ec7678</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>destroy</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a3ee97849eb5c84df12676547d5c48aaf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>regenerate</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>aeb7ea3b608ce63f9a344558657cc913a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>workerid</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a6a19a060f8f417fcc1f0e0196e599d33</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>workerorder</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a704b64ae35fc117b93a88ade0e1d6910</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>scheduled</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ad9c17b28982f4cf7074f2e39a45a9acc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned int</type>
      <name>mf_skip</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a56caa1da383d6627cb32c689562bca7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>priority</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a9d78b1dc8fe2691f38fb16cdf726a376</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_task_status</type>
      <name>status</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a71d0b3bfe39f4c5f0ecccd7bc39aacd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>magic</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ac212149d37dfc35de78cf1870ad982a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>sched_ctx</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a0764b7bdd11bfc686aa7ca4cddccd27a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>hypervisor_tag</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a48416cfa1692f8e8cb42d80ec9f1eb5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>starpu_task_bundle_t</type>
      <name>bundle</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a00b8193077f6e6cd5f40de671a9d776b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_profiling_task_info *</type>
      <name>profiling_info</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a913534c75325e4f5a304fcd7ba9d4e4f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>flops</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a840563895dbf036b6ffd783a8ea2504d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>predicted</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a94baa4ce4eeab0a09edbeb4476eba98f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>predicted_transfer</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a23770384ac7f49bb27b6593fedfa8d5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_task *</type>
      <name>prev</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a575cd1b50f8299c1c8dfe015036cbc31</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_task *</type>
      <name>next</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>a920349904ab4163f7f36b3f0f1dbd8a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>starpu_private</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>af4af32520531879aaddcb6ac4e9d1c54</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_task_list</name>
    <filename>group__API__Task__Lists.html</filename>
    <anchor>structstarpu__task__list</anchor>
    <member kind="variable">
      <type>struct starpu_task *</type>
      <name>head</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>ad49cf3591545b14a6a844889e1b6d4d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_task *</type>
      <name>tail</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>af035844f67064dd96eacd53c15f48af9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_top_data</name>
    <filename>group__API__StarPUTop__Interface.html</filename>
    <anchor>structstarpu__top__data</anchor>
    <member kind="variable">
      <type>unsigned int</type>
      <name>id</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ab08004f1be73431d392b876eb9e00a90</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>aef0a7e2d4a5b3281e48402213bbfd901</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>int_min_value</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a3243c1dc916476f113414cc03f73f67c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>int_max_value</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a51ff532ad5bf1b9de6e3981157246b9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>double_min_value</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a8c1bfa9a126fa76e3f2dbe906c3c25c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>double_max_value</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>afef50ec0b6f270b8daad5392609350a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>active</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>abe5be1f29129de881ff2b8d6d0c4bf35</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_top_data_type</type>
      <name>type</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a898926251cb31a76fda59bf9bed251c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_top_data *</type>
      <name>next</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>aa7566a03b688e0314f1e13f6ce5fe25c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_top_param</name>
    <filename>group__API__StarPUTop__Interface.html</filename>
    <anchor>structstarpu__top__param</anchor>
    <member kind="variable">
      <type>unsigned int</type>
      <name>id</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a2dd63e94fdeb6e6929d1c676cb0dd74c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a2f5eb37458810f97ffd4fb9652b70348</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_top_param_type</type>
      <name>type</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a972da3f06fcd5f114b74c6e6df3a859b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>value</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a16aa5a8f01a839a435d722a9e6f3fd09</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char **</type>
      <name>enum_values</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>adba7ba9b10313d38574220b2aae4db54</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nb_values</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>aac7c7c95ea1a484f22c80828288c6f25</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>callback</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ac683e206754c8009b8238fcd2037dd9c</anchor>
      <arglist>)(struct starpu_top_param *)</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>int_min_value</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a1d1c10d39d758d0902988e7e4d7fc4c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>int_max_value</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a05647b004b28e46a97461d9423ced955</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>double_min_value</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ac829995d14e3f1ac1bc6726ca0a03328</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>double_max_value</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a2264d890748ad325a82a50b8336a1dd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_top_param *</type>
      <name>next</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>a6d70b9fc16d0b4f897ec6c0ce9f613cc</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_tree</name>
    <filename>group__API__Tree.html</filename>
    <anchor>structstarpu__tree</anchor>
    <member kind="variable">
      <type>struct starpu_tree **</type>
      <name>nodes</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>a8f74fdf54efea0395ba33d9ce52577e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_tree *</type>
      <name>father</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>ac01e6be3a60c6aa9076738000d6d0905</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>arity</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>a35075f093a5a19fe76660fe477675893</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>id</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>ac97819c01fbab8fa16b3a8e9b9ee84e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>level</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>a76a0281cb770a7d13abd558326efa699</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>is_pu</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>a084ffa4ca156bc6069793eb8c4007375</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_variable_interface</name>
    <filename>group__API__Data__Interfaces.html</filename>
    <anchor>structstarpu__variable__interface</anchor>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a08694d4a3eba6826b7547d3bd1322702</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a22bff436f00d8f1bc3a780c3f9888759</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>dev_handle</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a0670ed209a3743268fe08824348449f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>offset</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aeb8ae0434284030f7c898374eedb5034</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a59ac4839d899cf1d8a0c5fea68652f95</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a08694d4a3eba6826b7547d3bd1322702</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a22bff436f00d8f1bc3a780c3f9888759</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>dev_handle</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a0670ed209a3743268fe08824348449f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>offset</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>aeb8ae0434284030f7c898374eedb5034</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a59ac4839d899cf1d8a0c5fea68652f95</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_vector_interface</name>
    <filename>group__API__Data__Interfaces.html</filename>
    <anchor>structstarpu__vector__interface</anchor>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>abf4cecec3e8bcec447ec51416b8ac3d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a763e990c19520a68ebde783297107a2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>dev_handle</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4e3533d0af8b68987b86432ce6e37a50</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>offset</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ab02b430abde33905606cadd02374fb59</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ad9ae80b9516186ddfd6ee34b74c85674</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a9ea66093fee786a725d8681e11c13c33</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>slice_base</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a90c8cddece28b8576636e6ae4953a30f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_data_interface_id</type>
      <name>id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>abf4cecec3e8bcec447ec51416b8ac3d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a763e990c19520a68ebde783297107a2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uintptr_t</type>
      <name>dev_handle</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a4e3533d0af8b68987b86432ce6e37a50</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>offset</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ab02b430abde33905606cadd02374fb59</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ad9ae80b9516186ddfd6ee34b74c85674</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a9ea66093fee786a725d8681e11c13c33</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>slice_base</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>a90c8cddece28b8576636e6ae4953a30f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>starpu_worker_collection</name>
    <filename>group__API__Workers__Properties.html</filename>
    <anchor>structstarpu__worker__collection</anchor>
    <member kind="variable">
      <type>void *</type>
      <name>workerids</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>a02433528694af02d6c2772b18248c3ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned</type>
      <name>nworkers</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>a10961e3af7749f812b744dbd9f85da96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>enum starpu_worker_collection_type</type>
      <name>type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>acbe5dc434c779fec539a427b8e8ab1aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned(*</type>
      <name>has_next</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>a454e150cad039a2dce78311bb84025a9</anchor>
      <arglist>)(struct starpu_worker_collection *workers, struct starpu_sched_ctx_iterator *it)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>get_next</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>afb3fae582e3e0ddf1aec43c553a01670</anchor>
      <arglist>)(struct starpu_worker_collection *workers, struct starpu_sched_ctx_iterator *it)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>add</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>a98b3c4781266f1d943fcdd3691ca4152</anchor>
      <arglist>)(struct starpu_worker_collection *workers, int worker)</arglist>
    </member>
    <member kind="variable">
      <type>int(*</type>
      <name>remove</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>adc06b99ca3665778ac4495441ef8716c</anchor>
      <arglist>)(struct starpu_worker_collection *workers, int worker)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>init</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>a5e9789880d5e494b6da0e0b4eaa1b3df</anchor>
      <arglist>)(struct starpu_worker_collection *workers)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>deinit</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>a21ca20b54436480ede90c514e828127c</anchor>
      <arglist>)(struct starpu_worker_collection *workers)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>init_iterator</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>a9c2191d4c76b3dafe860dfc887386764</anchor>
      <arglist>)(struct starpu_worker_collection *workers, struct starpu_sched_ctx_iterator *it)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>types_of_workers</name>
    <filename>sc__hypervisor__policy_8h.html</filename>
    <anchor>structtypes__of__workers</anchor>
  </compound>
  <compound kind="group">
    <name>API_Bitmap</name>
    <title>Bitmap</title>
    <filename>group__API__Bitmap.html</filename>
    <member kind="function">
      <type>struct starpu_bitmap *</type>
      <name>starpu_bitmap_create</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gad46a1d8559731ed2b5619f90240b6c1b</anchor>
      <arglist>(void) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_destroy</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga870bdc74a6ff9db855de24622ced5f5f</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_set</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gaf7debe919f07b41a8aa4d35262838e26</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_unset</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gab9dcc731b430f29d7cd65451df6d14c0</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_unset_all</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gafa9cd8be3ba6463b967e1c5bcb37587e</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_get</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga997748478e809c6dd577d204deaa5d57</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_unset_and</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gae528ad901cbbdda91d0aa25d4a97ff59</anchor>
      <arglist>(struct starpu_bitmap *a, struct starpu_bitmap *b, struct starpu_bitmap *c)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bitmap_or</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga9f3c69615da232aa98ca0d82b05d2fa2</anchor>
      <arglist>(struct starpu_bitmap *a, struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_and_get</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gac7dbca81bf45d4d07f774057ef6790bc</anchor>
      <arglist>(struct starpu_bitmap *b1, struct starpu_bitmap *b2, int e)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_cardinal</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga03a6d6e8dc484f0be14ebb733bc587d6</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_first</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga1a0689f2f21ccded428555a580567b61</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_last</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga6e979d89f277827e58aa0e6fe84c67cf</anchor>
      <arglist>(struct starpu_bitmap *b)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_next</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>gaa2063e19f69f9fd87dd3107cf49db94e</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bitmap_has_next</name>
      <anchorfile>group__API__Bitmap.html</anchorfile>
      <anchor>ga7765890a66b9ecb0cedfd30ad0180333</anchor>
      <arglist>(struct starpu_bitmap *b, int e)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Codelet_And_Tasks</name>
    <title>Codelet And Tasks</title>
    <filename>group__API__Codelet__And__Tasks.html</filename>
    <class kind="struct">starpu_codelet</class>
    <class kind="struct">starpu_data_descr</class>
    <class kind="struct">starpu_task</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_NOWHERE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga7d5977bc7532b4b357ea8b1017b5aaca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CPU</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaf577e4415a639beffbc48b65454b88ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CUDA</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga43c37484ac60c15cd6f45ab25c277213</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaec9e5cdf8ac48607ce4495ded31001d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MIC</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga6f980630004573f662253ff25317382e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCC</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad17a66a704d34499ea478841f3d3c691</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAIN_RAM</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga64855af2ea04f74a1a261724b3b79046</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIPLE_CPU_IMPLEMENTATIONS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga29760ecd052f42a017005498d707d649</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIPLE_CUDA_IMPLEMENTATIONS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad41f0586b5bd0f116a9106457117e7f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIPLE_OPENCL_IMPLEMENTATIONS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gabf8eee9a9ff1a110f06e6d9c95fd1e8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_NMAXBUFS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad9efd8b217907a2fe26d92bd91438cdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_NBUFFERS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga8bf45c6964c57e5a63f7ae225e1ec141</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_INITIALIZER</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gafe2c37f8164584418e7781fc15fb054c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_GET_NBUFFERS</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga829d31913f4b0890192d4924bd264f88</anchor>
      <arglist>(task)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_GET_HANDLE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga260a1f7f8bef6b255dd0e0e45481385e</anchor>
      <arglist>(task, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_SET_HANDLE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga82b2ad7adb9e1c70d4c901a89be88d5d</anchor>
      <arglist>(task, handle, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CODELET_GET_MODE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga5dea5d6f5018e13a6b2c5d7a09f08054</anchor>
      <arglist>(codelet, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CODELET_SET_MODE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gae7be080fd53d7a402620cabd4070b6ca</anchor>
      <arglist>(codelet, mode, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_GET_MODE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga649920660819ed7d9922e896e63acd95</anchor>
      <arglist>(task, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_SET_MODE</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga22df1754b8d59c3fa06a366554fa2c3a</anchor>
      <arglist>(task, mode, i)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TASK_INVALID</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga5cd260cfe96251890a0749743692de09</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_cpu_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga8a4fb4a4c3d4b96159949bda6b235c1f</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_cuda_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga54f6cb6c50dab31bd1d0aaa7f7f3c10b</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_opencl_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga5580d9f3ee1cf070e0bdca49534fecde</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>starpu_mic_kernel_t(*</type>
      <name>starpu_mic_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga59d5a960d0137ecc4c1c4d16087402df</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="typedef">
      <type>starpu_scc_kernel_t(*</type>
      <name>starpu_scc_func_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga06d73291da4a6e975b51d35ecf9428dc</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_mic_kernel_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga28acec4e7ec3e6dd5183fb7d4070cab1</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>starpu_scc_kernel_t</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga2d5c6d8edf1a289187c0f0b09b224a14</anchor>
      <arglist>)(void **, void *)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_codelet_type</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gac127ad106bf376da993d3f4caa979b2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SEQ</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ggac127ad106bf376da993d3f4caa979b2fa6622e698785a33f14549958f44577fcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SPMD</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ggac127ad106bf376da993d3f4caa979b2fad58c1d1b9c143aed79f1ddf6dc104746</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_FORKJOIN</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ggac127ad106bf376da993d3f4caa979b2faaaad35e6da5417989b55d70fa7e65447</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_task_status</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga0088c4ab87a08b3e7a375c1a79e91202</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a677b001ff383c2e11850a03595949a6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_READY</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a77b817e6cd032658581f7b9cf12dd3e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_RUNNING</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202afcf62642e96b8982135f58a477e9ea9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_FINISHED</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a01380324ad453dd9154c38582df6359e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED_ON_TAG</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202afa01b5f07d5e628759f9a26c7077f9d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED_ON_TASK</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a8f0b251db9e88be0bd7af98816c22a78</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED_ON_DATA</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a5ff984632a93a5e0c5d8579c78b1edb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SEQ</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ggac127ad106bf376da993d3f4caa979b2fa6622e698785a33f14549958f44577fcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SPMD</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ggac127ad106bf376da993d3f4caa979b2fad58c1d1b9c143aed79f1ddf6dc104746</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_FORKJOIN</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ggac127ad106bf376da993d3f4caa979b2faaaad35e6da5417989b55d70fa7e65447</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a677b001ff383c2e11850a03595949a6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_READY</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a77b817e6cd032658581f7b9cf12dd3e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_RUNNING</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202afcf62642e96b8982135f58a477e9ea9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_FINISHED</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a01380324ad453dd9154c38582df6359e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED_ON_TAG</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202afa01b5f07d5e628759f9a26c7077f9d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED_ON_TASK</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a8f0b251db9e88be0bd7af98816c22a78</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TASK_BLOCKED_ON_DATA</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gga0088c4ab87a08b3e7a375c1a79e91202a5ff984632a93a5e0c5d8579c78b1edb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_init</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga46b34c28e0b9b2e1726b9d033639dda7</anchor>
      <arglist>(struct starpu_codelet *cl)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_init</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gadf8b76ef62ba0e68684e07ebf1db1731</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_task_create</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga48c439a84fdee4310c3481ca7e56156a</anchor>
      <arglist>(void) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_task_dup</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gafae324cd8f5a0851d66bffc95bb28d49</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_clean</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaecb3efa04cb10b049b10c6166dd9c30c</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_destroy</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga8fdfb4c2276013b699f5398a1c528bba</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad915868fe3e292657cefc6e1c404c7f4</anchor>
      <arglist>(struct starpu_task *task) STARPU_WARN_UNUSED_RESULT</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_submit</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaa32228bf7f452f7d664986668ea46590</anchor>
      <arglist>(struct starpu_task *task) STARPU_WARN_UNUSED_RESULT</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_submit_to_ctx</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga3c6c4dd317df02d7021e23ab6b14e3e2</anchor>
      <arglist>(struct starpu_task *task, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_all</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gad0baa8dbfd13e5a7bc3651bcd76022aa</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_all_in_ctx</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaf963bbf24935bdf137d2eefa7fd06b80</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_n_submitted</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga724cda8b0ffeaa7532a5e573b86bc90b</anchor>
      <arglist>(unsigned n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_n_submitted_in_ctx</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gabba5bab6e4eb84f21f01ba53008a0cd3</anchor>
      <arglist>(unsigned sched_ctx_id, unsigned n)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_nready</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga18bec8430309a613520d6042d701790d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_nsubmitted</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gae2740e2b2b948a5c48f5424651dfc721</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_task_get_current</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gab0a96cce01be9f4eccad9c783e8e27bb</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_display_stats</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga0e7db78b32d57db548555c1425808dd0</anchor>
      <arglist>(struct starpu_codelet *cl)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_wait_for_no_ready</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga50ff3cb4b73a5e44e51035166127d40a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_set_implementation</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaabb4805df503ece40a5e092d8e99ae52</anchor>
      <arglist>(struct starpu_task *task, unsigned impl)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_task_get_implementation</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>gaac492d7132cc930b8483b571d0dd4333</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_create_sync_task</name>
      <anchorfile>group__API__Codelet__And__Tasks.html</anchorfile>
      <anchor>ga8b83ae8b1c2015f961e60627d4913549</anchor>
      <arglist>(starpu_tag_t sync_tag, unsigned ndeps, starpu_tag_t *deps, void(*callback)(void *), void *callback_arg)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_CUDA_Extensions</name>
    <title>CUDA Extensions</title>
    <filename>group__API__CUDA__Extensions.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_CUDA</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga384db70769609ec68fae5a0105821299</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAXCUDADEVS</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>gae2a0ce53013a0c3d7bafaf0ec000cdd5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CUDA_REPORT_ERROR</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>gae30cd6ebde1dc4001ae8a17cabd5dbc4</anchor>
      <arglist>(status)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CUBLAS_REPORT_ERROR</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga305c371567096cedd87290bf68123ced</anchor>
      <arglist>(status)</arglist>
    </member>
    <member kind="function">
      <type>cudaStream_t</type>
      <name>starpu_cuda_get_local_stream</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>gad7d80d054bd2b9570e1d7e24442e19c0</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>const struct cudaDeviceProp *</type>
      <name>starpu_cuda_get_device_properties</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga71b71760c8b71cb6c3f0dc7495c84036</anchor>
      <arglist>(unsigned workerid)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_cuda_report_error</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga08ef1dd9623d7adb87cede9e60d3e1dc</anchor>
      <arglist>(const char *func, const char *file, int line, cudaError_t status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_cuda_copy_async_sync</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga20c5b69812878a57e22f4f3c5cc8f56f</anchor>
      <arglist>(void *src_ptr, unsigned src_node, void *dst_ptr, unsigned dst_node, size_t ssize, cudaStream_t stream, enum cudaMemcpyKind kind)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_cuda_set_device</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga3b170653608d3381b660afba63be39da</anchor>
      <arglist>(unsigned devid)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_cublas_init</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga9f70358bd39f2228d7b0558702306d96</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_cublas_shutdown</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga3cd8378588620422bc6c3246016ffcd8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_cublas_report_error</name>
      <anchorfile>group__API__CUDA__Extensions.html</anchorfile>
      <anchor>ga1f4ce467c5135681226d3764911d838c</anchor>
      <arglist>(const char *func, const char *file, int line, int status)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Data_Interfaces</name>
    <title>Data Interfaces</title>
    <filename>group__API__Data__Interfaces.html</filename>
    <class kind="struct">starpu_data_interface_ops</class>
    <class kind="struct">starpu_data_copy_methods</class>
    <class kind="struct">starpu_variable_interface</class>
    <class kind="struct">starpu_vector_interface</class>
    <class kind="struct">starpu_matrix_interface</class>
    <class kind="struct">starpu_block_interface</class>
    <class kind="struct">starpu_bcsr_interface</class>
    <class kind="struct">starpu_csr_interface</class>
    <class kind="struct">starpu_coo_interface</class>
    <member kind="enumeration">
      <type></type>
      <name>starpu_data_interface_id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_UNKNOWN_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca26edb6f25f81276cc829b070fd240472</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MATRIX_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357caf3641ac475b4b69aa6ef5758b950348a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_BLOCK_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca61676e55a5c706ecb014c772a39e292d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_VECTOR_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357cabd89ff25621eab18a8a7306842c13217</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CSR_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca9566de0c4f51f4e48f6ca681d2701eed</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_BCSR_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357caf0717d5dd8045aa04c0d4ea65dc632be</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_VARIABLE_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357cae02c3d444d843270b692718f22173ab3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_VOID_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca3867ecf55ad294013fd36c502af39d12</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MULTIFORMAT_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357cace921145fc7dd66a423b1c52991c6335</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_COO_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357cad84c1b3c230ee8b5136302fa63dfc9d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MAX_INTERFACE_ID</name>
      <anchorfile>starpu__data__interfaces_8h.html</anchorfile>
      <anchor>gaa2f2140147f15e7b9eec1443690e357ca0eed341fd3aa3fdc35d2f38b3eae09b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_void_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab7f9484602aeef560cc2e5904e3b2cff</anchor>
      <arglist>(starpu_data_handle_t *handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_variable_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadcd1aee7fb7226edebe64dfcc46c1d69</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_variable_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf972d52af628f82488eeea311eadb4f6</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga98ea4083ac39db45b6be75354e66bca1</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, uint32_t nx, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gacb816db830d6991909a35a8c0e93a843</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7af287b9089acc7cf4b3b7ed19d82abb</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, uint32_t ld, uint32_t nx, uint32_t ny, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga22a8126d41b43e71c3c79e210401b12e</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset, uint32_t ld)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab67a02c78b113f6b3e031735418fd838</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uintptr_t ptr, uint32_t ldy, uint32_t ldz, uint32_t nx, uint32_t ny, uint32_t nz, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_ptr_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8aa19dcac39f053949c1bb1b4f5fad1d</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, uintptr_t ptr, uintptr_t dev_handle, size_t offset, uint32_t ldy, uint32_t ldz)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bcsr_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga336c1bc150930da84a2abc7d146839c3</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uint32_t nnz, uint32_t nrow, uintptr_t nzval, uint32_t *colind, uint32_t *rowptr, uint32_t firstentry, uint32_t r, uint32_t c, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_csr_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga81426fc1860aabaefed1a33946df6f51</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, uint32_t nnz, uint32_t nrow, uintptr_t nzval, uint32_t *colind, uint32_t *rowptr, uint32_t firstentry, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_coo_data_register</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gace26ae5c977d1fd0fbea538b582bab43</anchor>
      <arglist>(starpu_data_handle_t *handleptr, unsigned home_node, uint32_t nx, uint32_t ny, uint32_t n_values, uint32_t *columns, uint32_t *rows, uintptr_t values, size_t elemsize)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_data_get_interface_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab6e12b04b231773f2eff496f57d29ee8</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned memory_node)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_data_handle_to_pointer</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga011705e0a88798562f7afb2753a6b1ce</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_data_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gabcce7df711e56cafddfd82f8f5ee794b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_data_interface_id</type>
      <name>starpu_data_get_interface_id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad6ca41b04e0265671d4bf899dd433c1e</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_data_get_size</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac14cfc52c450c81dd4789a7aace79d3b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_pack</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad7e204d690945cbe5145bc8950078291</anchor>
      <arglist>(starpu_data_handle_t handle, void **ptr, starpu_ssize_t *count)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_unpack</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga97c7b78c97576eb44741bdd7e2183fa7</anchor>
      <arglist>(starpu_data_handle_t handle, void *ptr, size_t count)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_GET_PTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad99e209fafc3ad5e0c4da0de02e5bda2</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7d44b8a0594932a8cece13e1cc9932bb</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_GET_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf011a057a557a66271ce06e8109c11c4</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VARIABLE_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga748def68db84b3cb9da305b0e3f7c935</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_variable_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga988036ae1f9385832c2988064a4f27af</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_variable_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga540f980b1c4bee16c78d84097a8b07f9</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_PTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa23416a6d049b276fe57b19e069b68b3</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadccf196d01dd9c43e931827a86f886b2</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8fa1c7e18fa7c621ec68fc5aa32ef181</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_NX</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7cc322b2f03830ec7afbb58b7416a283</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga3a3cf303a3e3be144aab7abdf587c900</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VECTOR_GET_SLICE_BASE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa35025b4b6126c3f22232cea8915cdad</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_vector_get_nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga08bed7d1e9b7cb2b2b76932eb9c33353</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_vector_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gacd95c31b537322b1d4000a1248b9c425</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_vector_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gacee0a9e166f87b48da1f56e1ae5d988d</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_PTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gacd9e0b5bd06d1b7d88be4e0143be8a68</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac046f5fb0472cada47cd44650194b1c8</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac23356ed31839f1d1c52efccb7a99dd7</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_NX</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gae3d95a1379358cbd85c663c8462da98c</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_NY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab915146b6d59fbd8e6d04480511f7945</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_LD</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga6f28e1a774923108005760426cf8da5b</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MATRIX_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaef6039a7976d3ee6c6916df28a08da1a</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_matrix_get_nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga461a1d65e0f4559dacbc1ba77eafbb2b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_matrix_get_ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga720fcf673578ec23e583b5f674626847</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_matrix_get_local_ld</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga5c4dfa96a4ffa7be08e22a9dc20484e4</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_matrix_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga9f6457e4a7e558101dd6ad64076a6a99</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_matrix_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaebeefdd9980c18575924ed9860315bcf</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_PTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaaeff5c191edc5f3df8ba1a319acd0af9</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga335bad7c45aa5df70227ce11f03edc0f</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gafd9d133c4d658b777d9800eba79765b3</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_NX</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf0fa0d2dc99ab880ccfe12061770dad2</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_NY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga87725438ebaccd40d8b1b90e17b46247</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_NZ</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab316b2febb2b5fb92e83bbaab780f046</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_LDY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac2fe5221c0ccc0d082e5bc65830e497b</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_LDZ</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf3bd64a64625f45859f596db3bb9b40a</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BLOCK_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga3bb8d6c4169e578b219849148eece329</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_nx</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8599a8f318fcadbafcc55f9c568678f1</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_ny</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga81b34820b1a410d218ad08528c0317f6</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_nz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac7b7e5f3d4d9a1468df3b9b92743a9fd</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_local_ldy</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf1843d7f93510e6bf53da173eb9dd75e</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_block_get_local_ldz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga66508648e621019be00d86832d66f278</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_block_get_local_ptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga66df9aff4ea14be546df745f17365568</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_block_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga47cea6fc5b88aaa359066e8192fafe95</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_NNZ</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaea999f5e67e778ac28c0a8bc1633ff28</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_NZVAL</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad4850366b77ead9f60c6f7e1d1064b22</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_NZVAL_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga2cab8f7f6a3f484e16241c41239664fa</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_COLIND</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga588a7cbb105708b588e5fe008fbb9e9c</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_COLIND_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga59cbe4cadbe314b73807dc20363db5b9</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_ROWPTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga451ea6b212d0442b2bda2937c08968f5</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_ROWPTR_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga28e9d620c4dc360cdbb0437e81d66543</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_BCSR_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad3c08c06e55e86993fa5cb77dce95095</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_nnz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8a4c1ddb71e09437717e298c3e2c39f9</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_nrow</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab59c3b606776bdc54f6ccf3f1db4bf38</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_firstentry</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga8c04948b40dc3902e9b3de7a84383381</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_bcsr_get_local_nzval</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gae48e867d81c554b199b127cf9bd4f5b2</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t *</type>
      <name>starpu_bcsr_get_local_colind</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga54dca826f319369d94c4cd6be0eb4828</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t *</type>
      <name>starpu_bcsr_get_local_rowptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga97859bcb7bc60911362087eefc9249ed</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_r</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadcf7063b1cd8d0feed7054e6e4d9d217</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_bcsr_get_c</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga6d0c09ee7cd90ac23454ef2040a4bc25</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_bcsr_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga40f5b44c5d9bfd39f2f305aa32a25787</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_NNZ</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab9110ecfa66b148e770a375566c1c19d</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_NROW</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga86b64dc68625997cf3c0bb9eae3ee68a</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_NZVAL</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab7b07d0d2a253e02e1b2d2260cc3fa94</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_NZVAL_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga9dc1bbf7a41070a6fcf82e728cae1542</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_COLIND</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga567d2e3780dabc30269af5e54fe20660</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_COLIND_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaeed0ab452c0906a650097368bd1c6ba3</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_ROWPTR</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga69dd5090534b513a374089627254f73c</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_ROWPTR_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga21ae3f41a59615f892f196b8cab16ee4</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa46eb27abc7439c03776d68062f029d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_FIRSTENTRY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga83ba12ab43444d61f4966dc76aa160d6</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CSR_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga82eea89e2f23a9fdc1f34c03db68a97b</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_csr_get_nnz</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga66106513707ed71b8849b1c1bbb3df75</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_csr_get_nrow</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga72c9db50152ed5bf9f18ad3a355c5376</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_csr_get_firstentry</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga40f2d8762bc1060e939eecae75f9be08</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_csr_get_local_nzval</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga4f137b139435d4cddcd15704fe46cbbd</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t *</type>
      <name>starpu_csr_get_local_colind</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gad5ffed03f141096fca4e1e2583c84e22</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t *</type>
      <name>starpu_csr_get_local_rowptr</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga78a31ef2148b8439a2c0e897d9179b84</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>starpu_csr_get_elemsize</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab328b8e0b234147ebe36ceb8e7a55b0a</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_COLUMNS</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga56d6ae5fe9283bfb77e13f07c2513184</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_COLUMNS_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gade238eb388cbb34bf9a93643bfa4df83</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_ROWS</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac739182a43ec4dd00618d783b1f2a16c</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_ROWS_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab1c57c537a980a2de5e595b2f6204772</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_VALUES</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga001c8954d20044b466c661cf7c9f9fe0</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_VALUES_DEV_HANDLE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gadf8eac3f96b6e63911f23165141411df</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_OFFSET</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga1e92dc5d143be583708d87498d2dc9cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_NX</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gac77fef789ec34682ba00b38dcc501170</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_NY</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa15cff8f1148e572348095ac5a2ab421</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_NVALUES</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga550d6222ff484201dbd3d39bb7baeb7d</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_COO_GET_ELEMSIZE</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gae95b657d947e611060590d506d8e15dc</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_malloc_on_node_flags</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab2bf7713cad5570775bdf4efec79502d</anchor>
      <arglist>(unsigned dst_node, size_t size, int flags)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_free_on_node_flags</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga02005aa2a3c838802d95d0426a937d8d</anchor>
      <arglist>(unsigned dst_node, uintptr_t addr, size_t size, int flags)</arglist>
    </member>
    <member kind="function">
      <type>uintptr_t</type>
      <name>starpu_malloc_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gab91cbc596a65e6a4322b657c79934269</anchor>
      <arglist>(unsigned dst_node, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_free_on_node</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7e3ef9efbc7a65adad27f9ac27493493</anchor>
      <arglist>(unsigned dst_node, uintptr_t addr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_malloc_on_node_set_default_flags</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga2af8405c3f0fff62ea7269fc279cab55</anchor>
      <arglist>(unsigned node, int flags)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_interface_copy</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga7af8e4f90b557fefaa09f40a930efd74</anchor>
      <arglist>(uintptr_t src, size_t src_offset, unsigned src_node, uintptr_t dst, size_t dst_offset, unsigned dst_node, size_t size, void *async_data)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_hash_crc32c_be_n</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>ga86956bce305447697cb9346bee692b24</anchor>
      <arglist>(const void *input, size_t n, uint32_t inputcrc)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_hash_crc32c_be</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaa29d5f4bd11fce82cd9a01b0e860bf75</anchor>
      <arglist>(uint32_t input, uint32_t inputcrc)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_hash_crc32c_string</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaec6ca64cc2c52e319b9b73cc4f753658</anchor>
      <arglist>(const char *str, uint32_t inputcrc)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_interface_get_next_id</name>
      <anchorfile>group__API__Data__Interfaces.html</anchorfile>
      <anchor>gaf5ea640f2c977e3ae95a6be9b3be3bee</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Data_Management</name>
    <title>Data Management</title>
    <filename>group__API__Data__Management.html</filename>
    <member kind="typedef">
      <type>struct _starpu_data_state *</type>
      <name>starpu_data_handle_t</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga5f517ab725864d54b0459896a8f8ae07</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct starpu_arbiter *</type>
      <name>starpu_arbiter_t</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gab442391c29c432bdb20e5d70f008f48f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_data_access_mode</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1fb3a1ff8622747d653d1b5f41bc41db</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_NONE</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaacd20c7596d4c1ffc6dbeaa632a6a6a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_R</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaa4c2445a344c592fb7b1adfcf1d2e1f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_W</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba052ab75035ca297c7955363c605231c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_RW</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba20da2e02cd303015b5967dbf72ef3e1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCRATCH</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba2a5d5e11b2ec3aa14f5c9bf94accf6f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_REDUX</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba6011b5abab92fd996b29d622c4a488f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_COMMUTE</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba64859f7fc1e4f2484dd7f0500f12c30a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SSEND</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaf96ec45077e70239a826867ed897bd0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_LOCALITY</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaed3d2281d14b6e53ecb04c1c29a3d688</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_NONE</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaacd20c7596d4c1ffc6dbeaa632a6a6a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_R</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaa4c2445a344c592fb7b1adfcf1d2e1f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_W</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba052ab75035ca297c7955363c605231c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_RW</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba20da2e02cd303015b5967dbf72ef3e1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCRATCH</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba2a5d5e11b2ec3aa14f5c9bf94accf6f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_REDUX</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba6011b5abab92fd996b29d622c4a488f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_COMMUTE</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dba64859f7fc1e4f2484dd7f0500f12c30a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SSEND</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaf96ec45077e70239a826867ed897bd0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_LOCALITY</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gga1fb3a1ff8622747d653d1b5f41bc41dbaed3d2281d14b6e53ecb04c1c29a3d688</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_register</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga74d62dfa2a733db4bac71caaae751d9d</anchor>
      <arglist>(starpu_data_handle_t *handleptr, unsigned home_node, void *data_interface, struct starpu_data_interface_ops *ops)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_ptr_register</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1d0a0dc4903585e099b2e4b16a22946a</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_register_same</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1be29ee257e3d9c3321df759b8105cb1</anchor>
      <arglist>(starpu_data_handle_t *handledst, starpu_data_handle_t handlesrc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unregister</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga586146498466b60d6b81145dfaeb8948</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unregister_no_coherency</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gaa1f95d17759711f7703bf02820e2e49b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unregister_submit</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga4fa34753bff1d29c20f0a0e361020b4e</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_invalidate</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga4e82fe020ec010bcacb6aee16021607c</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_invalidate_submit</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga06b01fdf769f8f2eb222ecde42afbc81</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_wt_mask</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gae53e7b21c7426c9845a1046cfe5becce</anchor>
      <arglist>(starpu_data_handle_t handle, uint32_t wt_mask)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_fetch_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga9ce32e79b2f07fb474b6ae10006abb1f</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, unsigned async)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_prefetch_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga57687b811ced00dbfc35af73164a72aa</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, unsigned async)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_idle_prefetch_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gad5a24f94d0aa2bbfab8957c3dd13949a</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node, unsigned async)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_wont_use</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gafd4b4f7f9f0a26f65a1e149525a09bfd</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_lookup</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga75907283eef0a058a040ff06de87b4b2</anchor>
      <arglist>(const void *ptr)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_request_allocation</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gac561fd637be17b74f0456106dc25f5d1</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_query_status</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga9a6122bae224d714383ae775434cdce3</anchor>
      <arglist>(starpu_data_handle_t handle, int memory_node, int *is_allocated, int *is_valid, int *is_requested)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_advise_as_important</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gaedc8031c1b437d35232cd1700d83d472</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned is_important)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_reduction_methods</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga1f5ba1c1cfefc1f81a4095cf3c213e54</anchor>
      <arglist>(starpu_data_handle_t handle, struct starpu_codelet *redux_cl, struct starpu_codelet *init_cl)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_data_interface_ops *</type>
      <name>starpu_data_get_interface_ops</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gac0d64eee72db978a53bf3e5081766d1b</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_DATA_ACQUIRE_CB</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga2b9b64ac9a650d8c8942b4227e6fce75</anchor>
      <arglist>(handle, mode, code)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gae6eb535cf9bf46a7ef9ad2d845c675a2</anchor>
      <arglist>(starpu_data_handle_t handle, enum starpu_data_access_mode mode)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_cb</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga358aba7459b7f99a6dbaa189ce57b925</anchor>
      <arglist>(starpu_data_handle_t handle, enum starpu_data_access_mode mode, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_cb_sequential_consistency</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga70ee8a92cbb3935ec3ca0d40f443f860</anchor>
      <arglist>(starpu_data_handle_t handle, enum starpu_data_access_mode mode, void(*callback)(void *), void *arg, int sequential_consistency)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga3b2ca6406b5839062346200123551638</anchor>
      <arglist>(starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_on_node_cb</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga8db878885ebbee4729d8aed6a0479262</anchor>
      <arglist>(starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_acquire_on_node_cb_sequential_consistency</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gafd48044a523ed5a4e11598596096dc52</anchor>
      <arglist>(starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode, void(*callback)(void *), void *arg, int sequential_consistency)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_release</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>gadc145017bafa2948109c624715de77a2</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_release_on_node</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga5b95764e9545fc367cb995950e82f449</anchor>
      <arglist>(starpu_data_handle_t handle, int node)</arglist>
    </member>
    <member kind="function">
      <type>starpu_arbiter_t</type>
      <name>starpu_arbiter_create</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga8771ea54051655e68ed7a95b4b831fc7</anchor>
      <arglist>(void) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_assign_arbiter</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga9cbc51da2f2b7025c534d6862866bf81</anchor>
      <arglist>(starpu_data_handle_t handle, starpu_arbiter_t arbiter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_arbiter_destroy</name>
      <anchorfile>group__API__Data__Management.html</anchorfile>
      <anchor>ga337cf01e2a0ccc1c3cc2ea27d5bdf131</anchor>
      <arglist>(starpu_arbiter_t arbiter)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Out_Of_Core</name>
    <title>Out Of Core</title>
    <filename>group__API__Out__Of__Core.html</filename>
    <class kind="struct">starpu_disk_ops</class>
    <member kind="function">
      <type>int</type>
      <name>starpu_disk_register</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>gac9ed48246cb2b326ca6a199b6183aa75</anchor>
      <arglist>(struct starpu_disk_ops *func, void *parameter, starpu_ssize_t size)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_disk_open</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>gaddcdc1beb6f41ed8d38778fb51cc0e6d</anchor>
      <arglist>(unsigned node, void *pos, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_disk_close</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga8cd566efd70ad57cda18a6b9a7be4ac7</anchor>
      <arglist>(unsigned node, void *obj, size_t size)</arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_disk_ops</type>
      <name>starpu_disk_stdio_ops</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga5bb135a02ac97b9efa9ab0280a604868</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_disk_ops</type>
      <name>starpu_disk_unistd_ops</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga647a9db1632791e9410643658ad50ffd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_disk_ops</type>
      <name>starpu_disk_unistd_o_direct_ops</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga902a7c5a3920a874f6e4db36c86c1e4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct starpu_disk_ops</type>
      <name>starpu_disk_leveldb_ops</name>
      <anchorfile>group__API__Out__Of__Core.html</anchorfile>
      <anchor>ga4a1bd09de991de1491afa05f5e528e94</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Data_Partition</name>
    <title>Data Partition</title>
    <filename>group__API__Data__Partition.html</filename>
    <class kind="struct">starpu_data_filter</class>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga1363109ba0e36c1b6c7f1a40c9608791</anchor>
      <arglist>(starpu_data_handle_t initial_handle, struct starpu_data_filter *f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unpartition</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gae80794b9cad7855a3ee54a4361f656ed</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned gathering_node)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_get_nb_children</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga6a3f729055f14384e7397d2815a2c9a5</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_get_child</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga29e07c2c0604da63e7746a8018d8a62f</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned i)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_get_sub_data</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac24101bbe28b1d7d4a0874d349ba8979</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned depth,...)</arglist>
    </member>
    <member kind="function">
      <type>starpu_data_handle_t</type>
      <name>starpu_data_vget_sub_data</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7904efb86ab3f9d6d682a3a3be3646fe</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned depth, va_list pa)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_map_filters</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga551d6fa7fead5b9f7c8a85b1f9885e91</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned nfilters,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_vmap_filters</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga15a28291a5045ef7ed3c93afc94ed248</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned nfilters, va_list pa)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_plan</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gaa4407a8734e1fbdbb63b83351769476c</anchor>
      <arglist>(starpu_data_handle_t initial_handle, struct starpu_data_filter *f, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga994cbae9c619b070f8d219f6bfffff06</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_readonly_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7fcf158f5196d62610c5017993442c53</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_readwrite_upgrade_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga8a26c673507a7de484071e0926cb5638</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unpartition_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga46d2b144a7de2e17d17b1383ef5f522d</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children, int gathering_node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_unpartition_readonly_submit</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac1e66049a48764a29d867a02bcc9d0ce</anchor>
      <arglist>(starpu_data_handle_t initial_handle, unsigned nparts, starpu_data_handle_t *children, int gathering_node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_partition_clean</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac4f0b5ce7cdee16c1311595e9ca53e98</anchor>
      <arglist>(starpu_data_handle_t root_data, unsigned nparts, starpu_data_handle_t *children)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga212189d3b83dfa4e225609b5f2bf8461</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gaab49915dc0462c1b145bfb0a9ce4cf52</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_list</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gab9fa487bfff5ccdd59210bdde65a11db</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_vector_filter_divide_in_2</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gab639622ea4929c36df704a0bebfd3fac</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga8c86b2af9e0806e631c1cbb5d506506b</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga88fbca61843b76314e39a2c0f8b93d6c</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_vertical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga2925be576ac7d597ecead381ff32a894</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_matrix_filter_vertical_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7132923bd901e0e4254cc0b20d49997a</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga1a265ffca51fae58701832a4daa53bd9</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga7cc8832e25f2f4049ba5a0053b122dd9</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_vertical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga73a2c9af1200c68f0403e70e36c020d0</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_vertical_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gafa4818b571e98acd8696a1251b0d4e74</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_depth_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gaae4f93ab3326ded72c3a80d337e6f4a1</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_block_filter_depth_block_shadow</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>gac4b9ec529f67e5c300e7eed3e185fbaf</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bcsr_filter_canonical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga0e1bee4821237529d554605d333e9109</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_csr_filter_vertical_block</name>
      <anchorfile>group__API__Data__Partition.html</anchorfile>
      <anchor>ga554a2fb14fdee9353364c39f36ee3a6f</anchor>
      <arglist>(void *father_interface, void *child_interface, struct starpu_data_filter *f, unsigned id, unsigned nparts)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Expert_Mode</name>
    <title>Expert Mode</title>
    <filename>group__API__Expert__Mode.html</filename>
    <member kind="function">
      <type>void</type>
      <name>starpu_wake_all_blocked_workers</name>
      <anchorfile>group__API__Expert__Mode.html</anchorfile>
      <anchor>gaf5f4a32a78630fb051a846cbdcd77d8b</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_progression_hook_register</name>
      <anchorfile>group__API__Expert__Mode.html</anchorfile>
      <anchor>ga4e8d1e607b2383f80b232c27ec6a9386</anchor>
      <arglist>(unsigned(*func)(void *arg), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_progression_hook_deregister</name>
      <anchorfile>group__API__Expert__Mode.html</anchorfile>
      <anchor>ga5d52c35e46e387eed77dd9b7b39507a7</anchor>
      <arglist>(int hook_id)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Explicit_Dependencies</name>
    <title>Explicit Dependencies</title>
    <filename>group__API__Explicit__Dependencies.html</filename>
    <member kind="typedef">
      <type>uint64_t</type>
      <name>starpu_tag_t</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>ga6e19fdd4a84d04323ee05e9b7ec5990e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_declare_deps_array</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gada6692ca393aa820bc105252ca19830f</anchor>
      <arglist>(struct starpu_task *task, unsigned ndeps, struct starpu_task *task_array[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_get_task_succs</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>ga53dc7bff09b441ee6a200cae99ca2cb8</anchor>
      <arglist>(struct starpu_task *task, unsigned ndeps, struct starpu_task *task_array[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_get_task_scheduled_succs</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gae33176debf369b29f12e2e390f2d67e1</anchor>
      <arglist>(struct starpu_task *task, unsigned ndeps, struct starpu_task *task_array[])</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_declare_deps</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>ga863d71b775ec517493f81e731a64d134</anchor>
      <arglist>(starpu_tag_t id, unsigned ndeps,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_declare_deps_array</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gab5c86743355724ee2f9596de1ae221df</anchor>
      <arglist>(starpu_tag_t id, unsigned ndeps, starpu_tag_t *array)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_tag_wait</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gaf5090c3c45414d693445e79ab7842992</anchor>
      <arglist>(starpu_tag_t id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_tag_wait_array</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gaf5cf68c72a863a6ab0ef8737c92b57ba</anchor>
      <arglist>(unsigned ntags, starpu_tag_t *id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_restart</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gaef0b7cee7536d4a5de1b10888ab13add</anchor>
      <arglist>(starpu_tag_t id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_remove</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gadbed48d64331463d4f9a9090933e5b9f</anchor>
      <arglist>(starpu_tag_t id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tag_notify_from_apps</name>
      <anchorfile>group__API__Explicit__Dependencies.html</anchorfile>
      <anchor>gabdb8d20fca507a93450fe775233dd20c</anchor>
      <arglist>(starpu_tag_t id)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_FFT_Support</name>
    <title>FFT Support</title>
    <filename>group__API__FFT__Support.html</filename>
    <member kind="function">
      <type>void *</type>
      <name>starpufft_malloc</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>gabf7989748571ab9155a865a19002d1fa</anchor>
      <arglist>(size_t n)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpufft_free</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga6809e3bc78ad0d7f9df41e99ddada069</anchor>
      <arglist>(void *p)</arglist>
    </member>
    <member kind="function">
      <type>starpufft_plan</type>
      <name>starpufft_plan_dft_1d</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga642104c645445688177f1a44afeeefb0</anchor>
      <arglist>(int n, int sign, unsigned flags)</arglist>
    </member>
    <member kind="function">
      <type>starpufft_plan</type>
      <name>starpufft_plan_dft_2d</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>gaf263d43c43927007819edf069cc8c93b</anchor>
      <arglist>(int n, int m, int sign, unsigned flags)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpufft_start</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga8e37e0bd1edc887cf456dcf7d95b86c4</anchor>
      <arglist>(starpufft_plan p, void *in, void *out)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpufft_start_handle</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>gabff378384df24d2c2ea296397b600569</anchor>
      <arglist>(starpufft_plan p, starpu_data_handle_t in, starpu_data_handle_t out)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpufft_execute</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>gae73ea67d1d7df63938661c7a1105800a</anchor>
      <arglist>(starpufft_plan p, void *in, void *out)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpufft_execute_handle</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga22ad2f1c869305d11132322039d38876</anchor>
      <arglist>(starpufft_plan p, starpu_data_handle_t in, starpu_data_handle_t out)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpufft_cleanup</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga4670f7ac8c6980f78b80b48bbe561299</anchor>
      <arglist>(starpufft_plan p)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpufft_destroy_plan</name>
      <anchorfile>group__API__FFT__Support.html</anchorfile>
      <anchor>ga2b1a62489b5b48491067bc8dc39f4b16</anchor>
      <arglist>(starpufft_plan p)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_FxT_Support</name>
    <title>FxT Support</title>
    <filename>group__API__FxT__Support.html</filename>
    <class kind="struct">starpu_fxt_codelet_event</class>
    <class kind="struct">starpu_fxt_options</class>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_options_init</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>gaf4a2675d7c5f2dc473879ef0a0a2a114</anchor>
      <arglist>(struct starpu_fxt_options *options)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_generate_trace</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga9249a38d77ab128f665b4359ac0c4781</anchor>
      <arglist>(struct starpu_fxt_options *options)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_start_profiling</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga5793bf5c8fc0bedf76d18a5fa38593df</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_stop_profiling</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga52a3e1a95689315f0b789a3325ec2a90</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_autostart_profiling</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga6a7af6697ae7a67161d3655c59215c09</anchor>
      <arglist>(int autostart)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_write_data_trace</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>gaf3cbbddf8c840dae5a649800c0c08b5c</anchor>
      <arglist>(char *filename_in)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_fxt_trace_user_event</name>
      <anchorfile>group__API__FxT__Support.html</anchorfile>
      <anchor>ga2e55ad671e94b6161755717b1c8b44c1</anchor>
      <arglist>(unsigned long code)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Implicit_Data_Dependencies</name>
    <title>Implicit Data Dependencies</title>
    <filename>group__API__Implicit__Data__Dependencies.html</filename>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_default_sequential_consistency_flag</name>
      <anchorfile>group__API__Implicit__Data__Dependencies.html</anchorfile>
      <anchor>ga26f17239b14354e61eef0710ffadc434</anchor>
      <arglist>(unsigned flag)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_data_get_default_sequential_consistency_flag</name>
      <anchorfile>group__API__Implicit__Data__Dependencies.html</anchorfile>
      <anchor>gaef7203edaf1725977dffac23296ede4e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_set_sequential_consistency_flag</name>
      <anchorfile>group__API__Implicit__Data__Dependencies.html</anchorfile>
      <anchor>ga273df19e4cad1c05ec5df697bcec4444</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned flag)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_data_get_sequential_consistency_flag</name>
      <anchorfile>group__API__Implicit__Data__Dependencies.html</anchorfile>
      <anchor>gac88c5f320d2c900923b7934d9a733104</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Initialization_and_Termination</name>
    <title>Initialization and Termination</title>
    <filename>group__API__Initialization__and__Termination.html</filename>
    <class kind="struct">starpu_driver</class>
    <class kind="union">starpu_driver.id</class>
    <class kind="struct">starpu_conf</class>
    <member kind="function">
      <type>int</type>
      <name>starpu_init</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga9ce171bcbbee2edd169ba2649e6e75e3</anchor>
      <arglist>(struct starpu_conf *conf) STARPU_WARN_UNUSED_RESULT</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_initialize</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>gab2948c26e01bbc6b29f11bfc7268e7d9</anchor>
      <arglist>(struct starpu_conf *user_conf, int *argc, char ***argv)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_conf_init</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga7f7f154a1bf9600b8ccd10436412660e</anchor>
      <arglist>(struct starpu_conf *conf)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_shutdown</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga48edf5e30e71fbb71923e3867ad16c0a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_pause</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga887fbd250ba7843400b4438d617213d6</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_resume</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>gad1aff0c793b50e50f995232c110bde66</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_asynchronous_copy_disabled</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga2cf027abaf6581c7baf98f9caeb9e4e5</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_asynchronous_cuda_copy_disabled</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga994d261c0ac05522b02ed3ab72dce2bd</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_asynchronous_opencl_copy_disabled</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga8a817e603dd7ce77a7712f7c981da081</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_asynchronous_mic_copy_disabled</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>gacfd8a8d350b5d7f874bbeef57975634d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_topology_print</name>
      <anchorfile>group__API__Initialization__and__Termination.html</anchorfile>
      <anchor>ga0702f607811a2af4f7d2bc271eb434e9</anchor>
      <arglist>(FILE *f)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Insert_Task</name>
    <title>Insert_Task</title>
    <filename>group__API__Insert__Task.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_VALUE</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gaff4c08029dceb2d7f158d49def9aec00</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CL_ARGS</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga8be38949dd0489761bd2768f8e584c70</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CALLBACK</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gae053c49170a0f318357ba3aa8e08ea89</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CALLBACK_WITH_ARG</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga12a05bcd675b1b88ec73ea9d8c8ccd74</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CALLBACK_ARG</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gaa2c1a7638d3cf2ddfe6cc51aa2fc4a46</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PRIORITY</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gab9d32b41f7fbae16e9ed2331f6dde8ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_DATA_ARRAY</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga9eb57b0eb0ee7ce31661eed55d3a5f4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_DATA_MODE_ARRAY</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga30d3c482ec1e2b9b6868988abcb02596</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_EXECUTE_ON_WORKER</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga71d2b5f941b3c05d67a7d002e834a753</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_WORKER_ORDER</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gaff5e270481f2931229a4b388f386ddf0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TAG</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga66222fb40e0942f5b93aed24f0917e3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_TAG_ONLY</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga2a2f1dbde5737ec9cc60773ea5dd124d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_NAME</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga663bee5a319a5c763af85f0b0071f92f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_FLOPS</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga1a0a565f2de522abc9c5f3457397b095</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga45ae06dad37b9a8518198b1c3b06f276</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_insert_task</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga4907c59974b5b30964a979083853d24e</anchor>
      <arglist>(struct starpu_codelet *cl,...)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_insert</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gad79a50a21fe717126659b2998209c1c6</anchor>
      <arglist>(struct starpu_codelet *cl,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_pack_args</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga083f02c3ed2a1ab58058ba17e36e773b</anchor>
      <arglist>(void **arg_buffer, size_t *arg_buffer_size,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_unpack_args</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gaa2e8c2206e1ea56aec65b09efb82f13a</anchor>
      <arglist>(void *cl_arg,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_codelet_unpack_args_and_copyleft</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>gab3448f8c28eef89affc2169cad340a21</anchor>
      <arglist>(void *cl_arg, void *buffer, size_t buffer_size,...)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_task_build</name>
      <anchorfile>group__API__Insert__Task.html</anchorfile>
      <anchor>ga8f7f5033f2077ebae698741750884ff2</anchor>
      <arglist>(struct starpu_codelet *cl,...)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Theoretical_Lower_Bound_on_Execution_Time</name>
    <title>Theoretical Lower Bound on Execution Time</title>
    <filename>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</filename>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_start</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga284f3571becb60b2354cc1ce121e4778</anchor>
      <arglist>(int deps, int prio)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_stop</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga5f1859599a28105aea4c0f33fd871218</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_print_dot</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga7fc9141929ef926d346431307afb0ff1</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_compute</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga99f073d0ad7604366ef3a8f805b5f060</anchor>
      <arglist>(double *res, double *integer_res, int integer)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_print_lp</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga066ce2e396d5b676af7a5209b0079610</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_print_mps</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>gad0cf05d0bb9b21964fb911cfebf252df</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bound_print</name>
      <anchorfile>group__API__Theoretical__Lower__Bound__on__Execution__Time.html</anchorfile>
      <anchor>ga834aa7094c173d42985d4d70f1694f57</anchor>
      <arglist>(FILE *output, int integer)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_MIC_Extensions</name>
    <title>MIC Extensions</title>
    <filename>group__API__MIC__Extensions.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_MIC</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga08715106f0b8262604d469ed0aa962aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAXMICDEVS</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga06ae5976f7c5d39961019127497b097b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void *</type>
      <name>starpu_mic_func_symbol_t</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga23012ab87c695eae83f9ad32607f9043</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mic_register_kernel</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga83b91d88db5e1e326149f836b7b48ea1</anchor>
      <arglist>(starpu_mic_func_symbol_t *symbol, const char *func_name)</arglist>
    </member>
    <member kind="function">
      <type>starpu_mic_kernel_t</type>
      <name>starpu_mic_get_kernel</name>
      <anchorfile>group__API__MIC__Extensions.html</anchorfile>
      <anchor>ga17637a17347c4ecdec828cd996331710</anchor>
      <arglist>(starpu_mic_func_symbol_t symbol)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Miscellaneous_Helpers</name>
    <title>Miscellaneous Helpers</title>
    <filename>group__API__Miscellaneous__Helpers.html</filename>
    <member kind="function">
      <type>int</type>
      <name>starpu_data_cpy</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>gaaff4ecf5818fd6f8d005f6bf57b0eb4b</anchor>
      <arglist>(starpu_data_handle_t dst_handle, starpu_data_handle_t src_handle, int asynchronous, void(*callback_func)(void *), void *callback_arg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_execute_on_each_worker</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>ga7a97b0699b97b30b4d408c660be46102</anchor>
      <arglist>(void(*func)(void *), void *arg, uint32_t where)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_execute_on_each_worker_ex</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>ga77f0f3796a059b3651f7ecb4535dad82</anchor>
      <arglist>(void(*func)(void *), void *arg, uint32_t where, const char *name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_execute_on_specific_workers</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>ga090e97b588f83718c19f0ed3d07d1d70</anchor>
      <arglist>(void(*func)(void *), void *arg, unsigned num_workers, unsigned *workers, const char *name)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_timing_now</name>
      <anchorfile>group__API__Miscellaneous__Helpers.html</anchorfile>
      <anchor>ga34953348991f74a1cbd694fffb27f8b7</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Modularized_Scheduler</name>
    <title>Modularized Scheduler Interface</title>
    <filename>group__API__Modularized__Scheduler.html</filename>
    <class kind="struct">starpu_sched_component</class>
    <class kind="struct">starpu_sched_tree</class>
    <class kind="struct">starpu_sched_component_fifo_data</class>
    <class kind="struct">starpu_sched_component_prio_data</class>
    <class kind="struct">starpu_sched_component_mct_data</class>
    <class kind="struct">starpu_sched_component_perfmodel_select_data</class>
    <class kind="struct">starpu_sched_component_composed_recipe</class>
    <class kind="struct">starpu_sched_component_specs</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_COMPONENT_IS_HOMOGENEOUS</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gabd7a0274979659050496fcc1c66b7a1c</anchor>
      <arglist>(component)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_COMPONENT_IS_SINGLE_MEMORY_NODE</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gafb485fe5fb1f46c7ce7eb505815f306b</anchor>
      <arglist>(component)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_sched_component_properties</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gab40975877d0c861348ccc96602be2cae</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCHED_COMPONENT_HOMOGENEOUS</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ggab40975877d0c861348ccc96602be2caea6227222e65fd89d113fbba200d51bb9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCHED_COMPONENT_SINGLE_MEMORY_NODE</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ggab40975877d0c861348ccc96602be2caea20b7f2d35c5e42fcf7657d59741f7023</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCHED_COMPONENT_HOMOGENEOUS</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ggab40975877d0c861348ccc96602be2caea6227222e65fd89d113fbba200d51bb9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCHED_COMPONENT_SINGLE_MEMORY_NODE</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ggab40975877d0c861348ccc96602be2caea20b7f2d35c5e42fcf7657d59741f7023</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_tree *</type>
      <name>starpu_sched_tree_create</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga70c74dee688684fb09e45833094ba21a</anchor>
      <arglist>(unsigned sched_ctx_id) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_tree_destroy</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gab8540c0e69c3af17c424944af9b3c802</anchor>
      <arglist>(struct starpu_sched_tree *tree)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_tree_update_workers</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga61c2f443fb8d5b6a2a1404eae0e7868e</anchor>
      <arglist>(struct starpu_sched_tree *t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_tree_update_workers_in_ctx</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gae97d6b25d8143ef2f7b6260c6f550c5c</anchor>
      <arglist>(struct starpu_sched_tree *t)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_tree_push_task</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga511a8c5bc261cae8153cdfe7670a21a4</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_tree_add_workers</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga7ceaaeea8cc10f6d67706c4ee33e93ce</anchor>
      <arglist>(unsigned sched_ctx_id, int *workerids, unsigned nworkers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_tree_remove_workers</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga30b0cd3f3d64be88a163a31148c58f1f</anchor>
      <arglist>(unsigned sched_ctx_id, int *workerids, unsigned nworkers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_component_connect</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga90dfe435a72d46bd23ddaffa287833c2</anchor>
      <arglist>(struct starpu_sched_component *parent, struct starpu_sched_component *child)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_component_destroy</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga217b0c2bb96b4aa5f0ccda71c2d81f00</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_component_destroy_rec</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gaab8471a2921b0508b9269b08ecfba3b7</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_can_execute_task</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga9ed484d5707136420880285afe55bd3d</anchor>
      <arglist>(struct starpu_sched_component *component, struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>int STARPU_WARN_UNUSED_RESULT</type>
      <name>starpu_sched_component_execute_preds</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga79594b4403678fa1d6aa08f7fbe6e525</anchor>
      <arglist>(struct starpu_sched_component *component, struct starpu_task *task, double *length)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_sched_component_transfer_length</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga1460e5ad34f360408b3a0003aa0a2b9f</anchor>
      <arglist>(struct starpu_sched_component *component, struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_component *</type>
      <name>starpu_sched_component_worker_get</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga1fd3c2682a57ca9105965b942a5a1313</anchor>
      <arglist>(unsigned sched_ctx, int workerid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_worker_get_workerid</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga21df9bf6da463d3b945f0886939b637a</anchor>
      <arglist>(struct starpu_sched_component *worker_component)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_worker</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gae1ac4bb35944de31705ba297aef1f9a1</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_simple_worker</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga48a2bf7d22985f9a810f8a563153e936</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_combined_worker</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga8efad6df3a5b036619ea5c76fe50a0c5</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_component_worker_pre_exec_hook</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gae4a99812a77a736f4c5e2f64f9f1691c</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_component_worker_post_exec_hook</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gad085b7c0caa00c733805af119721530c</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_component *</type>
      <name>starpu_sched_component_fifo_create</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga0c419ef90f6adce7cc9ebca955822dbb</anchor>
      <arglist>(struct starpu_sched_tree *tree, struct starpu_sched_component_fifo_data *fifo_data) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_fifo</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga752889050651f6e3c83ddd2c3f96c0ff</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_component *</type>
      <name>starpu_sched_component_prio_create</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gac411c78168126af96e87c00a3f126976</anchor>
      <arglist>(struct starpu_sched_tree *tree, struct starpu_sched_component_prio_data *prio_data) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_prio</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga2a13df294dc02f7862389d1ab203c72e</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_tree_work_stealing_push_task</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga4fd6f598c30dac693e9e5b37f3e450f6</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_work_stealing</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gac3ab95326f770ecd208a9949d18ceddf</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_random</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gad76ac70069357b5a3d0ac0a1489016a2</anchor>
      <arglist>(struct starpu_sched_component *)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_eager</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gab68946728a662c919c0738b2c8f134cf</anchor>
      <arglist>(struct starpu_sched_component *)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_eager_calibration</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga5174f1acd22753db267b90ca4aa76cf0</anchor>
      <arglist>(struct starpu_sched_component *)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_component *</type>
      <name>starpu_sched_component_mct_create</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gaa207453a69f029b2f3c74bc8b9cffde2</anchor>
      <arglist>(struct starpu_sched_tree *tree, struct starpu_sched_component_mct_data *mct_data) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_mct</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gaa5f20788ff0d18a3a2f86a190a42ba19</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_component *</type>
      <name>starpu_sched_component_heft_create</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga9e38868b404f1b4c31e144bf96d4bb4c</anchor>
      <arglist>(struct starpu_sched_tree *tree, struct starpu_sched_component_mct_data *mct_data) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_heft</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gaa462b99f897b4a25105183e9017e47fa</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_component *</type>
      <name>starpu_sched_component_perfmodel_select_create</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga3abc030bc6b581c67635938e54cf4f02</anchor>
      <arglist>(struct starpu_sched_tree *tree, struct starpu_sched_component_perfmodel_select_data *perfmodel_select_data) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_component_is_perfmodel_select</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga710bd34c26b12cd28e053adb0328cee1</anchor>
      <arglist>(struct starpu_sched_component *component)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_component_composed_recipe *</type>
      <name>starpu_sched_component_composed_recipe_create</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga3ac4da6a19d9f0a34e870e20d6c34966</anchor>
      <arglist>(void) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_component_composed_recipe *</type>
      <name>starpu_sched_component_composed_recipe_create_singleton</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga0b4db1194be324a7cbd2c6486db867d7</anchor>
      <arglist>(struct starpu_sched_component *(*create_component)(struct starpu_sched_tree *tree, void *arg), void *arg) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_component_composed_recipe_add</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gaccfaa23c5e0948fec6be5f71fc69f32c</anchor>
      <arglist>(struct starpu_sched_component_composed_recipe *recipe, struct starpu_sched_component *(*create_component)(struct starpu_sched_tree *tree, void *arg), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_component_composed_recipe_destroy</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga2f27b106110cf20f52ed20e68ecd8613</anchor>
      <arglist>(struct starpu_sched_component_composed_recipe *)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_component *</type>
      <name>starpu_sched_component_composed_component_create</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>ga3c60576fc0645bec4cd90c758545fd40</anchor>
      <arglist>(struct starpu_sched_tree *tree, struct starpu_sched_component_composed_recipe *recipe) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_sched_tree *</type>
      <name>starpu_sched_component_make_scheduler</name>
      <anchorfile>group__API__Modularized__Scheduler.html</anchorfile>
      <anchor>gab17420484cfb68122d986d1feee91905</anchor>
      <arglist>(unsigned sched_ctx_id, struct starpu_sched_component_specs s)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_MPI_Support</name>
    <title>MPI Support</title>
    <filename>group__API__MPI__Support.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_MPI</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabefad62cd0f0ea270a95e97c0fae17d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_init_comm</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabdb1e15d1fc805115d660848a711f101</anchor>
      <arglist>(int *argc, char ***argv, int initialize_mpi, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_init</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga008d0df24380455d6b9301ae750ccd4c</anchor>
      <arglist>(int *argc, char ***argv, int initialize_mpi)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_initialize</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga0ec1ce958654468b20224c9ae772f98f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_initialize_extended</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga234e28905c976ed1b03fcdf75812a06d</anchor>
      <arglist>(int *rank, int *world_size)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_shutdown</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga87b22900dc132a958a0f7bce68d32153</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_comm_amounts_retrieve</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga3a6dcddd7eaf6cd3265fd837d67800a2</anchor>
      <arglist>(size_t *comm_amounts)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_send</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaca01fd4db21aebb91da808a3d03f36dc</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_recv</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaa92438804968616ed4fee7246ec254aa</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, MPI_Status *status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gafc29de4080f202511f2b558673304b8c</anchor>
      <arglist>(starpu_data_handle_t data_handle, starpu_mpi_req *req, int dest, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabb6c92b51c1e76408e0816385fde75c5</anchor>
      <arglist>(starpu_data_handle_t data_handle, starpu_mpi_req *req, int source, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae9506ceb82e72ab1096ac8443dbf13e4</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabdf54ffd31c0b0908d7751e1b2257897</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_detached_sequential_consistency</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga68cd36f12bdc9ee79034ee71b48ab8ef</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg, int sequential_consistency)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_issend</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga8374cb42eb85e86e58ccfa8e0c5e8aab</anchor>
      <arglist>(starpu_data_handle_t data_handle, starpu_mpi_req *req, int dest, int mpi_tag, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_issend_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae247da864753534ad06c6eef58db4f7e</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_wait</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaad03f0a288bad79078045130b9a83188</anchor>
      <arglist>(starpu_mpi_req *req, MPI_Status *status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_test</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae8e8389eb3e916131946028e9ebee670</anchor>
      <arglist>(starpu_mpi_req *req, int *flag, MPI_Status *status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_barrier</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga77aa675b91da2a069caeebdb11a3edc9</anchor>
      <arglist>(MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_wait_for_all</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gabb4f38872a0b5fd8122b681acc0cf932</anchor>
      <arglist>(MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gae6957a62f85340484cbcebbf65b573c4</anchor>
      <arglist>(starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga6cbbe5e484f38cb5e4e601330f2c5b0d</anchor>
      <arglist>(starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_isend_array_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga546dbbe7e9e40817b23bd37fb0eccfe1</anchor>
      <arglist>(unsigned array_size, starpu_data_handle_t *data_handle, int *dest, int *mpi_tag, MPI_Comm *comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_irecv_array_detached_unlock_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga11f71926b324d325d8a6f6b72b58cdf1</anchor>
      <arglist>(unsigned array_size, starpu_data_handle_t *data_handle, int *source, int *mpi_tag, MPI_Comm *comm, starpu_tag_t tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_get_communication_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga1d0194c08bd343a68c833f449fb62fa8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_set_communication_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaa734bb0c57e4a48a7fe6e41f2e563e68</anchor>
      <arglist>(int tag)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_datatype_register</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaf9345fd74fd060cc6712457e711b29c1</anchor>
      <arglist>(starpu_data_handle_t handle, starpu_mpi_datatype_allocate_func_t allocate_datatype_func, starpu_mpi_datatype_free_func_t free_datatype_func)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_datatype_unregister</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga02b8209239a54d25ec39d93f9ccb9805</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_cache_is_enabled</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gac0558236c9fb4cfc333f413c0ca191a9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_cache_set</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga82dceaf1ad42b9a0ab4ed22f9c03b1a8</anchor>
      <arglist>(int enabled)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_cache_flush</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gacb05635d95f946f099a1772c8bd59c8b</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_cache_flush_all_data</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gadf1f289d1cf35bd31df930bc347fb9ce</anchor>
      <arglist>(MPI_Comm comm)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_mpi_data_register</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gac45e4d77c3d077955bcdc6e1bf0e8cd8</anchor>
      <arglist>(data_handle, tag, rank)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_set_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga42225d9bf5ed973056f29f9b86907c8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_mpi_data_set_rank</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga79ca5db23d267ee7c199864993e434a9</anchor>
      <arglist>(handle, rank)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_set_rank</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gadce715a49b477ef0204be1a199abfddb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_get_rank</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga5281ad23b39e64ca254347e89cbb54e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_get_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaaf806ce3131edd757470c7401bd41bd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_EXECUTE_ON_NODE</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga750b87a6ea137c903f02c53029d1131d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_EXECUTE_ON_DATA</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gadc9112873f348b1f6daa0da4b93b061f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_data_register_comm</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga83339cfcecff5b7e64f85e3e391aa783</anchor>
      <arglist>(starpu_data_handle_t data_handle, int tag, int rank, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_data_set_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga5eb9762df37404b659a49daec17e9c82</anchor>
      <arglist>(starpu_data_handle_t handle, int tag)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_data_set_rank_comm</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga7d4b05715446d730022ea8a1fbac4f60</anchor>
      <arglist>(starpu_data_handle_t handle, int rank, MPI_Comm comm)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_data_get_rank</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga0a464b972068bac652622d6a1ed0a996</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_data_get_tag</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gacf1e74720ea6884fd8d1ded3cb4eaae9</anchor>
      <arglist>(starpu_data_handle_t handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_insert_task</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga04229edbc905be8eefce8fb58bfccf8f</anchor>
      <arglist>(MPI_Comm comm, struct starpu_codelet *codelet,...)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_task_insert</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaa823d6398e61516bba887b90ad048914</anchor>
      <arglist>(MPI_Comm comm, struct starpu_codelet *codelet,...)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_task *</type>
      <name>starpu_mpi_task_build</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga03f635684df3c110aea79ebd59f73165</anchor>
      <arglist>(MPI_Comm comm, struct starpu_codelet *codelet,...)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_task_post_build</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga9c609f3cc579af484808b3e1b8b787d9</anchor>
      <arglist>(MPI_Comm comm, struct starpu_codelet *codelet,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_get_data_on_node</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga0ca9fc89dc1315bf32f38cd14caf24a5</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle, int node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_get_data_on_node_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga6c581f7133773a98b583e0c51970ecb7</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle, int node, void(*callback)(void *), void *arg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_get_current_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga9eb17d58fc8f0c33fc4e5985d440079e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_set_current_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga35f7d1ea8e747cbf1637301b14ef0db8</anchor>
      <arglist>(int policy)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_register_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gab69b1f823043d16b7d0d799d1ad88a04</anchor>
      <arglist>(starpu_mpi_select_node_policy_func_t policy_func)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_node_selection_unregister_policy</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gad6453ef7697756dd3b1746399eec4867</anchor>
      <arglist>(int policy)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_mpi_redux_data</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga565996704c6707a55410488f8d569357</anchor>
      <arglist>(MPI_Comm comm, starpu_data_handle_t data_handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_scatter_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>ga39ae075749aef08653ade368b9a95ee0</anchor>
      <arglist>(starpu_data_handle_t *data_handles, int count, int root, MPI_Comm comm, void(*scallback)(void *), void *sarg, void(*rcallback)(void *), void *rarg)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_mpi_gather_detached</name>
      <anchorfile>group__API__MPI__Support.html</anchorfile>
      <anchor>gaf83405a3154137f00ccf554c5e5684b5</anchor>
      <arglist>(starpu_data_handle_t *data_handles, int count, int root, MPI_Comm comm, void(*scallback)(void *), void *sarg, void(*rcallback)(void *), void *rarg)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Multiformat_Data_Interface</name>
    <title>Multiformat Data Interface</title>
    <filename>group__API__Multiformat__Data__Interface.html</filename>
    <class kind="struct">starpu_multiformat_data_interface_ops</class>
    <class kind="struct">starpu_multiformat_interface</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_CPU_PTR</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>gaf0937aae4bedeb03e9ebbec36cc1dad6</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_CUDA_PTR</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>gaeb6356075b3f57e8c7aeccc85f076d87</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_OPENCL_PTR</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>ga0b6869745c464525b25a0757d112c318</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_MIC_PTR</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>ga9a85e9fa7f275ae4805a7208e4ef2702</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MULTIFORMAT_GET_NX</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>ga9719eab3a5e9fbaad2837b4a985a3a14</anchor>
      <arglist>(interface)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_multiformat_data_register</name>
      <anchorfile>group__API__Multiformat__Data__Interface.html</anchorfile>
      <anchor>gae36d714a8bf065925923a35fd6fd4ae5</anchor>
      <arglist>(starpu_data_handle_t *handle, unsigned home_node, void *ptr, uint32_t nobjects, struct starpu_multiformat_data_interface_ops *format_ops)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_OpenCL_Extensions</name>
    <title>OpenCL Extensions</title>
    <filename>group__API__OpenCL__Extensions.html</filename>
    <class kind="struct">starpu_opencl_program</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_OPENCL</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga160dc78c0f6a90b5aa2200b49c5b4d7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAXOPENCLDEVS</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga1b591248c13dc33e2e9b00ace593405e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL_DATADIR</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gababf3ee0552f34c89fdc8ebdec116dbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_context</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga7ad0ab374a65417ae3d3a9ceed8f24c4</anchor>
      <arglist>(int devid, cl_context *context)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_device</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga84a524eaac758722f083ee129a19a567</anchor>
      <arglist>(int devid, cl_device_id *device)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_queue</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga8318641373e06a2a635cb3ab377c5994</anchor>
      <arglist>(int devid, cl_command_queue *queue)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_current_context</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gad18d6a0dfbfadcb0fe0b2be6294aa87d</anchor>
      <arglist>(cl_context *context)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_get_current_queue</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gab43c1f30361aaa72ca98a9a9fdec792b</anchor>
      <arglist>(cl_command_queue *queue)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_set_kernel_args</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga80f968f210417eb9fbf2bfe82e1953a9</anchor>
      <arglist>(cl_int *err, cl_kernel *kernel,...)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_opencl_from_file</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga3441dd1c6e61717dd81a3a6eb0bf75a8</anchor>
      <arglist>(const char *source_file_name, struct starpu_opencl_program *opencl_programs, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_opencl_from_string</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga9768e086b947961c9db5199e676f05db</anchor>
      <arglist>(const char *opencl_program_source, struct starpu_opencl_program *opencl_programs, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_unload_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gaa38e7cb3231ed30303e50f46c8f6e39c</anchor>
      <arglist>(struct starpu_opencl_program *opencl_programs)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_load_program_source</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga7dd3784262c0be223394bc8c2fe81935</anchor>
      <arglist>(const char *source_file_name, char *located_file_name, char *located_dir_name, char *opencl_program_source)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_load_program_source_malloc</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gadc4689d1ad238ba0b296e205e3bb6317</anchor>
      <arglist>(const char *source_file_name, char **located_file_name, char **located_dir_name, char **opencl_program_source)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_compile_opencl_from_file</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga03f34ac98495afc5d0f268e366f80598</anchor>
      <arglist>(const char *source_file_name, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_compile_opencl_from_string</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gac543428352d04b3ff8f735cfc71c3b99</anchor>
      <arglist>(const char *opencl_program_source, const char *file_name, const char *build_options)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_binary_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga893f462bfed53eff3a54f341488db7ad</anchor>
      <arglist>(const char *kernel_id, struct starpu_opencl_program *opencl_programs)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_load_kernel</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga1d06b6c00b15f4fcd8d4c0c998f955ac</anchor>
      <arglist>(cl_kernel *kernel, cl_command_queue *queue, struct starpu_opencl_program *opencl_programs, const char *kernel_name, int devid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_release_kernel</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga68adf424491ec715e891000fa4a6030d</anchor>
      <arglist>(cl_kernel kernel)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_opencl_collect_stats</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gaeaabc8e5d90531a21a8307c06c659984</anchor>
      <arglist>(cl_event event)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL_DISPLAY_ERROR</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>gae023af317c4040f926d2c50a8f96b5d2</anchor>
      <arglist>(status)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL_REPORT_ERROR</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga6680ed21ce09f073fa0256169f4e4868</anchor>
      <arglist>(status)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENCL_REPORT_ERROR_WITH_MSG</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga23fa324d87f923f39f05bfdc9d3fe7e9</anchor>
      <arglist>(msg, status)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>starpu_opencl_error_string</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga7f8e1507dec24eaa7427923bbacb873f</anchor>
      <arglist>(cl_int status)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_opencl_display_error</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga9e0073e4c839eec6096f6eeca21d7e36</anchor>
      <arglist>(const char *func, const char *file, int line, const char *msg, cl_int status)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static __starpu_inline void</type>
      <name>starpu_opencl_report_error</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga15f6e4c82407cd3c16e461fc2f129a40</anchor>
      <arglist>(const char *func, const char *file, int line, const char *msg, cl_int status)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_allocate_memory</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga2708694c061fd89272b0355bcaac11df</anchor>
      <arglist>(int devid, cl_mem *addr, size_t size, cl_mem_flags flags)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_copy_ram_to_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga9a1331f26cc54a1f5406770f68b95d29</anchor>
      <arglist>(void *ptr, unsigned src_node, cl_mem buffer, unsigned dst_node, size_t size, size_t offset, cl_event *event, int *ret)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_copy_opencl_to_ram</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga2bd30ada22e761a6b889f0661b54592f</anchor>
      <arglist>(cl_mem buffer, unsigned src_node, void *ptr, unsigned dst_node, size_t size, size_t offset, cl_event *event, int *ret)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_copy_opencl_to_opencl</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga002e85a0f6bcd087df54287de5ae5e07</anchor>
      <arglist>(cl_mem src, unsigned src_node, size_t src_offset, cl_mem dst, unsigned dst_node, size_t dst_offset, size_t size, cl_event *event, int *ret)</arglist>
    </member>
    <member kind="function">
      <type>cl_int</type>
      <name>starpu_opencl_copy_async_sync</name>
      <anchorfile>group__API__OpenCL__Extensions.html</anchorfile>
      <anchor>ga95184b2188d67976e00bcb4637ac1933</anchor>
      <arglist>(uintptr_t src, size_t src_offset, unsigned src_node, uintptr_t dst, size_t dst_offset, unsigned dst_node, size_t size, cl_event *event)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_OpenMP_Runtime_Support</name>
    <title>OpenMP Runtime Support</title>
    <filename>group__API__OpenMP__Runtime__Support.html</filename>
    <class kind="struct">starpu_omp_lock_t</class>
    <class kind="struct">starpu_omp_nest_lock_t</class>
    <class kind="struct">starpu_omp_parallel_region_attr</class>
    <class kind="struct">starpu_omp_task_region_attr</class>
    <member kind="enumeration">
      <type></type>
      <name>starpu_omp_sched_value</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga33af06875785da29f3988d3a985e99f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_undefined</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a2b1faa3fe1b297e1d8b2c87bb804a46f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_static</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8ab6df9d00a7ef9f3acce6508d5b24079e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_dynamic</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a74f498a75f2ba89ef620a761bc2f1696</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_guided</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a08bd52bc5cc5af8ec937618f1402bb4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_auto</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8aa2a597e81534c3a170429c535b970466</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_runtime</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a9f23fc3844c5fbe86ba2b09ce4c73f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_omp_proc_bind_value</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gacc6078a78820c367f07d37da11c04520</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_undefined</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520ac9fd1a7f2323bf9cd55ef6ca44b09dea</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_false</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a86961832c7cc5d9f7821143e1c185257</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_true</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a645c937c29cc24aabe9fb1af0be00cfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_master</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520aee55c45804c821880591cf98989daa72</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_close</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a92928e124254f30c345ebf8c616e2846</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_spread</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a225aa415ff421aba9f93f09b8201725e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_undefined</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a2b1faa3fe1b297e1d8b2c87bb804a46f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_static</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8ab6df9d00a7ef9f3acce6508d5b24079e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_dynamic</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a74f498a75f2ba89ef620a761bc2f1696</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_guided</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a08bd52bc5cc5af8ec937618f1402bb4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_auto</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8aa2a597e81534c3a170429c535b970466</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_sched_runtime</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gga33af06875785da29f3988d3a985e99f8a9f23fc3844c5fbe86ba2b09ce4c73f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_undefined</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520ac9fd1a7f2323bf9cd55ef6ca44b09dea</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_false</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a86961832c7cc5d9f7821143e1c185257</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_true</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a645c937c29cc24aabe9fb1af0be00cfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_master</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520aee55c45804c821880591cf98989daa72</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_close</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a92928e124254f30c345ebf8c616e2846</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>starpu_omp_proc_bind_spread</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ggacc6078a78820c367f07d37da11c04520a225aa415ff421aba9f93f09b8201725e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_OPENMP</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga0a86f6a9d780b564dae9b9a71cfe5e19</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_init</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa5c38dfec313088ca5e527a97f40969d</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_shutdown</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa4b70af0a6cf757e05e300c5eeea0a46</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_parallel_region</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga41d9b4f1e57c02d34c4bd6e53469c3fc</anchor>
      <arglist>(const struct starpu_omp_parallel_region_attr *attr) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_master</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga3be0d3b2145764db53eb59001ac74e2b</anchor>
      <arglist>(void(*f)(void *arg), void *arg) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_master_inline</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gac9613ed1365bf0db54b6225a1d2c6124</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_barrier</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gac9279b86d3c79fc918856bf1f9cde7d6</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_critical</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga9f66990dcc824f3078afbba559817c8d</anchor>
      <arglist>(void(*f)(void *arg), void *arg, const char *name) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_critical_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaaf0d71971d1ab1b0d4f9919830693d98</anchor>
      <arglist>(const char *name) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_critical_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2f1f0d70df3de43bf9069eb4c2e1ca9f</anchor>
      <arglist>(const char *name) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_single</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga9db57e7c1c67a02837b279145fb1883d</anchor>
      <arglist>(void(*f)(void *arg), void *arg, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_single_inline</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa3127cc9d95397b4f066cef20f9ef99d</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_single_copyprivate</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga5790ead92feacb71e3882de730170b05</anchor>
      <arglist>(void(*f)(void *arg, void *data, unsigned long long data_size), void *arg, void *data, unsigned long long data_size) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_omp_single_copyprivate_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gabe2275b4c4295825181e559affb6dcf2</anchor>
      <arglist>(void *data) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_single_copyprivate_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga7082f9fb91da015537bf7e65611c9db6</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_for</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga343f59fea8ebbd23c73fcac25571ca20</anchor>
      <arglist>(void(*f)(unsigned long long _first_i, unsigned long long _nb_i, void *arg), void *arg, unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_first</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga896cbd300cf111eb09251995b0576533</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_first_i, unsigned long long *_nb_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_next</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga6551d8b2707c8808c78eb97955928bc8</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_first_i, unsigned long long *_nb_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_for_alt</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2c2739d7b7a1702540b878684b8e4e9b</anchor>
      <arglist>(void(*f)(unsigned long long _begin_i, unsigned long long _end_i, void *arg), void *arg, unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_first_alt</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga698809a6ff7858a8f26e96635b5efd7e</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_begin_i, unsigned long long *_end_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_for_inline_next_alt</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga0a2d26b924681a81e92562edf7fa6742</anchor>
      <arglist>(unsigned long long nb_iterations, unsigned long long chunk, int schedule, int ordered, unsigned long long *_begin_i, unsigned long long *_end_i) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_ordered</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gad8abcefdd1731fe8269d833e8103a945</anchor>
      <arglist>(void(*f)(void *arg), void *arg) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_ordered_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga82338737b7712d171bbbf6125bac33d3</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_ordered_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1080ae6a620b2c40c71311df32ab44d4</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_sections</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga544807d42cb7522f8441f1a499f203d3</anchor>
      <arglist>(unsigned long long nb_sections, void(**section_f)(void *arg), void **section_arg, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_sections_combined</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga650f12381df2c525c058e6c137930e50</anchor>
      <arglist>(unsigned long long nb_sections, void(*section_f)(unsigned long long section_num, void *arg), void *section_arg, int nowait) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_task_region</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gadaf4576512b9ecc58c71cb36a87105b7</anchor>
      <arglist>(const struct starpu_omp_task_region_attr *attr) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskwait</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga82292171a04e7de120b878ea52e32bd6</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskgroup</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga6561f2bb3fd370901216986ae8692cc7</anchor>
      <arglist>(void(*f)(void *arg), void *arg) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskgroup_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa63576df9729394a3d53137d7021bab5</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_taskgroup_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga541bb9f73081830e3173cf71d57bee94</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_num_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2c980c583e9faf124b4446fa90b5ece9</anchor>
      <arglist>(int threads) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga3f9e6a6eff37b7bde50539551d2f40f7</anchor>
      <arglist>() __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_thread_num</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gacc19dd986f7ad6aac6ac24f5f86aa912</anchor>
      <arglist>() __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_max_threads</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga417fc9433682c4817f35623bb2c9dafd</anchor>
      <arglist>() __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_procs</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gacc9c3792ea07d0bf694d8b0c8b8c5fb7</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_in_parallel</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gae134345a048428661b344b671155cdd9</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_dynamic</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga3cc840689933be3d9e7952eb76e0f44e</anchor>
      <arglist>(int dynamic_threads) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_dynamic</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaa15212ca3137d7aba827794b26fee9e0</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_nested</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga7d6a35f049f4c449c02680f480239754</anchor>
      <arglist>(int nested) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_nested</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1118d277474ac2dc1a22bf6ef4c59fa9</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_cancellation</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gabbd8c55707eb2173324c7dcfe04832f3</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_schedule</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga906913c3eb454506682a130c6bcd6337</anchor>
      <arglist>(enum starpu_omp_sched_value kind, int modifier) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_get_schedule</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8de196a9b2248ee34f9183a081da8be5</anchor>
      <arglist>(enum starpu_omp_sched_value *kind, int *modifier) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_thread_limit</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga05ecbf96c69f19525730f46d49d92937</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_max_active_levels</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga846b449fccf725b83acf1c3b14f3c740</anchor>
      <arglist>(int max_levels) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_max_active_levels</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gafe303ff7d26bcd668959ee967eedda97</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_level</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga51dbabbff069511b043833847fb45f30</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_ancestor_thread_num</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1b951f0922d794e6deb2579c81bcf41b</anchor>
      <arglist>(int level) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_team_size</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1a6d1009b6ff1f68617ade03554d03f3</anchor>
      <arglist>(int level) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_active_level</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga2cb37eec19b8a24242819e666ecea8fc</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_in_final</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaac482422d2b9bc26c1877f49eaa5fca2</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_omp_proc_bind_value</type>
      <name>starpu_omp_get_proc_bind</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga478c43dbbdd549ebe481610fd42e1b92</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_default_device</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga409d48dc7e7158cb7cbdaca31a4b0a27</anchor>
      <arglist>(int device_num) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_default_device</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga5a6c050bf14b81d49716946d68e1d7f9</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_devices</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga63f94bc02b7c56c48bd3cbd0a38796ee</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_num_teams</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga1b1d7573040775370ffd49b7accad0f2</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_get_team_num</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaff3980856fe3f8ebded80d72ef13ab78</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_is_initial_device</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaede763d0f3b47343eb1a5e64ceec0062</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_init_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8969523514417ac253a9007ffb1eabe0</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_destroy_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga58fb716554b6a79a3207ba114c49abdb</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gadca05b4b67afb0c82cdd39c4773b8fce</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_unset_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaf820ec6afd10011a711cfe1140f2789c</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_test_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gabba6e6a047b80568ba0c98d725290650</anchor>
      <arglist>(starpu_omp_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_init_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga444e5466c7f457ace4e9aa78cf27e6ef</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_destroy_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga0fb5be52e8f7630a4f24c2e844a22519</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_set_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8c913c32ba007ce53cd3c3cfcecf2342</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_unset_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaf704c62ed129ed488c4bfd3567ef244d</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_omp_test_nest_lock</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gab2727bdd191a5d1c495dc01ccf9872f0</anchor>
      <arglist>(starpu_omp_nest_lock_t *lock) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_atomic_fallback_inline_begin</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga86414f0d5062cf47d64b0599bcdd1ccf</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_atomic_fallback_inline_end</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gad8189d962807a8504a5fad8cc2bf58ba</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_omp_get_wtime</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>gaedec99a0472438a5da0c014a47087878</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_omp_get_wtick</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga706d36f77512013d4e98e8f42da02ff3</anchor>
      <arglist>(void) __STARPU_OMP_NOTHROW</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_omp_vector_annotate</name>
      <anchorfile>group__API__OpenMP__Runtime__Support.html</anchorfile>
      <anchor>ga8bc9734bc0f9fb5485b4708b4f3ea219</anchor>
      <arglist>(starpu_data_handle_t handle, uint32_t slice_base) __STARPU_OMP_NOTHROW</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Parallel_Tasks</name>
    <title>Parallel Tasks</title>
    <filename>group__API__Parallel__Tasks.html</filename>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_get_size</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga573d2513a10cf1d14fee0467eda0884d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_get_rank</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga4eb5aaf2f4e0c9a7f07cbc80f3ae9664</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_combined_worker_get_count</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga49a6732f413550652450baa64594bc5e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_get_id</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>gae53499613fb72e9aa1abad6437a926f9</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_assign_workerid</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga176c8b45655615fbd09f0b70ba8b78b5</anchor>
      <arglist>(int nworkers, int workerid_array[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_get_description</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga720a70f5297ffe3b5ea5cf6eff549c8c</anchor>
      <arglist>(int workerid, int *worker_size, int **combined_workerid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_combined_worker_can_execute_task</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>gae3fe82c940e36715145baa9fe003e453</anchor>
      <arglist>(unsigned workerid, struct starpu_task *task, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_parallel_task_barrier_init</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>ga3d362cb0de1da4e2659f602eff325479</anchor>
      <arglist>(struct starpu_task *task, int workerid)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_parallel_task_barrier_init_n</name>
      <anchorfile>group__API__Parallel__Tasks.html</anchorfile>
      <anchor>gaf99bc08bf6f586b658febdd1dc6fef80</anchor>
      <arglist>(struct starpu_task *task, int worker_size)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Performance_Model</name>
    <title>Performance Model</title>
    <filename>group__API__Performance__Model.html</filename>
    <class kind="struct">starpu_perfmodel</class>
    <class kind="struct">starpu_perfmodel_regression_model</class>
    <class kind="struct">starpu_perfmodel_per_arch</class>
    <class kind="struct">starpu_perfmodel_history_list</class>
    <class kind="struct">starpu_perfmodel_history_entry</class>
    <member kind="enumeration">
      <type></type>
      <name>starpu_perfmodel_type</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gae161a7cae376f3fc831a2b764e8144e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_PER_ARCH</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6ae4b97f418bd3496f23889a6950d7be82</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_COMMON</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6aeedb2a69541e9b7380f766711f8eab26</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_HISTORY_BASED</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6a61a1a08e950abce6779d8eaee6cfe395</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_REGRESSION_BASED</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6ab46450dd0cc93e3aaed1900afdcad26f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_NL_REGRESSION_BASED</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6a4b1935cc6389974fe69120e978aa3a29</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_PER_ARCH</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6ae4b97f418bd3496f23889a6950d7be82</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_COMMON</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6aeedb2a69541e9b7380f766711f8eab26</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_HISTORY_BASED</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6a61a1a08e950abce6779d8eaee6cfe395</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_REGRESSION_BASED</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6ab46450dd0cc93e3aaed1900afdcad26f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_NL_REGRESSION_BASED</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ggae161a7cae376f3fc831a2b764e8144e6a4b1935cc6389974fe69120e978aa3a29</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_init</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga299bb26d02dfb4802c633d06b8b5ff8b</anchor>
      <arglist>(struct starpu_perfmodel *model)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_free_sampling_directories</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga4da8a63a264195f54ccaa9f49d188897</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_load_file</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gaf02aaa9dd29cab640329f496aaefcc65</anchor>
      <arglist>(const char *filename, struct starpu_perfmodel *model)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_load_symbol</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga5ab7213f5c229c16e783004853fcba4c</anchor>
      <arglist>(const char *symbol, struct starpu_perfmodel *model)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_unload_model</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga96746e1665eeffec32a82a465325f7dc</anchor>
      <arglist>(struct starpu_perfmodel *model)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_debugfilepath</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga2ec4c9ec1ac320aae99e31871bfed90d</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_perfmodel_arch *arch, char *path, size_t maxlen, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>starpu_perfmodel_get_archtype_name</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga8b39921ed28de141fb99a79415b8c489</anchor>
      <arglist>(enum starpu_worker_archtype archtype)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_get_arch_name</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga73e84b64af1ce96eec9b1c83e7468c60</anchor>
      <arglist>(struct starpu_perfmodel_arch *arch, char *archname, size_t maxlen, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_perfmodel_arch *</type>
      <name>starpu_worker_get_perf_archtype</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gadb2ada1d388826505ff79352a469cac3</anchor>
      <arglist>(int workerid, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_list</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga7562bd09e4894eeb15391f2a0f21cf00</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_directory</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gae99274bc3bc2d6692c0e22113971b9ef</anchor>
      <arglist>(FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_print</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga19aebb6aee49b5760d4546d0044c87ea</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_perfmodel_arch *arch, unsigned nimpl, char *parameter, uint32_t *footprint, FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_perfmodel_print_all</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gab70e8b18742e868e5f6505814938bdd1</anchor>
      <arglist>(struct starpu_perfmodel *model, char *arch, char *parameter, uint32_t *footprint, FILE *output)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bus_print_bandwidth</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gac4075eddef3625eccbbc9f1670c69244</anchor>
      <arglist>(FILE *f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_bus_print_affinity</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga712a3e3498f113efb81c42c6b7a48335</anchor>
      <arglist>(FILE *f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_perfmodel_update_history</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>gaecb9341bff471557abbb63a966449481</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned cpuid, unsigned nimpl, double measured)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_transfer_bandwidth</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga767f88b087d4b0bf4086ade9bb673826</anchor>
      <arglist>(unsigned src_node, unsigned dst_node)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_transfer_latency</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga77915973bf2f03bd70fb84ee4c9c51a0</anchor>
      <arglist>(unsigned src_node, unsigned dst_node)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_transfer_predict</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga9a4af8d1b5a3486a5a21c72dd4a6c4c2</anchor>
      <arglist>(unsigned src_node, unsigned dst_node, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_perfmodel_history_based_expected_perf</name>
      <anchorfile>group__API__Performance__Model.html</anchorfile>
      <anchor>ga03fc571c3f6132339a75004899f577af</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_perfmodel_arch *arch, uint32_t footprint)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Profiling</name>
    <title>Profiling</title>
    <filename>group__API__Profiling.html</filename>
    <class kind="struct">starpu_profiling_task_info</class>
    <class kind="struct">starpu_profiling_worker_info</class>
    <class kind="struct">starpu_profiling_bus_info</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PROFILING_DISABLE</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gacd7d4b9e0d31ea949f7c14fed739a502</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PROFILING_ENABLE</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga24254aaa82ee94c3e6b8b2e21185db32</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_profiling_status_set</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gabeb22bbe8062a45507cfc6273aae51ae</anchor>
      <arglist>(int status)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_profiling_status_get</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gaf014e2e050ebd38bc4cffec0081f96bf</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_profiling_init</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga6ab0a1e4a8a55e0c54e2151fd0a82a36</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_profiling_set_id</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gad126fc77826a0472b6bd872e7ceb40d4</anchor>
      <arglist>(int new_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_profiling_worker_get_info</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga8a423df93ec48a7391b2f439357a5544</anchor>
      <arglist>(int workerid, struct starpu_profiling_worker_info *worker_info)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_profiling_info</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga76ad19fc8b99bcd42d505c76aafae6b6</anchor>
      <arglist>(int busid, struct starpu_profiling_bus_info *bus_info)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_count</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga8dc4665fdbbbf8ce8f1b905aa8411642</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_id</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga7c70803a54aa1e5b463ebb19110c4aa2</anchor>
      <arglist>(int src, int dst)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_src</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga858f465a2dcbcadb6169a7926e38a4d1</anchor>
      <arglist>(int busid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_bus_get_dst</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga8ded3aa2a94ec7a5cd46e2e70a918c65</anchor>
      <arglist>(int busid)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_timing_timespec_delay_us</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>gacf0b5499290848f07c97a32c6c2412db</anchor>
      <arglist>(struct timespec *start, struct timespec *end)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_timing_timespec_to_us</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga87639260ff5c89f466e83fcc093e77fe</anchor>
      <arglist>(struct timespec *ts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_profiling_bus_helper_display_summary</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga6511f50290edcabbdf605a1b74746645</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_profiling_worker_helper_display_summary</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga4e4f9fb593d29219a1109062ba9d72ef</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_data_display_memory_stats</name>
      <anchorfile>group__API__Profiling.html</anchorfile>
      <anchor>ga6b93e19cf6ddd63d7eaee6749a7bdcf1</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Running_Drivers</name>
    <title>Running Drivers</title>
    <filename>group__API__Running__Drivers.html</filename>
    <member kind="function">
      <type>int</type>
      <name>starpu_driver_run</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>ga68b320765b17e5e4a256f156c9932e69</anchor>
      <arglist>(struct starpu_driver *d)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_driver_init</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>gab274599ec82fe38cb41267b8a1384995</anchor>
      <arglist>(struct starpu_driver *d)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_driver_run_once</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>gad6d01b57f2f84df90f554ea205dcf90d</anchor>
      <arglist>(struct starpu_driver *d)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_driver_deinit</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>gac8f8848b64980676190d77d265fda841</anchor>
      <arglist>(struct starpu_driver *d)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_drivers_request_termination</name>
      <anchorfile>group__API__Running__Drivers.html</anchorfile>
      <anchor>ga497d48a24107dfebf6ef3ad40d4a6df8</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_SCC_Extensions</name>
    <title>SCC Extensions</title>
    <filename>group__API__SCC__Extensions.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_USE_SCC</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>gaed05094ef8e7bd93c99315705cc4b87a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAXSCCDEVS</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>ga651a759ee07740d52e96d15ca3c68ae8</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void *</type>
      <name>starpu_scc_func_symbol_t</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>ga70c024e24bd926107296a279fde13538</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_scc_register_kernel</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>ga32bb32785d9fd5f0ab47ed53c7db643f</anchor>
      <arglist>(starpu_scc_func_symbol_t *symbol, const char *func_name)</arglist>
    </member>
    <member kind="function">
      <type>starpu_scc_kernel_t</type>
      <name>starpu_scc_get_kernel</name>
      <anchorfile>group__API__SCC__Extensions.html</anchorfile>
      <anchor>gaf0d2b4ba0e50de56c994f9d351efdca3</anchor>
      <arglist>(starpu_scc_func_symbol_t symbol)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Scheduling_Contexts</name>
    <title>Scheduling Contexts</title>
    <filename>group__API__Scheduling__Contexts.html</filename>
    <class kind="struct">starpu_sched_ctx_performance_counters</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX_POLICY_NAME</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga66ed9279fc4eb746a0596c2f0be7daa2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX_POLICY_STRUCT</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga9cdf99b6998b774eb53a8cc845f4e597</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX_POLICY_MIN_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga80f88b1017cfde58c18381a52ba0a122</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_SCHED_CTX_POLICY_MAX_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga80c723b1a18a2104a28320430d8e8489</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_create</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga517b7cf4bfac60f2faf484584f19d9d3</anchor>
      <arglist>(int *workerids_ctx, int nworkers_ctx, const char *sched_ctx_name,...)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_create_inside_interval</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga1fbdfe8bacedde944e4c39e4bb168651</anchor>
      <arglist>(const char *policy_name, const char *sched_ctx_name, int min_ncpus, int max_ncpus, int min_ngpus, int max_ngpus, unsigned allow_overlap)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_register_close_callback</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaadbcaf9118792338aafe480da6fc6d94</anchor>
      <arglist>(unsigned sched_ctx_id, void(*close_callback)(unsigned sched_ctx_id, void *args), void *args)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_add_workers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga4938f463f59aaea3f0b766c5da770fb9</anchor>
      <arglist>(int *workerids_ctx, int nworkers_ctx, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_remove_workers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga5fced8ae787486d906b057c20dfe5926</anchor>
      <arglist>(int *workerids_ctx, int nworkers_ctx, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_display_workers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga02858b688df9fed9e593040de0d28357</anchor>
      <arglist>(unsigned sched_ctx_id, FILE *f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_delete</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga101c3a13d4adc8d932c46324ae71b93a</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_inheritor</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gad6b7f96254526bb145e0df5ed0354a13</anchor>
      <arglist>(unsigned sched_ctx_id, unsigned inheritor)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_context</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga4e1aaf03ba327f47e5027c6fb7e5ed00</anchor>
      <arglist>(unsigned *sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_get_context</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae6b235a9555a74e5cb4c89ca5117b419</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_stop_task_submission</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gafe295aec3c152fe1fc0dd74e17f82961</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_finished_submit</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga11e94f2968626d9b46ccf021057ddd90</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_get_workers_list</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga182b6196b075139f611cfd34b42b86e8</anchor>
      <arglist>(unsigned sched_ctx_id, int **workerids)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_get_nworkers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga18af575377ee066528081b5aec754ea2</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_get_nshared_workers</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae00f5710446d14161beefdad2e48833d</anchor>
      <arglist>(unsigned sched_ctx_id, unsigned sched_ctx_id2)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_contains_worker</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga0fe5c1b14fc254c8b2a737402870a149</anchor>
      <arglist>(int workerid, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_worker_get_id</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaa06e10ab1edfe31d7da2f3359630e6f4</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_overlapping_ctxs_on_worker</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga66d2d4d08df845a40b7d19f12bf791c0</anchor>
      <arglist>(int workerid)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MIN_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga7988c6119e176957987baac65f4fc62c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAX_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaf213c18d752d78286cff68a379ee7611</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_DEFAULT_PRIO</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gabd6c53517107a2b467bdbc8af21cee2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_set_min_priority</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga6b8a02442fc4b18ae7f1c67a1a4add35</anchor>
      <arglist>(unsigned sched_ctx_id, int min_prio)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_set_max_priority</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga1945cfb563891ad94861e5a5f6b01495</anchor>
      <arglist>(unsigned sched_ctx_id, int max_prio)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_get_min_priority</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga6ae38faa632f496d81f0cf3b0cec5c77</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_get_max_priority</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga1cfc7b78954e33f2738547cbf05e820e</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_min_priority_is_set</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gadaa6d7d42fb01d94aece1c82efb020a3</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_max_priority_is_set</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga56c083b8731592e1c51f5e81669a8eb5</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_worker_collection *</type>
      <name>starpu_sched_ctx_create_worker_collection</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae3c8990276aaca0a3b44e3177ab3e0fb</anchor>
      <arglist>(unsigned sched_ctx_id, enum starpu_worker_collection_type type) STARPU_ATTRIBUTE_MALLOC</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_delete_worker_collection</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga5de2e1e6295cf1fa5723d1164d0a76e6</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_worker_collection *</type>
      <name>starpu_sched_ctx_get_worker_collection</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga6e559a6889a89ec0b9a3b995a41f5f8f</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_perf_counters</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaee24cd7da1cde089e038b93f52d1bf1d</anchor>
      <arglist>(unsigned sched_ctx_id, void *perf_counters)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_call_pushed_task_cb</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga62867177a48ccd40ac944602630cd8f0</anchor>
      <arglist>(int workerid, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_notify_hypervisor_exists</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gae7de365db4b774cdea340f7548e93c9d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_sched_ctx_check_if_hypervisor_exists</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga95283c927a7787e2859cb4a42c93bf19</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_set_policy_data</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga20077926e6cd8264bcbb831a394c381a</anchor>
      <arglist>(unsigned sched_ctx_id, void *policy_data)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_sched_ctx_get_policy_data</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaacd0aff652dc75ac37aa51c9c826e043</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>starpu_sched_ctx_exec_parallel_code</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>gaf9a95bcf18710622750367c0ba6acbb4</anchor>
      <arglist>(void *(*func)(void *), void *param, unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_ctx_get_nready_tasks</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga5162ab4e7c0775a4357f951122784cb3</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_sched_ctx_get_nready_flops</name>
      <anchorfile>group__API__Scheduling__Contexts.html</anchorfile>
      <anchor>ga9cd4e3a034cb9ff6eb8281e449d395c9</anchor>
      <arglist>(unsigned sched_ctx_id)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Scheduling_Policy</name>
    <title>Scheduling Policy</title>
    <filename>group__API__Scheduling__Policy.html</filename>
    <class kind="struct">starpu_sched_policy</class>
    <member kind="function">
      <type>struct starpu_sched_policy **</type>
      <name>starpu_sched_get_predefined_policies</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gaca9fb71cec7aa99dfd7a7a6dcafefb88</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_worker_get_sched_condition</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gadf8e42d41b76117293db5651b0912c78</anchor>
      <arglist>(int workerid, starpu_pthread_mutex_t **sched_mutex, starpu_pthread_cond_t **sched_cond)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_set_min_priority</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gace525e755092440f63acbe5cc0b4d066</anchor>
      <arglist>(int min_prio)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_set_max_priority</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gab8e4a2cd9f4fbad7b20b1086494d842d</anchor>
      <arglist>(int max_prio)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_get_min_priority</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga6d8a3d42b5f2fee81785b3cf650fa6d2</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_sched_get_max_priority</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga6c2233ec86c827981117306f63da7679</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_push_local_task</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gae0b8a9bb53822f3d92cb2d19c2520e5e</anchor>
      <arglist>(int workerid, struct starpu_task *task, int back)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_push_task_end</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga24154b37920144de18bca59090429d6d</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_can_execute_task</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga2c45db0c01e86497082c907b9cb0f4cc</anchor>
      <arglist>(unsigned workerid, struct starpu_task *task, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_can_execute_task_impl</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga40338a935c8428167c73e1d5b681b4e5</anchor>
      <arglist>(unsigned workerid, struct starpu_task *task, unsigned *impl_mask)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_can_execute_task_first_impl</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga706830b50d163715c28d2fb19abd13bd</anchor>
      <arglist>(unsigned workerid, struct starpu_task *task, unsigned *nimpl)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_task_footprint</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga21385d19bc75a7581e774bd455c70790</anchor>
      <arglist>(struct starpu_perfmodel *model, struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>uint32_t</type>
      <name>starpu_task_data_footprint</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>gaa859b0ee59cfa2bde4d241f854c3bbea</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_expected_length</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga2dc83adec9e479e967a1d1c2ae40f916</anchor>
      <arglist>(struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_worker_get_relative_speedup</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga87e2aacf5892951d5333a3421e90f278</anchor>
      <arglist>(struct starpu_perfmodel_arch *perf_arch)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_expected_data_transfer_time</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga4faf5df3280a5331bdddd3f878a3024d</anchor>
      <arglist>(unsigned memory_node, struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_data_expected_transfer_time</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga1e159d56ffa2d7705bdfd1ceb93e0d64</anchor>
      <arglist>(starpu_data_handle_t handle, unsigned memory_node, enum starpu_data_access_mode mode)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_expected_energy</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga3e9efb4ca2cbc1c74757e0a9cb0f26c6</anchor>
      <arglist>(struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_expected_conversion_time</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga53b042c111b450ebaa99e26388394ff4</anchor>
      <arglist>(struct starpu_task *task, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_get_prefetch_flag</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga94edfd4a02666d8c1b8c506423586426</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_prefetch_task_input_on_node</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga3bf8df94c9ea3f1370b53fff54e72888</anchor>
      <arglist>(struct starpu_task *task, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_idle_prefetch_task_input_on_node</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga20525c5d674b32a9f64fbc9e23c5f55f</anchor>
      <arglist>(struct starpu_task *task, unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sched_ctx_worker_shares_tasks_lists</name>
      <anchorfile>group__API__Scheduling__Policy.html</anchorfile>
      <anchor>ga315482308105aa57ecc59e88f71823aa</anchor>
      <arglist>(int workerid, int sched_ctx_id)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Standard_Memory_Library</name>
    <title>Standard Memory Library</title>
    <filename>group__API__Standard__Memory__Library.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_malloc_pinned_if_possible</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gad1e07e6fc879ed3d05d2a6c5c2a04b56</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_data_free_pinned_if_possible</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga4e9089680daf0b2d7f26e4657ffe9f48</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MALLOC_PINNED</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga2abe959e39acfb75c7c0652706dfe84c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MALLOC_COUNT</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gaace5eebbb6662fb1be7c79a65464e1cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MALLOC_NORECLAIM</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gaaa8051f7b2b765dc707895572547c181</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MALLOC_SIMULATION_FOLDED</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga67c2ba763b260bea998b16881c788ae6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MEMORY_WAIT</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga8601b3e35b74e9cbbcefd7a9b9c6d0b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MEMORY_OVERFLOW</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gaffa4b2a7af68027551855bd6c9520914</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_malloc_flags</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gae3bd40d6e18d317fbb808962b9762c1a</anchor>
      <arglist>(void **A, size_t dim, int flags) STARPU_ATTRIBUTE_ALLOC_SIZE(2)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_malloc_set_align</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga52ec305b6afdd76130a4f6877e23849c</anchor>
      <arglist>(size_t align)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_malloc</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga4afc214c8964f0fc8d0f25d5a85ea6c8</anchor>
      <arglist>(void **A, size_t dim) STARPU_ATTRIBUTE_ALLOC_SIZE(2)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_free</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga889e8ea2a1becb4eff634e264973b261</anchor>
      <arglist>(void *A)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_free_flags</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gad60ddd8514e8ed664ecf1f21ad84e6f5</anchor>
      <arglist>(void *A, size_t dim, int flags)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_memory_pin</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga5a6ea6d03d7b0f4a97a8046b30ecd0bb</anchor>
      <arglist>(void *addr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_memory_unpin</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga3506e4b5ab7a605e25968883ded27d64</anchor>
      <arglist>(void *addr, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>starpu_ssize_t</type>
      <name>starpu_memory_get_total</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>gaccfcb477defdecd13c2d2471e9810c6b</anchor>
      <arglist>(unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>starpu_ssize_t</type>
      <name>starpu_memory_get_available</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga897b57b3349c1d495eeb144d64ff12c3</anchor>
      <arglist>(unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_memory_allocate</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga84894d9a6726bca707043f484bd1a8f5</anchor>
      <arglist>(unsigned node, size_t size, int flags)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_memory_deallocate</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga9e545c4701e6db29ab38bd0f7e4a76b5</anchor>
      <arglist>(unsigned node, size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_memory_wait_available</name>
      <anchorfile>group__API__Standard__Memory__Library.html</anchorfile>
      <anchor>ga71ec84f082fd96f53612e684eda0e178</anchor>
      <arglist>(unsigned node, size_t size)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Task_Bundles</name>
    <title>Task Bundles</title>
    <filename>group__API__Task__Bundles.html</filename>
    <member kind="typedef">
      <type>struct _starpu_task_bundle *</type>
      <name>starpu_task_bundle_t</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>ga1be1e845a54cec05cbc371d1f0c9a2e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_bundle_create</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>gadd37abde6806819967151a64146b19d6</anchor>
      <arglist>(starpu_task_bundle_t *bundle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_bundle_insert</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>gae8be4224d241ac2d30d9d573359e3280</anchor>
      <arglist>(starpu_task_bundle_t bundle, struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_task_bundle_remove</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>gad904bbf27569e421454a2c8b25458eb3</anchor>
      <arglist>(starpu_task_bundle_t bundle, struct starpu_task *task)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_task_bundle_close</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>ga7fcf9fedcf2a3f79784425431980a480</anchor>
      <arglist>(starpu_task_bundle_t bundle)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_bundle_expected_length</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>ga278de79c7cbeaa68e5239b415cc0d29c</anchor>
      <arglist>(starpu_task_bundle_t bundle, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_bundle_expected_energy</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>ga4eac9575958c784ce48d2787b908ab3b</anchor>
      <arglist>(starpu_task_bundle_t bundle, struct starpu_perfmodel_arch *arch, unsigned nimpl)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>starpu_task_bundle_expected_data_transfer_time</name>
      <anchorfile>group__API__Task__Bundles.html</anchorfile>
      <anchor>gaf589986f5e69b7452ba90857d1931594</anchor>
      <arglist>(starpu_task_bundle_t bundle, unsigned memory_node)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Task_Lists</name>
    <title>Task Lists</title>
    <filename>group__API__Task__Lists.html</filename>
    <class kind="struct">starpu_task_list</class>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE void</type>
      <name>starpu_task_list_init</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>ga5618612b0ce2f914ab9032474ddf072f</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE void</type>
      <name>starpu_task_list_push_front</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gabf5bb8daf00f9902ea501009720af19d</anchor>
      <arglist>(struct starpu_task_list *list, struct starpu_task *task)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE void</type>
      <name>starpu_task_list_push_back</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gae29a8aae4e82286c80656a7afdc7a106</anchor>
      <arglist>(struct starpu_task_list *list, struct starpu_task *task)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_front</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gad38d569521bb710afc2299a9150f083f</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_back</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>ga871dd2f2bb4429d4edc9777abf3490b3</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE int</type>
      <name>starpu_task_list_empty</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>ga477ef573c883af23f946ec291801458a</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE void</type>
      <name>starpu_task_list_erase</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gaedf8ff832aea57abbb8ab94304a85c13</anchor>
      <arglist>(struct starpu_task_list *list, struct starpu_task *task)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_pop_front</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gad0e3e0684ea77c786b7c5b83894b5e5d</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_pop_back</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gad88622cd18759da0a9b30ca75c11e22b</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_begin</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gac3ea85752262f5281b745b2792af9266</anchor>
      <arglist>(struct starpu_task_list *list)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE struct starpu_task *</type>
      <name>starpu_task_list_next</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>ga4630f5b59e347b36b27f025b9c8801a1</anchor>
      <arglist>(struct starpu_task *task)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static STARPU_INLINE int</type>
      <name>starpu_task_list_ismember</name>
      <anchorfile>group__API__Task__Lists.html</anchorfile>
      <anchor>gad9543a989bd53e77ef1731f4431b4da0</anchor>
      <arglist>(struct starpu_task_list *list, struct starpu_task *look)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Threads</name>
    <title>Threads</title>
    <filename>group__API__Threads.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_CREATE_ON</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaea6e537f9a02061690e845082158b3b4</anchor>
      <arglist>(name, thread, attr, routine, arg, where)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_CREATE</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga43ef5044dfe6169c422d3ded42aa69e4</anchor>
      <arglist>(thread, attr, routine, arg)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_INIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab1d60ac7acd857f056de5d9cdd3b8b93</anchor>
      <arglist>(mutex, attr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_DESTROY</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab99612af964c7e92d99e3fcc9513c909</anchor>
      <arglist>(mutex)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_LOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gacabc76c4b40c66603b5d417d7039439a</anchor>
      <arglist>(mutex)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_UNLOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga5e53025adc42966a97d616304bd654bd</anchor>
      <arglist>(mutex)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_KEY_CREATE</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab1f13e8c7f4e9c7a93e35ffa5f3c8925</anchor>
      <arglist>(key, destr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_KEY_DELETE</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gac1f65845c0c1c76e65c63fc5174ff5cb</anchor>
      <arglist>(key)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_SETSPECIFIC</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga0f4c9033a1cbf7b7ef88c5124a65149f</anchor>
      <arglist>(key, ptr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_GETSPECIFIC</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gae024574a5ff66361d2b00f10924dbf3a</anchor>
      <arglist>(key)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_INIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab24c9ad484185c1b0fc2518a27824041</anchor>
      <arglist>(rwlock, attr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_RDLOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaede3f824ce3ecf6079987a68da9eb621</anchor>
      <arglist>(rwlock)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_WRLOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga2331ffac0d863e2d34463003fa961550</anchor>
      <arglist>(rwlock)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_UNLOCK</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga7cf8cbee2a33b4714cf37724f657bd4a</anchor>
      <arglist>(rwlock)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_RWLOCK_DESTROY</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga348ba00ab90144b68f094631fb3657de</anchor>
      <arglist>(rwlock)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_INIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaba0e781d7d8a22c313200edaa9ec59f9</anchor>
      <arglist>(cond, attr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_DESTROY</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga0f576d7a841d598e1f8c78390a539ab6</anchor>
      <arglist>(cond)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_SIGNAL</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga42b7eaa98fe66b90eb67f3fdb51fd14d</anchor>
      <arglist>(cond)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_BROADCAST</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga81126f750a75beef8a9679dd261e1bd2</anchor>
      <arglist>(cond)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_WAIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gad37f49e0fb051b7a3a66208a4bc67397</anchor>
      <arglist>(cond, mutex)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_BARRIER_INIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga707b1afc766917d4ddbbe19e5569359b</anchor>
      <arglist>(barrier, attr, count)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_BARRIER_DESTROY</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga2bd1ebeca9a2d4eefc3d0a399ce3ae3c</anchor>
      <arglist>(barrier)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_BARRIER_WAIT</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaa0871b23fee2d5cfa1e4930108b53708</anchor>
      <arglist>(barrier)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_MUTEX_INITIALIZER</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gac93d8e7fcb44d354421a77b1c0bc9569</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_PTHREAD_COND_INITIALIZER</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gac83bf1cce7a509b76cae9e191fb39bb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_barrier_init</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab3f630ba782864f80fb7d8fceb7db399</anchor>
      <arglist>(starpu_pthread_barrier_t *barrier, const starpu_pthread_barrierattr_t *attr, unsigned count)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_barrier_destroy</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga0573dd7985127d363fd5dbcb8d67796b</anchor>
      <arglist>(starpu_pthread_barrier_t *barrier)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_barrier_wait</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gab522d9c9ae9747db8123344f49421725</anchor>
      <arglist>(starpu_pthread_barrier_t *barrier)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_init</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga3e6f70cbaed94500cd4b8229db361a7a</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock, int pshared)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_destroy</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga5ec606cedc543ee35f3c7dc8a8e2505b</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_lock</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga9ef49f133fa6f1374769aab8a4ff9474</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_trylock</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gac5bf366536c4f722dec41da48618b937</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_pthread_spin_unlock</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>gaa8d07e258b61aa10fc8ce2111f4a37a9</anchor>
      <arglist>(starpu_pthread_spinlock_t *lock)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_sleep</name>
      <anchorfile>group__API__Threads.html</anchorfile>
      <anchor>ga4c38ddeaf0bba0226f8a5c2c83cf7ab7</anchor>
      <arglist>(float nb_sec)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Toolbox</name>
    <title>Toolbox</title>
    <filename>group__API__Toolbox.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_GNUC_PREREQ</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga4fc771a29d7dc0e826da42777edbcf15</anchor>
      <arglist>(maj, min)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_UNLIKELY</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga23742c6fca8b5386e1f3e3f7bbf3a692</anchor>
      <arglist>(expr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_LIKELY</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga4456c2db2cc8df7c98d62ecc21b2bd1d</anchor>
      <arglist>(expr)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_UNUSED</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga9e7c20a6cdce868976a0046206b104e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_INTERNAL</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga903e600059ba1735d5d6bd688dc0cc7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_MALLOC</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga214c60ee1b6cbc27ab08ba6f339ef8e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_WARN_UNUSED_RESULT</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga71e28fc2a0a5267bc7edc0909d9d5117</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_PURE</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga057b12b615ce9e8a1fe7b5e10d9a5c2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ATTRIBUTE_ALIGNED</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gae8c513ecd1a5bd87a7bdd71260c49ffc</anchor>
      <arglist>(size)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_WARN_UNUSED_RESULT</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga718857e58c604e0fab6a45a217961277</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_POISON_PTR</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga4c4f93f8ed79333f5801058af4267d84</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MIN</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gad614bcaae1a110a4ddc0ddc3636dac5d</anchor>
      <arglist>(a, b)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAX</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gaa156afa885bb447bc0ced3ea66bcf7e6</anchor>
      <arglist>(a, b)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ASSERT</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga05ac0dda104331f57d85823a4f9318ce</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ASSERT_MSG</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gabb6941113e19ee4045a014943cd62aae</anchor>
      <arglist>(x, msg,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ABORT</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga3a4d1fb56668323d4f44c18f6a6dfb3f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_ABORT_MSG</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga4268950ebf63c0f1dabcfd0cd8808067</anchor>
      <arglist>(msg,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CHECK_RETURN_VALUE</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gaa3503ea5efd00806a0f9234f033e6ea1</anchor>
      <arglist>(err, message,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_CHECK_RETURN_VALUE_IS</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga1b41d799a04a1d6483e982ffb2f74432</anchor>
      <arglist>(err, value, message,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_RMB</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gaa5363993d4f519517316ccf9dc47a7bb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_WMB</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>ga07b2628bd6680fe49d92c7f816d7ce53</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static __starpu_inline int</type>
      <name>starpu_get_env_number</name>
      <anchorfile>group__API__Toolbox.html</anchorfile>
      <anchor>gac9eef7c5611291831a56af2134d9945c</anchor>
      <arglist>(const char *str)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_StarPUTop_Interface</name>
    <title>StarPU-Top Interface</title>
    <filename>group__API__StarPUTop__Interface.html</filename>
    <class kind="struct">starpu_top_data</class>
    <class kind="struct">starpu_top_param</class>
    <member kind="enumeration">
      <type></type>
      <name>starpu_top_data_type</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga7b7dfbb9c273503e6e7f5790b39e2d27</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_DATA_BOOLEAN</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga7b7dfbb9c273503e6e7f5790b39e2d27a8ae4c093533aae3d4ece7910d16fa7cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_DATA_INTEGER</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga7b7dfbb9c273503e6e7f5790b39e2d27a70a25dacfac1c08f2e73263f9b10a21e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_DATA_FLOAT</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga7b7dfbb9c273503e6e7f5790b39e2d27adfb99eeccad78dbb404dc39f058e22be</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_top_param_type</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga1979aeea98f354bd500f4902ef2299d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_BOOLEAN</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9a6e452fdfa703a7292d7526844309dd34</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_INTEGER</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9ac1737a4533dd745432df144d19f7874f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_FLOAT</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9a2d732b79d2e553a6d248a348b07e1422</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_ENUM</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9aaea8e0c88de44de91feaf091f2f8f431</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_top_message_type</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga568af4507d96390316b93c92fee45f52</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_GO</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a2980d96d782513d6a2ba7e320e4b67f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_SET</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a3c1b1dcb8ad446753dbde70039aac0e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_CONTINUE</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a9dad9fc65f92a2ece0935fd17ca0b1a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_ENABLE</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52ad2658f645e029c0013185fd47bb99cbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_DISABLE</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52ae0d6282a934eccc5bfee15a5657ce69a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_DEBUG</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a952a3745b30ea9defd6b4e397fe7bcc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_UNKNOW</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a49307bcbda7b5b9e34e0fd76fea2a372</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_DATA_BOOLEAN</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga7b7dfbb9c273503e6e7f5790b39e2d27a8ae4c093533aae3d4ece7910d16fa7cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_DATA_INTEGER</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga7b7dfbb9c273503e6e7f5790b39e2d27a70a25dacfac1c08f2e73263f9b10a21e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_DATA_FLOAT</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga7b7dfbb9c273503e6e7f5790b39e2d27adfb99eeccad78dbb404dc39f058e22be</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_BOOLEAN</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9a6e452fdfa703a7292d7526844309dd34</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_INTEGER</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9ac1737a4533dd745432df144d19f7874f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_FLOAT</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9a2d732b79d2e553a6d248a348b07e1422</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_TOP_PARAM_ENUM</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga1979aeea98f354bd500f4902ef2299d9aaea8e0c88de44de91feaf091f2f8f431</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_GO</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a2980d96d782513d6a2ba7e320e4b67f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_SET</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a3c1b1dcb8ad446753dbde70039aac0e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_CONTINUE</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a9dad9fc65f92a2ece0935fd17ca0b1a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_ENABLE</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52ad2658f645e029c0013185fd47bb99cbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_DISABLE</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52ae0d6282a934eccc5bfee15a5657ce69a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_DEBUG</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a952a3745b30ea9defd6b4e397fe7bcc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TOP_TYPE_UNKNOW</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gga568af4507d96390316b93c92fee45f52a49307bcbda7b5b9e34e0fd76fea2a372</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_data *</type>
      <name>starpu_top_add_data_boolean</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga016dd52f4fb957306fbc0d8dfdf50c71</anchor>
      <arglist>(const char *data_name, int active)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_data *</type>
      <name>starpu_top_add_data_integer</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gad2d523db513a2e74f8cc417926fe4886</anchor>
      <arglist>(const char *data_name, int minimum_value, int maximum_value, int active)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_data *</type>
      <name>starpu_top_add_data_float</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gae79ecd90078b87ebf97cbb127effde89</anchor>
      <arglist>(const char *data_name, double minimum_value, double maximum_value, int active)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_boolean</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gadae398d33233f7d7f8860562885c564d</anchor>
      <arglist>(const char *param_name, int *parameter_field, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_float</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga91db3cdf3b86e37066e625e504b33fb0</anchor>
      <arglist>(const char *param_name, double *parameter_field, double minimum_value, double maximum_value, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_integer</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga97bf62e4f04016e273f364ab99b3d146</anchor>
      <arglist>(const char *param_name, int *parameter_field, int minimum_value, int maximum_value, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_top_param *</type>
      <name>starpu_top_register_parameter_enum</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga3406e743f8db81f60c019b1be2bbb46f</anchor>
      <arglist>(const char *param_name, int *parameter_field, char **values, int nb_values, void(*callback)(struct starpu_top_param *))</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_init_and_wait</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga84638ef94fd71d3192c194053e91b4d8</anchor>
      <arglist>(const char *server_name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_parameter</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga4afdee5376361117e11e8a9ee4a47bad</anchor>
      <arglist>(const struct starpu_top_param *param)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_data_boolean</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga87b6dcc03287a2a19788c984443160c2</anchor>
      <arglist>(const struct starpu_top_data *data, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_data_integer</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga68fb3d6362e00a8d32668389e98dca07</anchor>
      <arglist>(const struct starpu_top_data *data, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_update_data_float</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga373e3fecfd9fccc0ac3e3857f2e5a0e8</anchor>
      <arglist>(const struct starpu_top_data *data, double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_task_prevision</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>gac361229a1e495768bf5f1653940528de</anchor>
      <arglist>(struct starpu_task *task, int devid, unsigned long long start, unsigned long long end)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_debug_log</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga07e4329145ef683b44df8f36e5331158</anchor>
      <arglist>(const char *message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_top_debug_lock</name>
      <anchorfile>group__API__StarPUTop__Interface.html</anchorfile>
      <anchor>ga414faad453d183d1c026eb04fc59b07f</anchor>
      <arglist>(const char *message)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Tree</name>
    <title>Tree</title>
    <filename>group__API__Tree.html</filename>
    <class kind="struct">starpu_tree</class>
    <member kind="function">
      <type>void</type>
      <name>starpu_tree_insert</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>ga318b363110b391920b4cb223fe8424bd</anchor>
      <arglist>(struct starpu_tree *tree, int id, int level, int is_pu, int arity, struct starpu_tree *father)</arglist>
    </member>
    <member kind="function">
      <type>struct starpu_tree *</type>
      <name>starpu_tree_get</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>ga57a6a8f70ee46ca8dfe503b52bd31e74</anchor>
      <arglist>(struct starpu_tree *tree, int id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_tree_free</name>
      <anchorfile>group__API__Tree.html</anchorfile>
      <anchor>ga6cc51deaa659ac90881ed1126432c4fb</anchor>
      <arglist>(struct starpu_tree *tree)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Versioning</name>
    <title>Versioning</title>
    <filename>group__API__Versioning.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MAJOR_VERSION</name>
      <anchorfile>group__API__Versioning.html</anchorfile>
      <anchor>ga61698352a3ca7e28b4bcdd8bcad8d8cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_MINOR_VERSION</name>
      <anchorfile>group__API__Versioning.html</anchorfile>
      <anchor>ga847f129c7b859f46c8bd69c9c8cbcde5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_RELEASE_VERSION</name>
      <anchorfile>group__API__Versioning.html</anchorfile>
      <anchor>ga43895a109dc6f27de709208f460b321d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_get_version</name>
      <anchorfile>group__API__Versioning.html</anchorfile>
      <anchor>gaa9151820d2ef6a8231b510b4b3c19ca4</anchor>
      <arglist>(int *major, int *minor, int *release)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_Workers_Properties</name>
    <title>Workers’ Properties</title>
    <filename>group__API__Workers__Properties.html</filename>
    <class kind="struct">starpu_worker_collection</class>
    <class kind="struct">starpu_sched_ctx_iterator</class>
    <member kind="define">
      <type>#define</type>
      <name>STARPU_NMAXWORKERS</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gad7c443d1341e4976d63fb5d77e74bf09</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>starpu_worker_get_id_check</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga177bffe36c7df3f91aaebe2dd03b8cda</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_node_kind</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga6f3fbb6e918d0135ccb68473a8ee5ca1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_UNUSED</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a92a0688575c41519f031fefd35f51c9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CPU_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a9578288c35b06ee94ab4ef6a41d16674</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CUDA_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a619d788452761975aa62ea517c86104c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_OPENCL_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a6e0c8b16179c2829393a373831c3b802</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MIC_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a61616139496d3d57b4cd181459709a11</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCC_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a7169c196e1585c24e9f2ab73d7d42e3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCC_SHM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1ad14cce897c4dc582b773d35ab93cec7c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_worker_archtype</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga173d616aefe98c33a47a847fd2fca37d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CPU_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37da5b5aeeaa21f925e63f5fed338baf0588</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CUDA_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37dac95bbacadbe0599c06a48059d2821611</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_OPENCL_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37da113d54380f4e41e4da1f94b1a779634f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MIC_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37da58985be48577b6cdb2baf5605e26698d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCC_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37dacb52719c80c5191875563436fca71096</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_ANY_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37dac4e189acc1d1b0971c8d950034048501</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>starpu_worker_collection_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga80b06886ee8a4c0e99b09ab638113af3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_WORKER_LIST</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga80b06886ee8a4c0e99b09ab638113af3a7fb30f298c82f9375f49a42664d473bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_UNUSED</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a92a0688575c41519f031fefd35f51c9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CPU_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a9578288c35b06ee94ab4ef6a41d16674</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CUDA_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a619d788452761975aa62ea517c86104c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_OPENCL_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a6e0c8b16179c2829393a373831c3b802</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MIC_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a61616139496d3d57b4cd181459709a11</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCC_RAM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1a7169c196e1585c24e9f2ab73d7d42e3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCC_SHM</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga6f3fbb6e918d0135ccb68473a8ee5ca1ad14cce897c4dc582b773d35ab93cec7c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_ANY_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37dac4e189acc1d1b0971c8d950034048501</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CPU_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37da5b5aeeaa21f925e63f5fed338baf0588</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_CUDA_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37dac95bbacadbe0599c06a48059d2821611</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_OPENCL_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37da113d54380f4e41e4da1f94b1a779634f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_MIC_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37da58985be48577b6cdb2baf5605e26698d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_SCC_WORKER</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga173d616aefe98c33a47a847fd2fca37dacb52719c80c5191875563436fca71096</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STARPU_WORKER_LIST</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gga80b06886ee8a4c0e99b09ab638113af3a7fb30f298c82f9375f49a42664d473bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga6312aa4b12ce7e06fe391bcafda6796a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_count_by_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga60b4a796a3dba40e5159158750805bf2</anchor>
      <arglist>(enum starpu_worker_archtype type)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_cpu_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga8c4e370ff327c38a3ec79c9249ab233a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_cuda_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gacb9d262c066c047ae55f691adc876db3</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_mic_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga9b6da05473fbc3dbebc4da805d10d404</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_mic_device_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga8be255f32d6f0a112b5f84bf572304cf</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_scc_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga9b0b790e402b113cc56af695167f96c9</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_opencl_worker_get_count</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga0dd6ff2d77c6a520a6cc4c051eb40bfa</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_id</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gac6d06f4e22b63bf50bc8e836cf16f81f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_ids_by_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gaddb73ee5de667ba2e3353eb977d22cde</anchor>
      <arglist>(enum starpu_worker_archtype type, int *workerids, int maxsize)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_by_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gab0ffe5622c5980444226f1ed8d6fee71</anchor>
      <arglist>(enum starpu_worker_archtype type, int num)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_by_devid</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gae6166f338640d033b740f5c8008b851b</anchor>
      <arglist>(enum starpu_worker_archtype type, int devid)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>starpu_worker_get_devid</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga0c9b9b7f5195cb8cbc5ee1223ec0f264</anchor>
      <arglist>(int id)</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_worker_archtype</type>
      <name>starpu_worker_get_type</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga4998e4e1dfa0d121059c60f790496a03</anchor>
      <arglist>(int id)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>starpu_worker_get_name</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gae35a841f996b8758f3d0ce2ac2d066a5</anchor>
      <arglist>(int id, char *dst, size_t maxlen)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>starpu_worker_get_memory_node</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga7de6654141ce89ea83c3aba60486396e</anchor>
      <arglist>(unsigned workerid)</arglist>
    </member>
    <member kind="function">
      <type>enum starpu_node_kind</type>
      <name>starpu_node_get_kind</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>ga9b2541fc1a20c4c486c499af01a3283c</anchor>
      <arglist>(unsigned node)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>starpu_worker_get_type_as_string</name>
      <anchorfile>group__API__Workers__Properties.html</anchorfile>
      <anchor>gab62d05f84a4ca631cc148e115d7dd995</anchor>
      <arglist>(enum starpu_worker_archtype type)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_SC_Hypervisor</name>
    <title>Scheduling Context Hypervisor - Building a new resizing policy</title>
    <filename>group__API__SC__Hypervisor.html</filename>
    <class kind="struct">sc_hypervisor_policy</class>
    <class kind="struct">sc_hypervisor_policy_config</class>
    <class kind="struct">sc_hypervisor_wrapper</class>
    <class kind="struct">sc_hypervisor_resize_ack</class>
    <class kind="struct">sc_hypervisor_policy_task_pool</class>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_post_resize_request</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga5f2b33a41a697759ee2a222e623f723c</anchor>
      <arglist>(unsigned sched_ctx, int task_tag)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>sc_hypervisor_get_size_req</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga1366fc9bb28170e9012dcad441696e52</anchor>
      <arglist>(unsigned **sched_ctxs, int *nsched_ctxs, int **workers, int *nworkers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_save_size_req</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga960ae31e317c65bfa9cc6a4e539a0c84</anchor>
      <arglist>(unsigned *sched_ctxs, int nsched_ctxs, int *workers, int nworkers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_free_size_req</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga5f34faa17f98f25bd874950ba1a15578</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>unsigned</type>
      <name>sc_hypervisor_can_resize</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>gadf5585c656c7db3ee8f7b630b86e25e3</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>struct sc_hypervisor_policy_config *</type>
      <name>sc_hypervisor_get_config</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>gaf3e884b4995e3dd608120e7fe0e48fae</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_set_config</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga838703a5d26ea31f7415155ecbf1e490</anchor>
      <arglist>(unsigned sched_ctx, void *config)</arglist>
    </member>
    <member kind="function">
      <type>unsigned *</type>
      <name>sc_hypervisor_get_sched_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga5450aa9edc6b19accef60539bbbd2e08</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>sc_hypervisor_get_nsched_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga7136151247abc7aab59ec8e0fe6e5224</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>struct sc_hypervisor_wrapper *</type>
      <name>sc_hypervisor_get_wrapper</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>gab02cf89fff4e322e47bc96fb3b60b1ac</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>sc_hypervisor_get_elapsed_flops_per_sched_ctx</name>
      <anchorfile>group__API__SC__Hypervisor.html</anchorfile>
      <anchor>ga32444b31927fbc49dc721ecbb66eae28</anchor>
      <arglist>(struct sc_hypervisor_wrapper *sc_w)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>API_SC_Hypervisor_usage</name>
    <title>Scheduling Context Hypervisor - Regular usage</title>
    <filename>group__API__SC__Hypervisor__usage.html</filename>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_MAX_IDLE</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga70c120158d02e550632de0ec5a207df4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_PRIORITY</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gad9267ab01a8e5db5b357d8749f9e8e1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_MIN_WORKERS</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga6085b1585a13c94d48f185687c6a117c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_MAX_WORKERS</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gaafac498199c6441e2d45c9b4eac8dfe4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_GRANULARITY</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gaf1f5a13894874d87363c32351f573c86</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_FIXED_WORKERS</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga94eea335704bbfa52bc025ad4d49eb14</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_MIN_TASKS</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga04164c5a91df51882e8bdd84fd1b1e3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_NEW_WORKERS_MAX_IDLE</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga4ab22667e3bd2e963f7451552f716540</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_TIME_TO_APPLY</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gac3e457f9b5741f587d241d62cc843f9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_ISPEED_W_SAMPLE</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gac3b55f616c270eb90c0103d6f062d384</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_ISPEED_CTX_SAMPLE</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga7ba4c0c3deca0dd4148ddd1f0b919792</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SC_HYPERVISOR_NULL</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gabf2475a79863da74418a12b2c5c37871</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void *</type>
      <name>sc_hypervisor_init</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga15d008933c3c89906f613b3a24fff3cc</anchor>
      <arglist>(struct sc_hypervisor_policy *policy)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_shutdown</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga07b07d1fe5d41055f451fc92c60361e1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_register_ctx</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga4c83e2984867267ea8c7da9fbaf23858</anchor>
      <arglist>(unsigned sched_ctx, double total_flops)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_unregister_ctx</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga0a20609c85fa479c3fd5892399099416</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_resize_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga67d1dbb2ab01c8b712a9cd270fba4155</anchor>
      <arglist>(unsigned *sched_ctxs, int nsched_ctxs, int *workers, int nworkers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_stop_resize</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gaa1e2878a06847489a4de2f33f62fa1af</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_start_resize</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gae3ad958a1d841587df13c814ce966477</anchor>
      <arglist>(unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>sc_hypervisor_get_policy</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga57103c5a62477b730b8d6d9a8f3514c7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_add_workers_to_sched_ctx</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga886533650f0dbbef618c7fdfa75f9f0e</anchor>
      <arglist>(int *workers_to_add, unsigned nworkers_to_add, unsigned sched_ctx)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_remove_workers_from_sched_ctx</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga37713b9ab6d282f2b705240e630f1a4c</anchor>
      <arglist>(int *workers_to_remove, unsigned nworkers_to_remove, unsigned sched_ctx, unsigned now)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_move_workers</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gaccfcc47b137572f79916bff5d12414d0</anchor>
      <arglist>(unsigned sender_sched_ctx, unsigned receiver_sched_ctx, int *workers_to_move, unsigned nworkers_to_move, unsigned now)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_size_ctxs</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gadd6bc55408b1f618edb44da49300bb5c</anchor>
      <arglist>(unsigned *sched_ctxs, int nsched_ctxs, int *workers, int nworkers)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_set_type_of_task</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga6398be50d801f0fc5ad73506e392d476</anchor>
      <arglist>(struct starpu_codelet *cl, unsigned sched_ctx, uint32_t footprint, size_t data_size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_update_diff_total_flops</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga4dfbeda855bb8963c5332fdd796c0dcf</anchor>
      <arglist>(unsigned sched_ctx, double diff_total_flops)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_update_diff_elapsed_flops</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>gac89f5d5eba3f79151ef6b2a0e895f144</anchor>
      <arglist>(unsigned sched_ctx, double diff_task_flops)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sc_hypervisor_ctl</name>
      <anchorfile>group__API__SC__Hypervisor__usage.html</anchorfile>
      <anchor>ga0a4b27500994ca2c56047a7da18c6216</anchor>
      <arglist>(unsigned sched_ctx,...)</arglist>
    </member>
  </compound>
  <compound kind="page">
    <name>BuildingAndInstallingStarPU</name>
    <title>Building and Installing StarPU</title>
    <filename>BuildingAndInstallingStarPU</filename>
    <docanchor file="BuildingAndInstallingStarPU" title="Installing a Binary Package">InstallingABinaryPackage</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Installing from Source">InstallingFromSource</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Optional Dependencies">OptionalDependencies</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Getting Sources">GettingSources</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Configuring StarPU">ConfiguringStarPU</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Building StarPU">BuildingStarPU</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Installing StarPU">InstallingStarPU</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Setting up Your Own Code">SettingUpYourOwnCode</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Setting Flags for Compiling, Linking and Running Applications">SettingFlagsForCompilingLinkingAndRunningApplications</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Running a Basic StarPU Application">RunningABasicStarPUApplication</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Running a Basic StarPU Application on Microsoft Visual C">RunningABasicStarPUApplicationOnMicrosoft</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Kernel Threads Started by StarPU">KernelThreadsStartedByStarPU</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Enabling OpenCL">EnablingOpenCL</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Benchmarking StarPU">BenchmarkingStarPU</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Task Size Overhead">TaskSizeOverhead</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Data Transfer Latency">DataTransferLatency</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Matrix-Matrix Multiplication">MatrixMatrixMultiplication</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Cholesky Factorization">CholeskyFactorization</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="LU Factorization">LUFactorization</docanchor>
    <docanchor file="BuildingAndInstallingStarPU" title="Simulated benchmarks">SimulatedBenchmarks</docanchor>
  </compound>
  <compound kind="page">
    <name>BasicExamples</name>
    <title>Basic Examples</title>
    <filename>BasicExamples</filename>
    <docanchor file="BasicExamples" title="Hello World Using The C Extension">HelloWorldUsingTheCExtension</docanchor>
    <docanchor file="BasicExamples" title="Hello World Using StarPU&apos;s API">HelloWorldUsingStarPUAPI</docanchor>
    <docanchor file="BasicExamples" title="Required Headers">RequiredHeaders</docanchor>
    <docanchor file="BasicExamples" title="Defining A Codelet">DefiningACodelet</docanchor>
    <docanchor file="BasicExamples" title="Submitting A Task">SubmittingATask</docanchor>
    <docanchor file="BasicExamples" title="Execution Of Hello World">ExecutionOfHelloWorld</docanchor>
    <docanchor file="BasicExamples" title="Passing Arguments To The Codelet">PassingArgumentsToTheCodelet</docanchor>
    <docanchor file="BasicExamples" title="Defining A Callback">DefiningACallback</docanchor>
    <docanchor file="BasicExamples" title="Where To Execute A Codelet">WhereToExecuteACodelet</docanchor>
    <docanchor file="BasicExamples" title="Vector Scaling Using the C Extension">VectorScalingUsingTheCExtension</docanchor>
    <docanchor file="BasicExamples" title="Adding an OpenCL Task Implementation">AddingAnOpenCLTaskImplementation</docanchor>
    <docanchor file="BasicExamples" title="Adding a CUDA Task Implementation">AddingACUDATaskImplementation</docanchor>
    <docanchor file="BasicExamples" title="Vector Scaling Using StarPU&apos;s API">VectorScalingUsingStarPUAPI</docanchor>
    <docanchor file="BasicExamples" title="Source Code of Vector Scaling">SourceCodeOfVectorScaling</docanchor>
    <docanchor file="BasicExamples" title="Execution of Vector Scaling">ExecutionOfVectorScaling</docanchor>
    <docanchor file="BasicExamples" title="Vector Scaling on an Hybrid CPU/GPU Machine">VectorScalingOnAnHybridCPUGPUMachine</docanchor>
    <docanchor file="BasicExamples" title="Definition of the CUDA Kernel">DefinitionOfTheCUDAKernel</docanchor>
    <docanchor file="BasicExamples" title="Definition of the OpenCL Kernel">DefinitionOfTheOpenCLKernel</docanchor>
    <docanchor file="BasicExamples" title="Definition of the Main Code">DefinitionOfTheMainCode</docanchor>
    <docanchor file="BasicExamples" title="Execution of Hybrid Vector Scaling">ExecutionOfHybridVectorScaling</docanchor>
  </compound>
  <compound kind="page">
    <name>AdvancedExamples</name>
    <title>Advanced Examples</title>
    <filename>AdvancedExamples</filename>
  </compound>
  <compound kind="page">
    <name>CheckListWhenPerformanceAreNotThere</name>
    <title>Check List When Performance Are Not There</title>
    <filename>CheckListWhenPerformanceAreNotThere</filename>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="Configuration That May Improve Performance">ConfigurationImprovePerformance</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="Data Related Features That May Improve Performance">DataRelatedFeaturesToImprovePerformance</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="Task Related Features That May Improve Performance">TaskRelatedFeaturesToImprovePerformance</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="Scheduling Related Features That May Improve Performance">SchedulingRelatedFeaturesToImprovePerformance</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="CUDA-specific Optimizations">CUDA-specificOptimizations</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="OpenCL-specific Optimizations">OpenCL-specificOptimizations</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="Detection Stuck Conditions">DetectionStuckConditions</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="How to limit memory used by StarPU and cache buffer allocations">HowToLimitMemoryPerNode</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="How To Reduce The Memory Footprint Of Internal Data Structures">HowToReduceTheMemoryFootprintOfInternalDataStructures</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="How to reuse memory">HowtoReuseMemory</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="Performance Model Calibration">PerformanceModelCalibration</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="Profiling">Profiling</docanchor>
    <docanchor file="CheckListWhenPerformanceAreNotThere" title="Overhead Profiling">OverheadProfiling</docanchor>
  </compound>
  <compound kind="page">
    <name>TasksInStarPU</name>
    <title>Tasks In StarPU</title>
    <filename>TasksInStarPU</filename>
    <docanchor file="TasksInStarPU" title="Task Granularity">TaskGranularity</docanchor>
    <docanchor file="TasksInStarPU" title="Task Submission">TaskSubmission</docanchor>
    <docanchor file="TasksInStarPU" title="Task Priorities">TaskPriorities</docanchor>
    <docanchor file="TasksInStarPU" title="Setting Many Data Handles For a Task">SettingManyDataHandlesForATask</docanchor>
    <docanchor file="TasksInStarPU" title="Setting a Variable number Data Handles For a Task">SettingVariableDataHandlesForATask</docanchor>
    <docanchor file="TasksInStarPU" title="Using Multiple Implementations Of A Codelet">UsingMultipleImplementationsOfACodelet</docanchor>
    <docanchor file="TasksInStarPU" title="Enabling Implementation According To Capabilities">EnablingImplementationAccordingToCapabilities</docanchor>
    <docanchor file="TasksInStarPU" title="Insert Task Utility">InsertTaskUtility</docanchor>
    <docanchor file="TasksInStarPU" title="Getting Task Children">GettingTaskChildren</docanchor>
    <docanchor file="TasksInStarPU" title="Parallel Tasks">ParallelTasks</docanchor>
    <docanchor file="TasksInStarPU" title="Fork-mode Parallel Tasks">Fork-modeParallelTasks</docanchor>
    <docanchor file="TasksInStarPU" title="SPMD-mode Parallel Tasks">SPMD-modeParallelTasks</docanchor>
    <docanchor file="TasksInStarPU" title="Parallel Tasks Performance">ParallelTasksPerformance</docanchor>
    <docanchor file="TasksInStarPU" title="Combined Workers">CombinedWorkers</docanchor>
    <docanchor file="TasksInStarPU" title="Concurrent Parallel Tasks">ConcurrentParallelTasks</docanchor>
    <docanchor file="TasksInStarPU" title="Synchronization tasks">SynchronizationTasks</docanchor>
  </compound>
  <compound kind="page">
    <name>DataManagement</name>
    <title>Data Management</title>
    <filename>DataManagement</filename>
    <docanchor file="DataManagement" title="Data Management">DataManagement</docanchor>
    <docanchor file="DataManagement" title="Data Prefetch">DataPrefetch</docanchor>
    <docanchor file="DataManagement" title="Partitioning Data">PartitioningData</docanchor>
    <docanchor file="DataManagement" title="Asynchronous Partitioning">AsynchronousPartitioning</docanchor>
    <docanchor file="DataManagement" title="Manual Partitioning">ManualPartitioning</docanchor>
    <docanchor file="DataManagement" title="Data Reduction">DataReduction</docanchor>
    <docanchor file="DataManagement" title="Commute Data Access">DataCommute</docanchor>
    <docanchor file="DataManagement" title="Concurrent Data accesses">ConcurrentDataAccess</docanchor>
    <docanchor file="DataManagement" title="Temporary Buffers">TemporaryBuffers</docanchor>
    <docanchor file="DataManagement" title="Temporary Data">TemporaryData</docanchor>
    <docanchor file="DataManagement" title="Scratch Data">ScratchData</docanchor>
    <docanchor file="DataManagement" title="The Multiformat Interface">TheMultiformatInterface</docanchor>
    <docanchor file="DataManagement" title="Defining A New Data Interface">DefiningANewDataInterface</docanchor>
    <docanchor file="DataManagement" title="Specifying a target node for task data">SpecifyingATargetNode</docanchor>
  </compound>
  <compound kind="page">
    <name>Scheduling</name>
    <title>Scheduling</title>
    <filename>Scheduling</filename>
    <docanchor file="Scheduling" title="Task Scheduling Policy">TaskSchedulingPolicy</docanchor>
    <docanchor file="Scheduling" title="Task Distribution Vs Data Transfer">TaskDistributionVsDataTransfer</docanchor>
    <docanchor file="Scheduling" title="Energy-based Scheduling">Energy-basedScheduling</docanchor>
    <docanchor file="Scheduling" title="Static Scheduling">StaticScheduling</docanchor>
    <docanchor file="Scheduling" title="Defining A New Scheduling Policy">DefiningANewSchedulingPolicy</docanchor>
    <docanchor file="Scheduling" title="Graph-based scheduling">GraphScheduling</docanchor>
    <docanchor file="Scheduling" title="Debugging scheduling">DebuggingScheduling</docanchor>
  </compound>
  <compound kind="page">
    <name>SchedulingContexts</name>
    <title>Scheduling Contexts</title>
    <filename>SchedulingContexts</filename>
    <docanchor file="SchedulingContexts" title="General Ideas">GeneralIdeas</docanchor>
    <docanchor file="SchedulingContexts" title="Creating A Context">CreatingAContext</docanchor>
    <docanchor file="SchedulingContexts" title="Modifying A Context">ModifyingAContext</docanchor>
    <docanchor file="SchedulingContexts" title="Submitting Tasks To A Context">SubmittingTasksToAContext</docanchor>
    <docanchor file="SchedulingContexts" title="Deleting A Context">DeletingAContext</docanchor>
    <docanchor file="SchedulingContexts" title="Emptying A Context">EmptyingAContext</docanchor>
    <docanchor file="SchedulingContexts" title="Contexts Sharing Workers">ContextsSharingWorkers</docanchor>
  </compound>
  <compound kind="page">
    <name>SchedulingContextHypervisor</name>
    <title>Scheduling Context Hypervisor</title>
    <filename>SchedulingContextHypervisor</filename>
    <docanchor file="SchedulingContextHypervisor" title="What Is The Hypervisor">WhatIsTheHypervisor</docanchor>
    <docanchor file="SchedulingContextHypervisor" title="Start the Hypervisor">StartTheHypervisor</docanchor>
    <docanchor file="SchedulingContextHypervisor" title="Interrogate The Runtime">InterrogateTheRuntime</docanchor>
    <docanchor file="SchedulingContextHypervisor" title="Trigger the Hypervisor">TriggerTheHypervisor</docanchor>
    <docanchor file="SchedulingContextHypervisor" title="Resizing Strategies">ResizingStrategies</docanchor>
    <docanchor file="SchedulingContextHypervisor" title="Defining A New Hypervisor Policy">DefiningANewHypervisorPolicy</docanchor>
  </compound>
  <compound kind="page">
    <name>DebuggingTools</name>
    <title>Debugging Tools</title>
    <filename>DebuggingTools</filename>
    <docanchor file="DebuggingTools" title="Using The Temanejo Task Debugger">UsingTheTemanejoTaskDebugger</docanchor>
  </compound>
  <compound kind="page">
    <name>OnlinePerformanceTools</name>
    <title>Online Performance Tools</title>
    <filename>OnlinePerformanceTools</filename>
    <docanchor file="OnlinePerformanceTools" title="On-line Performance Feedback">On-linePerformanceFeedback</docanchor>
    <docanchor file="OnlinePerformanceTools" title="Enabling On-line Performance Monitoring">EnablingOn-linePerformanceMonitoring</docanchor>
    <docanchor file="OnlinePerformanceTools" title="Per-task Feedback">Per-taskFeedback</docanchor>
    <docanchor file="OnlinePerformanceTools" title="Per-codelet Feedback">Per-codeletFeedback</docanchor>
    <docanchor file="OnlinePerformanceTools" title="Per-worker Feedback">Per-workerFeedback</docanchor>
    <docanchor file="OnlinePerformanceTools" title="Bus-related Feedback">Bus-relatedFeedback</docanchor>
    <docanchor file="OnlinePerformanceTools" title="StarPU-Top Interface">StarPU-TopInterface</docanchor>
    <docanchor file="OnlinePerformanceTools" title="Task And Worker Profiling">TaskAndWorkerProfiling</docanchor>
    <docanchor file="OnlinePerformanceTools" title="Performance Model Example">PerformanceModelExample</docanchor>
    <docanchor file="OnlinePerformanceTools" title="Data trace and tasks length">DataTrace</docanchor>
  </compound>
  <compound kind="page">
    <name>OfflinePerformanceTools</name>
    <title>Offline Performance Tools</title>
    <filename>OfflinePerformanceTools</filename>
    <docanchor file="OfflinePerformanceTools" title="Off-line Performance Feedback">Off-linePerformanceFeedback</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Generating Traces With FxT">GeneratingTracesWithFxT</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Creating a Gantt Diagram">CreatingAGanttDiagram</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Creating a DAG With Graphviz">CreatingADAGWithGraphviz</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Getting task details">TraceTaskDetails</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Monitoring Activity">MonitoringActivity</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Getting modular schedular animation">Animation</docanchor>
    <docanchor file="OfflinePerformanceTools" title="the scope of the trace">Limiting</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Performance Of Codelets">PerformanceOfCodelets</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Trace statistics">TraceStatistics</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Theoretical Lower Bound On Execution Time">TheoreticalLowerBoundOnExecutionTime</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Theoretical Lower Bound On Execution Time Example">TheoreticalLowerBoundOnExecutionTimeExample</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Memory Feedback">MemoryFeedback</docanchor>
    <docanchor file="OfflinePerformanceTools" title="Data Statistics">DataStatistics</docanchor>
  </compound>
  <compound kind="page">
    <name>FrequentlyAskedQuestions</name>
    <title>Frequently Asked Questions</title>
    <filename>FrequentlyAskedQuestions</filename>
    <docanchor file="FrequentlyAskedQuestions" title="How To Initialize A Computation Library Once For Each Worker?">HowToInitializeAComputationLibraryOnceForEachWorker</docanchor>
    <docanchor file="FrequentlyAskedQuestions" title="Using The Driver API">UsingTheDriverAPI</docanchor>
    <docanchor file="FrequentlyAskedQuestions" title="On-GPU Rendering">On-GPURendering</docanchor>
    <docanchor file="FrequentlyAskedQuestions" title="Using StarPU With MKL 11 (Intel Composer XE 2013)">UsingStarPUWithMKL</docanchor>
    <docanchor file="FrequentlyAskedQuestions" title="Thread Binding on NetBSD">ThreadBindingOnNetBSD</docanchor>
    <docanchor file="FrequentlyAskedQuestions" title="Interleaving StarPU and non-StarPU code">PauseResume</docanchor>
  </compound>
  <compound kind="page">
    <name>OutOfCore</name>
    <title>Out Of Core</title>
    <filename>OutOfCore</filename>
    <docanchor file="ModularizedScheduler" title="Introduction">Introduction</docanchor>
    <docanchor file="OutOfCore" title="Use a new disk memory">UseANewDiskMemory</docanchor>
    <docanchor file="OutOfCore" title="Disk functions">DiskFunctions</docanchor>
    <docanchor file="OutOfCore" title="Examples: disk_copy">ExampleDiskCopy</docanchor>
    <docanchor file="OutOfCore" title="Examples: disk_compute">ExampleDiskCompute</docanchor>
  </compound>
  <compound kind="page">
    <name>MPISupport</name>
    <title>MPI Support</title>
    <filename>MPISupport</filename>
    <docanchor file="MPISupport" title="used in this documentation">Example</docanchor>
    <docanchor file="MPISupport" title="About not using the MPI support">NotUsingMPISupport</docanchor>
    <docanchor file="MPISupport" title="Simple Example">SimpleExample</docanchor>
    <docanchor file="MPISupport" title="Point To Point Communication">PointToPointCommunication</docanchor>
    <docanchor file="MPISupport" title="Exchanging User Defined Data Interface">ExchangingUserDefinedDataInterface</docanchor>
    <docanchor file="MPISupport" title="MPI Insert Task Utility">MPIInsertTaskUtility</docanchor>
    <docanchor file="MPISupport" title="MPI cache support">MPICache</docanchor>
    <docanchor file="MPISupport" title="MPI Data migration">MPIMigration</docanchor>
    <docanchor file="MPISupport" title="MPI Collective Operations">MPICollective</docanchor>
  </compound>
  <compound kind="page">
    <name>FFTSupport</name>
    <title>FFT Support</title>
    <filename>FFTSupport</filename>
    <docanchor file="MICSCCSupport" title="Compilation">Compilation</docanchor>
  </compound>
  <compound kind="page">
    <name>MICSCCSupport</name>
    <title>MIC Xeon Phi / SCC Support</title>
    <filename>MICSCCSupport</filename>
    <docanchor file="MICSCCSupport" title="Porting Applications To MIC Xeon Phi / SCC">PortingApplicationsToMICSCC</docanchor>
    <docanchor file="MICSCCSupport" title="Launching Programs">LaunchingPrograms</docanchor>
  </compound>
  <compound kind="page">
    <name>cExtensions</name>
    <title>C Extensions</title>
    <filename>cExtensions</filename>
    <docanchor file="cExtensions" title="Defining Tasks">DefiningTasks</docanchor>
    <docanchor file="cExtensions" title="Initialization, Termination, and Synchronization">InitializationTerminationAndSynchronization</docanchor>
    <docanchor file="cExtensions" title="Registered Data Buffers">RegisteredDataBuffers</docanchor>
    <docanchor file="cExtensions" title="Using C Extensions Conditionally">UsingCExtensionsConditionally</docanchor>
  </compound>
  <compound kind="page">
    <name>SOCLOpenclExtensions</name>
    <title>SOCL OpenCL Extensions</title>
    <filename>SOCLOpenclExtensions</filename>
  </compound>
  <compound kind="page">
    <name>SimGridSupport</name>
    <title>SimGrid Support</title>
    <filename>SimGridSupport</filename>
    <docanchor file="SimGridSupport" title="Preparing your application for simulation.">Preparing</docanchor>
    <docanchor file="SimGridSupport" title="Calibration">Calibration</docanchor>
    <docanchor file="SimGridSupport" title="Simulation">Simulation</docanchor>
    <docanchor file="SimGridSupport" title="Simulation On Another Machine">SimulationOnAnotherMachine</docanchor>
    <docanchor file="SimGridSupport" title="Simulation examples">SimulationExamples</docanchor>
    <docanchor file="SimGridSupport" title="Simulations on fake machines">FakeSimulations</docanchor>
    <docanchor file="SimGridSupport" title="simulation">Tweaking</docanchor>
    <docanchor file="SimGridSupport" title="MPI applications">SimulationMPIApplications</docanchor>
    <docanchor file="SimGridSupport" title="Debugging applications">SimulationDebuggingApplications</docanchor>
    <docanchor file="SimGridSupport" title="Memory usage">SimulationMemoryUsage</docanchor>
  </compound>
  <compound kind="page">
    <name>OpenMPRuntimeSupport</name>
    <title>The StarPU OpenMP Runtime Support (SORS)</title>
    <filename>OpenMPRuntimeSupport</filename>
    <docanchor file="OpenMPRuntimeSupport" title="Implementation Details and Specificities">Implementation</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Main Thread">MainThread</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Extended Task Semantics">TaskSemantics</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Configuration">Configuration</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Initialization and Shutdown">InitExit</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Parallel Regions and Worksharing">Parallel</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Parallel Regions">OMPParallel</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Parallel For">OMPFor</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Sections">OMPSections</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Single">OMPSingle</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Tasks">Task</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Explicit Tasks">OMPTask</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Data Dependencies">DataDependencies</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="TaskWait and TaskGroup">TaskSyncs</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Synchronization Support">Synchronization</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Simple Locks">SimpleLock</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Nestable Locks">NestableLock</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Critical Sections">Critical</docanchor>
    <docanchor file="OpenMPRuntimeSupport" title="Barriers">Barrier</docanchor>
  </compound>
  <compound kind="page">
    <name>ExecutionConfigurationThroughEnvironmentVariables</name>
    <title>Execution Configuration Through Environment Variables</title>
    <filename>ExecutionConfigurationThroughEnvironmentVariables</filename>
    <docanchor file="CompilationConfiguration" title="Configuring Workers">ConfiguringWorkers</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_NCPU</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_NCPUS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_NCUDA</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_NWORKER_PER_CUDA</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_CUDA_PIPELINE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_NOPENCL</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_OPENCL_PIPELINE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_OPENCL_ON_CPUS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_OPENCL_ONLY_ON_CPUS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_NMIC</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_NMICCORES</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_NSCC</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WORKERS_NOBIND</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WORKERS_CPUID</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WORKERS_CUDAID</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WORKERS_OPENCLID</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WORKERS_MICID</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WORKERS_SCCID</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WORKER_TREE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_SINGLE_COMBINED_WORKER</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MIN_WORKERSIZE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MAX_WORKERSIZE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_SYNTHESIZE_ARITY_COMBINED_WORKER</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_DISABLE_ASYNCHRONOUS_COPY</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_DISABLE_ASYNCHRONOUS_CUDA_COPY</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_DISABLE_ASYNCHRONOUS_OPENCL_COPY</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_DISABLE_ASYNCHRONOUS_MIC_COPY</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_ENABLE_CUDA_GPU_GPU_DIRECT</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_DISABLE_PINNING</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables" title="Configuring The Scheduling Engine">ConfiguringTheSchedulingEngine</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_SCHED</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_CALIBRATE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_CALIBRATE_MINIMUM</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_BUS_CALIBRATE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_PREFETCH</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_SCHED_ALPHA</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_SCHED_BETA</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_SCHED_GAMMA</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_IDLE_POWER</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_PROFILING</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables" title="Extensions">Extensions</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">SOCL_OCL_LIB_OPENCL</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">OCL_ICD_VENDORS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_COMM_STATS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MPI_CACHE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MPI_COMM</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MPI_CACHE_STATS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_SIMGRID_CUDA_MALLOC_COST</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_SIMGRID_CUDA_QUEUE_COST</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MALLOC_SIMULATION_FOLD</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables" title="Miscellaneous And Debug">MiscellaneousAndDebug</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_HOME</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_PERF_MODEL_DIR</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_HOSTNAME</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_OPENCL_PROGRAM_DIR</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_SILENT</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_LOGFILENAME</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_FXT_PREFIX</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_LIMIT_CUDA_devid_MEM</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_LIMIT_CUDA_MEM</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_LIMIT_OPENCL_devid_MEM</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_LIMIT_OPENCL_MEM</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_LIMIT_CPU_MEM</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MINIMUM_AVAILABLE_MEM</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_TARGET_AVAILABLE_MEM</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MINIMUM_CLEAN_BUFFERS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_TARGET_CLEAN_BUFFERS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_DISK_SWAP</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_DISK_SWAP_BACKEND</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_DISK_SWAP_SIZE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_LIMIT_MAX_SUBMITTED_TASKS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_LIMIT_MIN_SUBMITTED_TASKS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_TRACE_BUFFER_SIZE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_GENERATE_TRACE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MEMORY_STATS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_MAX_MEMORY_USE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_BUS_STATS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WORKER_STATS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_STATS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WATCHDOG_TIMEOUT</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_WATCHDOG_CRASH</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_TASK_BREAK_ON_SCHED</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_TASK_BREAK_ON_PUSH</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_TASK_BREAK_ON_POP</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_DISABLE_KERNELS</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_HISTORY_MAX_ERROR</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">STARPU_RAND_SEED</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables" title="Configuring The Hypervisor">ConfiguringTheHypervisor</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">SC_HYPERVISOR_POLICY</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">SC_HYPERVISOR_TRIGGER_RESIZE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">SC_HYPERVISOR_START_RESIZE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">SC_HYPERVISOR_MAX_SPEED_GAP</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">SC_HYPERVISOR_STOP_PRINT</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">SC_HYPERVISOR_LAZY_RESIZE</docanchor>
    <docanchor file="ExecutionConfigurationThroughEnvironmentVariables">SC_HYPERVISOR_SAMPLE_CRITERIA</docanchor>
  </compound>
  <compound kind="page">
    <name>CompilationConfiguration</name>
    <title>Compilation Configuration</title>
    <filename>CompilationConfiguration</filename>
    <docanchor file="CompilationConfiguration" title="Common Configuration">CommonConfiguration</docanchor>
    <docanchor file="CompilationConfiguration">enable-debug</docanchor>
    <docanchor file="CompilationConfiguration">enable-spinlock-check</docanchor>
    <docanchor file="CompilationConfiguration">enable-fast</docanchor>
    <docanchor file="CompilationConfiguration">enable-verbose</docanchor>
    <docanchor file="CompilationConfiguration">enable-coverage</docanchor>
    <docanchor file="CompilationConfiguration">enable-quick-check</docanchor>
    <docanchor file="CompilationConfiguration">enable-long-check</docanchor>
    <docanchor file="CompilationConfiguration">enable-new-check</docanchor>
    <docanchor file="CompilationConfiguration">with-hwloc</docanchor>
    <docanchor file="CompilationConfiguration">without-hwloc</docanchor>
    <docanchor file="CompilationConfiguration">disable-build-doc</docanchor>
    <docanchor file="CompilationConfiguration">enable-maxcpus</docanchor>
    <docanchor file="CompilationConfiguration">disable-cpu</docanchor>
    <docanchor file="CompilationConfiguration">enable-maxcudadev</docanchor>
    <docanchor file="CompilationConfiguration">disable-cuda</docanchor>
    <docanchor file="CompilationConfiguration">with-cuda-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-cuda-include-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-cuda-lib-dir</docanchor>
    <docanchor file="CompilationConfiguration">disable-cuda-memcpy-peer</docanchor>
    <docanchor file="CompilationConfiguration">enable-maxopencldev</docanchor>
    <docanchor file="CompilationConfiguration">disable-opencl</docanchor>
    <docanchor file="CompilationConfiguration">with-opencl-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-opencl-include-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-opencl-lib-dir</docanchor>
    <docanchor file="CompilationConfiguration">enable-opencl-simulator</docanchor>
    <docanchor file="CompilationConfiguration">enable-maximplementations</docanchor>
    <docanchor file="CompilationConfiguration">enable-max-sched-ctxs</docanchor>
    <docanchor file="CompilationConfiguration">disable-asynchronous-copy</docanchor>
    <docanchor file="CompilationConfiguration">disable-asynchronous-cuda-copy</docanchor>
    <docanchor file="CompilationConfiguration">disable-asynchronous-opencl-copy</docanchor>
    <docanchor file="CompilationConfiguration">enable-maxmicthreads</docanchor>
    <docanchor file="CompilationConfiguration">disable-asynchronous-mic-copy</docanchor>
    <docanchor file="CompilationConfiguration">enable-maxnodes</docanchor>
    <docanchor file="CompilationConfiguration" title="Extension Configuration">ExtensionConfiguration</docanchor>
    <docanchor file="CompilationConfiguration">disable-fortran</docanchor>
    <docanchor file="CompilationConfiguration">disable-socl</docanchor>
    <docanchor file="CompilationConfiguration">disable-starpu-top</docanchor>
    <docanchor file="CompilationConfiguration">disable-gcc-extensions</docanchor>
    <docanchor file="CompilationConfiguration">with-mpicc</docanchor>
    <docanchor file="CompilationConfiguration">enable-mpi-progression-hook</docanchor>
    <docanchor file="CompilationConfiguration">with-coi-dir</docanchor>
    <docanchor file="CompilationConfiguration">mic-host</docanchor>
    <docanchor file="CompilationConfiguration">enable-openmp</docanchor>
    <docanchor file="CompilationConfiguration" title="Advanced Configuration">AdvancedConfiguration</docanchor>
    <docanchor file="CompilationConfiguration">enable-perf-debug</docanchor>
    <docanchor file="CompilationConfiguration">enable-model-debug</docanchor>
    <docanchor file="CompilationConfiguration">enable-paje-codelet-details</docanchor>
    <docanchor file="CompilationConfiguration">enable-fxt-lock</docanchor>
    <docanchor file="CompilationConfiguration">enable-stats</docanchor>
    <docanchor file="CompilationConfiguration">enable-maxbuffers</docanchor>
    <docanchor file="CompilationConfiguration">enable-allocation-cache</docanchor>
    <docanchor file="CompilationConfiguration">enable-opengl-render</docanchor>
    <docanchor file="CompilationConfiguration">enable-blas-lib</docanchor>
    <docanchor file="CompilationConfiguration">enable-leveldb</docanchor>
    <docanchor file="CompilationConfiguration">disable-starpufft</docanchor>
    <docanchor file="CompilationConfiguration">enable-starpufft-examples</docanchor>
    <docanchor file="CompilationConfiguration">with-fxt</docanchor>
    <docanchor file="CompilationConfiguration">with-perf-model-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-goto-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-atlas-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-mkl-cflags</docanchor>
    <docanchor file="CompilationConfiguration">with-mkl-ldflags</docanchor>
    <docanchor file="CompilationConfiguration">disable-build-tests</docanchor>
    <docanchor file="CompilationConfiguration">disable-build-examples</docanchor>
    <docanchor file="CompilationConfiguration">enable-sc-hypervisor</docanchor>
    <docanchor file="CompilationConfiguration">enable-memory-stats</docanchor>
    <docanchor file="CompilationConfiguration">enable-simgrid</docanchor>
    <docanchor file="CompilationConfiguration">with-simgrid-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-simgrid-include-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-simgrid-lib-dir</docanchor>
    <docanchor file="CompilationConfiguration">with-smpirun</docanchor>
    <docanchor file="CompilationConfiguration">enable-calibration-heuristic</docanchor>
  </compound>
  <compound kind="page">
    <name>Files</name>
    <title>Files</title>
    <filename>Files</filename>
  </compound>
  <compound kind="page">
    <name>FullSourceCodeVectorScal</name>
    <title>Full source code for the ’Scaling a Vector’ example</title>
    <filename>FullSourceCodeVectorScal</filename>
    <docanchor file="FullSourceCodeVectorScal" title="Main Application">MainApplication</docanchor>
    <docanchor file="FullSourceCodeVectorScal" title="CPU Kernel">CPUKernel</docanchor>
    <docanchor file="FullSourceCodeVectorScal" title="CUDA Kernel">CUDAKernel</docanchor>
    <docanchor file="FullSourceCodeVectorScal" title="OpenCL Kernel">OpenCLKernel</docanchor>
    <docanchor file="FullSourceCodeVectorScal" title="Invoking the Kernel">InvokingtheKernel</docanchor>
    <docanchor file="FullSourceCodeVectorScal" title="Source of the Kernel">SourceoftheKernel</docanchor>
  </compound>
  <compound kind="page">
    <name>GNUFreeDocumentationLicense</name>
    <title>The GNU Free Documentation License</title>
    <filename>GNUFreeDocumentationLicense</filename>
    <docanchor file="GNUFreeDocumentationLicense" title="ADDENDUM: How to use this License for your documents">ADDENDUM</docanchor>
  </compound>
  <compound kind="page">
    <name>ModularizedScheduler</name>
    <title>Modularized Schedulers</title>
    <filename>ModularizedScheduler</filename>
    <docanchor file="ModularizedScheduler" title="Using Modularized Schedulers">UsingModularizedSchedulers</docanchor>
    <docanchor file="ModularizedScheduler" title="Existing Modularized Schedulers">ExistingModularizedSchedulers</docanchor>
    <docanchor file="ModularizedScheduler" title="An Example : The Tree-Eager-Prefetching Strategy">ExampleTreeEagerPrefetchingStrategy</docanchor>
    <docanchor file="ModularizedScheduler">Interface</docanchor>
    <docanchor file="ModularizedScheduler" title="Build a Modularized Scheduler">BuildAModularizedScheduler</docanchor>
    <docanchor file="ModularizedScheduler" title="Pre-implemented Components">PreImplementedComponents</docanchor>
    <docanchor file="ModularizedScheduler" title="Progression And Validation Rules">ProgressionAndValidationRules</docanchor>
    <docanchor file="ModularizedScheduler" title="Implement a Modularized Scheduler">ImplementAModularizedScheduler</docanchor>
    <docanchor file="ModularizedScheduler" title="Write a Scheduling Component">WriteASchedulingComponent</docanchor>
    <docanchor file="ModularizedScheduler" title="Generic Scheduling Component">GenericSchedulingComponent</docanchor>
    <docanchor file="ModularizedScheduler" title="Instantiation : Redefine the Interface">InstantiationRedefineInterface</docanchor>
    <docanchor file="ModularizedScheduler" title="Detailed Progression and Validation Rules">DetailedProgressionAndValidationRules</docanchor>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>Introduction</title>
    <filename>index</filename>
    <docanchor file="index" title="Motivation">Motivation</docanchor>
    <docanchor file="index" title="StarPU in a Nutshell">StarPUInANutshell</docanchor>
    <docanchor file="index" title="Codelet and Tasks">CodeletAndTasks</docanchor>
    <docanchor file="index" title="StarPU Data Management Library">StarPUDataManagementLibrary</docanchor>
    <docanchor file="index" title="Application Taskification">ApplicationTaskification</docanchor>
    <docanchor file="index" title="Glossary">Glossary</docanchor>
    <docanchor file="index" title="Research Papers">ResearchPapers</docanchor>
    <docanchor file="index" title="StarPU Applications">StarPUApplications</docanchor>
    <docanchor file="index" title="Further Reading">FurtherReading</docanchor>
  </compound>
</tagfile>
