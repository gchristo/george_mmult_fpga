/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2009-2015  Université de Bordeaux
 * Copyright (C) 2010  Mehdi Juhoor <mjuhoor@gmail.com>
 * Copyright (C) 2010, 2011, 2012, 2013, 2014  CNRS
 * Copyright (C) 2011  Télécom-SudParis
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#include <starpu.h>
#include <starpu_conor.h>
#include <starpu_profiling.h>
#include <common/utils.h>
#include <common/config.h>
#include <core/debug.h>
#include <drivers/driver_common/driver_common.h>
#include "driver_conor.h"
#include <core/sched_policy.h>
#include <datawizard/memory_manager.h>
#include <datawizard/memory_nodes.h>
#include <datawizard/malloc.h>



/* the number of CONOR devices */
static unsigned nconorfpgas;

static size_t global_mem[STARPU_MAXCONORDEVS];

void starpu_conor_get_device(int devid,cn_device_id *device)
{
	*device = conorGetDeviceId();
}

void *_starpu_conor_worker(void *_arg){
	conor_debug("starpu_conor_worker succesfully called.\t:-)");
	return NULL;
}

int _starpu_conor_driver_run_once(struct _starpu_worker *worker){
	conor_debug("_starpu_conor_driver_run_once succesfully called.\t:-)");
	return 0;

}

int _starpu_run_conor(struct _starpu_worker *workerarg)
{
	/* Let's go ! */
//	_starpu_conor_worker(workerarg);
	
	conor_debug("_starpu_run_conor succesfully called :-)");
	return 0;
}
