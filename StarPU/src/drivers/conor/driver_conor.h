/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2009, 2010, 2012-2014  Université de Bordeaux
 * Copyright (C) 2010, 2012  CNRS
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#ifndef __DRIVER_CONOR_H__
#define __DRIVER_CONOR_H__

#ifdef STARPU_USE_CONOR
#include <conor.h>
#endif

#include <starpu.h>
#include <common/config.h>

#include <core/jobs.h>
#include <core/task.h>
#include <datawizard/datawizard.h>
#include <core/perfmodel/perfmodel.h>

#include <common/fxt.h>

struct _starpu_worker;

void starpu_conor_get_device(int, cn_device_id *);

void *_starpu_conor_worker(void *);

int _starpu_conor_driver_run_once(struct _starpu_worker *);

int _starpu_run_conor(struct _starpu_worker *);
#endif //  __DRIVER_CONOR_H__

