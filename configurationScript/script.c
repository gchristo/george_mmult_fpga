#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void get_popen(char *, char *,size_t );
void execute(char *);

void my_string_operation(const char *, char *, const char *, char*);
int str_length(char *);

int main(int argc, char *argv[]){

	char pcieid[12],command[200],buff[10], *bitstream=NULL;
	
	//Bitstream holds the string of the bitstream	
	bitstream=argv[1];
	printf("Bitstream to be flashed: %s\n",bitstream);
	
	//pcieid holds the id of the Xilinx card	
	sprintf(pcieid,"0000:");
	sprintf(command, "lspci | grep Xilinx");
	get_popen(command, pcieid+5, 8);
	
	printf("Board to be configured: %s\n",pcieid);
	
	//Preparation:
	printf("Setting pci linux controller to idle..\n");
	my_string_operation("echo 1 > /sys/bus/pci/devices/",pcieid,"/remove",command);
	printf("\tCommand is:\t%s\n",command);
	execute(command);
	
	//Re-configuration
	printf("Reconfiguring Device..\n");
	my_string_operation("xc3sprog -c jtaghs1_fast -v -p 0 ",bitstream,":w::bit",command);
	printf("\tCommand is:\t%s\n",command);
	execute(command);

	printf("Forcing linux kernel to rescan PCI devices..\n");
	my_string_operation("echo 1 > /sys/bus/pci/rescan","","",command);
	printf("\tCommand is:\t%s\n",command);
	execute(command);
	
	printf("Reconfiguration Completed Succesfully!\n");
	return 0;
}

void execute(char *command){
	system(command);
}

void get_popen(char *command, char *result, size_t res_size){
	FILE *pf;
	
	pf=popen(command,"r");

	fgets(result ,res_size ,pf);
	
	pclose(pf);
}

void my_string_operation(const char *s1,char *s2, const char *s3, char *result){
	sprintf(result, s1);
	int count, bitsize=strlen(s2);
	size_t index=strlen(s1);
	for(count=0; count<bitsize; count++)
		result[index+count]=s2[count];
	index+=bitsize;
	result[index]='\0';
	sprintf(result+index,s3);
	
}
