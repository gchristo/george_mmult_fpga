// ----------------------------------------------------------------------
// Copyright (c) 2016, The Regents of the University of California All
// rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// ----------------------------------------------------------------------
// Copyright (c) 2016, The Regents of the University of California All
// rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// 
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
// 
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
// 
//     * Neither the name of The Regents of the University of California
//       nor the names of its contributors may be used to endorse or
//       promote products derived from this software without specific
//       prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL REGENTS OF THE
// UNIVERSITY OF CALIFORNIA BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
// TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
// DAMAGE.
// ----------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include "timer.h"
#include "riffa.h"

#include <conor.h>

int read_input(void);

int main() {
	fpga_t * fpga;
	fpga_info_list info;
	int id;
	int num_chnls,chnl=0;
	
	size_t numWords_to_send,numWords_to_recv;
	numWords_to_send = 2; 
	numWords_to_recv = 1;

	unsigned int * sendBuffer;
	unsigned int * recvBuffer;
	
	GET_TIME_INIT(3);

		
	printf("Number of words to send: %d\n",numWords_to_send);
	printf("Number of words to receive: %d\n",numWords_to_recv);

	// Get the device with id
	// Initialization

	if (fpga == NULL) {
		printf("Could not get FPGA %d\n", id);
		return -1;
	}

	// Malloc the arrays
	sendBuffer = (unsigned int *)malloc(numWords_to_send<<2);
	if (sendBuffer == NULL) {
		printf("Could not malloc memory for sendBuffer\n");
		fpga_close(fpga);
		return -1;
	}

	recvBuffer = (unsigned int *)malloc(numWords_to_recv<<2);
	if (recvBuffer == NULL) {
		printf("Could not malloc memory for recvBuffer\n");
		free(sendBuffer);
		fpga_close(fpga);
		return -1;
	}
	int c;
	int rack,sack, i;
	while ( c=read_input() ){
		sendBuffer[0] = c;
		c = read_input();
		sendBuffer[1] = c;
		for (i = 0; i < numWords_to_send; i++) {
			printf("After assignment:\tsendBuff[%d]: %d\n", i, sendBuffer[i]);
		}
GET_TIME_VAL(0);
// Send the data
//		sack = send_data(fpga,chnl,sendBuffer,numWords_to_send);
		sack = conor_data_send(sendBuffer,numWords_to_send);
		printf("ack=%d\n",sack);
GET_TIME_VAL(1);
// Recv the data
		if (sack != 0) {
//		rack = recv_data(fpga,chnl,recvBuffer,numWords_to_recv);
		rack = conor_dat_recv(recvBuffer,numWords_to_recv);
		}
GET_TIME_VAL(2);
		for (i = 0; i < numWords_to_recv; i++) {
			printf("recvBuff[%d]: %d\n", i, recvBuffer[i]);
	// Display some data
		printf("send bw: %f MB/s %fms\n",
			sack*4.0/1024/1024/((TIME_VAL_TO_MS(1) - TIME_VAL_TO_MS(0))/1000.0),
			(TIME_VAL_TO_MS(1) - TIME_VAL_TO_MS(0)) );

		printf("recv bw: %f MB/s %fms\n",
			rack*4.0/1024/1024/((TIME_VAL_TO_MS(2) - TIME_VAL_TO_MS(1))/1000.0),
			(TIME_VAL_TO_MS(2) - TIME_VAL_TO_MS(1)) );
		}
	}


	// Done with device
	fpga_close(fpga);

	
	return 0;
}
int read_input(void){
	int c;
	printf("Please dear Gentleman, feed me :-)\n");
	scanf("%d",&c);
	return c;
}
