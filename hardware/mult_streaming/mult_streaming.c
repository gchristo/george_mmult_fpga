#include<augh.h>

#define len 1 
#define MYREAD(a,b,big) do{a=big;big=big>>32;b=big;}while(0) 
#define MYWRITE(a,b,big) do{big=a;big=big<<32;big=b;}while(0)

int64_t stdin;
int64_t stdout;

static inline uint64_t read64(){
	uint64_t u64 =0;
	fifo_read(stdin,&u64);
	return u64;
}

static inline void write64(uint8_t u64){
	fifo_write(stdout, &u64);
}

void augh_main(){
	uint32_t a[len],b[len];
	int32_t acc=0;
	int32_t count;

	int64_t rbuff;
	
	int32_t offset=0;
	int64_t result;
	
	rbuff=read64();
	MYREAD(a[0],b[0],rbuff);
	
	acc = a[0] * b[0];

	MYWRITE(offset,acc,result);
	write64(result);
}
