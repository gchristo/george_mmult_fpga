# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# IP: /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/PCIeGen1x8If64.xci
# IP: The module: 'PCIeGen1x8If64' is the root of the design. Do not add the DONT_TOUCH constraint.

# IPX: /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip.xcix
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==ip || ORIG_REF_NAME==ip}]

# XDC: /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64-PCIE_X0Y1.xdc
# XDC: The top module name and the constraint reference have the same name: 'PCIeGen1x8If64'. Do not add the DONT_TOUCH constraint.
set_property DONT_TOUCH TRUE [get_cells inst]

# XDC: /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/synth/PCIeGen1x8If64_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'PCIeGen1x8If64'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells inst]

# IP: /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/PCIeGen1x8If64.xci
# IP: The module: 'PCIeGen1x8If64' is the root of the design. Do not add the DONT_TOUCH constraint.

# IPX: /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip.xcix
#dup# set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==ip || ORIG_REF_NAME==ip}]

# XDC: /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64-PCIE_X0Y1.xdc
# XDC: The top module name and the constraint reference have the same name: 'PCIeGen1x8If64'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells inst]

# XDC: /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/synth/PCIeGen1x8If64_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'PCIeGen1x8If64'. Do not add the DONT_TOUCH constraint.
#dup# set_property DONT_TOUCH TRUE [get_cells inst]
