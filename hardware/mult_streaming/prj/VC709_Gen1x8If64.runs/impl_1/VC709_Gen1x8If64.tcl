proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  create_project -in_memory -part xc7vx690tffg1761-2
  set_property board_part xilinx.com:vc709:part0:1.5 [current_project]
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir /tima/altamaha-home/christod/workspace/hardware/mult_streaming/prj/VC709_Gen1x8If64.cache/wt [current_project]
  set_property parent.project_path /tima/altamaha-home/christod/workspace/hardware/mult_streaming/prj/VC709_Gen1x8If64.xpr [current_project]
  set_property ip_repo_paths /tima/altamaha-home/christod/workspace/hardware/mult_streaming/prj/VC709_Gen1x8If64.cache/ip [current_project]
  set_property ip_output_repo /tima/altamaha-home/christod/workspace/hardware/mult_streaming/prj/VC709_Gen1x8If64.cache/ip [current_project]
  add_files -quiet /tima/altamaha-home/christod/workspace/hardware/mult_streaming/prj/VC709_Gen1x8If64.runs/synth_1/VC709_Gen1x8If64.dcp
  add_files -quiet /tima/altamaha-home/christod/workspace/hardware/mult_streaming/prj/VC709_Gen1x8If64.runs/PCIeGen1x8If64_synth_1/PCIeGen1x8If64.dcp
  set_property netlist_only true [get_files /tima/altamaha-home/christod/workspace/hardware/mult_streaming/prj/VC709_Gen1x8If64.runs/PCIeGen1x8If64_synth_1/PCIeGen1x8If64.dcp]
  read_xdc -mode out_of_context -ref PCIeGen1x8If64 -cells inst /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/synth/PCIeGen1x8If64_ooc.xdc
  set_property processing_order EARLY [get_files /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/synth/PCIeGen1x8If64_ooc.xdc]
  read_xdc -ref PCIeGen1x8If64 -cells inst /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64-PCIE_X0Y1.xdc
  set_property processing_order EARLY [get_files /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64-PCIE_X0Y1.xdc]
  read_xdc /tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/constr/VC709_Gen1x8If64.xdc
  link_design -top VC709_Gen1x8If64 -part xc7vx690tffg1761-2
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  catch {write_debug_probes -quiet -force debug_nets}
  opt_design 
  write_checkpoint -force VC709_Gen1x8If64_opt.dcp
  report_drc -file VC709_Gen1x8If64_drc_opted.rpt
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  catch {write_hwdef -file VC709_Gen1x8If64.hwdef}
  place_design 
  write_checkpoint -force VC709_Gen1x8If64_placed.dcp
  report_io -file VC709_Gen1x8If64_io_placed.rpt
  report_utilization -file VC709_Gen1x8If64_utilization_placed.rpt -pb VC709_Gen1x8If64_utilization_placed.pb
  report_control_sets -verbose -file VC709_Gen1x8If64_control_sets_placed.rpt
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force VC709_Gen1x8If64_routed.dcp
  report_drc -file VC709_Gen1x8If64_drc_routed.rpt -pb VC709_Gen1x8If64_drc_routed.pb
  report_timing_summary -warn_on_violation -max_paths 10 -file VC709_Gen1x8If64_timing_summary_routed.rpt -rpx VC709_Gen1x8If64_timing_summary_routed.rpx
  report_power -file VC709_Gen1x8If64_power_routed.rpt -pb VC709_Gen1x8If64_power_summary_routed.pb
  report_route_status -file VC709_Gen1x8If64_route_status.rpt -pb VC709_Gen1x8If64_route_status.pb
  report_clock_utilization -file VC709_Gen1x8If64_clock_utilization_routed.rpt
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  catch { write_mem_info -force VC709_Gen1x8If64.mmi }
  write_bitstream -force VC709_Gen1x8If64.bit 
  catch { write_sysdef -hwdef VC709_Gen1x8If64.hwdef -bitfile VC709_Gen1x8If64.bit -meminfo VC709_Gen1x8If64.mmi -file VC709_Gen1x8If64.sysdef }
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
}

