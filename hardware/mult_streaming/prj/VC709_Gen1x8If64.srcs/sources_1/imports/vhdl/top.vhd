library ieee;
use ieee.std_logic_1164.all;

entity top is
	port (
		-- Access 'clock' model 'clock'
		clock : in  std_logic;
		-- Access 'reset' model 'reset'
		reset : in  std_logic;
		-- Access 'stdin' model 'fifo_in'
		stdin_data : in  std_logic_vector(63 downto 0);
		stdin_rdy : out std_logic;
		stdin_ack : in  std_logic;
		-- Access 'stdout' model 'fifo_out'
		stdout_data : out std_logic_vector(63 downto 0);
		stdout_rdy : out std_logic;
		stdout_ack : in  std_logic
	);
end top;

architecture augh of top is

	-- Declaration of components

	component augh_main_a is
		port (
			-- Access 'clk' model 'clock'
			clk : in  std_logic;
			-- Access 'ra0' model 'mem_ra'
			ra0_data : out std_logic_vector(31 downto 0);
			ra0_addr : in  std_logic;
			-- Access 'wa0' model 'mem_wa'
			wa0_data : in  std_logic_vector(31 downto 0);
			wa0_addr : in  std_logic;
			wa0_en : in  std_logic
		);
	end component;

	component augh_main_b is
		port (
			-- Access 'clk' model 'clock'
			clk : in  std_logic;
			-- Access 'ra0' model 'mem_ra'
			ra0_data : out std_logic_vector(31 downto 0);
			ra0_addr : in  std_logic;
			-- Access 'wa0' model 'mem_wa'
			wa0_data : in  std_logic_vector(31 downto 0);
			wa0_addr : in  std_logic;
			wa0_en : in  std_logic
		);
	end component;

	component fsm_4 is
		port (
			-- Access 'clock' model 'clock'
			clock : in  std_logic;
			-- Access 'reset' model 'reset'
			reset : in  std_logic;
			-- Ports not part of access
			out2 : out std_logic;
			out3 : out std_logic;
			out4 : out std_logic;
			out6 : out std_logic;
			out27 : out std_logic;
			out29 : out std_logic;
			in0 : in  std_logic;
			out0 : out std_logic;
			out16 : out std_logic;
			out17 : out std_logic;
			out21 : out std_logic;
			out22 : out std_logic;
			out23 : out std_logic;
			out26 : out std_logic;
			out8 : out std_logic;
			out9 : out std_logic;
			in1 : in  std_logic;
			out10 : out std_logic
		);
	end component;

	component mul_3 is
		port (
			-- Ports not part of access
			in_a : in  std_logic_vector(31 downto 0);
			in_b : in  std_logic_vector(31 downto 0);
			result : out std_logic_vector(31 downto 0)
		);
	end component;

	-- Declaration of signals

	signal sig_clock : std_logic;
	signal sig_reset : std_logic;
	signal sig_34 : std_logic_vector(31 downto 0);
	signal sig_35 : std_logic;
	signal sig_36 : std_logic;
	signal sig_37 : std_logic;
	signal sig_38 : std_logic;
	signal sig_39 : std_logic;
	signal sig_40 : std_logic;
	signal sig_41 : std_logic;
	signal sig_42 : std_logic;
	signal sig_43 : std_logic;
	signal sig_44 : std_logic;
	signal sig_45 : std_logic;
	signal sig_46 : std_logic;
	signal sig_47 : std_logic;
	signal sig_48 : std_logic;
	signal sig_49 : std_logic_vector(31 downto 0);
	signal sig_50 : std_logic_vector(31 downto 0);

	-- Other inlined components

	signal read64_u64 : std_logic_vector(63 downto 0) := (others => '0');
	signal mux_29 : std_logic_vector(63 downto 0);
	signal write64_u64 : std_logic_vector(7 downto 0) := (others => '0');
	signal mux_17 : std_logic_vector(7 downto 0);
	signal mux_15 : std_logic_vector(63 downto 0);
	signal augh_main_acc : std_logic_vector(7 downto 0) := (others => '0');
	signal mux_9 : std_logic_vector(7 downto 0);
	signal augh_main_rbuff : std_logic_vector(63 downto 0) := (others => '0');
	signal augh_main_result : std_logic_vector(7 downto 0) := (others => '0');
	signal read64_ret0_0 : std_logic_vector(63 downto 0) := (others => '0');

	-- This utility function is used for to generate concatenations of std_logic

	-- Little utility function to ease concatenation of an std_logic
	-- and explicitely return an std_logic_vector
	function repeat(N: natural; B: std_logic) return std_logic_vector is
		variable result: std_logic_vector(N-1 downto 0);
	begin
		result := (others => B);
		return result;
	end;

begin

	-- Instantiation of components

	augh_main_a_i : augh_main_a port map (
			-- Access 'clk' model 'clock'
		clk => sig_clock,
			-- Access 'ra0' model 'mem_ra'
		ra0_data => sig_50,
		ra0_addr => '0',
			-- Access 'wa0' model 'mem_wa'
		wa0_data => augh_main_rbuff(31 downto 0),
		wa0_addr => '0',
		wa0_en => sig_38
	);

	augh_main_b_i : augh_main_b port map (
			-- Access 'clk' model 'clock'
		clk => sig_clock,
			-- Access 'ra0' model 'mem_ra'
		ra0_data => sig_49,
		ra0_addr => '0',
			-- Access 'wa0' model 'mem_wa'
		wa0_data => augh_main_rbuff(31 downto 0),
		wa0_addr => '0',
		wa0_en => sig_41
	);

	fsm_4_i : fsm_4 port map (
			-- Access 'clock' model 'clock'
		clock => sig_clock,
			-- Access 'reset' model 'reset'
		reset => sig_reset,
			-- Ports not part of access
		out2 => stdin_rdy,
		out3 => sig_48,
		out4 => sig_47,
		out6 => sig_46,
		out27 => sig_45,
		out29 => sig_44,
		in0 => stdout_ack,
		out0 => stdout_rdy,
		out16 => sig_43,
		out17 => sig_42,
		out21 => sig_41,
		out22 => sig_40,
		out23 => sig_39,
		out26 => sig_38,
		out8 => sig_37,
		out9 => sig_36,
		in1 => stdin_ack,
		out10 => sig_35
	);

	mul_3_i : mul_3 port map (
			-- Ports not part of access
		in_a => sig_50,
		in_b => sig_49,
		result => sig_34
	);

	-- Behaviour of component 'mux_29' model 'mux'
	mux_29 <=
		stdin_data when sig_48 = '1' else
		"0000000000000000000000000000000000000000000000000000000000000000";

	-- Behaviour of component 'mux_17' model 'mux'
	mux_17 <=
		sig_34(7 downto 0) when sig_43 = '1' else
		"00000000";

	-- Behaviour of component 'mux_15' model 'mux'
	mux_15 <=
		(repeat(32, augh_main_rbuff(63)) & augh_main_rbuff(63 downto 32)) when sig_40 = '1' else
		read64_ret0_0 when sig_45 = '1' else
		"0000000000000000000000000000000000000000000000000000000000000000";

	-- Behaviour of component 'mux_9' model 'mux'
	mux_9 <=
		augh_main_acc when sig_37 = '1' else
		"00000000" when sig_35 = '1' else
		"00000000";

	-- Behaviour of all components of model 'reg'
	-- Registers with clock = sig_clock and no reset
	process(sig_clock)
	begin
		if rising_edge(sig_clock) then
			if sig_47 = '1' then
				read64_u64 <= mux_29;
			end if;
			if sig_46 = '1' then
				write64_u64 <= augh_main_result;
			end if;
			if sig_42 = '1' then
				augh_main_acc <= mux_17;
			end if;
			if sig_39 = '1' then
				augh_main_rbuff <= mux_15;
			end if;
			if sig_36 = '1' then
				augh_main_result <= mux_9;
			end if;
			if sig_44 = '1' then
				read64_ret0_0 <= read64_u64;
			end if;
		end if;
	end process;

	-- Remaining signal assignments
	-- Those who are not assigned by component instantiation

	sig_clock <= clock;
	sig_reset <= reset;

	-- Remaining top-level ports assignments
	-- Those who are not assigned by component instantiation

	stdout_data <= "00000000000000000000000000000000000000000000000000000000" & write64_u64;

end architecture;
