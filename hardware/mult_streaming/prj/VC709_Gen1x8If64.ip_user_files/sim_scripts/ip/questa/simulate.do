onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib PCIeGen1x8If64_opt

do {wave.do}

view wave
view structure
view signals

do {PCIeGen1x8If64.udo}

run -all

quit -force
