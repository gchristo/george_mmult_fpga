vlib work
vlib msim

vlib msim/xil_defaultlib

vmap xil_defaultlib msim/xil_defaultlib

vlog -work xil_defaultlib -64 -incr \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_7vx.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_bram_7vx.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_bram_7vx_8k.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_bram_7vx_16k.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_bram_7vx_cpl.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_bram_7vx_rep.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_bram_7vx_rep_8k.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_bram_7vx_req.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_init_ctrl_7vx.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_pipe_lane.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_pipe_misc.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_pipe_pipeline.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_top.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_force_adapt.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pipe_clock.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pipe_drp.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pipe_eq.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pipe_rate.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pipe_reset.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pipe_sync.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pipe_user.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pipe_wrapper.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_qpll_drp.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_qpll_reset.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_qpll_wrapper.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_rxeq_scan.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_gt_wrapper.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_gt_top.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_gt_common.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_gtx_cpllpd_ovrd.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_tlp_tph_tbl_7vx.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/source/PCIeGen1x8If64_pcie_3_0_7vx.v" \
"/tima/altamaha-home/christod/experiment/xilinx/vc709/VC709_Gen1x8If64/ip/sim/PCIeGen1x8If64.v" \


vlog -work xil_defaultlib "glbl.v"

