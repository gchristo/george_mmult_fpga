library ieee;
use ieee.std_logic_1164.all;

library ieee;
use ieee.numeric_std.all;

entity fsm_4 is
	port (
		-- Access 'clock' model 'clock'
		clock : in  std_logic;
		-- Access 'reset' model 'reset'
		reset : in  std_logic;
		-- Ports not part of access
		out2 : out std_logic;
		out3 : out std_logic;
		out4 : out std_logic;
		out6 : out std_logic;
		out27 : out std_logic;
		out29 : out std_logic;
		in0 : in  std_logic;
		out0 : out std_logic;
		out16 : out std_logic;
		out17 : out std_logic;
		out21 : out std_logic;
		out22 : out std_logic;
		out23 : out std_logic;
		out26 : out std_logic;
		out8 : out std_logic;
		out9 : out std_logic;
		in1 : in  std_logic;
		out10 : out std_logic
	);
end fsm_4;

architecture augh of fsm_4 is

	signal state_cur  : std_logic_vector(0 to 16) := (16 => '1', others => '0');
	signal state_next : std_logic_vector(0 to 16) := (16 => '1', others => '0');

	-- Buffers for outputs

begin

	-- Sequential process
	-- Set the current state

	process (clock)
	begin
		if rising_edge(clock) then

			-- Next state
			state_cur <= state_next;
			-- Buffers for outputs

		end if;
	end process;

	-- Combinatorial process
	-- Compute the next state
	-- Compute the outputs

	process (
		-- Inputs of the FSM
		reset, in0, in1,
		-- Current state
		state_cur
	)
	begin

		-- Reset the next state value

		state_next <= (others => '0');

		-- Default value to the outputs or output buffers

		out0 <= '0';
		out2 <= '0';
		out3 <= '0';
		out4 <= '0';
		out6 <= '0';
		out8 <= '0';
		out9 <= '0';
		out10 <= '0';
		out16 <= '0';
		out17 <= '0';
		out21 <= '0';
		out22 <= '0';
		out23 <= '0';
		out26 <= '0';
		out27 <= '0';
		out29 <= '0';

		-- For all states, compute the next state bits
		--   And the outputs, and the next value for buffered outputs

		if state_cur(0) = '1' then
			-- Next state
			if (not (in0)) = '1' then
				state_next(0) <= '1';
				-- Next values for buffered outputs
			else
				state_next(16) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
			out0 <= '1';
		end if;

		if state_cur(1) = '1' then
			-- Next state
			if (not (in1)) = '1' then
				state_next(1) <= '1';
				-- Next values for buffered outputs
			else
				state_next(13) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
			out4 <= '1';
			out3 <= '1';
			out2 <= '1';
		end if;

		if state_cur(2) = '1' then
			-- Next state
			state_next(0) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out6 <= '1';
		end if;

		if state_cur(3) = '1' then
			-- Next state
			state_next(2) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(4) = '1' then
			-- Next state
			state_next(3) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out9 <= '1';
			out8 <= '1';
		end if;

		if state_cur(5) = '1' then
			-- Next state
			state_next(4) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out10 <= '1';
			out9 <= '1';
		end if;

		if state_cur(6) = '1' then
			-- Next state
			state_next(5) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		if state_cur(7) = '1' then
			-- Next state
			state_next(6) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out17 <= '1';
			out16 <= '1';
		end if;

		if state_cur(8) = '1' then
			-- Next state
			state_next(7) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(9) = '1' then
			-- Next state
			state_next(8) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out21 <= '1';
		end if;

		if state_cur(10) = '1' then
			-- Next state
			state_next(9) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out23 <= '1';
			out22 <= '1';
		end if;

		if state_cur(11) = '1' then
			-- Next state
			state_next(10) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out26 <= '1';
		end if;

		if state_cur(12) = '1' then
			-- Next state
			state_next(11) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out27 <= '1';
			out23 <= '1';
		end if;

		if state_cur(13) = '1' then
			-- Next state
			state_next(12) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out29 <= '1';
		end if;

		if state_cur(14) = '1' then
			-- Next state
			state_next(1) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out4 <= '1';
		end if;

		if state_cur(15) = '1' then
			-- Next state
			state_next(14) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
		end if;

		-- Info: This is the init/reset state
		if state_cur(16) = '1' then
			-- Next state
			state_next(15) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out17 <= '1';
		end if;

		-- Reset input
		if reset = '1' then
			-- Set the reset state
			state_next <= (16 => '1', others => '0');
			-- Note: Resetting all buffers for outputs here is not necessary.
			-- It would cost hardware. They will be reset at the next clock front.
			-- Reset state: set the buffered outputs
		end if;

	end process;

	-- Assignment of buffered outputs


end architecture;

