library ieee;
use ieee.std_logic_1164.all;

entity top is
	port (
		-- Access 'clock' model 'clock'
		clock : in  std_logic;
		-- Access 'reset' model 'reset'
		reset : in  std_logic;
		-- Access 'stdin' model 'fifo_in'
		stdin_data : in  std_logic_vector(63 downto 0);
		stdin_rdy : out std_logic;
		stdin_ack : in  std_logic;
		-- Access 'stdout' model 'fifo_out'
		stdout_data : out std_logic_vector(63 downto 0);
		stdout_rdy : out std_logic;
		stdout_ack : in  std_logic
	);
end top;

architecture augh of top is

	-- Declaration of components

	component add_14 is
		port (
			-- Ports not part of access
			in_a : in  std_logic_vector(31 downto 0);
			in_b : in  std_logic_vector(31 downto 0);
			result : out std_logic_vector(31 downto 0)
		);
	end component;

	component augh_main_a is
		port (
			-- Access 'ra0' model 'mem_ra'
			ra0_data : out std_logic_vector(31 downto 0);
			ra0_addr : in  std_logic_vector(16 downto 0);
			-- Access 'wa0' model 'mem_wa'
			wa0_en : in  std_logic;
			wa0_data : in  std_logic_vector(31 downto 0);
			wa0_addr : in  std_logic_vector(16 downto 0);
			-- Access 'clk' model 'clock'
			clk : in  std_logic
		);
	end component;

	component augh_main_b is
		port (
			-- Access 'ra0' model 'mem_ra'
			ra0_data : out std_logic_vector(31 downto 0);
			ra0_addr : in  std_logic_vector(16 downto 0);
			-- Access 'wa0' model 'mem_wa'
			wa0_en : in  std_logic;
			wa0_data : in  std_logic_vector(31 downto 0);
			wa0_addr : in  std_logic_vector(16 downto 0);
			-- Access 'clk' model 'clock'
			clk : in  std_logic
		);
	end component;

	component augh_main_c is
		port (
			-- Access 'ra0' model 'mem_ra'
			ra0_data : out std_logic_vector(31 downto 0);
			ra0_addr : in  std_logic_vector(15 downto 0);
			-- Access 'wa0' model 'mem_wa'
			wa0_en : in  std_logic;
			wa0_data : in  std_logic_vector(31 downto 0);
			wa0_addr : in  std_logic_vector(15 downto 0);
			-- Access 'clk' model 'clock'
			clk : in  std_logic
		);
	end component;

	component mul_16 is
		port (
			-- Ports not part of access
			in_a : in  std_logic_vector(31 downto 0);
			in_b : in  std_logic_vector(31 downto 0);
			result : out std_logic_vector(31 downto 0)
		);
	end component;

	component fsm_17 is
		port (
			-- Access 'clock' model 'clock'
			clock : in  std_logic;
			-- Access 'reset' model 'reset'
			reset : in  std_logic;
			-- Ports not part of access
			in4 : in  std_logic;
			out37 : out std_logic;
			out40 : out std_logic;
			out41 : out std_logic;
			out42 : out std_logic;
			out50 : out std_logic;
			out51 : out std_logic;
			out52 : out std_logic;
			out53 : out std_logic;
			out54 : out std_logic;
			out56 : out std_logic;
			out62 : out std_logic;
			out63 : out std_logic;
			out64 : out std_logic;
			out65 : out std_logic;
			out67 : out std_logic;
			out72 : out std_logic;
			out75 : out std_logic;
			out76 : out std_logic;
			out78 : out std_logic;
			out79 : out std_logic;
			in5 : in  std_logic;
			in6 : in  std_logic;
			in7 : in  std_logic;
			in8 : in  std_logic;
			in9 : in  std_logic;
			in10 : in  std_logic;
			out6 : out std_logic;
			out9 : out std_logic;
			in1 : in  std_logic;
			out10 : out std_logic;
			out12 : out std_logic;
			out16 : out std_logic;
			out17 : out std_logic;
			out18 : out std_logic;
			out19 : out std_logic;
			out20 : out std_logic;
			out24 : out std_logic;
			in2 : in  std_logic;
			out27 : out std_logic;
			out28 : out std_logic;
			out31 : out std_logic;
			in3 : in  std_logic;
			out32 : out std_logic;
			out34 : out std_logic;
			in0 : in  std_logic;
			out0 : out std_logic;
			out2 : out std_logic;
			out3 : out std_logic;
			out4 : out std_logic;
			out5 : out std_logic
		);
	end component;

	component sub_15 is
		port (
			-- Ports not part of access
			le : out std_logic;
			in_a : in  std_logic_vector(31 downto 0);
			in_b : in  std_logic_vector(31 downto 0);
			sign : in  std_logic;
			result : out std_logic_vector(31 downto 0)
		);
	end component;

	-- Declaration of signals

	signal sig_clock : std_logic;
	signal sig_reset : std_logic;
	signal augh_test_9 : std_logic;
	signal augh_test_10 : std_logic;
	signal augh_test_11 : std_logic;
	signal augh_test_12 : std_logic;
	signal sig_80 : std_logic_vector(31 downto 0);
	signal sig_81 : std_logic;
	signal sig_82 : std_logic;
	signal sig_83 : std_logic;
	signal sig_84 : std_logic;
	signal sig_85 : std_logic;
	signal sig_86 : std_logic;
	signal sig_87 : std_logic;
	signal sig_88 : std_logic;
	signal sig_89 : std_logic;
	signal sig_90 : std_logic;
	signal sig_91 : std_logic;
	signal sig_92 : std_logic;
	signal sig_93 : std_logic;
	signal sig_94 : std_logic;
	signal sig_95 : std_logic;
	signal sig_96 : std_logic;
	signal sig_97 : std_logic;
	signal sig_98 : std_logic;
	signal sig_99 : std_logic;
	signal sig_100 : std_logic;
	signal sig_101 : std_logic;
	signal sig_102 : std_logic;
	signal sig_103 : std_logic;
	signal sig_104 : std_logic;
	signal sig_105 : std_logic;
	signal sig_106 : std_logic;
	signal augh_test_0 : std_logic;
	signal augh_test_1 : std_logic;
	signal augh_test_4 : std_logic;
	signal augh_test_5 : std_logic;
	signal augh_test_8 : std_logic;
	signal sig_107 : std_logic;
	signal sig_108 : std_logic;
	signal sig_109 : std_logic;
	signal sig_110 : std_logic;
	signal sig_111 : std_logic;
	signal sig_112 : std_logic;
	signal sig_113 : std_logic;
	signal sig_114 : std_logic;
	signal sig_115 : std_logic;
	signal sig_116 : std_logic;
	signal sig_117 : std_logic;
	signal sig_118 : std_logic;
	signal sig_119 : std_logic;
	signal sig_120 : std_logic_vector(31 downto 0);
	signal sig_121 : std_logic_vector(31 downto 0);
	signal sig_122 : std_logic_vector(31 downto 0);
	signal sig_123 : std_logic_vector(31 downto 0);
	signal sig_124 : std_logic_vector(31 downto 0);
	signal sig_125 : std_logic_vector(31 downto 0);
	signal sig_126 : std_logic_vector(15 downto 0);
	signal sig_127 : std_logic_vector(16 downto 0);
	signal sig_128 : std_logic_vector(16 downto 0);

	-- Other inlined components

	signal mux_54 : std_logic_vector(63 downto 0);
	signal read64_ret0_2 : std_logic_vector(63 downto 0) := (others => '0');
	signal read64_ret1_6 : std_logic_vector(63 downto 0) := (others => '0');
	signal mux_41 : std_logic_vector(15 downto 0);
	signal mux_36 : std_logic_vector(31 downto 0);
	signal mux_38 : std_logic_vector(31 downto 0);
	signal read64_u64 : std_logic_vector(63 downto 0) := (others => '0');
	signal mux_34 : std_logic_vector(31 downto 0);
	signal mux_30 : std_logic_vector(31 downto 0);
	signal mux_27 : std_logic_vector(8 downto 0);
	signal mux_24 : std_logic_vector(63 downto 0);
	signal mux_26 : std_logic_vector(31 downto 0);
	signal augh_main_acc : std_logic_vector(31 downto 0) := (others => '0');
	signal augh_main_i : std_logic_vector(31 downto 0) := (others => '0');
	signal augh_main_j : std_logic_vector(31 downto 0) := (others => '0');
	signal augh_main_count : std_logic_vector(31 downto 0) := (others => '0');
	signal augh_main_rbuff : std_logic_vector(63 downto 0) := (others => '0');
	signal augh_main_b_buf : std_logic_vector(31 downto 0) := (others => '0');
	signal augh_main_a_buf : std_logic_vector(31 downto 0) := (others => '0');
	signal mux_67 : std_logic_vector(31 downto 0);
	signal mux_68 : std_logic_vector(31 downto 0);
	signal mux_47 : std_logic_vector(16 downto 0);
	signal mux_51 : std_logic_vector(16 downto 0);
	signal augh_main_c_buf : std_logic_vector(31 downto 0) := (others => '0');

	-- This utility function is used for to generate concatenations of std_logic

	-- Little utility function to ease concatenation of an std_logic
	-- and explicitely return an std_logic_vector
	function repeat(N: natural; B: std_logic) return std_logic_vector is
		variable result: std_logic_vector(N-1 downto 0);
	begin
		result := (others => B);
		return result;
	end;

begin

	-- Instantiation of components

	add_14_i : add_14 port map (
			-- Ports not part of access
		in_a => mux_67,
		in_b => mux_68,
		result => sig_124
	);

	augh_main_a_i : augh_main_a port map (
			-- Access 'ra0' model 'mem_ra'
		ra0_data => sig_123,
		ra0_addr => sig_128,
			-- Access 'wa0' model 'mem_wa'
		wa0_en => sig_108,
		wa0_data => augh_main_rbuff(31 downto 0),
		wa0_addr => mux_51,
			-- Access 'clk' model 'clock'
		clk => sig_clock
	);

	augh_main_b_i : augh_main_b port map (
			-- Access 'ra0' model 'mem_ra'
		ra0_data => sig_122,
		ra0_addr => sig_127,
			-- Access 'wa0' model 'mem_wa'
		wa0_en => sig_114,
		wa0_data => augh_main_rbuff(31 downto 0),
		wa0_addr => mux_47,
			-- Access 'clk' model 'clock'
		clk => sig_clock
	);

	augh_main_c_i : augh_main_c port map (
			-- Access 'ra0' model 'mem_ra'
		ra0_data => sig_121,
		ra0_addr => mux_41,
			-- Access 'wa0' model 'mem_wa'
		wa0_en => sig_87,
		wa0_data => augh_main_acc,
		wa0_addr => sig_126,
			-- Access 'clk' model 'clock'
		clk => sig_clock
	);

	mul_16_i : mul_16 port map (
			-- Ports not part of access
		in_a => augh_main_a_buf,
		in_b => augh_main_b_buf,
		result => sig_120
	);

	fsm_17_i : fsm_17 port map (
			-- Access 'clock' model 'clock'
		clock => sig_clock,
			-- Access 'reset' model 'reset'
		reset => sig_reset,
			-- Ports not part of access
		in4 => augh_test_10,
		out37 => sig_119,
		out40 => sig_118,
		out41 => sig_117,
		out42 => sig_116,
		out50 => sig_115,
		out51 => sig_114,
		out52 => sig_113,
		out53 => sig_112,
		out54 => sig_111,
		out56 => sig_110,
		out62 => sig_109,
		out63 => sig_108,
		out64 => sig_107,
		out65 => sig_106,
		out67 => sig_105,
		out72 => sig_104,
		out75 => sig_103,
		out76 => sig_102,
		out78 => sig_101,
		out79 => sig_100,
		in5 => augh_test_9,
		in6 => augh_test_8,
		in7 => augh_test_5,
		in8 => augh_test_4,
		in9 => augh_test_1,
		in10 => augh_test_0,
		out6 => sig_99,
		out9 => sig_98,
		in1 => stdin_ack,
		out10 => sig_97,
		out12 => sig_96,
		out16 => sig_95,
		out17 => sig_94,
		out18 => sig_93,
		out19 => sig_92,
		out20 => sig_91,
		out24 => sig_90,
		in2 => augh_test_12,
		out27 => sig_89,
		out28 => sig_88,
		out31 => sig_87,
		in3 => augh_test_11,
		out32 => sig_86,
		out34 => sig_85,
		in0 => stdout_ack,
		out0 => stdout_rdy,
		out2 => stdin_rdy,
		out3 => sig_84,
		out4 => sig_83,
		out5 => sig_82
	);

	sub_15_i : sub_15 port map (
			-- Ports not part of access
		le => sig_81,
		in_a => mux_26,
		in_b => sig_125,
		sign => '1',
		result => sig_80
	);

	-- Behaviour of component 'mux_54' model 'mux'
	mux_54 <=
		stdin_data when sig_84 = '1' else
		"0000000000000000000000000000000000000000000000000000000000000000";

	-- Behaviour of component 'mux_41' model 'mux'
	mux_41 <=
		(augh_main_j(7 downto 0) & augh_main_i(7 downto 0)) when sig_100 = '1' else
		(sig_124(7 downto 0) & augh_main_i(7 downto 0)) when sig_102 = '1' else
		"0000000000000000";

	-- Behaviour of component 'mux_36' model 'mux'
	mux_36 <=
		sig_124 when sig_82 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_38' model 'mux'
	mux_38 <=
		sig_124 when sig_119 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_34' model 'mux'
	mux_34 <=
		sig_124 when sig_88 = '1' else
		(sig_124(30 downto 0) & augh_main_j(0)) when sig_97 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_30' model 'mux'
	mux_30 <=
		sig_124 when sig_86 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_27' model 'mux'
	mux_27 <=
		"111111111" when sig_116 = '1' else
		"011111111" when sig_91 = '1' else
		"000000000";

	-- Behaviour of component 'mux_24' model 'mux'
	mux_24 <=
		read64_ret0_2 when sig_106 = '1' else
		(augh_main_rbuff(31 downto 0) & "00000000000000000000000000000000") when sig_93 = '1' else
		(repeat(32, augh_main_rbuff(63)) & augh_main_rbuff(63 downto 32)) when sig_113 = '1' else
		read64_ret1_6 when sig_111 = '1' else
		("00000000000000000000000000000000" & augh_main_c_buf) when sig_95 = '1' else
		"0000000000000000000000000000000000000000000000000000000000000000";

	-- Behaviour of component 'mux_26' model 'mux'
	mux_26 <=
		augh_main_i when sig_90 = '1' else
		augh_main_count when sig_117 = '1' else
		augh_main_j when sig_92 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_67' model 'mux'
	mux_67 <=
		augh_main_acc when sig_119 = '1' else
		(augh_main_j(31) & augh_main_j(31 downto 1)) when sig_97 = '1' else
		augh_main_j when sig_89 = '1' else
		augh_main_count when sig_86 = '1' else
		augh_main_i when sig_82 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_68' model 'mux'
	mux_68 <=
		sig_120 when sig_119 = '1' else
		"00000000000000000000000000000001" when sig_99 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_47' model 'mux'
	mux_47 <=
		(augh_main_j(8 downto 0) & augh_main_i(7 downto 0)) when sig_112 = '1' else
		(sig_124(8 downto 0) & augh_main_i(7 downto 0)) when sig_115 = '1' else
		"00000000000000000";

	-- Behaviour of component 'mux_51' model 'mux'
	mux_51 <=
		(augh_main_j(8 downto 0) & augh_main_i(7 downto 0)) when sig_107 = '1' else
		(sig_124(8 downto 0) & augh_main_i(7 downto 0)) when sig_109 = '1' else
		"00000000000000000";

	-- Behaviour of all components of model 'reg'
	-- Registers with clock = sig_clock and no reset
	process(sig_clock)
	begin
		if rising_edge(sig_clock) then
			if sig_105 = '1' then
				read64_ret0_2 <= read64_u64;
			end if;
			if sig_110 = '1' then
				read64_ret1_6 <= read64_u64;
			end if;
			if sig_83 = '1' then
				read64_u64 <= mux_54;
			end if;
			if sig_118 = '1' then
				augh_main_acc <= mux_38;
			end if;
			if sig_98 = '1' then
				augh_main_i <= mux_36;
			end if;
			if sig_96 = '1' then
				augh_main_j <= mux_34;
			end if;
			if sig_85 = '1' then
				augh_main_count <= mux_30;
			end if;
			if sig_94 = '1' then
				augh_main_rbuff <= mux_24;
			end if;
			if sig_104 = '1' then
				augh_main_b_buf <= sig_122;
			end if;
			if sig_103 = '1' then
				augh_main_a_buf <= sig_123;
			end if;
			if sig_101 = '1' then
				augh_main_c_buf <= sig_121;
			end if;
		end if;
	end process;

	-- Remaining signal assignments
	-- Those who are not assigned by component instantiation

	sig_clock <= clock;
	sig_reset <= reset;
	augh_test_9 <= sig_81;
	augh_test_10 <= sig_81;
	augh_test_11 <= sig_81;
	augh_test_12 <= sig_81;
	augh_test_0 <= sig_81;
	augh_test_1 <= sig_81;
	augh_test_4 <= sig_81;
	augh_test_5 <= sig_81;
	augh_test_8 <= sig_81;
	sig_125 <= "00000000000000000000000" & mux_27;
	sig_126 <= augh_main_i(7 downto 0) & augh_main_j(7 downto 0);
	sig_127 <= augh_main_count(8 downto 0) & augh_main_i(7 downto 0);
	sig_128 <= augh_main_count(8 downto 0) & augh_main_j(7 downto 0);

	-- Remaining top-level ports assignments
	-- Those who are not assigned by component instantiation

	stdout_data <= "0000000000000000000000000000000000000000000000000000000000000000";

end architecture;
