library ieee;
use ieee.std_logic_1164.all;

library ieee;
use ieee.numeric_std.all;

entity mul_16 is
	port (
		-- Ports not part of access
		in_a : in  std_logic_vector(31 downto 0);
		in_b : in  std_logic_vector(31 downto 0);
		result : out std_logic_vector(31 downto 0)
	);
end mul_16;

architecture augh of mul_16 is

	signal tmp_res : signed(63 downto 0);

	-- Little utility functions to make VHDL syntactically correct
	--   with the syntax signed(vector) when 'vector' is a std_logic.
	--   This happens when data signals are 1-bit large, for example.

	function to_signed(B: std_logic) return signed is
		variable V: signed(0 downto 0);
	begin
		V(0) := B;
		return V;
	end;
	function to_signed(V: std_logic_vector) return signed is
	begin
		return signed(V);
	end;

	function to_unsigned(B: std_logic) return unsigned is
		variable V: unsigned(0 downto 0);
	begin
		V(0) := B;
		return V;
	end;
	function to_unsigned(V: std_logic_vector) return unsigned is
	begin
		return unsigned(V);
	end;

begin

	-- The actual multiplication
	tmp_res <= to_signed(in_a) * to_signed(in_b);

	-- Set the output
	result <= std_logic_vector(tmp_res(31 downto 0));

end architecture;
