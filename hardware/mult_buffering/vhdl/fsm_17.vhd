library ieee;
use ieee.std_logic_1164.all;

library ieee;
use ieee.numeric_std.all;

entity fsm_17 is
	port (
		-- Access 'clock' model 'clock'
		clock : in  std_logic;
		-- Access 'reset' model 'reset'
		reset : in  std_logic;
		-- Ports not part of access
		in4 : in  std_logic;
		out37 : out std_logic;
		out40 : out std_logic;
		out41 : out std_logic;
		out42 : out std_logic;
		out50 : out std_logic;
		out51 : out std_logic;
		out52 : out std_logic;
		out53 : out std_logic;
		out54 : out std_logic;
		out56 : out std_logic;
		out62 : out std_logic;
		out63 : out std_logic;
		out64 : out std_logic;
		out65 : out std_logic;
		out67 : out std_logic;
		out72 : out std_logic;
		out75 : out std_logic;
		out76 : out std_logic;
		out78 : out std_logic;
		out79 : out std_logic;
		in5 : in  std_logic;
		in6 : in  std_logic;
		in7 : in  std_logic;
		in8 : in  std_logic;
		in9 : in  std_logic;
		in10 : in  std_logic;
		out6 : out std_logic;
		out9 : out std_logic;
		in1 : in  std_logic;
		out10 : out std_logic;
		out12 : out std_logic;
		out16 : out std_logic;
		out17 : out std_logic;
		out18 : out std_logic;
		out19 : out std_logic;
		out20 : out std_logic;
		out24 : out std_logic;
		in2 : in  std_logic;
		out27 : out std_logic;
		out28 : out std_logic;
		out31 : out std_logic;
		in3 : in  std_logic;
		out32 : out std_logic;
		out34 : out std_logic;
		in0 : in  std_logic;
		out0 : out std_logic;
		out2 : out std_logic;
		out3 : out std_logic;
		out4 : out std_logic;
		out5 : out std_logic
	);
end fsm_17;

architecture augh of fsm_17 is

	signal state_cur  : std_logic_vector(0 to 56) := (52 => '1', others => '0');
	signal state_next : std_logic_vector(0 to 56) := (52 => '1', others => '0');

	-- Buffers for outputs
	signal out3_buf : std_logic := '0';
	signal out3_bufn : std_logic;
	signal out5_buf : std_logic := '0';
	signal out5_bufn : std_logic;
	signal out6_buf : std_logic := '0';
	signal out6_bufn : std_logic;
	signal out10_buf : std_logic := '0';
	signal out10_bufn : std_logic;
	signal out16_buf : std_logic := '0';
	signal out16_bufn : std_logic;
	signal out19_buf : std_logic := '0';
	signal out19_bufn : std_logic;
	signal out20_buf : std_logic := '0';
	signal out20_bufn : std_logic;
	signal out24_buf : std_logic := '0';
	signal out24_bufn : std_logic;
	signal out27_buf : std_logic := '0';
	signal out27_bufn : std_logic;
	signal out42_buf : std_logic := '0';
	signal out42_bufn : std_logic;
	signal out52_buf : std_logic := '0';
	signal out52_bufn : std_logic;

begin

	-- Sequential process
	-- Set the current state

	process (clock)
	begin
		if rising_edge(clock) then

			-- Next state
			state_cur <= state_next;
			-- Buffers for outputs
			out3_buf <= out3_bufn;
			out5_buf <= out5_bufn;
			out6_buf <= out6_bufn;
			out10_buf <= out10_bufn;
			out16_buf <= out16_bufn;
			out19_buf <= out19_bufn;
			out20_buf <= out20_bufn;
			out24_buf <= out24_bufn;
			out27_buf <= out27_bufn;
			out42_buf <= out42_bufn;
			out52_buf <= out52_bufn;

		end if;
	end process;

	-- Combinatorial process
	-- Compute the next state
	-- Compute the outputs

	process (
		-- Inputs of the FSM
		reset, in4, in5, in6, in7, in8, in9, in10, in1, in2, in3, in0,
		-- Current state
		state_cur
	)
	begin

		-- Reset the next state value

		state_next <= (others => '0');

		-- Default value to the outputs or output buffers

		out0 <= '0';
		out2 <= '0';
		out3_bufn <= '0';
		out4 <= '0';
		out5_bufn <= '0';
		out6_bufn <= '0';
		out9 <= '0';
		out10_bufn <= '0';
		out12 <= '0';
		out16_bufn <= '0';
		out17 <= '0';
		out18 <= '0';
		out19_bufn <= '0';
		out20_bufn <= '0';
		out24_bufn <= '0';
		out27_bufn <= '0';
		out28 <= '0';
		out31 <= '0';
		out32 <= '0';
		out34 <= '0';
		out37 <= '0';
		out40 <= '0';
		out41 <= '0';
		out42_bufn <= '0';
		out50 <= '0';
		out51 <= '0';
		out52_bufn <= '0';
		out53 <= '0';
		out54 <= '0';
		out56 <= '0';
		out62 <= '0';
		out63 <= '0';
		out64 <= '0';
		out65 <= '0';
		out67 <= '0';
		out72 <= '0';
		out75 <= '0';
		out76 <= '0';
		out78 <= '0';
		out79 <= '0';

		-- For all states, compute the next state bits
		--   And the outputs, and the next value for buffered outputs

		if state_cur(0) = '1' then
			-- Next state
			if (not (in0)) = '1' then
				state_next(0) <= '1';
				-- Next values for buffered outputs
			else
				state_next(4) <= '1';
				-- Next values for buffered outputs
				out10_bufn <= '1';
				out6_bufn <= '1';
			end if;
			-- Assignment of non-buffered outputs
			out0 <= '1';
		end if;

		if state_cur(1) = '1' then
			-- Next state
			if (not (in1)) = '1' then
				state_next(1) <= '1';
				-- Next values for buffered outputs
				out3_bufn <= '1';
			else
				state_next(46) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
			out4 <= '1';
			out2 <= '1';
		end if;

		if state_cur(2) = '1' then
			-- Next state
			if (not (in1)) = '1' then
				state_next(2) <= '1';
				-- Next values for buffered outputs
				out3_bufn <= '1';
			else
				state_next(33) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
			out4 <= '1';
			out2 <= '1';
		end if;

		if state_cur(3) = '1' then
			-- Next state
			state_next(12) <= '1';
			-- Next values for buffered outputs
			out24_bufn <= '1';
			out20_bufn <= '1';
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		if state_cur(4) = '1' then
			-- Next state
			state_next(10) <= '1';
			-- Next values for buffered outputs
			out20_bufn <= '1';
			out19_bufn <= '1';
			-- Assignment of non-buffered outputs
			out12 <= '1';
		end if;

		if state_cur(5) = '1' then
			-- Next state
			state_next(0) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(6) = '1' then
			-- Next state
			state_next(5) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(7) = '1' then
			-- Next state
			state_next(6) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out17 <= '1';
		end if;

		if state_cur(8) = '1' then
			-- Next state
			state_next(55) <= '1';
			-- Next values for buffered outputs
			out27_bufn <= '1';
			out6_bufn <= '1';
			-- Assignment of non-buffered outputs
			out18 <= '1';
			out17 <= '1';
		end if;

		if state_cur(9) = '1' then
			-- Next state
			state_next(8) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out17 <= '1';
		end if;

		if state_cur(10) = '1' then
			-- Next state
			if (in2) = '1' then
				state_next(56) <= '1';
				-- Next values for buffered outputs
			else
				state_next(3) <= '1';
				-- Next values for buffered outputs
				out6_bufn <= '1';
				out5_bufn <= '1';
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(11) = '1' then
			-- Next state
			state_next(10) <= '1';
			-- Next values for buffered outputs
			out20_bufn <= '1';
			out19_bufn <= '1';
			-- Assignment of non-buffered outputs
			out12 <= '1';
		end if;

		if state_cur(12) = '1' then
			-- Next state
			if (in3) = '1' then
				state_next(11) <= '1';
				-- Next values for buffered outputs
			else
				state_next(52) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(13) = '1' then
			-- Next state
			state_next(12) <= '1';
			-- Next values for buffered outputs
			out24_bufn <= '1';
			out20_bufn <= '1';
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		if state_cur(14) = '1' then
			-- Next state
			state_next(24) <= '1';
			-- Next values for buffered outputs
			out24_bufn <= '1';
			out20_bufn <= '1';
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		if state_cur(15) = '1' then
			-- Next state
			state_next(22) <= '1';
			-- Next values for buffered outputs
			out20_bufn <= '1';
			out19_bufn <= '1';
			-- Assignment of non-buffered outputs
			out28 <= '1';
			out12 <= '1';
		end if;

		if state_cur(16) = '1' then
			-- Next state
			state_next(15) <= '1';
			-- Next values for buffered outputs
			out27_bufn <= '1';
			out6_bufn <= '1';
			-- Assignment of non-buffered outputs
			out31 <= '1';
		end if;

		if state_cur(17) = '1' then
			-- Next state
			state_next(19) <= '1';
			-- Next values for buffered outputs
			out42_bufn <= '1';
			-- Assignment of non-buffered outputs
			out34 <= '1';
			out32 <= '1';
		end if;

		if state_cur(18) = '1' then
			-- Next state
			state_next(17) <= '1';
			-- Next values for buffered outputs
			out6_bufn <= '1';
			-- Assignment of non-buffered outputs
			out40 <= '1';
			out37 <= '1';
		end if;

		if state_cur(19) = '1' then
			-- Next state
			if (in4) = '1' then
				state_next(53) <= '1';
				-- Next values for buffered outputs
			else
				state_next(16) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
			out41 <= '1';
		end if;

		if state_cur(20) = '1' then
			-- Next state
			state_next(19) <= '1';
			-- Next values for buffered outputs
			out42_bufn <= '1';
			-- Assignment of non-buffered outputs
			out34 <= '1';
		end if;

		if state_cur(21) = '1' then
			-- Next state
			state_next(20) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out40 <= '1';
		end if;

		if state_cur(22) = '1' then
			-- Next state
			if (in5) = '1' then
				state_next(21) <= '1';
				-- Next values for buffered outputs
			else
				state_next(14) <= '1';
				-- Next values for buffered outputs
				out6_bufn <= '1';
				out5_bufn <= '1';
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(23) = '1' then
			-- Next state
			state_next(22) <= '1';
			-- Next values for buffered outputs
			out20_bufn <= '1';
			out19_bufn <= '1';
			-- Assignment of non-buffered outputs
			out12 <= '1';
		end if;

		if state_cur(24) = '1' then
			-- Next state
			if (in6) = '1' then
				state_next(23) <= '1';
				-- Next values for buffered outputs
			else
				state_next(13) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(25) = '1' then
			-- Next state
			state_next(24) <= '1';
			-- Next values for buffered outputs
			out24_bufn <= '1';
			out20_bufn <= '1';
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		if state_cur(26) = '1' then
			-- Next state
			state_next(37) <= '1';
			-- Next values for buffered outputs
			out24_bufn <= '1';
			out20_bufn <= '1';
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		if state_cur(27) = '1' then
			-- Next state
			state_next(35) <= '1';
			-- Next values for buffered outputs
			out42_bufn <= '1';
			out19_bufn <= '1';
			-- Assignment of non-buffered outputs
			out12 <= '1';
		end if;

		if state_cur(28) = '1' then
			-- Next state
			state_next(27) <= '1';
			-- Next values for buffered outputs
			out10_bufn <= '1';
			out6_bufn <= '1';
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(29) = '1' then
			-- Next state
			state_next(28) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out51 <= '1';
			out50 <= '1';
		end if;

		if state_cur(30) = '1' then
			-- Next state
			state_next(29) <= '1';
			-- Next values for buffered outputs
			out27_bufn <= '1';
			out6_bufn <= '1';
			-- Assignment of non-buffered outputs
			out17 <= '1';
		end if;

		if state_cur(31) = '1' then
			-- Next state
			state_next(30) <= '1';
			-- Next values for buffered outputs
			out52_bufn <= '1';
			-- Assignment of non-buffered outputs
			out53 <= '1';
			out51 <= '1';
		end if;

		if state_cur(32) = '1' then
			-- Next state
			state_next(31) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out54 <= '1';
			out17 <= '1';
		end if;

		if state_cur(33) = '1' then
			-- Next state
			state_next(32) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out56 <= '1';
		end if;

		if state_cur(34) = '1' then
			-- Next state
			state_next(2) <= '1';
			-- Next values for buffered outputs
			out3_bufn <= '1';
			-- Assignment of non-buffered outputs
			out4 <= '1';
		end if;

		if state_cur(35) = '1' then
			-- Next state
			if (in7) = '1' then
				state_next(34) <= '1';
				-- Next values for buffered outputs
			else
				state_next(26) <= '1';
				-- Next values for buffered outputs
				out6_bufn <= '1';
				out5_bufn <= '1';
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(36) = '1' then
			-- Next state
			state_next(35) <= '1';
			-- Next values for buffered outputs
			out42_bufn <= '1';
			out19_bufn <= '1';
			-- Assignment of non-buffered outputs
			out12 <= '1';
		end if;

		if state_cur(37) = '1' then
			-- Next state
			if (in8) = '1' then
				state_next(36) <= '1';
				-- Next values for buffered outputs
			else
				state_next(25) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(38) = '1' then
			-- Next state
			state_next(37) <= '1';
			-- Next values for buffered outputs
			out24_bufn <= '1';
			out20_bufn <= '1';
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		if state_cur(39) = '1' then
			-- Next state
			state_next(50) <= '1';
			-- Next values for buffered outputs
			out24_bufn <= '1';
			out20_bufn <= '1';
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		if state_cur(40) = '1' then
			-- Next state
			state_next(48) <= '1';
			-- Next values for buffered outputs
			out42_bufn <= '1';
			out19_bufn <= '1';
			-- Assignment of non-buffered outputs
			out12 <= '1';
		end if;

		if state_cur(41) = '1' then
			-- Next state
			state_next(40) <= '1';
			-- Next values for buffered outputs
			out10_bufn <= '1';
			out6_bufn <= '1';
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(42) = '1' then
			-- Next state
			state_next(41) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out63 <= '1';
			out62 <= '1';
		end if;

		if state_cur(43) = '1' then
			-- Next state
			state_next(42) <= '1';
			-- Next values for buffered outputs
			out27_bufn <= '1';
			out6_bufn <= '1';
			-- Assignment of non-buffered outputs
			out17 <= '1';
		end if;

		if state_cur(44) = '1' then
			-- Next state
			state_next(43) <= '1';
			-- Next values for buffered outputs
			out52_bufn <= '1';
			-- Assignment of non-buffered outputs
			out64 <= '1';
			out63 <= '1';
		end if;

		if state_cur(45) = '1' then
			-- Next state
			state_next(44) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out65 <= '1';
			out17 <= '1';
		end if;

		if state_cur(46) = '1' then
			-- Next state
			state_next(45) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out67 <= '1';
		end if;

		if state_cur(47) = '1' then
			-- Next state
			state_next(1) <= '1';
			-- Next values for buffered outputs
			out3_bufn <= '1';
			-- Assignment of non-buffered outputs
			out4 <= '1';
		end if;

		if state_cur(48) = '1' then
			-- Next state
			if (in9) = '1' then
				state_next(47) <= '1';
				-- Next values for buffered outputs
			else
				state_next(39) <= '1';
				-- Next values for buffered outputs
				out6_bufn <= '1';
				out5_bufn <= '1';
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(49) = '1' then
			-- Next state
			state_next(48) <= '1';
			-- Next values for buffered outputs
			out42_bufn <= '1';
			out19_bufn <= '1';
			-- Assignment of non-buffered outputs
			out12 <= '1';
		end if;

		if state_cur(50) = '1' then
			-- Next state
			if (in10) = '1' then
				state_next(49) <= '1';
				-- Next values for buffered outputs
			else
				state_next(38) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(51) = '1' then
			-- Next state
			state_next(50) <= '1';
			-- Next values for buffered outputs
			out24_bufn <= '1';
			out20_bufn <= '1';
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		-- Info: This is the init/reset state
		if state_cur(52) = '1' then
			-- Next state
			state_next(51) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out40 <= '1';
		end if;

		if state_cur(53) = '1' then
			-- Next state
			state_next(54) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out72 <= '1';
		end if;

		if state_cur(54) = '1' then
			-- Next state
			state_next(18) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out75 <= '1';
		end if;

		if state_cur(55) = '1' then
			-- Next state
			state_next(7) <= '1';
			-- Next values for buffered outputs
			out16_bufn <= '1';
			-- Assignment of non-buffered outputs
			out78 <= '1';
			out76 <= '1';
		end if;

		if state_cur(56) = '1' then
			-- Next state
			state_next(9) <= '1';
			-- Next values for buffered outputs
			out16_bufn <= '1';
			-- Assignment of non-buffered outputs
			out78 <= '1';
			out79 <= '1';
		end if;

		-- Reset input
		if reset = '1' then
			-- Set the reset state
			state_next <= (52 => '1', others => '0');
			-- Note: Resetting all buffers for outputs here is not necessary.
			-- It would cost hardware. They will be reset at the next clock front.
			-- Reset state: set the buffered outputs
		end if;

	end process;

	-- Assignment of buffered outputs

	out3 <= out3_buf;
	out5 <= out5_buf;
	out6 <= out6_buf;
	out10 <= out10_buf;
	out16 <= out16_buf;
	out19 <= out19_buf;
	out20 <= out20_buf;
	out24 <= out24_buf;
	out27 <= out27_buf;
	out42 <= out42_buf;
	out52 <= out52_buf;

end architecture;

