#include<augh.h>

#define k 512
#define nxC 256
#define nyC 256

//if(1){}
#define MYREAD(a,b,big) do{a=big;big=big>>32;b=big;}while(0) 
#define MYWRITE(a,b,big) do{big=a;big=big<<32;big=b;}while(0)

#define DDOT(a,b,aout) for(count=0;count<len;count++) aout+=a[count]*b[count];

int64_t stdin;
int64_t stdout;

static inline uint64_t read64(){
	uint64_t u64 =0;
	fifo_read(stdin,&u64);
	return u64;
}

static inline void write64(uint8_t u64){
	fifo_write(stdout, &u64);
}

void augh_main(){
	uint32_t a[k][nxC],b[k][nyC],c[nxC][nyC];
	int32_t acc=0;
	int32_t i,j,count;

	int64_t rbuff;
	
//	int32_t offset=0;
	int64_t result;
	for(i=0; i<nxC; i++){
		for(j=0; j<k; j+=2){
			rbuff=read64();
			MYREAD(a[j][i],a[j+1][i],rbuff);
		}
	}
	for(i=0; i<nxC; i++){
		for(j=0; j<k; j+=2){
			rbuff=read64();
			MYREAD(b[j][i],b[j+1][i],rbuff);
		}
	}

	for(i=0; i<nxC; i++){
		for(j=0; j<nxC; j++){
			acc=0;
			for(count=0; count<k; count++){
				acc+= a[count][j]*b[count][i];
			}
			c[i][j]=acc;
		}
	}
		
	for(i=0; i<nxC; i++){
		for(j=0; j<nxC; j+=2){
			MYWRITE(c[j][i],c[j+1][i],rbuff);
			write64(result);
		}
	}

}
