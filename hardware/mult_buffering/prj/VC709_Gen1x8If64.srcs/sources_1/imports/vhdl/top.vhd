library ieee;
use ieee.std_logic_1164.all;

entity top is
	port (
		-- Access 'clock' model 'clock'
		clock : in  std_logic;
		-- Access 'reset' model 'reset'
		reset : in  std_logic;
		-- Access 'stdout' model 'fifo_out'
		stdout_data : out std_logic_vector(63 downto 0);
		stdout_rdy : out std_logic;
		stdout_ack : in  std_logic;
		-- Access 'stdin' model 'fifo_in'
		stdin_data : in  std_logic_vector(63 downto 0);
		stdin_rdy : out std_logic;
		stdin_ack : in  std_logic
	);
end top;

architecture augh of top is

	-- Declaration of components

	component augh_main_a is
		port (
			-- Access 'ra0' model 'mem_ra'
			ra0_data : out std_logic_vector(31 downto 0);
			ra0_addr : in  std_logic_vector(1 downto 0);
			-- Access 'clk' model 'clock'
			clk : in  std_logic;
			-- Access 'wa0' model 'mem_wa'
			wa0_data : in  std_logic_vector(31 downto 0);
			wa0_addr : in  std_logic_vector(1 downto 0);
			wa0_en : in  std_logic
		);
	end component;

	component augh_main_b is
		port (
			-- Access 'ra0' model 'mem_ra'
			ra0_data : out std_logic_vector(31 downto 0);
			ra0_addr : in  std_logic_vector(1 downto 0);
			-- Access 'clk' model 'clock'
			clk : in  std_logic;
			-- Access 'wa0' model 'mem_wa'
			wa0_data : in  std_logic_vector(31 downto 0);
			wa0_addr : in  std_logic_vector(1 downto 0);
			wa0_en : in  std_logic
		);
	end component;

	component mul_10 is
		port (
			-- Ports not part of access
			in_b : in  std_logic_vector(31 downto 0);
			in_a : in  std_logic_vector(31 downto 0);
			result : out std_logic_vector(31 downto 0)
		);
	end component;

	component fsm_11 is
		port (
			-- Access 'clock' model 'clock'
			clock : in  std_logic;
			-- Access 'reset' model 'reset'
			reset : in  std_logic;
			-- Ports not part of access
			out12 : out std_logic;
			out13 : out std_logic;
			out15 : out std_logic;
			out16 : out std_logic;
			out21 : out std_logic;
			out24 : out std_logic;
			out30 : out std_logic;
			out34 : out std_logic;
			out35 : out std_logic;
			out36 : out std_logic;
			out37 : out std_logic;
			out38 : out std_logic;
			out39 : out std_logic;
			out41 : out std_logic;
			out46 : out std_logic;
			out47 : out std_logic;
			out48 : out std_logic;
			out49 : out std_logic;
			out51 : out std_logic;
			in3 : in  std_logic;
			in4 : in  std_logic;
			in0 : in  std_logic;
			out0 : out std_logic;
			out2 : out std_logic;
			out3 : out std_logic;
			out4 : out std_logic;
			out6 : out std_logic;
			out8 : out std_logic;
			out9 : out std_logic;
			in1 : in  std_logic;
			out10 : out std_logic;
			in2 : in  std_logic
		);
	end component;

	component sub_9 is
		port (
			-- Ports not part of access
			le : out std_logic;
			sign : in  std_logic;
			in_b : in  std_logic_vector(31 downto 0);
			in_a : in  std_logic_vector(31 downto 0);
			result : out std_logic_vector(31 downto 0)
		);
	end component;

	component add_8 is
		port (
			-- Ports not part of access
			in_b : in  std_logic_vector(31 downto 0);
			in_a : in  std_logic_vector(31 downto 0);
			result : out std_logic_vector(31 downto 0)
		);
	end component;

	-- Declaration of signals

	signal sig_clock : std_logic;
	signal sig_reset : std_logic;
	signal sig_82 : std_logic;
	signal sig_83 : std_logic;
	signal sig_84 : std_logic_vector(31 downto 0);
	signal sig_85 : std_logic_vector(31 downto 0);
	signal sig_86 : std_logic_vector(31 downto 0);
	signal sig_56 : std_logic_vector(31 downto 0);
	signal sig_57 : std_logic_vector(31 downto 0);
	signal sig_58 : std_logic;
	signal sig_59 : std_logic;
	signal sig_60 : std_logic;
	signal sig_61 : std_logic;
	signal sig_62 : std_logic;
	signal sig_63 : std_logic;
	signal sig_64 : std_logic;
	signal sig_65 : std_logic;
	signal sig_66 : std_logic;
	signal sig_67 : std_logic;
	signal sig_68 : std_logic;
	signal sig_69 : std_logic;
	signal sig_70 : std_logic;
	signal sig_71 : std_logic;
	signal sig_72 : std_logic;
	signal sig_73 : std_logic;
	signal sig_74 : std_logic;
	signal sig_75 : std_logic;
	signal sig_76 : std_logic;
	signal sig_77 : std_logic;
	signal sig_78 : std_logic;
	signal sig_79 : std_logic;
	signal sig_80 : std_logic;
	signal sig_81 : std_logic;
	signal augh_test_0 : std_logic;
	signal augh_test_3 : std_logic;
	signal augh_test_6 : std_logic;

	-- Other inlined components

	signal mux_29 : std_logic_vector(31 downto 0);
	signal write64_u64 : std_logic_vector(7 downto 0) := (others => '0');
	signal read64_ret0_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal read64_ret1_4 : std_logic_vector(63 downto 0) := (others => '0');
	signal mux_25 : std_logic_vector(31 downto 0);
	signal read64_u64 : std_logic_vector(63 downto 0) := (others => '0');
	signal mux_32 : std_logic_vector(1 downto 0);
	signal mux_19 : std_logic_vector(63 downto 0);
	signal mux_16 : std_logic_vector(31 downto 0);
	signal augh_main_acc : std_logic_vector(31 downto 0) := (others => '0');
	signal mux_12 : std_logic_vector(7 downto 0);
	signal augh_main_count : std_logic_vector(31 downto 0) := (others => '0');
	signal augh_main_rbuff : std_logic_vector(63 downto 0) := (others => '0');
	signal augh_main_result : std_logic_vector(7 downto 0) := (others => '0');
	signal mux_36 : std_logic_vector(1 downto 0);
	signal mux_39 : std_logic_vector(63 downto 0);
	signal mux_14 : std_logic_vector(31 downto 0);

	-- This utility function is used for to generate concatenations of std_logic

	-- Little utility function to ease concatenation of an std_logic
	-- and explicitely return an std_logic_vector
	function repeat(N: natural; B: std_logic) return std_logic_vector is
		variable result: std_logic_vector(N-1 downto 0);
	begin
		result := (others => B);
		return result;
	end;

begin

	-- Instantiation of components

	augh_main_a_i : augh_main_a port map (
			-- Access 'ra0' model 'mem_ra'
		ra0_data => sig_86,
		ra0_addr => augh_main_count(1 downto 0),
			-- Access 'clk' model 'clock'
		clk => sig_clock,
			-- Access 'wa0' model 'mem_wa'
		wa0_data => augh_main_rbuff(31 downto 0),
		wa0_addr => mux_36,
		wa0_en => sig_68
	);

	augh_main_b_i : augh_main_b port map (
			-- Access 'ra0' model 'mem_ra'
		ra0_data => sig_85,
		ra0_addr => augh_main_count(1 downto 0),
			-- Access 'clk' model 'clock'
		clk => sig_clock,
			-- Access 'wa0' model 'mem_wa'
		wa0_data => augh_main_rbuff(31 downto 0),
		wa0_addr => mux_32,
		wa0_en => sig_75
	);

	mul_10_i : mul_10 port map (
			-- Ports not part of access
		in_b => sig_85,
		in_a => sig_86,
		result => sig_84
	);

	fsm_11_i : fsm_11 port map (
			-- Access 'clock' model 'clock'
		clock => sig_clock,
			-- Access 'reset' model 'reset'
		reset => sig_reset,
			-- Ports not part of access
		out12 => sig_83,
		out13 => sig_82,
		out15 => sig_81,
		out16 => sig_80,
		out21 => sig_79,
		out24 => sig_78,
		out30 => sig_77,
		out34 => sig_76,
		out35 => sig_75,
		out36 => sig_74,
		out37 => sig_73,
		out38 => sig_72,
		out39 => sig_71,
		out41 => sig_70,
		out46 => sig_69,
		out47 => sig_68,
		out48 => sig_67,
		out49 => sig_66,
		out51 => sig_65,
		in3 => augh_test_3,
		in4 => augh_test_0,
		in0 => stdout_ack,
		out0 => stdout_rdy,
		out2 => stdin_rdy,
		out3 => sig_64,
		out4 => sig_63,
		out6 => sig_62,
		out8 => sig_61,
		out9 => sig_60,
		in1 => stdin_ack,
		out10 => sig_59,
		in2 => augh_test_6
	);

	sub_9_i : sub_9 port map (
			-- Ports not part of access
		le => sig_58,
		sign => '1',
		in_b => "00000000000000000000000000000011",
		in_a => augh_main_count,
		result => sig_57
	);

	add_8_i : add_8 port map (
			-- Ports not part of access
		in_b => mux_14,
		in_a => mux_16,
		result => sig_56
	);

	-- Behaviour of component 'mux_29' model 'mux'
	mux_29 <=
		sig_56 when sig_79 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_25' model 'mux'
	mux_25 <=
		sig_56 when sig_81 = '1' else
		(sig_56(30 downto 0) & augh_main_count(0)) when sig_77 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_32' model 'mux'
	mux_32 <=
		sig_56(1 downto 0) when sig_76 = '1' else
		augh_main_count(1 downto 0) when sig_72 = '1' else
		"00";

	-- Behaviour of component 'mux_19' model 'mux'
	mux_19 <=
		(repeat(32, augh_main_rbuff(63)) & augh_main_rbuff(63 downto 32)) when sig_74 = '1' else
		read64_ret1_4 when sig_71 = '1' else
		read64_ret0_1 when sig_66 = '1' else
		"0000000000000000000000000000000000000000000000000000000000000000";

	-- Behaviour of component 'mux_16' model 'mux'
	mux_16 <=
		augh_main_count when sig_83 = '1' else
		augh_main_acc when sig_79 = '1' else
		(augh_main_count(31) & augh_main_count(31 downto 1)) when sig_77 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of component 'mux_12' model 'mux'
	mux_12 <=
		augh_main_acc(7 downto 0) when sig_61 = '1' else
		"00000000" when sig_59 = '1' else
		"00000000";

	-- Behaviour of component 'mux_36' model 'mux'
	mux_36 <=
		sig_56(1 downto 0) when sig_69 = '1' else
		augh_main_count(1 downto 0) when sig_67 = '1' else
		"00";

	-- Behaviour of component 'mux_39' model 'mux'
	mux_39 <=
		stdin_data when sig_64 = '1' else
		"0000000000000000000000000000000000000000000000000000000000000000";

	-- Behaviour of component 'mux_14' model 'mux'
	mux_14 <=
		"00000000000000000000000000000001" when sig_82 = '1' else
		sig_84 when sig_79 = '1' else
		"00000000000000000000000000000000";

	-- Behaviour of all components of model 'reg'
	-- Registers with clock = sig_clock and no reset
	process(sig_clock)
	begin
		if rising_edge(sig_clock) then
			if sig_62 = '1' then
				write64_u64 <= augh_main_result;
			end if;
			if sig_65 = '1' then
				read64_ret0_1 <= read64_u64;
			end if;
			if sig_70 = '1' then
				read64_ret1_4 <= read64_u64;
			end if;
			if sig_63 = '1' then
				read64_u64 <= mux_39;
			end if;
			if sig_78 = '1' then
				augh_main_acc <= mux_29;
			end if;
			if sig_80 = '1' then
				augh_main_count <= mux_25;
			end if;
			if sig_73 = '1' then
				augh_main_rbuff <= mux_19;
			end if;
			if sig_60 = '1' then
				augh_main_result <= mux_12;
			end if;
		end if;
	end process;

	-- Remaining signal assignments
	-- Those who are not assigned by component instantiation

	sig_clock <= clock;
	sig_reset <= reset;
	augh_test_0 <= sig_58;
	augh_test_3 <= sig_58;
	augh_test_6 <= sig_58;

	-- Remaining top-level ports assignments
	-- Those who are not assigned by component instantiation

	stdout_data <= "00000000000000000000000000000000000000000000000000000000" & write64_u64;

end architecture;
