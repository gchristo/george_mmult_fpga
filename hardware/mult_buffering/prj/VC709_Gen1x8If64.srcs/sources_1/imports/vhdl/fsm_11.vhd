library ieee;
use ieee.std_logic_1164.all;

library ieee;
use ieee.numeric_std.all;

entity fsm_11 is
	port (
		-- Access 'clock' model 'clock'
		clock : in  std_logic;
		-- Access 'reset' model 'reset'
		reset : in  std_logic;
		-- Ports not part of access
		out12 : out std_logic;
		out13 : out std_logic;
		out15 : out std_logic;
		out16 : out std_logic;
		out21 : out std_logic;
		out24 : out std_logic;
		out30 : out std_logic;
		out34 : out std_logic;
		out35 : out std_logic;
		out36 : out std_logic;
		out37 : out std_logic;
		out38 : out std_logic;
		out39 : out std_logic;
		out41 : out std_logic;
		out46 : out std_logic;
		out47 : out std_logic;
		out48 : out std_logic;
		out49 : out std_logic;
		out51 : out std_logic;
		in3 : in  std_logic;
		in4 : in  std_logic;
		in0 : in  std_logic;
		out0 : out std_logic;
		out2 : out std_logic;
		out3 : out std_logic;
		out4 : out std_logic;
		out6 : out std_logic;
		out8 : out std_logic;
		out9 : out std_logic;
		in1 : in  std_logic;
		out10 : out std_logic;
		in2 : in  std_logic
	);
end fsm_11;

architecture augh of fsm_11 is

	signal state_cur  : std_logic_vector(0 to 33) := (33 => '1', others => '0');
	signal state_next : std_logic_vector(0 to 33) := (33 => '1', others => '0');

	-- Buffers for outputs
	signal out3_buf : std_logic := '0';
	signal out3_bufn : std_logic;
	signal out12_buf : std_logic := '0';
	signal out12_bufn : std_logic;
	signal out13_buf : std_logic := '0';
	signal out13_bufn : std_logic;
	signal out30_buf : std_logic := '0';
	signal out30_bufn : std_logic;
	signal out36_buf : std_logic := '0';
	signal out36_bufn : std_logic;

	-- Retiming: counters
	signal rtmcounter0 :      unsigned(4 downto 0) := (others => '0');
	signal rtmcounter0_next : unsigned(4 downto 0);

	-- Retiming: Output of comparators
	signal rtmcmp9 : std_logic;

begin

	-- Sequential process
	-- Set the current state

	process (clock)
	begin
		if rising_edge(clock) then

			-- Next state
			state_cur <= state_next;
			-- Buffers for outputs
			out3_buf <= out3_bufn;
			out12_buf <= out12_bufn;
			out13_buf <= out13_bufn;
			out30_buf <= out30_bufn;
			out36_buf <= out36_bufn;
			-- Retiming: counters
			rtmcounter0 <= rtmcounter0_next;

		end if;
	end process;

	-- Combinatorial process
	-- Compute the next state
	-- Compute the outputs

	process (
		-- Inputs of the FSM
		reset, in3, in4, in0, in1, in2,
		-- Retiming: outputs of the comparators
		rtmcmp9,
		-- Retiming: the counters
		rtmcounter0,
		-- Current state
		state_cur
	)
	begin

		-- Reset the next state value

		state_next <= (others => '0');

		-- Default value to the outputs or output buffers

		out0 <= '0';
		out2 <= '0';
		out3_bufn <= '0';
		out4 <= '0';
		out6 <= '0';
		out8 <= '0';
		out9 <= '0';
		out10 <= '0';
		out12_bufn <= '0';
		out13_bufn <= '0';
		out15 <= '0';
		out16 <= '0';
		out21 <= '0';
		out24 <= '0';
		out30_bufn <= '0';
		out34 <= '0';
		out35 <= '0';
		out36_bufn <= '0';
		out37 <= '0';
		out38 <= '0';
		out39 <= '0';
		out41 <= '0';
		out46 <= '0';
		out47 <= '0';
		out48 <= '0';
		out49 <= '0';
		out51 <= '0';

		-- Retiming: default value for counters
		rtmcounter0_next <= (others => '0');

		-- For all states, compute the next state bits
		--   And the outputs, and the next value for buffered outputs

		if state_cur(0) = '1' then
			-- Next state
			if (not (in0)) = '1' then
				state_next(0) <= '1';
				-- Next values for buffered outputs
			else
				state_next(33) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
			out0 <= '1';
		end if;

		if state_cur(1) = '1' then
			-- Next state
			if (not (in1)) = '1' then
				state_next(1) <= '1';
				-- Next values for buffered outputs
				out3_bufn <= '1';
			else
				state_next(28) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
			out4 <= '1';
			out2 <= '1';
		end if;

		if state_cur(2) = '1' then
			-- Next state
			if (not (in1)) = '1' then
				state_next(2) <= '1';
				-- Next values for buffered outputs
				out3_bufn <= '1';
			else
				state_next(18) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
			out4 <= '1';
			out2 <= '1';
		end if;

		if state_cur(3) = '1' then
			-- Next state
			state_next(0) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out6 <= '1';
		end if;

		if state_cur(4) = '1' then
			-- Next state
			state_next(3) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(5) = '1' then
			-- Next state
			state_next(4) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out9 <= '1';
			out8 <= '1';
		end if;

		if state_cur(6) = '1' then
			-- Next state
			state_next(5) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out10 <= '1';
			out9 <= '1';
		end if;

		if state_cur(7) = '1' then
			-- Next state
			state_next(6) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out9 <= '1';
		end if;

		if state_cur(8) = '1' then
			-- Next state
			state_next(10) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out16 <= '1';
			out15 <= '1';
		end if;

		if state_cur(9) = '1' then
			if rtmcmp9 = '1' then
				-- Next state
				state_next(8) <= '1';
				-- Next values for buffered outputs
				out13_bufn <= '1';
				out12_bufn <= '1';
				-- Last cycle of current state: assignment of non-buffered outputs
				out24 <= '1';
			else  -- Stay in the current state
				state_next(9) <= '1';
				rtmcounter0_next <= rtmcounter0 + 1;
				-- Maintain buffered outputs
			end if;
			-- Assignment of non-buffered outputs;
				out21 <= '1';
		end if;

		if state_cur(10) = '1' then
			-- Next state
			if (in2) = '1' then
				state_next(9) <= '1';
				-- Next values for buffered outputs
			else
				state_next(7) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(11) = '1' then
			-- Next state
			state_next(10) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out16 <= '1';
		end if;

		if state_cur(12) = '1' then
			-- Next state
			state_next(20) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out16 <= '1';
		end if;

		if state_cur(13) = '1' then
			-- Next state
			state_next(12) <= '1';
			-- Next values for buffered outputs
			out30_bufn <= '1';
			out13_bufn <= '1';
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(14) = '1' then
			-- Next state
			state_next(13) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out35 <= '1';
			out34 <= '1';
		end if;

		if state_cur(15) = '1' then
			-- Next state
			state_next(14) <= '1';
			-- Next values for buffered outputs
			out13_bufn <= '1';
			out12_bufn <= '1';
			-- Assignment of non-buffered outputs
			out37 <= '1';
		end if;

		if state_cur(16) = '1' then
			-- Next state
			state_next(15) <= '1';
			-- Next values for buffered outputs
			out36_bufn <= '1';
			-- Assignment of non-buffered outputs
			out38 <= '1';
			out35 <= '1';
		end if;

		if state_cur(17) = '1' then
			-- Next state
			state_next(16) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out39 <= '1';
			out37 <= '1';
		end if;

		if state_cur(18) = '1' then
			-- Next state
			state_next(17) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out41 <= '1';
		end if;

		if state_cur(19) = '1' then
			-- Next state
			state_next(2) <= '1';
			-- Next values for buffered outputs
			out3_bufn <= '1';
			-- Assignment of non-buffered outputs
			out4 <= '1';
		end if;

		if state_cur(20) = '1' then
			-- Next state
			if (in3) = '1' then
				state_next(19) <= '1';
				-- Next values for buffered outputs
			else
				state_next(11) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(21) = '1' then
			-- Next state
			state_next(20) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out16 <= '1';
		end if;

		if state_cur(22) = '1' then
			-- Next state
			state_next(30) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out16 <= '1';
		end if;

		if state_cur(23) = '1' then
			-- Next state
			state_next(22) <= '1';
			-- Next values for buffered outputs
			out30_bufn <= '1';
			out13_bufn <= '1';
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(24) = '1' then
			-- Next state
			state_next(23) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out47 <= '1';
			out46 <= '1';
		end if;

		if state_cur(25) = '1' then
			-- Next state
			state_next(24) <= '1';
			-- Next values for buffered outputs
			out13_bufn <= '1';
			out12_bufn <= '1';
			-- Assignment of non-buffered outputs
			out37 <= '1';
		end if;

		if state_cur(26) = '1' then
			-- Next state
			state_next(25) <= '1';
			-- Next values for buffered outputs
			out36_bufn <= '1';
			-- Assignment of non-buffered outputs
			out48 <= '1';
			out47 <= '1';
		end if;

		if state_cur(27) = '1' then
			-- Next state
			state_next(26) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out49 <= '1';
			out37 <= '1';
		end if;

		if state_cur(28) = '1' then
			-- Next state
			state_next(27) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out51 <= '1';
		end if;

		if state_cur(29) = '1' then
			-- Next state
			state_next(1) <= '1';
			-- Next values for buffered outputs
			out3_bufn <= '1';
			-- Assignment of non-buffered outputs
			out4 <= '1';
		end if;

		if state_cur(30) = '1' then
			-- Next state
			if (in4) = '1' then
				state_next(29) <= '1';
				-- Next values for buffered outputs
			else
				state_next(21) <= '1';
				-- Next values for buffered outputs
			end if;
			-- Assignment of non-buffered outputs
		end if;

		if state_cur(31) = '1' then
			-- Next state
			state_next(30) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out16 <= '1';
		end if;

		if state_cur(32) = '1' then
			-- Next state
			state_next(31) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
		end if;

		-- Info: This is the init/reset state
		if state_cur(33) = '1' then
			-- Next state
			state_next(32) <= '1';
			-- Next values for buffered outputs
			-- Assignment of non-buffered outputs
			out24 <= '1';
		end if;

		-- Reset input
		if reset = '1' then
			-- Set the reset state
			state_next <= (33 => '1', others => '0');
			-- Note: Resetting all buffers for outputs here is not necessary.
			-- It would cost hardware. They will be reset at the next clock front.
			-- Retiming: counters
			rtmcounter0_next <= (others => '0');
			-- Reset state: set the buffered outputs
		end if;

	end process;

	-- Assignment of buffered outputs

	out3 <= out3_buf;
	out12 <= out12_buf;
	out13 <= out13_buf;
	out30 <= out30_buf;
	out36 <= out36_buf;

	-- Retiming: the comparators

	rtmcmp9 <= '1' when state_cur(9) = '1' and rtmcounter0 = 1 else '0';

end architecture;

